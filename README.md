Maak maar een afspraak!

Maven -> Run Configurations:

#### Integratietest

Voor het lokaal uitvoeren van alle integratietesten, te vinden in backend/src/it/java  
Met deze configuration wordt het volgende uitgevoerd:

1. Alle unittesten uit backend/src/test worden uitgevoerd
2. De .jar wordt gemaakt
3. Docker image wordt gemaakt
4. Docker container wordt gemaakt
5. Docker container voor MariaDB wordt binnengehaald
6. Beide containers worden gestart
7. Integratietesten uit backend/src/it worden uitgevoerd
8. De boel wordt opgeruimd

#### Run LokaalDoet het volgende:

1. H2 database wordt gestart
2. Applicatie wordt gestart  
   Vervolgens is de applicatie benaderbaar op http://localhost:8080  
   Zie http://localhost:8080/swagger-ui/index.html?configUrl=/v3/api-docs/swagger-config#/ voor alle rest endpoints  
   Database is in te zien op http://localhost:8080/h2-console username sa, password leeg (dit werkt op dit moment niet
   goed)

#### Draaien via Docker:

1. Ga naar de docker folder
2. docker login registry.gitlab.com
3. docker-compose up -d
4. http://localhost:8585/actuator/health