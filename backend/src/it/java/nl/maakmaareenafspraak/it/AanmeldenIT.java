package nl.maakmaareenafspraak.it;

import com.google.gson.Gson;
import lombok.extern.slf4j.Slf4j;
import nl.maakmaareenafspraak.it.mailhog.ApiResponse;
import nl.maakmaareenafspraak.web.controller.aanmelden.*;
import nl.maakmaareenafspraak.web.controller.dl3b.Openingstijd;
import nl.maakmaareenafspraak.web.controller.dl3b.Werktijd;
import nl.maakmaareenafspraak.web.domain.OpvragenSalonResponse;

import java.time.LocalTime;
import java.util.Comparator;
import java.util.List;
import java.util.UUID;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.stream.Collectors;

import static com.google.common.collect.Lists.newArrayList;
import static java.time.DayOfWeek.WEDNESDAY;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

@Slf4j
public class AanmeldenIT extends AbstractIntegratieTest {
    private final String NAAMSALON = UUID.randomUUID().toString();
    private final String ADRES = "Adres 1";
    private final String POSTCODE = "1111AA";
    private final String HUISNUMMER = "26";
    private final String PLAATS = "Plaats";
    private final String TELEFOONNUMMER = "0612345678";
    private final String EMAIL = "info@salon.nl";
    private final String NAAM_MEDEWERKER_1 = UUID.randomUUID().toString();
    private final String TIJDELIJKE_ID_MEDEWERKER_1 = UUID.randomUUID().toString();
    private final String NAAM_MEDEWERKER_2 = UUID.randomUUID().toString();
    private final String TIJDELIJKE_ID_MEDEWERKER_2 = UUID.randomUUID().toString();

    @Override
    public boolean inloggen() {
        return false;
    }

    @Override
    public void voerTestUit() {
        var aantalMails = getAantalMails();

        var aanmelden = new Aanmelden();
        aanmelden.setNaamSalon(NAAMSALON);
        aanmelden.setAdres(ADRES);
        aanmelden.setHuisnummer(HUISNUMMER);
        aanmelden.setPostcode(POSTCODE);
        aanmelden.setPlaats(PLAATS);
        aanmelden.setTelefoonnummer(TELEFOONNUMMER);
        aanmelden.setEmail(EMAIL);

        //Medewerkers
        var medewerker1 = new Medewerker();
        medewerker1.setId(TIJDELIJKE_ID_MEDEWERKER_1);
        medewerker1.setNaam(NAAM_MEDEWERKER_1);
        medewerker1.setAanwezig(true);
        medewerker1.setBeheerder(true);
        var medewerker2 = new Medewerker();
        medewerker2.setId(TIJDELIJKE_ID_MEDEWERKER_2);
        medewerker2.setNaam(NAAM_MEDEWERKER_2);
        medewerker2.setAanwezig(true);
        aanmelden.setMedewerkers(newArrayList(medewerker1, medewerker2));

        //Tijden
        var dagMetTijd = new DagMetTijd();
        var dag = new Dag();
        dag.setCode("WEDNESDAY");
        dagMetTijd.setDag(dag);
        var tijd = new Tijd();
        tijd.setMedewerkers(newArrayList(medewerker1));
        tijd.setDag(dag);
        tijd.setStart(LocalTime.of(9, 0));
        tijd.setEind(LocalTime.of(11, 0));
        dagMetTijd.getTijden().add(tijd);
        var tijd1 = new Tijd();
        tijd1.setMedewerkers(newArrayList(medewerker2));
        tijd1.setDag(dag);
        tijd1.setStart(LocalTime.of(13, 0));
        tijd1.setEind(LocalTime.of(15, 0));
        dagMetTijd.getTijden().add(tijd1);
        aanmelden.setDagenMetTijden(newArrayList(dagMetTijd));

        HttpActies<Aanmelden, Long> aanmeldenHttpActies = new HttpActies<>(Aanmelden.class, Long.class);
        Long salonId = aanmeldenHttpActies.postUItvoeren(aanmelden, "aanmelden");

        HttpActies<OpvragenSalonResponse, Long> salonHttpActies = new HttpActies<>(OpvragenSalonResponse.class, Long.class);
        var response = salonHttpActies.getUitvoeren("salon/lees/" + salonId, OpvragenSalonResponse.class);
        var salon = response.getSalon();
        assertEquals(NAAMSALON, salon.getNaam());
        assertEquals(ADRES, salon.getStraat());
        assertEquals(HUISNUMMER, salon.getHuisnummer());
        assertEquals(POSTCODE, salon.getPostcode());
        assertEquals(PLAATS, salon.getPlaats());
        assertEquals(TELEFOONNUMMER, salon.getTelefoonnummer());
        assertEquals(EMAIL, salon.getEmailadres());

        assertEquals(2, salon.getMedewerkers().size());
        List<String> namen = salon.getMedewerkers().stream()//
                .map(medewerker -> medewerker.getNaam())//
                .collect(Collectors.toList());
        assertTrue(namen.contains(NAAM_MEDEWERKER_1));
        assertTrue(namen.contains(NAAM_MEDEWERKER_2));

        HttpActies<Openingstijd, Long> openingstijdenHttpActies = new HttpActies<>(Openingstijd.class, Long.class);
        List<Openingstijd> openingstijden = openingstijdenHttpActies.haalLijstOp("dl3b/openingstijden/" + salonId);
        assertEquals(2, openingstijden.size());
        assertEquals(WEDNESDAY.toString(), openingstijden.get(0).getDag());
        assertEquals("09:00", openingstijden.get(0).getStart());
        assertEquals("11:00", openingstijden.get(0).getEind());
        assertEquals(WEDNESDAY.toString(), openingstijden.get(1).getDag());
        assertEquals("13:00", openingstijden.get(1).getStart());
        assertEquals("15:00", openingstijden.get(1).getEind());

        AtomicBoolean eerste = new AtomicBoolean(true);
        salon.getMedewerkers()//
                .stream().sorted(Comparator.comparing(nl.maakmaareenafspraak.web.domain.Medewerker::getId))//
                .forEach(medewerker -> {
                    HttpActies<Werktijd, Long> werktijdenHttpActies = new HttpActies<>(Werktijd.class, Long.class);
                    List<Werktijd> openingstijden1 = werktijdenHttpActies.haalLijstOp("dl3b/werktijden/" + medewerker.getId());
                    assertEquals(1, openingstijden1.size());
                    if (eerste.get()) {
                        assertEquals(WEDNESDAY.toString(), openingstijden1.get(0).getDag());
                        assertEquals("09:00", openingstijden1.get(0).getStart());
                        assertEquals("11:00", openingstijden1.get(0).getEind());
                        eerste.set(false);
                    } else {
                        assertEquals(WEDNESDAY.toString(), openingstijden1.get(0).getDag());
                        assertEquals("13:00", openingstijden1.get(0).getStart());
                        assertEquals("15:00", openingstijden1.get(0).getEind());
                    }
                });

        assertEquals(aantalMails + 1, getAantalMails());

        salonHttpActies.deleteUitvoeren("salon/verwijderen/" + salonId);
    }

    private int getAantalMails() {
        HttpActies<String, Long> mailhogActies = new HttpActies<>(String.class, Long.class);
        var stringResponse = mailhogActies.getUitvoeren("http://" + host + ":8025/api/v2/messages", String.class, true);
        var apiResponse = new Gson().fromJson(stringResponse, ApiResponse.class);

        return apiResponse.getTotal();
    }
}
