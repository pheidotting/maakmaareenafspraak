package nl.maakmaareenafspraak.it;


import nl.maakmaareenafspraak.web.controller.instellingen.Salon;
import nl.maakmaareenafspraak.web.domain.Medewerker;
import nl.maakmaareenafspraak.web.security.JwtResponse;
import nl.maakmaareenafspraak.web.security.LoginRequest;

import java.util.List;
import java.util.UUID;

import static org.junit.jupiter.api.Assertions.*;

public class SalonInstellingenBeherenIT extends AbstractIntegratieTest {
    @Override
    public boolean inloggen() {
        return false;
    }

    @Override
    public void voerTestUit() {
        var gebruikersnaam = UUID.randomUUID().toString();
        var wachtwoord = UUID.randomUUID().toString();

        HttpActies<Long, Long> salonHttpActies = new HttpActies<>(Long.class, Long.class);
        var salonId = salonHttpActies.putUitvoeren("dl3b/salon/" + UUID.randomUUID().toString());
        var medewerkerId = salonHttpActies.putUitvoeren("dl3b/medewerker/" + salonId + "/0/true/" + gebruikersnaam);
        HttpActies<Boolean, Boolean> wwHttpActies = new HttpActies<>(Boolean.class, Boolean.class);
        wwHttpActies.putUitvoeren("dl3b/wijzigwachtwoord/" + medewerkerId + "/" + wachtwoord);

        HttpActies<LoginRequest, JwtResponse> inloggenHttpActies = new HttpActies<>(LoginRequest.class, JwtResponse.class);

        LoginRequest loginRequest = new LoginRequest();
        loginRequest.setPassword(wachtwoord);
        loginRequest.setUsername(gebruikersnaam);
        JwtResponse inloggenResponse = inloggenHttpActies.postUItvoeren(loginRequest, "auth/signin");
        token = inloggenResponse.getToken();

        HttpActies<Salon, Long> salonHttpActies2 = new HttpActies<>(Salon.class, Long.class);
        var salon = salonHttpActies2.getUitvoeren("instellingen/beherensalon/lees", Salon.class);
        assertNull(salon.getEmailadres());
        assertNull(salon.getPlaats());
        assertNull(salon.getPostcode());
        assertNull(salon.getStraat());
        assertNull(salon.getTelefoonnummer());

        var email = "email";
        var plaats = "plaats";
        var postcode = "postcode";
        var straat = "straat";
        var telefoonnummer = "telefoonnummer";
        salon.setEmailadres(email);
        salon.setPlaats(plaats);
        salon.setPostcode(postcode);
        salon.setStraat(straat);
        salon.setTelefoonnummer(telefoonnummer);

        salonHttpActies2.postUItvoeren(salon, "instellingen/beherensalon/opslaan");
        var salonOpgeslagen = salonHttpActies2.getUitvoeren("instellingen/beherensalon/lees", Salon.class);
        assertEquals(email, salonOpgeslagen.getEmailadres());
        assertEquals(plaats, salonOpgeslagen.getPlaats());
        assertEquals(postcode, salonOpgeslagen.getPostcode());
        assertEquals(straat, salonOpgeslagen.getStraat());
        assertEquals(telefoonnummer, salonOpgeslagen.getTelefoonnummer());

        var mw = new Medewerker();
        mw.setId(medewerkerId);
        mw.setNaam("Hendrik Haverkamp");
        HttpActies<Medewerker, Long> medewerkerHttpActies = new HttpActies<>(Medewerker.class, Long.class);
        List<Medewerker> medewerkers = medewerkerHttpActies.haalLijstOp("instellingen/medewerkers/lijst");
        assertEquals(1, medewerkers.size());
        var retMw = medewerkerHttpActies.postUItvoeren(mw, "instellingen/medewerkers/opslaan");
        assertEquals(medewerkerId, retMw);

        medewerkers = medewerkerHttpActies.haalLijstOp("instellingen/medewerkers/lijst");
        assertEquals(1, medewerkers.size());
        assertEquals("Hendrik Haverkamp", medewerkers.get(0).getNaam());

        var mw2 = new Medewerker();
        mw2.setNaam("Nieuwe Medewerker");
        var retMw2 = medewerkerHttpActies.postUItvoeren(mw2, "instellingen/medewerkers/opslaan");
        assertNotNull(retMw2);
        medewerkers = medewerkerHttpActies.haalLijstOp("instellingen/medewerkers/lijst");
        assertEquals(2, medewerkers.size());
        assertEquals("Hendrik Haverkamp", medewerkers.get(0).getNaam());
        assertEquals("Nieuwe Medewerker", medewerkers.get(1).getNaam());

        HttpActies<Boolean, Boolean> booleanHttpActies = new HttpActies<>(Boolean.class, Boolean.class);
        booleanHttpActies.deleteUitvoeren("instellingen/medewerkers/verwijder/" + medewerkers.get(1).getId());
        medewerkers = medewerkerHttpActies.haalLijstOp("instellingen/medewerkers/lijst");
        assertEquals(1, medewerkers.size());
        assertEquals("Hendrik Haverkamp", medewerkers.get(0).getNaam());

        salonHttpActies.deleteUitvoeren("salon/verwijderen/" + salonId);
    }
}
