package nl.maakmaareenafspraak.it.mailhog;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ApiResponse {
    int total;
}
