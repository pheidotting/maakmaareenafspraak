package nl.maakmaareenafspraak.it;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.common.base.Stopwatch;
import com.google.common.util.concurrent.RateLimiter;
import nl.maakmaareenafspraak.web.domain.Salon;
import nl.maakmaareenafspraak.web.security.JwtResponse;
import nl.maakmaareenafspraak.web.security.LoginRequest;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayNameGeneration;
import org.junit.jupiter.api.DisplayNameGenerator;
import org.junit.jupiter.api.Test;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;

import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.UUID;
import java.util.concurrent.TimeUnit;

import static com.google.common.base.Stopwatch.createStarted;
import static java.util.stream.Collectors.toList;
import static org.junit.jupiter.api.Assertions.fail;

@DisplayNameGeneration(AbstractIntegratieTest.CustomDisplayNameGenerator.class)
public abstract class AbstractIntegratieTest {
    protected final static String LOCALHOST = "localhost";
    protected final static String DOCKER = "docker";
    protected static String host;
    protected static RestTemplate restTemplate = new RestTemplate();
    protected static ObjectMapper mapper = new ObjectMapper();
    protected static String naam;
    protected static String email;
    protected static String wachtwoord;
    protected static String token;

    @BeforeEach
    public void init() throws Exception {
        naam = UUID.randomUUID().toString();
        email = UUID.randomUUID().toString();
        wachtwoord = UUID.randomUUID().toString();
        Stopwatch stopwatch = createStarted();
        ActuatorStatus response = null;
        RateLimiter rateLimiter = RateLimiter.create(3);
        host = LOCALHOST;
        System.out.println("SUT opzoeken");
        while ((response == null || !"UP".equals(response.getStatus())) && stopwatch.elapsed(TimeUnit.MINUTES) <= 2) {
            rateLimiter.acquire();
            System.out.println("http://" + host + ":9292/actuator/health");
            try {
                ResponseEntity<ActuatorStatus> inloggenResponseEntity = restTemplate.getForEntity("http://" + host + ":9292/actuator/health", ActuatorStatus.class);
                response = inloggenResponseEntity.getBody();
            } catch (Exception e) {
                if (LOCALHOST.equals(host)) {
                    host = DOCKER;
                } else {
                    host = LOCALHOST;
                }
            }
        }
        if (response == null || !"UP".equals(response.getStatus())) {
            fail("Connectie kon niet worden gemaakt met SUT");
        }
        if (inloggen()) {
            HttpActies<Long, Long> klantHttpActies = new HttpActies<>(Long.class, Long.class);
            klantHttpActies.putUitvoeren("dl3b/klant/" + naam + "/" + email + "/" + wachtwoord);

            HttpActies<LoginRequest, JwtResponse> inloggenHttpActies = new HttpActies<>(LoginRequest.class, JwtResponse.class);

            LoginRequest loginRequest = new LoginRequest();
            loginRequest.setPassword(wachtwoord);
            loginRequest.setUsername(email);
            JwtResponse inloggenResponse = inloggenHttpActies.postUItvoeren(loginRequest, "auth/signin");
            token = inloggenResponse.getToken();
        }
    }

    public abstract boolean inloggen();

    public abstract void voerTestUit();

    @Test
    public void test() {
        voerTestUit();
    }

    static class CustomDisplayNameGenerator extends DisplayNameGenerator.Standard {

        @Override
        public String generateDisplayNameForClass(Class<?> testClass) {
            return replaceCamelCase(testClass.getSimpleName().replace("IT", "Integratie Test"));
        }

        @Override
        public String generateDisplayNameForNestedClass(Class<?> nestedClass) {
            return super.generateDisplayNameForNestedClass(nestedClass);
        }

        @Override
        public String generateDisplayNameForMethod(Class<?> testClass, Method testMethod) {
            return replaceCamelCase(testClass.getSimpleName().replace("IT", "Integratie Test"));
        }

        private String replaceCamelCase(String camelCase) {
            StringBuilder result = new StringBuilder();
            result.append(camelCase.charAt(0));
            for (int i = 1; i < camelCase.length(); i++) {
                if (Character.isUpperCase(camelCase.charAt(i))) {
                    result.append(' ');
                }
                result.append(camelCase.charAt(i));
            }
            return result.toString();
        }
    }

    public class HttpActies<T, U> {
        private Class<T> clazzT;
        private Class<U> clazzU;

        public HttpActies(Class<T> clazzT, Class<U> clazzU) {
            this.clazzT = clazzT;
            this.clazzU = clazzU;
        }


        public List<T> haalLijstOp(String url, boolean absoluteUrl) {
            String urlString;
            if (absoluteUrl) {
                urlString = url;
            } else {
                urlString = "http://" + host + ":9292/" + url;
            }
            HttpHeaders headers = new HttpHeaders();
            System.out.println(urlString);
            if (token != null) {
                headers.add(HttpHeaders.AUTHORIZATION, "Bearer " + token);
            }
            HttpEntity<T> request = new HttpEntity<>(headers);
            ResponseEntity<Object[]> objects = restTemplate.exchange(urlString, HttpMethod.GET, request, Object[].class);
            if (objects != null) {
                return Arrays.stream(objects.getBody()).map(object -> mapper.convertValue(object, clazzT)).collect(toList());
            } else {
                return new ArrayList<>();
            }
        }

        public List<T> haalLijstOp(String url) {
            return haalLijstOp(url, false);
        }

        public T getUitvoeren(String url, Class clazz) {
            return getUitvoeren(url, clazz, false);
        }

        public T getUitvoeren(String url, Class clazz, boolean absoluteUrl) {
            HttpHeaders headers = new HttpHeaders();
            String urlString;
            if (absoluteUrl) {
                urlString = url;
            } else {
                urlString = "http://" + host + ":9292/" + url;
            }
            if (token != null) {
                headers.add(HttpHeaders.AUTHORIZATION, "Bearer " + token);
            }
            HttpEntity<T> request = new HttpEntity<>(headers);
            ResponseEntity<T> response = restTemplate.exchange(urlString, HttpMethod.GET, request, clazz);
            return response.getBody();
        }

        public U postUItvoeren(T t, String url) {
            HttpHeaders headers = new HttpHeaders();
            if (token != null) {
                headers.add(HttpHeaders.AUTHORIZATION, "Bearer " + token);
            }
            HttpEntity<T> request = new HttpEntity<>(t, headers);
            ResponseEntity<U> response = restTemplate.postForEntity("http://" + host + ":9292/" + url, request, clazzU);

            return response.getBody();
        }

        public T putUitvoeren(String url) {
            HttpHeaders headers = new HttpHeaders();
            if (token != null) {
                headers.add(HttpHeaders.AUTHORIZATION, "Bearer " + token);
            }
            HttpEntity<T> httpEntity = new HttpEntity<>(headers);
            System.out.println("http://" + host + ":9292/" + url);
            ResponseEntity<T> response = restTemplate.exchange("http://" + host + ":9292/" + url, HttpMethod.PUT, httpEntity, clazzT);

            return response.getBody();
        }

        public void deleteUitvoeren(String url) {
            HttpHeaders headers = new HttpHeaders();
            if (token != null) {
                headers.add(HttpHeaders.AUTHORIZATION, "Bearer " + token);
            }
            HttpEntity<T> httpEntity = new HttpEntity<>(headers);
            System.out.println("http://" + host + ":9292/" + url);
            restTemplate.exchange("http://" + host + ":9292/" + url, HttpMethod.DELETE, httpEntity, clazzT);
        }
    }

    protected String random() {
        return UUID.randomUUID().toString();
    }

    protected Salon genereerSalon(int aantalBehandelingen, int aantalMedewerkers) {
        var salon = new Salon();
        salon.setNaam(random());

        HttpActies<Salon, Long> salonHttpActies = new HttpActies<>(Salon.class, Long.class);
        Long salonId = salonHttpActies.postUItvoeren(salon, "instellingen/beherensalon/opslaan");

        List<Long> behandelingIds = new ArrayList();
        for (int i = 0; i < aantalBehandelingen; i++) {
            behandelingIds.add(genereerBehandeling(salonId, 1L, 10D));
        }
        List<Long> medewekerIds = new ArrayList<>();
        for (int i = 0; i < aantalMedewerkers; i++) {
            medewekerIds.add(genereerMedewerker(salonId, behandelingIds));
        }

        List<Long> openingstijdenIds = genereerOpeningstijden(salonId);

        medewekerIds.stream().forEach(this::genereerWerktijden);

        return salon;
    }

    protected Long genereerBehandeling(Long salonId, Long tijdsduur, Double prijs) {
        HttpActies<Long, Long> behandelingHttpActies = new HttpActies<>(Long.class, Long.class);
        return behandelingHttpActies.putUitvoeren("dl3b/behandeling/" + salonId + "/" + tijdsduur + "/" + prijs);
    }

    protected Long genereerMedewerker(Long salonId, List<Long> behandelingen) {
        HttpActies<Long, Long> behandelingHttpActies = new HttpActies<>(Long.class, Long.class);

        String beh = String.join("-", behandelingen.stream().map(aLong -> aLong.toString()).collect(toList()));

        return behandelingHttpActies.putUitvoeren("dl3b/medewerker/" + salonId + "/" + beh + "/false/");
    }

    protected List<Long> genereerOpeningstijden(Long salonId) {
        HttpActies<Long, Long> openingstijdActies = new HttpActies<>(Long.class, Long.class);

        List<Long> ids = new ArrayList<>();
        ids.add(openingstijdActies.putUitvoeren("dl3b/openingstijd/" + salonId + "/TUESDAY/9-00/12-00"));
        ids.add(openingstijdActies.putUitvoeren("dl3b/openingstijd/" + salonId + "/TUESDAY/12-00/17-30"));
        ids.add(openingstijdActies.putUitvoeren("dl3b/openingstijd/" + salonId + "/WEDNESDAY/9-00/12-00"));
        ids.add(openingstijdActies.putUitvoeren("dl3b/openingstijd/" + salonId + "/WEDNESDAY/12-00/17-30"));
        ids.add(openingstijdActies.putUitvoeren("dl3b/openingstijd/" + salonId + "/THURSDAY/9-00/12-00"));
        ids.add(openingstijdActies.putUitvoeren("dl3b/openingstijd/" + salonId + "/THURSDAY/12-00/17-30"));
        ids.add(openingstijdActies.putUitvoeren("dl3b/openingstijd/" + salonId + "/THURSDAY/18-00/21-30"));
        ids.add(openingstijdActies.putUitvoeren("dl3b/openingstijd/" + salonId + "/FRIDAY/9-00/12-00"));
        ids.add(openingstijdActies.putUitvoeren("dl3b/openingstijd/" + salonId + "/FRIDAY/12-00/17-30"));
        ids.add(openingstijdActies.putUitvoeren("dl3b/openingstijd/" + salonId + "/SATURDAY/9-00/13-00"));

        return ids;
    }

    protected List<Long> genereerWerktijden(Long medewerkerId) {
        HttpActies<Long, Long> werktijdActies = new HttpActies<>(Long.class, Long.class);

        List<Long> ids = new ArrayList<>();
        ids.add(werktijdActies.putUitvoeren("dl3b/werktijd/" + medewerkerId + "/TUESDAY/9-00/12-00"));
        ids.add(werktijdActies.putUitvoeren("dl3b/werktijd/" + medewerkerId + "/TUESDAY/12-00/17-30"));
        ids.add(werktijdActies.putUitvoeren("dl3b/werktijd/" + medewerkerId + "/WEDNESDAY/9-00/12-00"));
        ids.add(werktijdActies.putUitvoeren("dl3b/werktijd/" + medewerkerId + "/WEDNESDAY/12-00/17-30"));
        ids.add(werktijdActies.putUitvoeren("dl3b/werktijd/" + medewerkerId + "/THURSDAY/9-00/12-00"));
        ids.add(werktijdActies.putUitvoeren("dl3b/werktijd/" + medewerkerId + "/THURSDAY/12-00/17-30"));
        ids.add(werktijdActies.putUitvoeren("dl3b/werktijd/" + medewerkerId + "/THURSDAY/18-00/21-30"));
        ids.add(werktijdActies.putUitvoeren("dl3b/werktijd/" + medewerkerId + "/FRIDAY/9-00/12-00"));
        ids.add(werktijdActies.putUitvoeren("dl3b/werktijd/" + medewerkerId + "/FRIDAY/12-00/17-30"));
        ids.add(werktijdActies.putUitvoeren("dl3b/werktijd/" + medewerkerId + "/SATURDAY/9-00/13-00"));

        return ids;
    }
}
