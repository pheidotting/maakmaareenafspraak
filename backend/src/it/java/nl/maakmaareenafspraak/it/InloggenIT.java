//package nl.maakmaareenafspraak.it;
//
//import nl.maakmaareenafspraak.web.security.JwtResponse;
//import nl.maakmaareenafspraak.web.security.LoginRequest;
//
//import static org.junit.jupiter.api.Assertions.assertEquals;
//import static org.junit.jupiter.api.Assertions.assertNotNull;
//
//public class InloggenIT extends AbstractIntegratieTest {
//    @Override
//    public boolean inloggen() {
//        return false;
//    }
//
//    @Override
//    public void voerTestUit() {
//        String naam = "NAAM";
//        String email = "a@b.c";
//        String emailFout = "aa@b.c";
//        String wachtwoord = "pass";
//        String wachtwoordFout = "anders";
//
//        HttpActies<Long, Long> klantHttpActies = new HttpActies<>(Long.class, Long.class);
//        Long klantId = klantHttpActies.putUItvoeren("dl3b/klant/" + naam + "/" + email + "/" + wachtwoord);
//
//        HttpActies<LoginRequest, JwtResponse> inloggenHttpActies = new HttpActies<>(LoginRequest.class, JwtResponse.class);
//
//        LoginRequest loginRequest = new LoginRequest();
//        loginRequest.setPassword(wachtwoord);
//        loginRequest.setUsername(email);
//        JwtResponse response = inloggenHttpActies.postUItvoeren(loginRequest, "auth/signin");
//        assertNotNull(response.getToken());
//
//        loginRequest.setPassword(wachtwoordFout);
//        loginRequest.setUsername(email);
//        try {
//            inloggenHttpActies.postUItvoeren(loginRequest, "auth/signin");
//        } catch (Exception e) {
//            assertEquals("401 : [no body]", e.getMessage());
//        }
//
//        loginRequest.setPassword(wachtwoord);
//        loginRequest.setUsername(emailFout);
//        try {
//            inloggenHttpActies.postUItvoeren(loginRequest, "auth/signin");
//        } catch (Exception e) {
//            assertEquals("401 : [no body]", e.getMessage());
//        }
//    }
//}
