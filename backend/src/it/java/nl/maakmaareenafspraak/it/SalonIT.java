//package nl.maakmaareenafspraak.it;
//
//import lombok.extern.slf4j.Slf4j;
//import nl.maakmaareenafspraak.web.domain.Salon;
//
//import java.util.List;
//
//import static org.assertj.core.api.Assertions.assertThat;
//import static org.junit.jupiter.api.Assertions.assertNotNull;
//
//@Slf4j
//public class SalonIT extends AbstractIntegratieTest {
//    @Override
//    public boolean inloggen() {
//        return true;
//    }
//
//    @Override
//    public void voerTestUit() {
//        Salon salon = maakSalon();
//
//        HttpActies<Salon, Long> salonHttpActies = new HttpActies<>(Salon.class, Long.class);
//        salonHttpActies.postUItvoeren(salon, "salon/opslaan");
//
//        List<Salon> salons = salonHttpActies.haalLijstOp("salon/alles");
//
//        assertThat(salons.size()).isEqualTo(1);
//        Salon salonOpgehaald = salons.get(0);
//
//        controleerSalon(salonOpgehaald,salon);
//
//        salon.setId(salonOpgehaald.getId());
//        salon.setStraat(null);
//        salon.setPostcode(null);
//        salon.setPlaats(null);
//
//        salonHttpActies.postUItvoeren(salon, "salon/opslaan");
//
//        salons = salonHttpActies.haalLijstOp("salon/alles");
//
//        assertThat(salons.size()).isEqualTo(1);
//        salonOpgehaald = salons.get(0);
//        controleerSalon(salonOpgehaald,salon);
//
//        salonHttpActies.deleteUitvoeren("verwijderen/" + salonOpgehaald.getId());
//        salons = salonHttpActies.haalLijstOp("salon/alles");
//
//        assertThat(salons.size()).isZero();
//    }
//
//    private Salon maakSalon(){
//        var salon = new Salon();
//        salon.setEmailadres("mail@salon1.nl");
//        salon.setNaam("Salon1");
//        salon.setPlaats("Plaats");
//        salon.setPostcode("1111AA");
//        salon.setStraat("Straatnaam 1");
//        salon.setTelefoonnummer("telefoon");
//
//        return salon;
//    }
//
//    private void controleerSalon(Salon salonOpgehaald,Salon salon){
//        assertThat(salonOpgehaald.getEmailadres()).isEqualTo(salon.getEmailadres());
//        assertThat(salonOpgehaald.getNaam()).isEqualTo(salon.getNaam());
//        assertThat(salonOpgehaald.getPlaats()).isEqualTo(salon.getPlaats());
//        assertThat(salonOpgehaald.getPostcode()).isEqualTo(salon.getPostcode());
//        assertThat(salonOpgehaald.getStraat()).isEqualTo(salon.getStraat());
//        assertThat(salonOpgehaald.getTelefoonnummer()).isEqualTo(salon.getTelefoonnummer());
//        assertNotNull(salonOpgehaald.getId());
//    }
//}
