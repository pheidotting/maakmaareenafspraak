package nl.maakmaareenafspraak.it;

import lombok.extern.slf4j.Slf4j;
import nl.maakmaareenafspraak.web.controller.afspraak.AfspraakMetNietIngelogdeKlant;
import nl.maakmaareenafspraak.web.domain.Salon;

import java.time.LocalDateTime;
import java.util.List;
import java.util.UUID;

import static com.google.common.collect.Lists.newArrayList;
import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.*;

@Slf4j
public class AfsprakenIT extends AbstractIntegratieTest {
    @Override
    public boolean inloggen() {
        return true;
    }

    @Override
    public void voerTestUit() {
        Salon salon = genereerSalon(0, 0);

        HttpActies<Salon, Long> salonHttpActies = new HttpActies<>(Salon.class, Long.class);
        List<Salon> salons = salonHttpActies.haalLijstOp("salon/alles");

        assertThat(salons.size()).isEqualTo(1);
        Salon salonOpgehaald = salons.get(0);

        controleerSalon(salonOpgehaald, salon);

        HttpActies<Long, Long> behandelingHttpActies = new HttpActies<>(Long.class, Long.class);
        Long behandeling1Id = behandelingHttpActies.putUitvoeren("dl3b/behandeling/" + salonOpgehaald.getId() + "/1/2");
        Long behandeling2Id = behandelingHttpActies.putUitvoeren("dl3b/behandeling/" + salonOpgehaald.getId() + "/2/2");
        Long behandeling3Id = behandelingHttpActies.putUitvoeren("dl3b/behandeling/" + salonOpgehaald.getId() + "/3/2");

        Long medewerker1Id = behandelingHttpActies.putUitvoeren("dl3b/medewerker/" + salonOpgehaald.getId() + "/" + behandeling1Id + "-" + behandeling2Id + "-" + behandeling3Id + "/false/a");
        genereerWerktijden(medewerker1Id);

        AfspraakMetNietIngelogdeKlant afspraakMetNietIngelogdeKlant = new AfspraakMetNietIngelogdeKlant();
        afspraakMetNietIngelogdeKlant.setNaam(UUID.randomUUID().toString());
        afspraakMetNietIngelogdeKlant.setEmailadres(UUID.randomUUID().toString());
        afspraakMetNietIngelogdeKlant.setTelefoonnummer(UUID.randomUUID().toString().substring(0, 20));
        afspraakMetNietIngelogdeKlant.setTijdstip(LocalDateTime.of(2022, 6, 28, 11, 0));
        afspraakMetNietIngelogdeKlant.setOpmerking(UUID.randomUUID().toString());
        afspraakMetNietIngelogdeKlant.setSalonId(salonOpgehaald.getId());
        afspraakMetNietIngelogdeKlant.setVoorkeurMedewerkerId(medewerker1Id);
        afspraakMetNietIngelogdeKlant.setBehandelingIds(newArrayList(behandeling1Id, behandeling2Id, behandeling3Id));

        HttpActies<AfspraakMetNietIngelogdeKlant, Boolean> afspraakMetNietIngelogdeKlantHttpActies = new HttpActies<>(AfspraakMetNietIngelogdeKlant.class, Boolean.class);
        Boolean response = afspraakMetNietIngelogdeKlantHttpActies.postUItvoeren(afspraakMetNietIngelogdeKlant, "afspraak/opslaanAfspraakMetNietIngelogdeKlant");
        assertTrue(response);
        afspraakMetNietIngelogdeKlant.setOpmerking(UUID.randomUUID().toString());
        Boolean response2 = afspraakMetNietIngelogdeKlantHttpActies.postUItvoeren(afspraakMetNietIngelogdeKlant, "afspraak/opslaanAfspraakMetNietIngelogdeKlant");
        assertFalse(response2);

        salonHttpActies.deleteUitvoeren("salon/verwijderen/" + salonOpgehaald.getId());
    }

    private void controleerSalon(Salon salonOpgehaald, Salon salon) {
        assertThat(salonOpgehaald.getEmailadres()).isEqualTo(salon.getEmailadres());
        assertThat(salonOpgehaald.getNaam()).isEqualTo(salon.getNaam());
        assertThat(salonOpgehaald.getPlaats()).isEqualTo(salon.getPlaats());
        assertThat(salonOpgehaald.getPostcode()).isEqualTo(salon.getPostcode());
        assertThat(salonOpgehaald.getStraat()).isEqualTo(salon.getStraat());
        assertThat(salonOpgehaald.getTelefoonnummer()).isEqualTo(salon.getTelefoonnummer());
        assertNotNull(salonOpgehaald.getId());
    }
}
