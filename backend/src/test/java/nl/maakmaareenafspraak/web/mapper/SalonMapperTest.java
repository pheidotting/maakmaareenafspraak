package nl.maakmaareenafspraak.web.mapper;

import nl.maakmaareenafspraak.domain.Adres;
import nl.maakmaareenafspraak.web.domain.Salon;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import static org.assertj.core.api.Assertions.assertThat;

class SalonMapperTest {
    @Test
    @DisplayName("Test de SalonMapper")
    void testMapper() {
        nl.maakmaareenafspraak.domain.Salon salon = new nl.maakmaareenafspraak.domain.Salon();
        salon.setId(1L);
        salon.setNaam("Salon Sabine");
        salon.setTelefoonnummer("telefoonn");
        salon.setEmailadres("email");

        Adres adres = new Adres();
        adres.setId(2L);
        adres.setPlaats("plaats");
        adres.setPostcode("postcode");
        adres.setStraat("straat");

        salon.setAdres(adres);

        SalonMapper salonMapper = new SalonMapper();
        Salon pureSalon = salonMapper.mapVanDomainNaarWeb().apply(salon);
        assertThat(pureSalon.getId()).isEqualTo(salon.getId());
        assertThat(pureSalon.getNaam()).isEqualTo(salon.getNaam());
        assertThat(pureSalon.getTelefoonnummer()).isEqualTo(salon.getTelefoonnummer());
        assertThat(pureSalon.getEmailadres()).isEqualTo(salon.getEmailadres());
        assertThat(pureSalon.getPlaats()).isEqualTo(adres.getPlaats());
        assertThat(pureSalon.getPostcode()).isEqualTo(adres.getPostcode());
        assertThat(pureSalon.getStraat()).isEqualTo(adres.getStraat());

    }
}