package nl.maakmaareenafspraak.repository;

import nl.maakmaareenafspraak.domain.*;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.test.context.ContextConfiguration;

import java.time.DayOfWeek;
import java.time.LocalDateTime;
import java.time.LocalTime;

import static org.assertj.core.api.Assertions.assertThat;

@DataJpaTest
@ContextConfiguration(classes = {AfspraakRepositoryTest.class})
class WerktijdenEnOpeningstijdenTest {
    @Autowired
    private TestEntityManager entityManager;

    @DisplayName("Werktijden bij medewerker")
    @Test
    void werktijdenBijMedewerker() {
        Medewerker medewerker = new Medewerker();
        medewerker.setNaam("medewerker1");

        Werktijd werktijd1 = medewerker.addWerktijd();
        werktijd1.setDag(DayOfWeek.MONDAY);
        werktijd1.setStart(LocalTime.of(11, 0));
        werktijd1.setEind(LocalTime.of(12, 15));

        Werktijd werktijd2 = medewerker.addWerktijd();
        werktijd2.setDag(DayOfWeek.THURSDAY);
        werktijd2.setStart(LocalTime.of(13, 15));
        werktijd2.setEind(LocalTime.of(15, 45));

        ExtraWerktijd extraWerktijd = medewerker.addExtraWerktijd();
        extraWerktijd.setStart(LocalDateTime.now());
        extraWerktijd.setEind(LocalDateTime.now());

        Verlof verlof = medewerker.addVerlof();
        verlof.setStart(LocalDateTime.now());
        verlof.setEind(LocalDateTime.now());

        entityManager.persist(medewerker);

        Medewerker opgehaaldeMedewerker = entityManager.find(Medewerker.class, medewerker.getId());
        assertThat(opgehaaldeMedewerker).isEqualTo(medewerker);
        assertThat(opgehaaldeMedewerker.getWerktijden().size()).isEqualTo(2);
        assertThat(opgehaaldeMedewerker.getExtraWerktijden().size()).isEqualTo(1);
        assertThat(opgehaaldeMedewerker.getVerloven().size()).isEqualTo(1);
    }

    @DisplayName("Openingstijden bij salon")
    @Test
    void openingstijdenBijSalon() {
        Salon salon = new Salon();
        salon.setNaam("naamSalon");

        Openingstijd openingstijd1 = salon.addOpeningstijd();
        openingstijd1.setDag(DayOfWeek.MONDAY);
        openingstijd1.setStart(LocalTime.of(11, 0));
        openingstijd1.setEind(LocalTime.of(12, 15));

        Openingstijd openingstijd2 = salon.addOpeningstijd();
        openingstijd2.setDag(DayOfWeek.THURSDAY);
        openingstijd2.setStart(LocalTime.of(13, 15));
        openingstijd2.setEind(LocalTime.of(15, 45));

        ExtraOpeningstijd extraOpeningstijd = salon.addExtraOpeningstijd();
        extraOpeningstijd.setStart(LocalDateTime.now());
        extraOpeningstijd.setEind(LocalDateTime.now());

        ExtraGesloten extraGesloten = salon.addExtraGesloten();
        extraGesloten.setStart(LocalDateTime.now());
        extraGesloten.setEind(LocalDateTime.now());

        entityManager.persist(salon);

        Salon opgehaaldeSalon = entityManager.find(Salon.class, salon.getId());
        assertThat(opgehaaldeSalon).isEqualTo(salon);
        assertThat(opgehaaldeSalon.getOpeningstijden().size()).isEqualTo(2);
        assertThat(opgehaaldeSalon.getExtraOpeningstijden().size()).isEqualTo(1);
        assertThat(opgehaaldeSalon.getExtraGeslotens().size()).isEqualTo(1);
    }
}
