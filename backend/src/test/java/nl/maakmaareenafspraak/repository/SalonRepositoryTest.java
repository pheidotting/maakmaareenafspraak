package nl.maakmaareenafspraak.repository;

import nl.maakmaareenafspraak.domain.Adres;
import nl.maakmaareenafspraak.domain.Behandeling;
import nl.maakmaareenafspraak.domain.Medewerker;
import nl.maakmaareenafspraak.domain.Salon;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.test.context.ContextConfiguration;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.util.Lists.newArrayList;
import static org.junit.jupiter.api.Assertions.assertNull;

@DataJpaTest
@ContextConfiguration(classes = {SalonRepositoryTest.class})
@EnableJpaRepositories(basePackages = {"nl.maakmaareenafspraak.*"})
@EntityScan("nl.maakmaareenafspraak.domain")
class SalonRepositoryTest {
    @Autowired
    private TestEntityManager entityManager;

    @Autowired
    private SalonRepository salonRepository;

    @Test
    @DisplayName("Test het opslaan en ophalen van een Salon zonder alles")
    void testOpslaanSalon() {
        Salon salon = new Salon();
        salon.setNaam("Salon1");
        salon.setEmailadres("mail@salon1.nl");
        salon.setTelefoonnummer("0612345678");

        salonRepository.save(salon);

        Salon salonOpgehaald = salonRepository.getOne(salon.getId());
        assertThat(salonOpgehaald).isEqualTo(salon);
        assertNull(salonOpgehaald.getAdres());
        assertThat(salonOpgehaald.getMedewerkers().size()).isEqualTo(0);
    }

    @Test
    @DisplayName("Test het opslaan en ophalen van een Salon mét een adres")
    void testOpslaanSalonMetAdres() {
        Salon salon = new Salon();
        salon.setNaam("Salon1");
        salon.setEmailadres("mail@salon1.nl");
        salon.setTelefoonnummer("0612345678");

        Adres adres = new Adres();
        adres.setSalon(salon);
        adres.setPlaats("Plaatsnaam");
        adres.setPostcode("1234AA");
        adres.setStraat("Staatnaam 1");

        salon.setAdres(adres);

        salonRepository.save(salon);

        Salon salonOpgehaald = salonRepository.getOne(salon.getId());
        assertThat(salonOpgehaald).isEqualTo(salon);
        assertThat(salonOpgehaald.getAdres()).isEqualTo(adres);
    }

    @Test
    @DisplayName("Test het opslaan en ophalen van een Salon mét medewerkers")
    void testOpslaanSalonMetMedewerkers() {
        Salon salon = new Salon();
        salon.setNaam("Salon1");
        salon.setEmailadres("mail@salon1.nl");
        salon.setTelefoonnummer("0612345678");

        Medewerker medewerker1 = salon.addMedewerker();
        medewerker1.setNaam("medewerker1");
        Medewerker medewerker2 = salon.addMedewerker();
        medewerker2.setNaam("medewerker2");

        salonRepository.save(salon);

        Salon salonOpgehaald = salonRepository.getOne(salon.getId());
        assertThat(salonOpgehaald).isEqualTo(salon);
        assertThat(salonOpgehaald.getMedewerkers().size()).isEqualTo(2);
        assertThat(newArrayList(salonOpgehaald.getMedewerkers()).get(0)).isEqualTo(medewerker1);
        assertThat(newArrayList(salonOpgehaald.getMedewerkers()).get(1)).isEqualTo(medewerker2);
    }

    @Test
    @DisplayName("Test het opslaan en ophalen van een Salon mét behandelingen")
    void testOpslaanSalonMetBehanlingen() {
        Salon salon = new Salon();
        salon.setNaam("Salon1");
        salon.setEmailadres("mail@salon1.nl");
        salon.setTelefoonnummer("0612345678");

        Behandeling behandeling1 = salon.addBehandeling();
        behandeling1.setNaam("naam behandeling 1");
        behandeling1.setOmschrijving("blablabla1");
        behandeling1.setTijdsduur(20L);
        behandeling1.setPrijs(10.5D);
        Behandeling behandeling2 = salon.addBehandeling();
        behandeling2.setNaam("naam behandeling 2");
        behandeling2.setOmschrijving("blablabla2");
        behandeling2.setTijdsduur(30L);
        behandeling2.setPrijs(1234.56);

        salonRepository.save(salon);

        Salon salonOpgehaald = salonRepository.getOne(salon.getId());
        assertThat(salonOpgehaald).isEqualTo(salon);
        //        assertThat(salonOpgehaald.getBehandelingen().size()).isEqualTo(2);
        //        assertThat(newArrayList(salonOpgehaald.getBehandelingen()).get(0)).isEqualTo(behandeling1);
        //        assertThat(newArrayList(salonOpgehaald.getBehandelingen()).get(1)).isEqualTo(behandeling2);
    }

    @DisplayName("Medewerker opslaan met behandelingen")
    @Test
    void testMedewerkerBehandelingen() {
        Medewerker medewerker1 = new Medewerker();
        medewerker1.setNaam("medewerker1");
        Medewerker medewerker2 = new Medewerker();
        medewerker2.setNaam("medewerker2");

        Behandeling behandeling1 = new Behandeling();
        behandeling1.setNaam("behandeling1");
        Behandeling behandeling2 = new Behandeling();
        behandeling2.setNaam("behandeling2");
        Behandeling behandeling3 = new Behandeling();
        behandeling3.setNaam("behandeling3");

        medewerker1.getBehandelingen().add(behandeling1);
        medewerker1.getBehandelingen().add(behandeling2);
        medewerker2.getBehandelingen().add(behandeling2);
        medewerker2.getBehandelingen().add(behandeling3);

        entityManager.persist(medewerker1);
        entityManager.persist(medewerker2);

        Medewerker medewerkerOpgehaald1 = entityManager.find(Medewerker.class, medewerker1.getId());
        Medewerker medewerkerOpgehaald2 = entityManager.find(Medewerker.class, medewerker2.getId());
        assertThat(medewerkerOpgehaald1).isEqualTo(medewerker1);
        assertThat(medewerkerOpgehaald2).isEqualTo(medewerker2);
        assertThat(medewerkerOpgehaald1.getBehandelingen().size()).isEqualTo(2);
        assertThat(medewerkerOpgehaald2.getBehandelingen().size()).isEqualTo(2);
    }
}
