package nl.maakmaareenafspraak.repository;

import nl.maakmaareenafspraak.domain.Afspraak;
import nl.maakmaareenafspraak.domain.Behandeling;
import nl.maakmaareenafspraak.domain.Salon;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.test.context.ContextConfiguration;

import java.time.LocalDateTime;
import java.util.HashSet;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertEquals;

@DataJpaTest
@ContextConfiguration(classes = {AfspraakRepositoryTest.class})
@EnableJpaRepositories(basePackages = {"nl.maakmaareenafspraak.*"})
@EntityScan("nl.maakmaareenafspraak.domain")
class AfspraakRepositoryTest {

    @Autowired
    private TestEntityManager entityManager;
    @Autowired
    private AfspraakRepository afspraakRepository;

    @Test
    @DisplayName("Test het opslaan en ophalen van een Afspraak zonder alles")
    void testOpslaanAfspraak() {
        Afspraak afspraak = new Afspraak();
        afspraak.setOpmerking("Lorem ipsum");
        afspraak.setTijdstip(LocalDateTime.now());

        afspraakRepository.save(afspraak);

        Afspraak afspraakOpgehaald = afspraakRepository.getOne(afspraak.getId());
        assertThat(afspraakOpgehaald).isEqualTo(afspraak);
    }

    @Test
    @DisplayName("Test het opslaan en ophalen van een Afspraak met behandeling en niet ingelogde klant")
    void testOpslaanAfspraakMetBehandeling() {
        Salon salon = new Salon();
        salon.setNaam("NaamSalon");

        Behandeling behandeling1 = salon.addBehandeling();
        behandeling1.setNaam("een");
        Behandeling behandeling2 = salon.addBehandeling();
        behandeling2.setNaam("twee");

        entityManager.persist(salon);

        Afspraak afspraak = new Afspraak();
        afspraak.setTijdstip(LocalDateTime.now());
        afspraak.setBehandelingen(new HashSet<>());
        afspraak.getBehandelingen().add(behandeling1);
        afspraak.getBehandelingen().add(behandeling2);

        afspraakRepository.save(afspraak);

        Afspraak opgehaaldeAfspraak = afspraakRepository.getOne(afspraak.getId());
        assertThat(opgehaaldeAfspraak.getBehandelingen().size()).isEqualTo(2);
    }

    @Test
    @DisplayName("Test het ophalen van afspraken binnen bepaalde tijden")
    void testTijden() {
        Salon salon = new Salon();
        salon.setNaam("Salon");

        Behandeling behandeling1 = salon.addBehandeling();
        behandeling1.setNaam("een");

        entityManager.persist(salon);

        LocalDateTime tijd = LocalDateTime.of(2021, 4, 1, 9, 0);

        Afspraak afspraak1 = new Afspraak();
        afspraak1.setTijdstip(tijd);
        afspraak1.setBehandelingen(new HashSet<>());
        afspraak1.getBehandelingen().add(behandeling1);
        afspraak1.setSalon(salon);

        afspraakRepository.save(afspraak1);

        assertEquals(1, afspraakRepository.haalAfsprakenOp(salon, tijd, tijd).size());
        assertEquals(1, afspraakRepository.haalAfsprakenOp(salon, tijd, tijd.plusMinutes(1)).size());
        assertEquals(1, afspraakRepository.haalAfsprakenOp(salon, tijd.minusMinutes(1), tijd.plusMinutes(1)).size());
        assertEquals(0, afspraakRepository.haalAfsprakenOp(salon, tijd.plusMinutes(1), tijd.plusMinutes(2)).size());
    }
}