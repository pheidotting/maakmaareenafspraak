package nl.maakmaareenafspraak.repository;

import nl.maakmaareenafspraak.domain.Adres;
import nl.maakmaareenafspraak.domain.Klant;
import nl.maakmaareenafspraak.domain.NietIngelogdeKlant;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.test.context.ContextConfiguration;

import static org.assertj.core.api.Assertions.assertThat;

@DataJpaTest
@ContextConfiguration(classes = {KlantRepositoryTest.class})
@EnableJpaRepositories(basePackages = {"nl.maakmaareenafspraak.*"})
@EntityScan("nl.maakmaareenafspraak.domain")
class KlantRepositoryTest {

   @Autowired
   private TestEntityManager entityManager;
   @Autowired
   private KlantRepository klantRepository;

   @Test
   @DisplayName("Test een gewone klant")
   void testOpslaanGewoneKlant() {
      Klant klant = new Klant();
      klant.setNaam("Naam Adres");
      klant.setEmailadres("email");
      klant.setTelefoonnummer("telefoon");

      Adres adres = new Adres();
      adres.setStraat("straat");
      adres.setKlant(klant);
      adres.setPostcode("12434AA");
      adres.setPlaats("plaats");

      klant.setAdres(adres);

      klantRepository.save(klant);

      Klant klantOpgehaald = (Klant) klantRepository.getOne(klant.getId());
      assertThat(klant).isEqualTo(klantOpgehaald);
   }

   @Test
   @DisplayName("Test een niet ingelogde klant")
   void testOpslaanNietIngelogdeKlant() {
      NietIngelogdeKlant klant = new NietIngelogdeKlant();
      klant.setNaam("Naam Adres");
      klant.setEmailadres("email");
      klant.setTelefoonnummer("telefoon");

      klantRepository.save(klant);

      NietIngelogdeKlant klantOpgehaald = (NietIngelogdeKlant) klantRepository.getOne(klant.getId());
      assertThat(klant).isEqualTo(klantOpgehaald);
   }
}