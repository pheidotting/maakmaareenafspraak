package nl.maakmaareenafspraak.repository;

import nl.maakmaareenafspraak.domain.Beheerder;
import nl.maakmaareenafspraak.domain.Medewerker;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.test.context.ContextConfiguration;

import static org.junit.jupiter.api.Assertions.*;

@DataJpaTest
@ContextConfiguration(classes = {MedewerkerRepositoryTest.class})
@EnableJpaRepositories(basePackages = {"nl.maakmaareenafspraak.*"})
@EntityScan("nl.maakmaareenafspraak.domain")
class MedewerkerRepositoryTest {

    @Autowired
    private TestEntityManager entityManager;
    @Autowired
    private MedewerkerRepository medewerkerRepository;

    @Test
    void test() {
        var medewerker = new Medewerker();
        medewerker.setNaam("medewerker");
        var beheerder = new Beheerder();
        beheerder.setNaam("beheerder");

        medewerkerRepository.save(medewerker);
        medewerkerRepository.save(beheerder);

        var opgehaaldMedewerker = medewerkerRepository.findById(medewerker.getId()).get();
        var opgehaalBeheerder = medewerkerRepository.findById(beheerder.getId()).get();

        assertEquals("medewerker", opgehaaldMedewerker.getNaam());
        assertTrue(opgehaaldMedewerker instanceof Medewerker);
        assertEquals("beheerder", opgehaalBeheerder.getNaam());
        assertTrue(opgehaalBeheerder instanceof Beheerder);
    }

    @Test
    void testZoekOpGebruikersNaam() {
        var medewewerker1 = new Medewerker();
        medewewerker1.setGebruikersnaam("mw1");
        var medewewerker2 = new Medewerker();
        medewewerker2.setGebruikersnaam("mw2");

        medewerkerRepository.save(medewewerker1);
        medewerkerRepository.save(medewewerker2);

        assertEquals(medewewerker1, medewerkerRepository.zoekOpGebruikersnaam("mw1"));
        assertEquals(medewewerker2, medewerkerRepository.zoekOpGebruikersnaam("mw2"));
        assertNull(medewerkerRepository.zoekOpGebruikersnaam("mw3"));
    }

}