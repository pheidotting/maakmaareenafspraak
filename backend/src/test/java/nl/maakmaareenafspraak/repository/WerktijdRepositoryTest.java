package nl.maakmaareenafspraak.repository;

import nl.maakmaareenafspraak.domain.Medewerker;
import nl.maakmaareenafspraak.domain.Werktijd;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.test.context.ContextConfiguration;

import java.time.LocalTime;
import java.util.List;

import static org.assertj.core.util.Lists.newArrayList;
import static org.junit.jupiter.api.Assertions.assertEquals;

@DataJpaTest
@ContextConfiguration(classes = {WerktijdRepositoryTest.class})
@EnableJpaRepositories(basePackages = {"nl.maakmaareenafspraak.*"})
@EntityScan("nl.maakmaareenafspraak.domain")
class WerktijdRepositoryTest {
    @Autowired
    private TestEntityManager entityManager;

    @Autowired
    private WerktijdRepository werktijdRepository;

    @Test
    @DisplayName("Test het opslaan en ophalen van een Werktijd via de query werktijdenBijMedewerker")
    void testOpslaanEnOphalenWerktijden() {
        Medewerker medewerker1 = medewerker("naam1");
        Medewerker medewerker2 = medewerker("naam2");
        Medewerker medewerker3 = medewerker("naam3");

        entityManager.persist(medewerker1);
        entityManager.persist(medewerker2);
        entityManager.persist(medewerker3);

        Werktijd werktijd1 = werktijd(medewerker1, LocalTime.of(1, 2));
        Werktijd werktijd2 = werktijd(medewerker1, LocalTime.of(1, 3));
        Werktijd werktijd3 = werktijd(medewerker2, LocalTime.of(1, 4));
        Werktijd werktijd4 = werktijd(medewerker3, LocalTime.of(1, 5));

        werktijdRepository.save(werktijd1);
        werktijdRepository.save(werktijd2);
        werktijdRepository.save(werktijd3);
        werktijdRepository.save(werktijd4);

        List<Werktijd> werktijden = werktijdRepository.werktijdenBijMedewerker(newArrayList(medewerker1, medewerker2));
        assertEquals(3, werktijden.size());
        assertEquals(LocalTime.of(1, 2), werktijden.get(0).getStart());
        assertEquals(LocalTime.of(1, 3), werktijden.get(1).getStart());
        assertEquals(LocalTime.of(1, 4), werktijden.get(2).getStart());
    }

    private Werktijd werktijd(Medewerker medewerker, LocalTime localTime) {
        Werktijd werktijd = new Werktijd();
        werktijd.setMedewerker(medewerker);
        werktijd.setStart(localTime);
        return werktijd;
    }

    private Medewerker medewerker(String naam) {
        Medewerker medewerker = new Medewerker();
        medewerker.setNaam(naam);
        return medewerker;
    }
}
