package nl.maakmaareenafspraak.repository;

import nl.maakmaareenafspraak.domain.Behandeling;
import nl.maakmaareenafspraak.domain.ExtraOpeningstijd;
import nl.maakmaareenafspraak.domain.Salon;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.test.context.ContextConfiguration;

import java.time.LocalDateTime;

import static org.junit.jupiter.api.Assertions.assertEquals;

@DataJpaTest
@ContextConfiguration(classes = {ExtraOpeningstijdRepositoryTest.class})
@EnableJpaRepositories(basePackages = {"nl.maakmaareenafspraak.*"})
@EntityScan("nl.maakmaareenafspraak.domain")
class ExtraOpeningstijdRepositoryTest {

    @Autowired
    private TestEntityManager entityManager;
    @Autowired
    private ExtraOpeningstijdRepository extraOpeningstijdRepository;

    @Test
    @DisplayName("Test het ophalen binnen bepaalde tijden")
    void testTijden() {
        Salon salon = new Salon();
        salon.setNaam("Salon");

        Behandeling behandeling1 = salon.addBehandeling();
        behandeling1.setNaam("een");

        entityManager.persist(salon);

        LocalDateTime start = LocalDateTime.of(2021, 4, 1, 9, 0);
        LocalDateTime eind = LocalDateTime.of(2021, 4, 1, 10, 0);

        ExtraOpeningstijd extraOpeningstijd = new ExtraOpeningstijd();
        extraOpeningstijd.setSalon(salon);
        extraOpeningstijd.setStart(start);
        extraOpeningstijd.setEind(eind);

        extraOpeningstijdRepository.save(extraOpeningstijd);

        assertEquals(1, extraOpeningstijdRepository.extraOpeningstijdenBijSalon(salon, start, eind).size());
        assertEquals(1, extraOpeningstijdRepository.extraOpeningstijdenBijSalon(salon, start, eind.plusMinutes(1)).size());
        assertEquals(1, extraOpeningstijdRepository.extraOpeningstijdenBijSalon(salon, start.minusMinutes(1), eind.plusMinutes(1)).size());
        assertEquals(0, extraOpeningstijdRepository.extraOpeningstijdenBijSalon(salon, start.plusMinutes(1), eind.plusMinutes(2)).size());
    }
}