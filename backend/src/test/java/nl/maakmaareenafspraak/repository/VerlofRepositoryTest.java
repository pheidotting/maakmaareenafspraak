package nl.maakmaareenafspraak.repository;

import nl.maakmaareenafspraak.domain.Behandeling;
import nl.maakmaareenafspraak.domain.Medewerker;
import nl.maakmaareenafspraak.domain.Salon;
import nl.maakmaareenafspraak.domain.Verlof;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.test.context.ContextConfiguration;

import java.time.LocalDateTime;

import static org.assertj.core.util.Lists.newArrayList;
import static org.junit.jupiter.api.Assertions.assertEquals;

@DataJpaTest
@ContextConfiguration(classes = {VerlofRepositoryTest.class})
@EnableJpaRepositories(basePackages = {"nl.maakmaareenafspraak.*"})
@EntityScan("nl.maakmaareenafspraak.domain")
class VerlofRepositoryTest {
    @Autowired
    private TestEntityManager entityManager;

    @Autowired
    private VerlofRepository verlofRepository;


    @Test
    @DisplayName("Test het ophalen binnen bepaalde tijden")
    void testTijden() {
        Salon salon = new Salon();
        salon.setNaam("Salon");

        Behandeling behandeling1 = salon.addBehandeling();
        behandeling1.setNaam("een");
        Medewerker medewerker = salon.addMedewerker();
        medewerker.setNaam("naamMW");

        entityManager.persist(salon);
        entityManager.persist(medewerker);

        LocalDateTime start = LocalDateTime.of(2021, 4, 1, 9, 0);
        LocalDateTime eind = LocalDateTime.of(2021, 4, 1, 10, 0);

        Verlof extraWerktijd = new Verlof();
        extraWerktijd.setMedewerker(medewerker);
        extraWerktijd.setStart(start);
        extraWerktijd.setEind(eind);

        verlofRepository.save(extraWerktijd);

        assertEquals(1, verlofRepository.verlovenBijMedewerker(newArrayList(medewerker), start, eind).size());
        assertEquals(1, verlofRepository.verlovenBijMedewerker(newArrayList(medewerker), start, eind.plusMinutes(1)).size());
        assertEquals(1, verlofRepository.verlovenBijMedewerker(newArrayList(medewerker), start.minusMinutes(1), eind.plusMinutes(1)).size());
        assertEquals(0, verlofRepository.verlovenBijMedewerker(newArrayList(medewerker), start.plusMinutes(1), eind.plusMinutes(2)).size());
    }
}
