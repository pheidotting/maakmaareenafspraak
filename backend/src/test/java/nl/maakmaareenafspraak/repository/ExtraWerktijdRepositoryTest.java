package nl.maakmaareenafspraak.repository;

import nl.maakmaareenafspraak.domain.Behandeling;
import nl.maakmaareenafspraak.domain.ExtraWerktijd;
import nl.maakmaareenafspraak.domain.Medewerker;
import nl.maakmaareenafspraak.domain.Salon;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.test.context.ContextConfiguration;

import java.time.LocalDateTime;
import java.util.List;

import static org.assertj.core.util.Lists.newArrayList;
import static org.junit.jupiter.api.Assertions.assertEquals;

@DataJpaTest
@ContextConfiguration(classes = {ExtraWerktijdRepositoryTest.class})
@EnableJpaRepositories(basePackages = {"nl.maakmaareenafspraak.*"})
@EntityScan("nl.maakmaareenafspraak.domain")
class ExtraWerktijdRepositoryTest {
    @Autowired
    private TestEntityManager entityManager;

    @Autowired
    private ExtraWerktijdRepository extraWerktijdRepository;

    @Test
    @DisplayName("Test het opslaan en ophalen van een ExtraWerktijd via de query extraWerktijdenBijMedewerker")
    void testOpslaanEnOphalenExtraWerktijden() {
        Medewerker medewerker1 = medewerker("naam1");
        Medewerker medewerker2 = medewerker("naam2");
        Medewerker medewerker3 = medewerker("naam3");

        entityManager.persist(medewerker1);
        entityManager.persist(medewerker2);
        entityManager.persist(medewerker3);

        ExtraWerktijd extraWerktijd1 = extraWerktijd(medewerker1, LocalDateTime.of(2021, 1, 1, 1, 2));
        ExtraWerktijd extraWerktijd2 = extraWerktijd(medewerker1, LocalDateTime.of(2021, 1, 1, 1, 3));
        ExtraWerktijd extraWerktijd3 = extraWerktijd(medewerker2, LocalDateTime.of(2021, 1, 1, 1, 4));
        ExtraWerktijd extraWerktijd4 = extraWerktijd(medewerker3, LocalDateTime.of(2021, 1, 1, 1, 5));

        extraWerktijdRepository.save(extraWerktijd1);
        extraWerktijdRepository.save(extraWerktijd2);
        extraWerktijdRepository.save(extraWerktijd3);
        extraWerktijdRepository.save(extraWerktijd4);

        List<ExtraWerktijd> extraWerktijden = extraWerktijdRepository.extraWerktijdenBijMedewerker(newArrayList(medewerker1, medewerker2), LocalDateTime.of(2021, 1, 1, 1, 1), LocalDateTime.of(2021, 1, 1, 1, 5).plusHours(3));
        assertEquals(3, extraWerktijden.size());
        assertEquals(LocalDateTime.of(2021, 1, 1, 1, 2), extraWerktijden.get(0).getStart());
        assertEquals(LocalDateTime.of(2021, 1, 1, 1, 3), extraWerktijden.get(1).getStart());
        assertEquals(LocalDateTime.of(2021, 1, 1, 1, 4), extraWerktijden.get(2).getStart());
    }

    private ExtraWerktijd extraWerktijd(Medewerker medewerker, LocalDateTime localDateTime) {
        ExtraWerktijd extraWerktijd = new ExtraWerktijd();
        extraWerktijd.setMedewerker(medewerker);
        extraWerktijd.setStart(localDateTime);
        extraWerktijd.setEind(extraWerktijd.getStart().plusHours(3));
        return extraWerktijd;
    }

    private Medewerker medewerker(String naam) {
        Medewerker medewerker = new Medewerker();
        medewerker.setNaam(naam);
        return medewerker;
    }

    @Test
    @DisplayName("Test het ophalen binnen bepaalde tijden")
    void testTijden() {
        Salon salon = new Salon();
        salon.setNaam("Salon");

        Behandeling behandeling1 = salon.addBehandeling();
        behandeling1.setNaam("een");
        Medewerker medewerker = salon.addMedewerker();
        medewerker.setNaam("naamMW");

        entityManager.persist(salon);
        entityManager.persist(medewerker);

        LocalDateTime start = LocalDateTime.of(2021, 4, 1, 9, 0);
        LocalDateTime eind = LocalDateTime.of(2021, 4, 1, 10, 0);

        ExtraWerktijd extraWerktijd = new ExtraWerktijd();
        extraWerktijd.setMedewerker(medewerker);
        extraWerktijd.setStart(start);
        extraWerktijd.setEind(eind);

        extraWerktijdRepository.save(extraWerktijd);

        assertEquals(1, extraWerktijdRepository.extraWerktijdenBijMedewerker(newArrayList(medewerker), start, eind).size());
        assertEquals(1, extraWerktijdRepository.extraWerktijdenBijMedewerker(newArrayList(medewerker), start, eind.plusMinutes(1)).size());
        assertEquals(1, extraWerktijdRepository.extraWerktijdenBijMedewerker(newArrayList(medewerker), start.minusMinutes(1), eind.plusMinutes(1)).size());
        assertEquals(0, extraWerktijdRepository.extraWerktijdenBijMedewerker(newArrayList(medewerker), start.plusMinutes(1), eind.plusMinutes(2)).size());
    }
}
