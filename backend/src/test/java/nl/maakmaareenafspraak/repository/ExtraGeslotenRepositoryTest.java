package nl.maakmaareenafspraak.repository;

import nl.maakmaareenafspraak.domain.Behandeling;
import nl.maakmaareenafspraak.domain.ExtraGesloten;
import nl.maakmaareenafspraak.domain.Salon;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.test.context.ContextConfiguration;

import java.time.LocalDateTime;

import static org.junit.jupiter.api.Assertions.assertEquals;

@DataJpaTest
@ContextConfiguration(classes = {ExtraGeslotenRepositoryTest.class})
@EnableJpaRepositories(basePackages = {"nl.maakmaareenafspraak.*"})
@EntityScan("nl.maakmaareenafspraak.domain")
class ExtraGeslotenRepositoryTest {

    @Autowired
    private TestEntityManager entityManager;
    @Autowired
    private ExtraGeslotenRepository extraGeslotenRepository;

    @Test
    @DisplayName("Test het ophalen binnen bepaalde tijden")
    void testTijden() {
        Salon salon = new Salon();
        salon.setNaam("Salon");

        Behandeling behandeling1 = salon.addBehandeling();
        behandeling1.setNaam("een");

        entityManager.persist(salon);

        LocalDateTime start = LocalDateTime.of(2021, 4, 1, 9, 0);
        LocalDateTime eind = LocalDateTime.of(2021, 4, 1, 10, 0);

        ExtraGesloten extraGesloten = new ExtraGesloten();
        extraGesloten.setSalon(salon);
        extraGesloten.setStart(start);
        extraGesloten.setEind(eind);

        extraGeslotenRepository.save(extraGesloten);

        assertEquals(1, extraGeslotenRepository.extraGeslotenenBijMedewerker(salon, start, eind).size());
        assertEquals(1, extraGeslotenRepository.extraGeslotenenBijMedewerker(salon, start, eind.plusMinutes(1)).size());
        assertEquals(1, extraGeslotenRepository.extraGeslotenenBijMedewerker(salon, start.minusMinutes(1), eind.plusMinutes(1)).size());
        assertEquals(0, extraGeslotenRepository.extraGeslotenenBijMedewerker(salon, start.plusMinutes(1), eind.plusMinutes(2)).size());
    }
}