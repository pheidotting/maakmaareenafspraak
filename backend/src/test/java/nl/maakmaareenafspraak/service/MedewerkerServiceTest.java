package nl.maakmaareenafspraak.service;

import nl.maakmaareenafspraak.domain.Medewerker;
import nl.maakmaareenafspraak.repository.MedewerkerRepository;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.mockito.Mockito.verifyNoMoreInteractions;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class MedewerkerServiceTest {
    @Mock
    private MedewerkerRepository medewerkerRepository;

    @InjectMocks
    private MedewerkerService medewerkerService = new MedewerkerService();

    @Test
    void genereerWijzigWachtwoordCode() {
        var medewerker = new Medewerker();
        medewerkerService.genereerWijzigWachtwoordCode(medewerker);
        assertEquals(30, medewerker.getWijzigWachtwoordCode().length());
        assertNotNull(medewerker.getWijzigWachtwoordCodeTijd());
    }

    @Test
    void genereerGebruikersnaam() {
        var gebruikersnaam = "betsytruuskappers.truus";
        when(medewerkerRepository.zoekOpGebruikersnaam(gebruikersnaam)).thenReturn(null);

        assertEquals(gebruikersnaam, medewerkerService.genereerGebruikersnaam("Betsy & Truus Kappers", "Truus"));

        verifyNoMoreInteractions(medewerkerRepository);
    }

    @Test
    void genereerGebruikersnaamKomtAlVoor() {
        var gebruikersnaam = "betsytruuskappers.truus";
        when(medewerkerRepository.zoekOpGebruikersnaam(gebruikersnaam)).thenReturn(new Medewerker());

        var result = medewerkerService.genereerGebruikersnaam("Betsy & Truus Kappers", "Truus");
        assertNotNull(result);
        assertEquals(10, result.length());

        verifyNoMoreInteractions(medewerkerRepository);
    }

    @Test
    void genereerGebruikersnaamLangeNaam() {
        var gebruikersnaam = "loremipsumdolorsitametconsecteturadipiscingelit.donecconsectetursuscipitdolornecvulputatecrasacplace";
        when(medewerkerRepository.zoekOpGebruikersnaam(gebruikersnaam)).thenReturn(null);

        var result = medewerkerService.genereerGebruikersnaam("Lorem ipsum dolor sit amet, consectetur adipiscing elit.", "Donec consectetur suscipit dolor nec vulputate. Cras ac placerat.");
        assertEquals(100, result.length());
        assertEquals(gebruikersnaam, result);

        verifyNoMoreInteractions(medewerkerRepository);
    }
}