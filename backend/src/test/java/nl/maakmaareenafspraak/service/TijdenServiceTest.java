//package nl.maakmaareenafspraak.service;
//
//import nl.maakmaareenafspraak.domain.*;
//import nl.maakmaareenafspraak.repository.*;
//import nl.maakmaareenafspraak.web.domain.Dag;
//import nl.maakmaareenafspraak.web.domain.Tijdstip;
//import org.junit.jupiter.api.Test;
//import org.junit.jupiter.api.extension.ExtendWith;
//import org.mockito.InjectMocks;
//import org.mockito.Mock;
//import org.mockito.Mockito;
//import org.mockito.junit.jupiter.MockitoExtension;
//
//import java.time.DayOfWeek;
//import java.time.LocalDate;
//import java.time.LocalDateTime;
//import java.time.LocalTime;
//import java.util.List;
//import java.util.Set;
//import java.util.function.Consumer;
//
//import static com.google.common.collect.Lists.newArrayList;
//import static com.google.common.collect.Sets.newHashSet;
//import static org.junit.jupiter.api.Assertions.assertEquals;
//
//@ExtendWith(MockitoExtension.class)
//class TijdenServiceTest {
//    LocalDate beginDatum = LocalDate.now().plusYears(1).withMonth(3).withDayOfMonth(29);
//    int jaar = beginDatum.getYear();
//    LocalDate eindDatum = beginDatum.plusDays(6);
//
//    @Mock
//    private SalonService salonService;
//    @Mock
//    private OpeningstijdRepository openingstijdRepository;
//    @Mock
//    private ExtraOpeningstijdRepository extraOpeningstijdRepository;
//    @Mock
//    private ExtraGeslotenRepository extraGeslotenRepository;
//    @Mock
//    private WerktijdRepository werktijdRepository;
//    @Mock
//    private ExtraWerktijdRepository extraWerktijdRepository;
//    @Mock
//    private VerlofRepository verlofRepository;
//    @Mock
//    private AfspraakRepository afspraakRepository;
//
//    @InjectMocks
//    private TijdenService tijdenService = new TijdenService();
//
//    @Test
//    void ophalenWerktijden() {
//        Salon salon = new Salon();
//        Medewerker medewerker1 = medewerker(1L, "naam1");
//        Medewerker medewerker2 = medewerker(2L, "naam2");
//        salon.setMedewerkers(newHashSet(medewerker1, medewerker2));
//        List<Medewerker> medewerkers = newArrayList(medewerker1, medewerker2);
//        List<Werktijd> werktijden = werktijden(medewerker1, medewerker2);
//
//        LocalDateTime beginTijd = LocalDateTime.of(beginDatum, LocalTime.of(0, 0));
//        LocalDateTime eindTijd = LocalDateTime.of(eindDatum, LocalTime.of(23, 59));
//
//        Mockito.when(openingstijdRepository.openingstijdenBijSalon(salon)).thenReturn(openingstijden(salon));
//        Mockito.when(extraOpeningstijdRepository.extraOpeningstijdenBijSalon(salon, beginTijd, eindTijd)).thenReturn(extraOpeningstijden(salon));
//        Mockito.when(extraGeslotenRepository.extraGeslotenenBijMedewerker(salon, beginTijd, eindTijd)).thenReturn(extraGeslotens(salon));
//        Mockito.when(werktijdRepository.werktijdenBijMedewerker(medewerkers)).thenReturn(werktijden);
//        Mockito.when(extraWerktijdRepository.extraWerktijdenBijMedewerker(medewerkers, beginTijd, eindTijd)).thenReturn(extraWerktijden(medewerker1, medewerker2));
//        Mockito.when(verlofRepository.verlovenBijMedewerker(medewerkers, beginTijd, eindTijd)).thenReturn(verloven(medewerker1, medewerker2));
//        Mockito.when(afspraakRepository.haalAfsprakenOp(salon, beginTijd, eindTijd)).thenReturn(afspraken(medewerker1, medewerker2, behandeling(15L), behandeling(30L)));
//
//        List<Dag> dagen = tijdenService.ophalenWerktijden(salon, beginDatum, eindDatum);
//
//        dagen.stream().forEach(new Consumer<Dag>() {
//            @Override
//            public void accept(Dag dag) {
//                System.out.println(dag.getDatum());
//                dag.getTijdstippen().stream().forEach(new Consumer<Tijdstip>() {
//                    @Override
//                    public void accept(Tijdstip tijdstip) {
//                        System.out.print("   " + tijdstip.getTijd());
//                        tijdstip.getMedewekerIds().stream().forEach(new Consumer<Long>() {
//                            @Override
//                            public void accept(Long aLong) {
//                                System.out.print(" " + aLong);
//                            }
//                        });
//                        System.out.println("");
//                    }
//                });
//            }
//        });
//
//        assertEquals(7, dagen.size());
//        assertEquals(LocalDate.of(jaar, 3, 29), dagen.get(0).getDatum());
//        assertEquals(26, dagen.get(0).getTijdstippen().size());
//        assertEquals(LocalTime.of(9, 0), dagen.get(0).getTijdstippen().get(0).getTijd());
//        assertEquals(2, dagen.get(0).getTijdstippen().get(0).getMedewekerIds().size());
//        assertEquals(1, dagen.get(0).getTijdstippen().get(0).getMedewekerIds().get(0));
//
//        assertEquals(LocalDate.of(jaar, 3, 30), dagen.get(1).getDatum());
//        assertEquals(12, dagen.get(1).getTijdstippen().size());
//
//        assertEquals(LocalDate.of(jaar, 3, 31), dagen.get(2).getDatum());
//        assertEquals(4, dagen.get(2).getTijdstippen().size());
//
//        assertEquals(LocalDate.of(jaar, 4, 1), dagen.get(3).getDatum());
//        assertEquals(4, dagen.get(3).getTijdstippen().size());
//
//        //        Mockito.verify(werktijdRepository).;
//    }
//
//    private List<Afspraak> afspraken(Medewerker medewerker1, Medewerker medewerker2, Behandeling behandeling1, Behandeling behandeling2) {
//        Afspraak afspraak1 = afspraak(null, LocalDateTime.of(jaar, 3, 29, 9, 30), newHashSet(behandeling1));
//        Afspraak afspraak2 = afspraak(medewerker1, LocalDateTime.of(jaar, 3, 29, 13, 30), newHashSet(behandeling1, behandeling2));
//        Afspraak afspraak3 = afspraak(medewerker2, LocalDateTime.of(jaar, 3, 29, 15, 30), newHashSet(behandeling1, behandeling2));
//
//        return newArrayList(afspraak1, afspraak2, afspraak3);
//    }
//
//    private Behandeling behandeling(Long tijdsduur) {
//        Behandeling behandeling = new Behandeling();
//        behandeling.setTijdsduur(tijdsduur);
//        return behandeling;
//    }
//
//    private Afspraak afspraak(Medewerker medewerker, LocalDateTime localDateTime, Set<Behandeling> behandelingen) {
//        Afspraak afspraak = new Afspraak();
//        afspraak.setBehandelingen(behandelingen);
//        afspraak.setMedewerker(medewerker);
//        afspraak.setTijdstip(localDateTime);
//        return afspraak;
//    }
//
//    private Medewerker medewerker(Long id, String naam) {
//        Medewerker medewerker = new Medewerker();
//        medewerker.setId(id);
//        medewerker.setNaam(naam);
//        return medewerker;
//    }
//
//    private List<Werktijd> werktijden(Medewerker medewerker1, Medewerker medewerker2) {
//        Werktijd maandag1 = werktijd(medewerker1, LocalTime.of(9, 0), LocalTime.of(12, 0), DayOfWeek.MONDAY);
//        Werktijd maandag2 = werktijd(medewerker1, LocalTime.of(13, 0), LocalTime.of(17, 0), DayOfWeek.MONDAY);
//        Werktijd maandag3 = werktijd(medewerker2, LocalTime.of(13, 0), LocalTime.of(17, 0), DayOfWeek.MONDAY);
//        Werktijd dinsdag1 = werktijd(medewerker1, LocalTime.of(9, 0), LocalTime.of(12, 0), DayOfWeek.TUESDAY);
//        Werktijd dinsdag2 = werktijd(medewerker1, LocalTime.of(13, 0), LocalTime.of(17, 0), DayOfWeek.TUESDAY);
//        Werktijd dinsdag3 = werktijd(medewerker2, LocalTime.of(9, 0), LocalTime.of(12, 0), DayOfWeek.TUESDAY);
//        Werktijd dinsdag4 = werktijd(medewerker2, LocalTime.of(13, 0), LocalTime.of(17, 0), DayOfWeek.TUESDAY);
//        Werktijd woensdag1 = werktijd(medewerker1, LocalTime.of(9, 0), LocalTime.of(12, 0), DayOfWeek.WEDNESDAY);
//        Werktijd woensdag2 = werktijd(medewerker2, LocalTime.of(9, 0), LocalTime.of(12, 0), DayOfWeek.WEDNESDAY);
//        Werktijd woensdag3 = werktijd(medewerker2, LocalTime.of(13, 0), LocalTime.of(17, 0), DayOfWeek.WEDNESDAY);
//
//        return newArrayList(maandag1, maandag2, maandag3, dinsdag1, dinsdag2, dinsdag3, dinsdag4, woensdag1, woensdag2, woensdag3);
//    }
//
//    private List<Openingstijd> openingstijden(Salon salon) {
//        Openingstijd maandag1 = openingstijd(salon, LocalTime.of(9, 0), LocalTime.of(12, 0), DayOfWeek.MONDAY);
//        Openingstijd maandag2 = openingstijd(salon, LocalTime.of(13, 0), LocalTime.of(17, 0), DayOfWeek.MONDAY);
//        Openingstijd dinsdag1 = openingstijd(salon, LocalTime.of(9, 0), LocalTime.of(12, 0), DayOfWeek.TUESDAY);
//        Openingstijd dinsdag2 = openingstijd(salon, LocalTime.of(13, 0), LocalTime.of(17, 0), DayOfWeek.TUESDAY);
//        Openingstijd woensdag1 = openingstijd(salon, LocalTime.of(9, 0), LocalTime.of(12, 0), DayOfWeek.WEDNESDAY);
//
//        return newArrayList(maandag1, maandag2, dinsdag1, dinsdag2, woensdag1);
//    }
//
//    public List<ExtraOpeningstijd> extraOpeningstijden(Salon salon) {
//        ExtraOpeningstijd extraOpeningstijd1 = extraOpeningstijd(salon, LocalDateTime.of(jaar, 3, 31, 14, 0), LocalDateTime.of(jaar, 3, 31, 15, 0));
//        ExtraOpeningstijd extraOpeningstijd2 = extraOpeningstijd(salon, LocalDateTime.of(jaar, 4, 1, 10, 0), LocalDateTime.of(jaar, 4, 1, 11, 0));
//
//        return newArrayList(extraOpeningstijd1, extraOpeningstijd2);
//    }
//
//    public List<ExtraGesloten> extraGeslotens(Salon salon) {
//        ExtraGesloten extraGesloten = extraGesloten(salon, LocalDateTime.of(jaar, 3, 29, 16, 30), LocalDateTime.of(jaar, 3, 29, 17, 0));
//
//        return newArrayList(extraGesloten);
//    }
//
//    public List<ExtraWerktijd> extraWerktijden(Medewerker medewerker1, Medewerker medewerker2) {
//        ExtraWerktijd extraWerktijd1 = extraWerktijd(medewerker1, LocalDateTime.of(jaar, 3, 31, 14, 0), LocalDateTime.of(jaar, 3, 31, 15, 0));
//        ExtraWerktijd extraWerktijd2 = extraWerktijd(medewerker2, LocalDateTime.of(jaar, 4, 1, 10, 0), LocalDateTime.of(jaar, 4, 1, 11, 0));
//
//        return newArrayList(extraWerktijd1, extraWerktijd2);
//    }
//
//    public List<Verlof> verloven(Medewerker medewerker1, Medewerker medewerker2) {
//        Verlof verlof = verlof(medewerker1, LocalDateTime.of(jaar, 3, 29, 11, 30), LocalDateTime.of(jaar, 3, 29, 12, 0));
//
//        return newArrayList(verlof);
//    }
//
//    private Werktijd werktijd(Medewerker medewerker, LocalTime begintijd, LocalTime eindtijd, DayOfWeek dag) {
//        Werktijd werktijd = new Werktijd();
//        werktijd.setMedewerker(medewerker);
//        werktijd.setDag(dag);
//        werktijd.setStart(begintijd);
//        werktijd.setEind(eindtijd);
//        return werktijd;
//    }
//
//    private Openingstijd openingstijd(Salon salon, LocalTime begintijd, LocalTime eindtijd, DayOfWeek dag) {
//        Openingstijd openingstijd = new Openingstijd();
//        openingstijd.setSalon(salon);
//        openingstijd.setDag(dag);
//        openingstijd.setStart(begintijd);
//        openingstijd.setEind(eindtijd);
//        return openingstijd;
//    }
//
//    private ExtraGesloten extraGesloten(Salon salon, LocalDateTime begintijd, LocalDateTime eindtijd) {
//        ExtraGesloten extraGesloten = new ExtraGesloten();
//        extraGesloten.setSalon(salon);
//        extraGesloten.setStart(begintijd);
//        extraGesloten.setEind(eindtijd);
//        return extraGesloten;
//    }
//
//    private ExtraWerktijd extraWerktijd(Medewerker medewerker, LocalDateTime start, LocalDateTime eind) {
//        ExtraWerktijd extraWerktijd = new ExtraWerktijd();
//        extraWerktijd.setMedewerker(medewerker);
//        extraWerktijd.setStart(start);
//        extraWerktijd.setEind(eind);
//        return extraWerktijd;
//    }
//
//    private ExtraOpeningstijd extraOpeningstijd(Salon salon, LocalDateTime start, LocalDateTime eind) {
//        ExtraOpeningstijd extraOpeningstijd = new ExtraOpeningstijd();
//        extraOpeningstijd.setSalon(salon);
//        extraOpeningstijd.setStart(start);
//        extraOpeningstijd.setEind(eind);
//        return extraOpeningstijd;
//    }
//
//    private Verlof verlof(Medewerker medewerker, LocalDateTime start, LocalDateTime eind) {
//        Verlof verlof = new Verlof();
//        verlof.setMedewerker(medewerker);
//        verlof.setStart(start);
//        verlof.setEind(eind);
//        return verlof;
//    }
//}