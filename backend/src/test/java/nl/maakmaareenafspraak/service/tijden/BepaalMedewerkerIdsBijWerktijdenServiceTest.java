package nl.maakmaareenafspraak.service.tijden;

import nl.maakmaareenafspraak.domain.Medewerker;
import nl.maakmaareenafspraak.domain.Salon;
import nl.maakmaareenafspraak.domain.Werktijd;
import nl.maakmaareenafspraak.repository.WerktijdRepository;
import nl.maakmaareenafspraak.web.domain.Dag;
import nl.maakmaareenafspraak.web.domain.Tijdstip;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import java.time.LocalDateTime;
import java.time.LocalTime;
import java.util.List;
import java.util.Set;

import static com.google.common.collect.Lists.newArrayList;
import static com.google.common.collect.Sets.newHashSet;
import static java.time.DayOfWeek.WEDNESDAY;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class BepaalMedewerkerIdsBijWerktijdenServiceTest {
    @Mock
    private WerktijdRepository werktijdRepository;

    @InjectMocks
    private BepaalMedewerkerIdsBijWerktijdenService bepaalMedewerkerIdsBijWerktijdenService = new BepaalMedewerkerIdsBijWerktijdenService();

    @Test
    void test() {
        var beginTijd = LocalDateTime.of(2021, 5, 5, 9, 0);
        var eindTijd = LocalDateTime.of(2021, 5, 6, 17, 0);

        var medewerker = new Medewerker();
        medewerker.setId(4L);
        var salon = new Salon();
        salon.getMedewerkers().add(medewerker);
        var dag = new Dag();
        dag.setDatum(beginTijd.toLocalDate());
        var werktijd1 = new Werktijd();
        werktijd1.setMedewerker(medewerker);
        werktijd1.setStart(beginTijd.toLocalTime());
        werktijd1.setEind(eindTijd.toLocalTime());
        werktijd1.setDag(WEDNESDAY);

        var tijdstip1 = new Tijdstip();
        tijdstip1.setTijd(LocalTime.of(9, 0));
        var tijdstip2 = new Tijdstip();
        tijdstip2.setTijd(LocalTime.of(9, 30));
        var tijdstip3 = new Tijdstip();
        tijdstip3.setTijd(LocalTime.of(10, 0));
        var tijdstip4 = new Tijdstip();
        tijdstip4.setTijd(LocalTime.of(10, 30));

        dag.setDatum(beginTijd.toLocalDate());
        dag.setTijdstippen(newArrayList(tijdstip1, tijdstip2));

        when(werktijdRepository.werktijdenBijMedewerker(newArrayList(medewerker))).thenReturn(newArrayList(werktijd1));

        Set<Dag> result = bepaalMedewerkerIdsBijWerktijdenService.bepaalMedewerkerIdsVoorWerktijden(salon, newHashSet(dag));

        Mockito.verifyNoMoreInteractions(werktijdRepository);

        assertEquals(1, result.size());
        List<Dag> resultList = newArrayList(result);
        assertEquals(2, resultList.get(0).getTijdstippen().size());
        List<Tijdstip> tijdstips = newArrayList(resultList.get(0).getTijdstippen());
        assertEquals(1, tijdstips.get(0).getMedewekerIds().size());
        assertEquals(4L, tijdstips.get(0).getMedewekerIds().get(0));
    }
}
