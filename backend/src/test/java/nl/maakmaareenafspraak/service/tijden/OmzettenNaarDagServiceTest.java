package nl.maakmaareenafspraak.service.tijden;

import nl.maakmaareenafspraak.web.domain.Dag;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.util.Comparator;
import java.util.List;

import static com.google.common.collect.Lists.newArrayList;
import static java.util.stream.Collectors.toList;
import static org.junit.jupiter.api.Assertions.assertEquals;

@ExtendWith(MockitoExtension.class)
class OmzettenNaarDagServiceTest {
    private OmzettenNaarDagService omzettenNaarDagService = new OmzettenNaarDagService();

    @Test
    void omzettenNaarDagService() {
        var tijd1 = LocalDateTime.of(2021, 5, 14, 9, 0);
        var tijd2 = LocalDateTime.of(2021, 5, 14, 9, 15);
        var tijd3 = LocalDateTime.of(2021, 5, 15, 9, 0);

        List<LocalDateTime> tijden = newArrayList(tijd1, tijd2, tijd3);

        List<Dag> result = omzettenNaarDagService.omzettenNaarDagService(tijden).stream().sorted(Comparator.comparing(Dag::getDatum)).collect(toList());
        assertEquals(2, result.size());
        assertEquals(LocalDate.of(2021, 5, 14), result.get(0).getDatum());
        assertEquals(2, result.get(0).getTijdstippen().size());
        assertEquals(LocalTime.of(9, 0), result.get(0).getTijdstippen().get(0).getTijd());
        assertEquals(LocalTime.of(9, 15), result.get(0).getTijdstippen().get(1).getTijd());
        assertEquals(LocalDate.of(2021, 5, 15), result.get(1).getDatum());
        assertEquals(1, result.get(1).getTijdstippen().size());
        assertEquals(LocalTime.of(9, 0), result.get(1).getTijdstippen().get(0).getTijd());
    }
}