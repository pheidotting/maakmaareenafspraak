package nl.maakmaareenafspraak.service.tijden;

import nl.maakmaareenafspraak.domain.Openingstijd;
import nl.maakmaareenafspraak.domain.Salon;
import nl.maakmaareenafspraak.repository.OpeningstijdRepository;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.time.DayOfWeek;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.util.List;

import static com.google.common.collect.Lists.newArrayList;
import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class GenereerTijdenServiceTest {
    @Mock
    private OpeningstijdRepository openingstijdRepository;

    @InjectMocks
    private GenereerTijdenService genereerTijdenService = new GenereerTijdenService();

    @Test
    @DisplayName("Begintijd 10:00, eindtijd 12:00, 1 dag, 8 tijdstippen")
    void genereerTijden() {
        var salon = new Salon();
        var beginDatum = LocalDate.of(2021, 5, 5);
        var eindDatum = LocalDate.of(2021, 5, 5);

        var openingstijd = new Openingstijd();
        openingstijd.setStart(LocalTime.of(10, 0));
        openingstijd.setEind(LocalTime.of(12, 0));
        openingstijd.setDag(DayOfWeek.WEDNESDAY);

        when(openingstijdRepository.openingstijdenBijSalon(salon)).thenReturn(newArrayList(openingstijd));

        List<LocalDateTime> result = genereerTijdenService.genereerTijden(salon, beginDatum, eindDatum);

        assertThat(result.size()).isEqualTo(8);
        assertThat(result.get(0)).isEqualTo(LocalDateTime.of(2021, 5, 5, 10, 0));
        assertThat(result.get(1)).isEqualTo(LocalDateTime.of(2021, 5, 5, 10, 15));
        assertThat(result.get(2)).isEqualTo(LocalDateTime.of(2021, 5, 5, 10, 30));
        assertThat(result.get(3)).isEqualTo(LocalDateTime.of(2021, 5, 5, 10, 45));
        assertThat(result.get(4)).isEqualTo(LocalDateTime.of(2021, 5, 5, 11, 0));
        assertThat(result.get(5)).isEqualTo(LocalDateTime.of(2021, 5, 5, 11, 15));
        assertThat(result.get(6)).isEqualTo(LocalDateTime.of(2021, 5, 5, 11, 30));
        assertThat(result.get(7)).isEqualTo(LocalDateTime.of(2021, 5, 5, 11, 45));
    }

    @Test
    @DisplayName("2 dagen, 11 tijdstippen")
    void genereerTijdenTweeDagen() {
        var salon = new Salon();
        var beginDatum = LocalDate.of(2021, 5, 5);
        var eindDatum = LocalDate.of(2021, 5, 6);

        var openingstijdWoensdag = new Openingstijd();
        openingstijdWoensdag.setStart(LocalTime.of(10, 0));
        openingstijdWoensdag.setEind(LocalTime.of(11, 0));
        openingstijdWoensdag.setDag(DayOfWeek.WEDNESDAY);
        var openingstijdDonderdagMiddag = new Openingstijd();
        openingstijdDonderdagMiddag.setStart(LocalTime.of(13, 0));
        openingstijdDonderdagMiddag.setEind(LocalTime.of(13, 45));
        openingstijdDonderdagMiddag.setDag(DayOfWeek.THURSDAY);
        var openingstijdDonderdagAvond = new Openingstijd();
        openingstijdDonderdagAvond.setStart(LocalTime.of(18, 0));
        openingstijdDonderdagAvond.setEind(LocalTime.of(19, 15));
        openingstijdDonderdagAvond.setDag(DayOfWeek.THURSDAY);

        when(openingstijdRepository.openingstijdenBijSalon(salon)).thenReturn(newArrayList(openingstijdWoensdag, openingstijdDonderdagMiddag, openingstijdDonderdagAvond));

        List<LocalDateTime> result = genereerTijdenService.genereerTijden(salon, beginDatum, eindDatum);

        assertThat(result.size()).isEqualTo(12);
        assertThat(result.get(0)).isEqualTo(LocalDateTime.of(2021, 5, 5, 10, 0));
        assertThat(result.get(1)).isEqualTo(LocalDateTime.of(2021, 5, 5, 10, 15));
        assertThat(result.get(2)).isEqualTo(LocalDateTime.of(2021, 5, 5, 10, 30));
        assertThat(result.get(3)).isEqualTo(LocalDateTime.of(2021, 5, 5, 10, 45));
        assertThat(result.get(4)).isEqualTo(LocalDateTime.of(2021, 5, 6, 13, 0));
        assertThat(result.get(5)).isEqualTo(LocalDateTime.of(2021, 5, 6, 13, 15));
        assertThat(result.get(6)).isEqualTo(LocalDateTime.of(2021, 5, 6, 13, 30));
        assertThat(result.get(7)).isEqualTo(LocalDateTime.of(2021, 5, 6, 18, 0));
        assertThat(result.get(8)).isEqualTo(LocalDateTime.of(2021, 5, 6, 18, 15));
        assertThat(result.get(9)).isEqualTo(LocalDateTime.of(2021, 5, 6, 18, 30));
        assertThat(result.get(10)).isEqualTo(LocalDateTime.of(2021, 5, 6, 18, 45));
        assertThat(result.get(11)).isEqualTo(LocalDateTime.of(2021, 5, 6, 19, 0));
    }
}