package nl.maakmaareenafspraak.service.tijden;

import nl.maakmaareenafspraak.domain.ExtraWerktijd;
import nl.maakmaareenafspraak.domain.Medewerker;
import nl.maakmaareenafspraak.domain.Salon;
import nl.maakmaareenafspraak.repository.ExtraWerktijdRepository;
import nl.maakmaareenafspraak.web.domain.Dag;
import nl.maakmaareenafspraak.web.domain.Tijdstip;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import java.time.LocalDateTime;
import java.time.LocalTime;
import java.util.List;
import java.util.Set;

import static com.google.common.collect.Lists.newArrayList;
import static com.google.common.collect.Sets.newHashSet;
import static org.junit.jupiter.api.Assertions.assertEquals;

@ExtendWith(MockitoExtension.class)
class BepaalMedewerkerIdsBijExtraWerktijdenServiceTest {

    @Mock
    private ExtraWerktijdRepository extraWerktijdRepository;

    @InjectMocks
    private BepaalMedewerkerIdsBijExtraWerktijdenService bepaalMedewerkerIdsBijExtraWerktijdenService = new BepaalMedewerkerIdsBijExtraWerktijdenService();

    @Test
    @DisplayName("Begintijd 10:00, eindtijd 12:00, 1 dag, 8 tijdstippen")
    void bepaalMedewerkerIdsVoorWerktijden() {
        var beginTijd = LocalDateTime.of(2021, 5, 5, 9, 0);
        var eindTijd = LocalDateTime.of(2021, 5, 6, 17, 0);

        var salon = new Salon();
        var medewerker1 = new Medewerker();
        medewerker1.setId(1L);
        medewerker1.setNaam("naam1");
        var medewerker2 = new Medewerker();
        medewerker2.setId(2L);
        medewerker2.setNaam("naam2");
        salon.getMedewerkers().add(medewerker1);
        salon.getMedewerkers().add(medewerker2);

        var tijdstip1 = new Tijdstip();
        tijdstip1.setTijd(LocalTime.of(9, 0));
        var tijdstip2 = new Tijdstip();
        tijdstip2.setTijd(LocalTime.of(9, 30));
        var tijdstip3 = new Tijdstip();
        tijdstip3.setTijd(LocalTime.of(10, 0));
        var tijdstip4 = new Tijdstip();
        tijdstip4.setTijd(LocalTime.of(10, 30));

        var dag1 = new Dag();
        dag1.setDatum(beginTijd.toLocalDate());
        dag1.setTijdstippen(newArrayList(tijdstip1, tijdstip2));
        var dag2 = new Dag();
        dag2.setDatum(beginTijd.toLocalDate());
        dag2.setTijdstippen(newArrayList(tijdstip3, tijdstip4));

        Mockito.when(extraWerktijdRepository.extraWerktijdenBijMedewerker(newArrayList(medewerker2, medewerker1), beginTijd, eindTijd)).thenReturn(extraWerktijden(medewerker1, medewerker2));

        Set<Dag> result = bepaalMedewerkerIdsBijExtraWerktijdenService.bepaalMedewerkerIdsVoorWerktijden(salon, newHashSet(dag1, dag2), beginTijd, eindTijd);

        Mockito.verifyNoMoreInteractions(extraWerktijdRepository);

        assertEquals(2, result.size());
        List<Dag> resultList = newArrayList(result);
        assertEquals(2, resultList.get(0).getTijdstippen().size());
        assertEquals(1, resultList.get(0).getTijdstippen().get(0).getMedewekerIds().size());
        assertEquals(1, resultList.get(0).getTijdstippen().get(1).getMedewekerIds().size());
        assertEquals(2, resultList.get(1).getTijdstippen().size());
        assertEquals(1, resultList.get(1).getTijdstippen().get(0).getMedewekerIds().size());
        assertEquals(1, resultList.get(1).getTijdstippen().get(1).getMedewekerIds().size());
    }

    public List<ExtraWerktijd> extraWerktijden(Medewerker medewerker1, Medewerker medewerker2) {
        ExtraWerktijd extraWerktijd1 = extraWerktijd(medewerker1, LocalDateTime.of(2021, 5, 5, 9, 0), LocalDateTime.of(2021, 5, 5, 9, 30));
        ExtraWerktijd extraWerktijd2 = extraWerktijd(medewerker2, LocalDateTime.of(2021, 5, 5, 10, 0), LocalDateTime.of(2021, 5, 5, 11, 0));

        return newArrayList(extraWerktijd1, extraWerktijd2);
    }

    private ExtraWerktijd extraWerktijd(Medewerker medewerker, LocalDateTime start, LocalDateTime eind) {
        ExtraWerktijd extraWerktijd = new ExtraWerktijd();
        extraWerktijd.setMedewerker(medewerker);
        extraWerktijd.setStart(start);
        extraWerktijd.setEind(eind);
        return extraWerktijd;
    }
}