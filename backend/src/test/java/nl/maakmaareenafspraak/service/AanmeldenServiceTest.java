package nl.maakmaareenafspraak.service;

import nl.maakmaareenafspraak.domain.Beheerder;
import nl.maakmaareenafspraak.domain.Medewerker;
import nl.maakmaareenafspraak.domain.Salon;
import nl.maakmaareenafspraak.service.aanmelden.*;
import nl.maakmaareenafspraak.web.controller.aanmelden.Aanmelden;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.HashMap;
import java.util.Map;

import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
class AanmeldenServiceTest {
    @Mock
    private SalonOpslaanService salonOpslaanService;
    @Mock
    private MedewerkersOpslaanService medewerkersOpslaanService;
    @Mock
    private OpeningstijdOpslaanService openingstijdOpslaanService;
    @Mock
    private WerktijdenOpslaanService werktijdenOpslaanService;
    @Mock
    private BehandelingenOpslaanService behandelingenOpslaanService;
    @Mock
    private MailStuurService mailStuurService;

    @InjectMocks
    private AanmeldenService aanmeldenService = new AanmeldenService();

    @Test
    void test() {
        var aanmelden = new Aanmelden();
        var salon = new Salon();
        var beheerder = new Beheerder();
        var medewerker = new Medewerker();
        salon.getMedewerkers().add(medewerker);
        salon.getMedewerkers().add(beheerder);
        Map<String, Medewerker> IDENMEDEWEKER = new HashMap<>();

        when(salonOpslaanService.salonOpslaan(aanmelden)).thenReturn(salon);
        when(medewerkersOpslaanService.opslaanMedewerkers(aanmelden, salon)).thenReturn(IDENMEDEWEKER);

        aanmeldenService.aanmelden(aanmelden);

        verify(openingstijdOpslaanService, times(1)).opslaanOpeningstijden(aanmelden, salon);
        verify(werktijdenOpslaanService, times(1)).opslaanWerktijden(aanmelden, IDENMEDEWEKER);
        verify(behandelingenOpslaanService, times(1)).opslaanBehandelingen(aanmelden, salon, IDENMEDEWEKER);
//        verify(mailStuurService, times(1)).verstuurWachtwoordInstelMail(salon, beheerder);
    }
}