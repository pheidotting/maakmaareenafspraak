package nl.maakmaareenafspraak.service;

import nl.maakmaareenafspraak.domain.*;
import nl.maakmaareenafspraak.repository.AfspraakRepository;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.time.DayOfWeek;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.util.Map;

import static com.google.common.collect.Lists.newArrayList;
import static com.google.common.collect.Sets.newHashSet;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
class AfspraakServiceTest {
    @Mock
    private AfspraakRepository afspraakRepository;
    @Mock
    private KlantService klantService;
    @Mock
    private SalonService salonService;
    @Mock
    private BehandelingService behandelingService;
    @Mock
    private AfspraakCheckService afspraakCheckService;
    @Mock
    private OpeningstijdService openingstijdService;

    @InjectMocks
    private AfspraakService afspraakService = new AfspraakService();

    @Test
    @DisplayName("Test het opslaan van een Afspraak met een niet ingelogde Klant")
    void opslaanNietIngelogdeKlant() {
        Long salonId = 3L;
        String opmerking = "Oppmerkinggg";
        String email = "aa@bb.cc";
        String naamklant = "naam klant";
        String tel = "telefoon";
        LocalDateTime tijdstip = LocalDateTime.now();
        Long behandeling1Id = 4L;
        Long behandeling2Id = 5L;

        NietIngelogdeKlant klant = new NietIngelogdeKlant();
        klant.setEmailadres(email);
        klant.setNaam(naamklant);
        klant.setTelefoonnummer(tel);
        NietIngelogdeKlant klantMetId = klant;
        klantMetId.setId(2L);

        Salon salon = new Salon();
        salon.setId(salonId);
        salon.setNaam("Naam Salon");

        Behandeling behandeling1 = new Behandeling();
        Behandeling behandeling2 = new Behandeling();

        Afspraak afspraak = new Afspraak(null, tijdstip, opmerking, klant, salon, null, newHashSet(behandeling1, behandeling2));
        Afspraak afspraakMetId = new Afspraak(1L, tijdstip, opmerking, klant, salon, null, newHashSet(behandeling1, behandeling2));

        doNothing().when(klantService).opslaan(klant);
        when(salonService.lees(salonId)).thenReturn(salon);
        when(afspraakRepository.save(afspraak)).thenReturn(afspraakMetId);
        when(behandelingService.lees(behandeling1Id)).thenReturn(behandeling1);
        when(behandelingService.lees(behandeling2Id)).thenReturn(behandeling2);
        when(afspraakCheckService.isDeTeMakenAfspraakCorrect(afspraak)).thenReturn(true);

        afspraakService.afspraakOpslaan(tijdstip, opmerking, naamklant, email, tel, salonId, null, newArrayList(behandeling1Id, behandeling2Id));

        verifyNoMoreInteractions(afspraakRepository, klantService, salonService, behandelingService, afspraakCheckService);
    }
    @Test
    @DisplayName("Test het opslaan van een Afspraak met een be Klant")
    void opslaanMetIngelogdeKlant() {
        Long salonId = 3L;
        Long klantId = 2L;
        String opmerking = "Oppmerkinggg";
        String email = "aa@bb.cc";
        String naamklant = "naam klant";
        String tel = "telefoon";
        LocalDateTime tijdstip = LocalDateTime.now();
        Long behandeling1Id = 4L;
        Long behandeling2Id = 5L;

        Klant klant = new Klant();
        klant.setEmailadres(email);
        klant.setNaam(naamklant);
        klant.setTelefoonnummer(tel);
        klant.setId(klantId);

        Salon salon = new Salon();
        salon.setId(salonId);
        salon.setNaam("Naam Salon");

        Behandeling behandeling1 = new Behandeling();
        Behandeling behandeling2 = new Behandeling();

        Afspraak afspraak = new Afspraak(null, tijdstip, opmerking, klant, salon, null, newHashSet(behandeling1, behandeling2));
        Afspraak afspraakMetId = new Afspraak(1L, tijdstip, opmerking, klant, salon, null, newHashSet(behandeling1, behandeling2));

        when(klantService.lees(klantId)).thenReturn(klant);
        when(salonService.lees(salonId)).thenReturn(salon);
        when(afspraakRepository.save(afspraak)).thenReturn(afspraakMetId);
        when(behandelingService.lees(behandeling1Id)).thenReturn(behandeling1);
        when(behandelingService.lees(behandeling2Id)).thenReturn(behandeling2);
        when(afspraakCheckService.isDeTeMakenAfspraakCorrect(afspraak)).thenReturn(true);

        afspraakService.afspraakOpslaan(tijdstip, opmerking, klantId, salonId, null, newArrayList(behandeling1Id, behandeling2Id));

        verifyNoMoreInteractions(afspraakRepository, klantService, salonService, behandelingService, afspraakCheckService);
    }

    @Test
    @DisplayName("Test het ophalen van het gemiddelde aantal afspraken per dag")
    void testHaalGemiddeldAantalAfsprakenPerDag() {
        var salon = new Salon();
        var vandaag = LocalDate.of(2021, 5, 26);

        var openingstijd1 = new Openingstijd();
        openingstijd1.setDag(DayOfWeek.MONDAY);
        var openingstijd2 = new Openingstijd();
        openingstijd2.setDag(DayOfWeek.TUESDAY);
        var openingstijd3 = new Openingstijd();
        openingstijd3.setDag(DayOfWeek.WEDNESDAY);
        var openingstijd4 = new Openingstijd();
        openingstijd4.setDag(DayOfWeek.WEDNESDAY);
        var openingstijd5 = new Openingstijd();
        openingstijd5.setDag(DayOfWeek.THURSDAY);

        var afspraak1 = new Afspraak();
        var afspraak2 = new Afspraak();
        var afspraak3 = new Afspraak();

        when(openingstijdService.openingstijdenBijSalon(salon)).thenReturn(newArrayList(openingstijd1, openingstijd2, openingstijd3, openingstijd4, openingstijd5));

        var start = LocalTime.of(0, 0);
        var eind = LocalTime.of(23, 59);
        System.out.println(vandaag);
        when(afspraakRepository.haalAfsprakenOpBijSalonTussenTweeTijdstippen(salon, LocalDateTime.of(vandaag, start), LocalDateTime.of(vandaag, eind))).thenReturn(newArrayList(afspraak1));
        when(afspraakRepository.haalAfsprakenOpBijSalonTussenTweeTijdstippen(salon, LocalDateTime.of(vandaag.plusDays(1), start), LocalDateTime.of(vandaag.plusDays(1), eind))).thenReturn(newArrayList(afspraak1, afspraak2));
        when(afspraakRepository.haalAfsprakenOpBijSalonTussenTweeTijdstippen(salon, LocalDateTime.of(vandaag.plusDays(5), start), LocalDateTime.of(vandaag.plusDays(5), eind))).thenReturn(newArrayList(afspraak1, afspraak2, afspraak3));
        when(afspraakRepository.haalAfsprakenOpBijSalonTussenTweeTijdstippen(salon, LocalDateTime.of(vandaag.plusDays(6), start), LocalDateTime.of(vandaag.plusDays(6), eind))).thenReturn(newArrayList(afspraak1, afspraak2));
        when(afspraakRepository.haalAfsprakenOpBijSalonTussenTweeTijdstippen(salon, LocalDateTime.of(vandaag.plusDays(7), start), LocalDateTime.of(vandaag.plusDays(7), eind))).thenReturn(newArrayList(afspraak1));
        when(afspraakRepository.haalAfsprakenOpBijSalonTussenTweeTijdstippen(salon, LocalDateTime.of(vandaag.plusDays(8), start), LocalDateTime.of(vandaag.plusDays(8), eind))).thenReturn(newArrayList(afspraak1, afspraak2));
        when(afspraakRepository.haalAfsprakenOpBijSalonTussenTweeTijdstippen(salon, LocalDateTime.of(vandaag.plusDays(12), start), LocalDateTime.of(vandaag.plusDays(12), eind))).thenReturn(newArrayList(afspraak1, afspraak2, afspraak3));

        Map<LocalDate, Long> result = afspraakService.haalAantalAfsprakenPerDag(salon, vandaag);

        assertEquals(7, result.size());
        assertNotNull(result.get(vandaag));
        assertEquals(1L, result.get(vandaag));
        assertNotNull(result.get(vandaag.plusDays(1)));
        assertEquals(2L, result.get(vandaag.plusDays(1)));
        assertNotNull(result.get(vandaag.plusDays(5)));
        assertEquals(3L, result.get(vandaag.plusDays(5)));
        assertNotNull(result.get(vandaag.plusDays(6)));
        assertEquals(2L, result.get(vandaag.plusDays(6)));
        assertNotNull(result.get(vandaag.plusDays(7)));
        assertEquals(1L, result.get(vandaag.plusDays(7)));
        assertNotNull(result.get(vandaag.plusDays(8)));
        assertEquals(2L, result.get(vandaag.plusDays(8)));
        assertNotNull(result.get(vandaag.plusDays(12)));
        assertEquals(3L, result.get(vandaag.plusDays(12)));

        verifyNoMoreInteractions(afspraakRepository, klantService, salonService, behandelingService, afspraakCheckService, openingstijdService);
    }

}