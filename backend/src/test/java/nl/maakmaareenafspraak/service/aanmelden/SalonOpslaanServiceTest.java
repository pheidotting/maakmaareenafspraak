package nl.maakmaareenafspraak.service.aanmelden;

import nl.maakmaareenafspraak.domain.Salon;
import nl.maakmaareenafspraak.service.SalonService;
import nl.maakmaareenafspraak.web.controller.aanmelden.Aanmelden;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.*;
import org.mockito.junit.jupiter.MockitoExtension;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNull;

@ExtendWith(MockitoExtension.class)
class SalonOpslaanServiceTest {
    @Mock
    private SalonService salonService;

    @Captor
    ArgumentCaptor<Salon> salonCaptor;

    @InjectMocks
    private SalonOpslaanService salonOpslaanService = new SalonOpslaanService();

    @Test
    void salonOpslaan() {
        var NAAM_SALON = "Los Pollos Hermanos";
        var EMAIL_SALON = "gus@lospolloshermanos.com";
        var ADRES_SALON = "Boogschutter 26";
        var POSTCODE_SALON = "7891TN";
        var PLAATS_SALON = "Klazienaveen";

        var aanmelden = new Aanmelden();
        aanmelden.setNaamSalon(NAAM_SALON);
        aanmelden.setEmail(EMAIL_SALON);
        aanmelden.setAdres(ADRES_SALON);
        aanmelden.setPostcode(POSTCODE_SALON);
        aanmelden.setPlaats(PLAATS_SALON);

        salonOpslaanService.salonOpslaan(aanmelden);

        Mockito.verify(salonService).opslaan(salonCaptor.capture());
        var salon = salonCaptor.getValue();
        assertEquals(NAAM_SALON, salon.getNaam());
        assertEquals(EMAIL_SALON, salon.getEmailadres());
        assertEquals(ADRES_SALON, salon.getAdres().getStraat());
        assertEquals(POSTCODE_SALON, salon.getAdres().getPostcode());
        assertEquals(PLAATS_SALON, salon.getAdres().getPlaats());
    }

    @Test
    @DisplayName("Test dat er niet een leeg adres wordt opgeslagen als dat niet wordt ingegeven")
    void aanmeldenLeegAdres() {
        var NAAM_SALON = "Los Pollos Hermanos";
        var EMAIL_SALON = "gus@lospolloshermanos.com";

        var aanmelden = new Aanmelden();
        aanmelden.setNaamSalon(NAAM_SALON);
        aanmelden.setEmail(EMAIL_SALON);

        salonOpslaanService.salonOpslaan(aanmelden);

        Mockito.verify(salonService).opslaan(salonCaptor.capture());
        var salon = salonCaptor.getValue();
        assertEquals(NAAM_SALON, salon.getNaam());
        assertEquals(EMAIL_SALON, salon.getEmailadres());
        assertNull(salon.getAdres());
    }
}