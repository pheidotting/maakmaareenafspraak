package nl.maakmaareenafspraak.service.aanmelden;

import nl.maakmaareenafspraak.domain.Medewerker;
import nl.maakmaareenafspraak.domain.Werktijd;
import nl.maakmaareenafspraak.service.WerktijdService;
import nl.maakmaareenafspraak.web.controller.aanmelden.Aanmelden;
import nl.maakmaareenafspraak.web.controller.aanmelden.Dag;
import nl.maakmaareenafspraak.web.controller.aanmelden.DagMetTijd;
import nl.maakmaareenafspraak.web.controller.aanmelden.Tijd;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.*;
import org.mockito.junit.jupiter.MockitoExtension;

import java.time.LocalTime;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static com.google.common.collect.Lists.newArrayList;
import static java.time.DayOfWeek.TUESDAY;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.times;

@ExtendWith(MockitoExtension.class)
class WerktijdenOpslaanServiceTest {
    @Mock
    private WerktijdService werktijdService;

    @Captor
    ArgumentCaptor<Werktijd> werktijdCaptor;

    @InjectMocks
    private WerktijdenOpslaanService werktijdenOpslaanService = new WerktijdenOpslaanService();

    @Test
    void test() {
        var NAAM_MW_1 = "Gus Fring";
        var NAAM_MW_2 = "Heisenberg";

        Map<String, Medewerker> IDENMEDEWEKER = new HashMap<>();

        var medewerker1 = new nl.maakmaareenafspraak.domain.Medewerker();
        medewerker1.setNaam(NAAM_MW_1);
        var medewerker2 = new nl.maakmaareenafspraak.domain.Medewerker();
        medewerker2.setNaam(NAAM_MW_2);
        IDENMEDEWEKER.put("AA", medewerker1);
        IDENMEDEWEKER.put("BB", medewerker2);

        var medewerkerBijTIjd1Aanwezig = new nl.maakmaareenafspraak.web.controller.aanmelden.Medewerker();
        medewerkerBijTIjd1Aanwezig.setId("AA");
        medewerkerBijTIjd1Aanwezig.setAanwezig(true);
        var medewerkerBijTIjd1Afwezig = new nl.maakmaareenafspraak.web.controller.aanmelden.Medewerker();
        medewerkerBijTIjd1Afwezig.setId("AA");
        medewerkerBijTIjd1Afwezig.setAanwezig(false);
        var medewerkerBijTIjd2Aanwezig = new nl.maakmaareenafspraak.web.controller.aanmelden.Medewerker();
        medewerkerBijTIjd2Aanwezig.setId("BB");
        medewerkerBijTIjd2Aanwezig.setAanwezig(true);
        var medewerkerBijTIjd2Afwezig = new nl.maakmaareenafspraak.web.controller.aanmelden.Medewerker();
        medewerkerBijTIjd2Afwezig.setId("BB");
        medewerkerBijTIjd2Afwezig.setAanwezig(false);

        var aanmelden = new Aanmelden();
        var dag = new Dag();
        dag.setCode("TUESDAY");
        var dagMetTijd = new DagMetTijd();
        dagMetTijd.setDag(dag);
        var tijd = new Tijd();
        tijd.setStart(LocalTime.of(9, 0));
        tijd.setEind(LocalTime.of(10, 0));
        tijd.setMedewerkers(newArrayList(medewerkerBijTIjd1Aanwezig, medewerkerBijTIjd2Afwezig));
        dagMetTijd.getTijden().add(tijd);
        var tijd2 = new Tijd();
        tijd2.setStart(LocalTime.of(11, 0));
        tijd2.setEind(LocalTime.of(12, 0));
        tijd2.setMedewerkers(newArrayList(medewerkerBijTIjd1Afwezig, medewerkerBijTIjd2Aanwezig));
        dagMetTijd.getTijden().add(tijd2);

        aanmelden.getDagenMetTijden().add(dagMetTijd);

        werktijdenOpslaanService.opslaanWerktijden(aanmelden, IDENMEDEWEKER);

        Mockito.verify(werktijdService, times(2)).opslaan(werktijdCaptor.capture());
        List<Werktijd> werktijden = werktijdCaptor.getAllValues();
        var werktijd1 = werktijden.get(0);
        assertEquals(TUESDAY, werktijd1.getDag());
        assertEquals(LocalTime.of(9, 0), werktijd1.getStart());
        assertEquals(LocalTime.of(10, 0), werktijd1.getEind());
        assertEquals(NAAM_MW_1, werktijd1.getMedewerker().getNaam());
        var werktijd2 = werktijden.get(1);
        assertEquals(TUESDAY, werktijd2.getDag());
        assertEquals(LocalTime.of(11, 0), werktijd2.getStart());
        assertEquals(LocalTime.of(12, 0), werktijd2.getEind());
        assertEquals(NAAM_MW_2, werktijd2.getMedewerker().getNaam());
    }
}