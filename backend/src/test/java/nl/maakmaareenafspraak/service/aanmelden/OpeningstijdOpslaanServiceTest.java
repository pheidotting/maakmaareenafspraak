package nl.maakmaareenafspraak.service.aanmelden;

import nl.maakmaareenafspraak.domain.Openingstijd;
import nl.maakmaareenafspraak.domain.Salon;
import nl.maakmaareenafspraak.service.OpeningstijdService;
import nl.maakmaareenafspraak.web.controller.aanmelden.Aanmelden;
import nl.maakmaareenafspraak.web.controller.aanmelden.Dag;
import nl.maakmaareenafspraak.web.controller.aanmelden.DagMetTijd;
import nl.maakmaareenafspraak.web.controller.aanmelden.Tijd;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.*;
import org.mockito.junit.jupiter.MockitoExtension;

import java.time.LocalTime;
import java.util.List;

import static java.time.DayOfWeek.TUESDAY;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.times;

@ExtendWith(MockitoExtension.class)
class OpeningstijdOpslaanServiceTest {
    @Mock
    private OpeningstijdService openingstijdService;

    @Captor
    ArgumentCaptor<Openingstijd> openingstijdCaptor;

    @InjectMocks
    private OpeningstijdOpslaanService openingstijdOpslaanService = new OpeningstijdOpslaanService();

    @Test
    void test() {
        var SALON = new Salon();

        var aanmelden = new Aanmelden();
        var dag = new Dag();
        dag.setCode("TUESDAY");
        var dagMetTijd = new DagMetTijd();
        dagMetTijd.setDag(dag);
        var tijd = new Tijd();
        tijd.setStart(LocalTime.of(9, 0));
        tijd.setEind(LocalTime.of(10, 0));
        //        tijd.setMedewerkers(newArrayList(medewerker1));
        dagMetTijd.getTijden().add(tijd);
        var tijd2 = new Tijd();
        tijd2.setStart(LocalTime.of(11, 0));
        tijd2.setEind(LocalTime.of(12, 0));
        //        tijd2.setMedewerkers(newArrayList(medewerker2));
        dagMetTijd.getTijden().add(tijd2);

        aanmelden.getDagenMetTijden().add(dagMetTijd);

        openingstijdOpslaanService.opslaanOpeningstijden(aanmelden, SALON);

        Mockito.verify(openingstijdService, times(2)).opslaan(openingstijdCaptor.capture());
        List<Openingstijd> openingstijden = openingstijdCaptor.getAllValues();
        var openingstijd = openingstijden.get(0);
        assertEquals(TUESDAY, openingstijd.getDag());
        assertEquals(LocalTime.of(9, 0), openingstijd.getStart());
        assertEquals(LocalTime.of(10, 0), openingstijd.getEind());
        assertEquals(SALON, openingstijd.getSalon());
        var openingstijd2 = openingstijden.get(1);
        assertEquals(TUESDAY, openingstijd2.getDag());
        assertEquals(LocalTime.of(11, 0), openingstijd2.getStart());
        assertEquals(LocalTime.of(12, 0), openingstijd2.getEind());
        assertEquals(SALON, openingstijd2.getSalon());
    }

}