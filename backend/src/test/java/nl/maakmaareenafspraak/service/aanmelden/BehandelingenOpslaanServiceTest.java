package nl.maakmaareenafspraak.service.aanmelden;

import nl.maakmaareenafspraak.domain.Behandeling;
import nl.maakmaareenafspraak.domain.Categorie;
import nl.maakmaareenafspraak.domain.Medewerker;
import nl.maakmaareenafspraak.domain.Salon;
import nl.maakmaareenafspraak.service.BehandelingService;
import nl.maakmaareenafspraak.service.CategorieService;
import nl.maakmaareenafspraak.service.MedewerkerService;
import nl.maakmaareenafspraak.web.controller.aanmelden.Aanmelden;
import nl.maakmaareenafspraak.web.controller.aanmelden.BehandelingHolder;
import nl.maakmaareenafspraak.web.controller.aanmelden.TreeNode;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.*;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static com.google.common.collect.Lists.newArrayList;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.times;

@ExtendWith(MockitoExtension.class)
class BehandelingenOpslaanServiceTest {
    @Mock
    private CategorieService categorieService;
    @Mock
    private BehandelingService behandelingService;
    @Mock
    private MedewerkerService medewerkerService;

    @Captor
    ArgumentCaptor<Categorie> categorieCaptor;
    @Captor
    ArgumentCaptor<Behandeling> behandelingCaptor;
    @Captor
    ArgumentCaptor<Medewerker> medewerkerCaptor;

    @InjectMocks
    private BehandelingenOpslaanService behandelingenOpslaanService = new BehandelingenOpslaanService();

    @Test
    void test() {
        var NAAM_MW_1 = "Gus Fring";
        var NAAM_MW_2 = "Heisenberg";
        var SALON = new Salon();

        Map<String, Medewerker> IDENMEDEWEKER = new HashMap<>();

        var medewerker1 = new nl.maakmaareenafspraak.domain.Medewerker();
        medewerker1.setNaam(NAAM_MW_1);
        var medewerker2 = new nl.maakmaareenafspraak.domain.Medewerker();
        medewerker2.setNaam(NAAM_MW_2);
        IDENMEDEWEKER.put("AA", medewerker1);
        IDENMEDEWEKER.put("BB", medewerker2);

        var medewerkerBijBehandeling1Aanwezig = new nl.maakmaareenafspraak.web.controller.aanmelden.Medewerker();
        medewerkerBijBehandeling1Aanwezig.setId("AA");
        medewerkerBijBehandeling1Aanwezig.setAanwezig(true);
        var medewerkerBijBehandeling1Afwezig = new nl.maakmaareenafspraak.web.controller.aanmelden.Medewerker();
        medewerkerBijBehandeling1Afwezig.setId("AA");
        medewerkerBijBehandeling1Afwezig.setAanwezig(false);
        var medewerkerBijBehandeling2Aanwezig = new nl.maakmaareenafspraak.web.controller.aanmelden.Medewerker();
        medewerkerBijBehandeling2Aanwezig.setId("BB");
        medewerkerBijBehandeling2Aanwezig.setAanwezig(true);
        var medewerkerBijBehandeling2Afwezig = new nl.maakmaareenafspraak.web.controller.aanmelden.Medewerker();
        medewerkerBijBehandeling2Afwezig.setId("BB");
        medewerkerBijBehandeling2Afwezig.setAanwezig(false);

        var aanmelden = new Aanmelden();
        TreeNode treeNode1 = new TreeNode();
        var categorie1 = new nl.maakmaareenafspraak.web.controller.aanmelden.Categorie();
        categorie1.setNaam("Categorie1");
        categorie1.setOmschrijving("Omschrijving1");
        var behandeling1 = new nl.maakmaareenafspraak.web.controller.aanmelden.Behandeling();
        behandeling1.setNaam("Behandeling1");
        behandeling1.setTijdsduur(1L);
        behandeling1.setPrijs(10D);
        behandeling1.setMedewerkers(newArrayList(medewerkerBijBehandeling1Aanwezig, medewerkerBijBehandeling1Afwezig));
        var behandelingHolder1 = new BehandelingHolder();
        behandelingHolder1.setData(behandeling1);
        var behandeling2 = new nl.maakmaareenafspraak.web.controller.aanmelden.Behandeling();
        behandeling2.setNaam("Behandeling2");
        behandeling2.setTijdsduur(2L);
        behandeling2.setPrijs(20D);
        behandeling2.setMedewerkers(newArrayList(medewerkerBijBehandeling1Afwezig, medewerkerBijBehandeling2Aanwezig));
        var behandelingHolder2 = new BehandelingHolder();
        behandelingHolder2.setData(behandeling2);
        treeNode1.setData(categorie1);
        treeNode1.setChildren(newArrayList(behandelingHolder1, behandelingHolder2));
        aanmelden.setBehandelingen(newArrayList(treeNode1));

        behandelingenOpslaanService.opslaanBehandelingen(aanmelden, SALON, IDENMEDEWEKER);

        Mockito.verify(categorieService, times(1)).opslaan(categorieCaptor.capture());
        assertEquals("Categorie1", categorieCaptor.getValue().getNaam());
        assertEquals("Omschrijving1", categorieCaptor.getValue().getOmschrijving());

        Mockito.verify(behandelingService, times(2)).opslaan(behandelingCaptor.capture());
        List<Behandeling> behandelingen = behandelingCaptor.getAllValues();
        assertEquals("Behandeling1", behandelingen.get(0).getNaam());
        assertEquals(1L, behandelingen.get(0).getTijdsduur());
        assertEquals(10D, behandelingen.get(0).getPrijs());
        assertEquals("Behandeling2", behandelingen.get(1).getNaam());
        assertEquals(2L, behandelingen.get(1).getTijdsduur());
        assertEquals(20D, behandelingen.get(1).getPrijs());

        Mockito.verify(medewerkerService, times(2)).opslaan(medewerkerCaptor.capture());
        List<Medewerker> medewerkers = medewerkerCaptor.getAllValues();
        assertEquals(2, medewerkers.size());
        assertEquals(NAAM_MW_1, medewerkers.get(0).getNaam());
        assertEquals(1, medewerkers.get(0).getBehandelingen().size());
        assertEquals("Behandeling1", newArrayList(medewerkers.get(0).getBehandelingen()).get(0).getNaam());
        assertEquals(NAAM_MW_2, medewerkers.get(1).getNaam());
        assertEquals(1, medewerkers.get(1).getBehandelingen().size());
        assertEquals("Behandeling2", newArrayList(medewerkers.get(1).getBehandelingen()).get(0).getNaam());
    }
}