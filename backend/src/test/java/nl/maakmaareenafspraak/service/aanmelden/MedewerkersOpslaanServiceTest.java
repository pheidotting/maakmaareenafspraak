package nl.maakmaareenafspraak.service.aanmelden;

import nl.maakmaareenafspraak.domain.Medewerker;
import nl.maakmaareenafspraak.domain.Salon;
import nl.maakmaareenafspraak.service.MedewerkerService;
import nl.maakmaareenafspraak.web.controller.aanmelden.Aanmelden;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.*;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.List;
import java.util.Map;

import static com.google.common.collect.Lists.newArrayList;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.mockito.Mockito.times;

@ExtendWith(MockitoExtension.class)
class MedewerkersOpslaanServiceTest {
    @Mock
    private MedewerkerService medewerkerService;

    @Captor
    ArgumentCaptor<nl.maakmaareenafspraak.domain.Medewerker> medewerkerCaptor;

    @InjectMocks
    private MedewerkersOpslaanService medewerkersOpslaanService = new MedewerkersOpslaanService();

    @Test
    void test() {
        var NAAM_MW_1 = "Gus Fring";
        var NAAM_MW_2 = "Heisenberg";
        var SALON = new Salon();

        var aanmelden = new Aanmelden();
        var medewerker1 = new nl.maakmaareenafspraak.web.controller.aanmelden.Medewerker();
        medewerker1.setNaam(NAAM_MW_1);
        medewerker1.setId("AA");
        var medewerker2 = new nl.maakmaareenafspraak.web.controller.aanmelden.Medewerker();
        medewerker2.setNaam(NAAM_MW_2);
        medewerker2.setId("BB");
        aanmelden.setMedewerkers(newArrayList(medewerker1, medewerker2));

        Map<String, Medewerker> idEnMedeweker = medewerkersOpslaanService.opslaanMedewerkers(aanmelden, SALON);

        Mockito.verify(medewerkerService, times(2)).opslaan(medewerkerCaptor.capture());
        List<nl.maakmaareenafspraak.domain.Medewerker> medewerkers = medewerkerCaptor.getAllValues();
        assertEquals(NAAM_MW_1, medewerkers.get(0).getNaam());
        assertEquals(NAAM_MW_2, medewerkers.get(1).getNaam());

        assertEquals(2, idEnMedeweker.size());
        idEnMedeweker.keySet().stream().forEach(s -> assertNotNull(s));
    }
}