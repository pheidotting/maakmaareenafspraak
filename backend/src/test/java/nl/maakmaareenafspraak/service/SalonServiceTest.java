package nl.maakmaareenafspraak.service;

import nl.maakmaareenafspraak.domain.Adres;
import nl.maakmaareenafspraak.domain.Salon;
import nl.maakmaareenafspraak.repository.AdresRepository;
import nl.maakmaareenafspraak.repository.SalonRepository;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

@ExtendWith(MockitoExtension.class)
class SalonServiceTest {
    @Mock
    private AdresRepository adresRepository;
    @Mock
    private SalonRepository salonRepository;

    @InjectMocks
    private SalonService salonService = new SalonService();

    @Test
    @DisplayName("Test het opslaan van een Salon")
    void opslaan() {
        Salon salon = new Salon();


        salonService.opslaan(salon);

        Mockito.verify(salonRepository).save(salon);
    }

    @Test
    @DisplayName("Test het opslaan van een Salon met een adres")
    void metAdres() {
        Adres adres = new Adres();
        adres.setStraat("Straatnaam");
        Salon salon = new Salon();
        salon.setAdres(adres);


        salonService.opslaan(salon);

        Mockito.verify(salonRepository).save(salon);
    }
}