package nl.maakmaareenafspraak.service.checks;

import nl.maakmaareenafspraak.domain.Afspraak;
import nl.maakmaareenafspraak.domain.Behandeling;
import nl.maakmaareenafspraak.domain.Klant;
import nl.maakmaareenafspraak.service.AfspraakService;
import nl.maakmaareenafspraak.service.KlantService;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.time.LocalDateTime;

import static com.google.common.collect.Lists.newArrayList;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.Mockito.verifyNoMoreInteractions;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class HeeftDeKlantOpDatzelfdeTijdstipAlEenAfspraakCheckTest {

    @Mock
    private KlantService klantService;
    @Mock
    private AfspraakService afspraakService;

    @InjectMocks
    private HeeftDeKlantOpDatzelfdeTijdstipAlEenAfspraakCheck heeftDeKlantOpDatzelfdeTijdstipAlEenAfspraakCheck = new HeeftDeKlantOpDatzelfdeTijdstipAlEenAfspraakCheck();

    @Test
    void isCorrect() {
        var afspraakTeMaken = new Afspraak();
        var klant = new Klant();
        klant.setId(1L);
        afspraakTeMaken.setKlant(klant);
        afspraakTeMaken.setTijdstip(LocalDateTime.of(2021, 5, 2, 10, 15));
        var behandeling = new Behandeling();
        behandeling.setTijdsduur(2L);
        afspraakTeMaken.getBehandelingen().add(behandeling);

        var afspraak1 = new Afspraak();
        afspraak1.getBehandelingen().add(behandeling);
        afspraak1.setTijdstip(LocalDateTime.of(2021, 5, 2, 11, 0));
        var afspraak2 = new Afspraak();
        afspraak2.getBehandelingen().add(behandeling);
        afspraak2.setTijdstip(LocalDateTime.of(2021, 5, 2, 9, 0));

        when(klantService.lees(1L)).thenReturn(klant);
        when(afspraakService.haalAfsprakenOpBijEenKlant(klant)).thenReturn(newArrayList(afspraak1, afspraak2));

        assertTrue(heeftDeKlantOpDatzelfdeTijdstipAlEenAfspraakCheck.isCorrect(afspraakTeMaken));

        verifyNoMoreInteractions(klantService, afspraakService);
    }

    @Test
    void isNietCorrect() {
        var afspraakTeMaken = new Afspraak();
        var klant = new Klant();
        klant.setId(1L);
        afspraakTeMaken.setKlant(klant);
        afspraakTeMaken.setTijdstip(LocalDateTime.of(2021, 5, 2, 10, 15));
        var behandeling = new Behandeling();
        behandeling.setTijdsduur(2L);
        afspraakTeMaken.getBehandelingen().add(behandeling);

        var afspraak1 = new Afspraak();
        afspraak1.getBehandelingen().add(behandeling);
        afspraak1.setTijdstip(LocalDateTime.of(2021, 5, 2, 10, 30));
        var afspraak2 = new Afspraak();
        afspraak2.getBehandelingen().add(behandeling);
        afspraak2.setTijdstip(LocalDateTime.of(2021, 5, 2, 9, 0));

        when(klantService.lees(1L)).thenReturn(klant);
        when(afspraakService.haalAfsprakenOpBijEenKlant(klant)).thenReturn(newArrayList(afspraak1, afspraak2));

        assertFalse(heeftDeKlantOpDatzelfdeTijdstipAlEenAfspraakCheck.isCorrect(afspraakTeMaken));

        verifyNoMoreInteractions(klantService, afspraakService);
    }
}