package nl.maakmaareenafspraak.service.checks;

import nl.maakmaareenafspraak.AfspraakBuilder;
import nl.maakmaareenafspraak.domain.Afspraak;
import nl.maakmaareenafspraak.domain.Behandeling;
import nl.maakmaareenafspraak.domain.Medewerker;
import nl.maakmaareenafspraak.domain.Salon;
import nl.maakmaareenafspraak.service.TijdenService;
import nl.maakmaareenafspraak.web.domain.Dag;
import nl.maakmaareenafspraak.web.domain.Tijdstip;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.time.LocalDateTime;
import java.time.LocalTime;

import static com.google.common.collect.Lists.newArrayList;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.Mockito.verifyNoMoreInteractions;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class CheckTijdBeschikbaarServiceTest {
    @Mock
    private TijdenService tijdenService;

    @InjectMocks
    private CheckTijdBeschikbaarService checkTijdBeschikbaarService = new CheckTijdBeschikbaarService();

    @Test
    @DisplayName("Check 'false' als betreffende tijdstip niet (meer) beschikbaar is, True, want tijden genoeg")
    void checkNietMeerBeschikbaar() {
        Salon salon = new Salon();
        Behandeling behandeling1 = new Behandeling();
        behandeling1.setTijdsduur(1L);
        Behandeling behandeling2 = new Behandeling();
        behandeling2.setTijdsduur(2L);

        Afspraak afspraak = AfspraakBuilder.getInstance()//
                .withTijdstip(LocalDateTime.of(2021, 2, 1, 10, 0))//
                .withSalon(salon)//
                .withBehandeling(behandeling1)//
                .withBehandeling(behandeling2)//
                .build();

        var dag = new Dag();
        dag.setDatum(afspraak.getTijdstip().toLocalDate());
        Tijdstip tijdstip1 = new Tijdstip();
        tijdstip1.setTijd(LocalTime.of(9, 45));
        Tijdstip tijdstip2 = new Tijdstip();
        tijdstip2.setTijd(LocalTime.of(10, 0));
        tijdstip2.setAantalAfspraken(1);
        tijdstip2.setMedewekerIds(newArrayList(1L, 2L));
        Tijdstip tijdstip3 = new Tijdstip();
        tijdstip3.setTijd(LocalTime.of(10, 15));
        tijdstip3.setAantalAfspraken(1);
        tijdstip3.setMedewekerIds(newArrayList(1L, 2L));
        Tijdstip tijdstip4 = new Tijdstip();
        tijdstip4.setTijd(LocalTime.of(10, 30));
        tijdstip4.setAantalAfspraken(1);
        tijdstip4.setMedewekerIds(newArrayList(1L, 2L));
        Tijdstip tijdstip5 = new Tijdstip();
        tijdstip5.setTijd(LocalTime.of(10, 45));
        tijdstip5.setAantalAfspraken(1);
        tijdstip5.setMedewekerIds(newArrayList(1L, 2L));
        Tijdstip tijdstip6 = new Tijdstip();
        tijdstip6.setTijd(LocalTime.of(11, 0));
        dag.setTijdstippen(newArrayList(tijdstip1, tijdstip2, tijdstip3, tijdstip4, tijdstip5, tijdstip6));
        when(tijdenService.ophalenWerktijden(salon, afspraak.getTijdstip().toLocalDate(), afspraak.getTijdstip().toLocalDate())).thenReturn(newArrayList(dag));

        assertTrue(checkTijdBeschikbaarService.isCorrect(afspraak));

        verifyNoMoreInteractions(tijdenService);
    }

    @Test
    @DisplayName("Check 'false' als betreffende tijdstip niet (meer) beschikbaar is, True, want voorkeurmedewerker beschikbaar")
    void checkNietMeerBeschikbaarMetVoorkeur() {
        Salon salon = new Salon();
        Behandeling behandeling1 = new Behandeling();
        behandeling1.setTijdsduur(1L);
        Behandeling behandeling2 = new Behandeling();
        behandeling2.setTijdsduur(2L);
        Medewerker medewerker = new Medewerker();
        medewerker.setId(2L);

        Afspraak afspraak = AfspraakBuilder.getInstance()//
                .withTijdstip(LocalDateTime.of(2021, 2, 1, 10, 0))//
                .withSalon(salon)//
                .withBehandeling(behandeling1)//
                .withBehandeling(behandeling2)//
                .build();

        var dag = new Dag();
        dag.setDatum(afspraak.getTijdstip().toLocalDate());
        Tijdstip tijdstip1 = new Tijdstip();
        tijdstip1.setTijd(LocalTime.of(9, 45));
        Tijdstip tijdstip2 = new Tijdstip();
        tijdstip2.setTijd(LocalTime.of(10, 0));
        tijdstip2.setMedewekerIds(newArrayList(1L, 2L));
        Tijdstip tijdstip3 = new Tijdstip();
        tijdstip3.setTijd(LocalTime.of(10, 15));
        tijdstip3.setMedewekerIds(newArrayList(1L, 2L));
        Tijdstip tijdstip4 = new Tijdstip();
        tijdstip4.setTijd(LocalTime.of(10, 30));
        tijdstip4.setMedewekerIds(newArrayList(1L, 2L));
        Tijdstip tijdstip5 = new Tijdstip();
        tijdstip5.setTijd(LocalTime.of(10, 45));
        tijdstip5.setMedewekerIds(newArrayList(1L, 2L));
        Tijdstip tijdstip6 = new Tijdstip();
        tijdstip6.setTijd(LocalTime.of(11, 0));
        dag.setTijdstippen(newArrayList(tijdstip1, tijdstip2, tijdstip3, tijdstip4, tijdstip5, tijdstip6));
        when(tijdenService.ophalenWerktijden(salon, afspraak.getTijdstip().toLocalDate(), afspraak.getTijdstip().toLocalDate())).thenReturn(newArrayList(dag));

        assertTrue(checkTijdBeschikbaarService.isCorrect(afspraak));

        verifyNoMoreInteractions(tijdenService);
    }

    @Test
    @DisplayName("Check 'false' als betreffende tijdstip niet (meer) beschikbaar is, False, want voorkeur medewerker is bezet")
    void checkNietMeerBeschikbaarFalseVoorkeurMedewerker() {
        Salon salon = new Salon();
        Medewerker medewerker = new Medewerker();
        medewerker.setId(3L);
        Behandeling behandeling1 = new Behandeling();
        behandeling1.setTijdsduur(1L);
        Behandeling behandeling2 = new Behandeling();
        behandeling2.setTijdsduur(2L);

        Afspraak afspraak = AfspraakBuilder.getInstance()//
                .withTijdstip(LocalDateTime.of(2021, 2, 1, 10, 0))//
                .withSalon(salon)//
                .withMedewerker(medewerker)//
                .withBehandeling(behandeling1)//
                .withBehandeling(behandeling2)//
                .build();

        var dag = new Dag();
        dag.setDatum(afspraak.getTijdstip().toLocalDate());
        Tijdstip tijdstip1 = new Tijdstip();
        tijdstip1.setTijd(LocalTime.of(9, 45));
        Tijdstip tijdstip2 = new Tijdstip();
        tijdstip2.setTijd(LocalTime.of(10, 0));
        tijdstip2.setMedewekerIds(newArrayList(1L, 2L));
        Tijdstip tijdstip3 = new Tijdstip();
        tijdstip3.setTijd(LocalTime.of(10, 15));
        tijdstip3.setMedewekerIds(newArrayList(1L, 2L));
        Tijdstip tijdstip4 = new Tijdstip();
        tijdstip4.setTijd(LocalTime.of(10, 30));
        tijdstip4.setMedewekerIds(newArrayList(1L, 2L));
        Tijdstip tijdstip5 = new Tijdstip();
        tijdstip5.setTijd(LocalTime.of(10, 45));
        tijdstip5.setMedewekerIds(newArrayList(1L, 2L));
        Tijdstip tijdstip6 = new Tijdstip();
        tijdstip6.setTijd(LocalTime.of(11, 0));
        dag.setTijdstippen(newArrayList(tijdstip1, tijdstip2, tijdstip3, tijdstip4, tijdstip5, tijdstip6));
        when(tijdenService.ophalenWerktijden(salon, afspraak.getTijdstip().toLocalDate(), afspraak.getTijdstip().toLocalDate())).thenReturn(newArrayList(dag));

        assertFalse(checkTijdBeschikbaarService.isCorrect(afspraak));

        verifyNoMoreInteractions(tijdenService);
    }

    @Test
    @DisplayName("Check 'false' als betreffende tijdstip niet (meer) beschikbaar is, False, want 1 tijd is vergeven")
    void checkNietMeerBeschikbaarFalse() {
        Salon salon = new Salon();
        Behandeling behandeling1 = new Behandeling();
        behandeling1.setTijdsduur(1L);
        Behandeling behandeling2 = new Behandeling();
        behandeling2.setTijdsduur(2L);

        Afspraak afspraak = AfspraakBuilder.getInstance()//
                .withTijdstip(LocalDateTime.of(2021, 2, 1, 10, 0))//
                .withSalon(salon)//
                .withBehandeling(behandeling1)//
                .withBehandeling(behandeling2)//
                .build();

        var dag = new Dag();
        dag.setDatum(afspraak.getTijdstip().toLocalDate());
        Tijdstip tijdstip1 = new Tijdstip();
        tijdstip1.setTijd(LocalTime.of(9, 45));
        Tijdstip tijdstip2 = new Tijdstip();
        tijdstip2.setTijd(LocalTime.of(10, 0));
        tijdstip2.setAantalAfspraken(1);
        tijdstip2.setMedewekerIds(newArrayList(1L, 2L));
        Tijdstip tijdstip3 = new Tijdstip();
        tijdstip3.setTijd(LocalTime.of(10, 15));
        tijdstip3.setAantalAfspraken(1);
        tijdstip3.setMedewekerIds(newArrayList(1L, 2L));
        Tijdstip tijdstip4 = new Tijdstip();
        tijdstip4.setTijd(LocalTime.of(10, 30));
        tijdstip4.setAantalAfspraken(2);
        tijdstip4.setMedewekerIds(newArrayList(1L, 2L));
        Tijdstip tijdstip5 = new Tijdstip();
        tijdstip5.setTijd(LocalTime.of(10, 45));
        tijdstip5.setAantalAfspraken(1);
        tijdstip5.setMedewekerIds(newArrayList(1L, 2L));
        Tijdstip tijdstip6 = new Tijdstip();
        tijdstip6.setTijd(LocalTime.of(11, 0));
        dag.setTijdstippen(newArrayList(tijdstip1, tijdstip2, tijdstip3, tijdstip4, tijdstip5, tijdstip6));
        when(tijdenService.ophalenWerktijden(salon, afspraak.getTijdstip().toLocalDate(), afspraak.getTijdstip().toLocalDate())).thenReturn(newArrayList(dag));

        assertFalse(checkTijdBeschikbaarService.isCorrect(afspraak));

        verifyNoMoreInteractions(tijdenService);
    }

}