package nl.maakmaareenafspraak.service.checks;

import nl.maakmaareenafspraak.domain.Afspraak;
import nl.maakmaareenafspraak.domain.Behandeling;
import nl.maakmaareenafspraak.domain.Medewerker;
import nl.maakmaareenafspraak.service.MedewerkerService;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.Mockito.verifyNoMoreInteractions;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class MagDeVoorkeurMedewerkerDezeBehandelingWelUitvoerenCheckTest {

    @Mock
    private MedewerkerService medewerkerService;

    @InjectMocks
    private MagDeVoorkeurMedewerkerDezeBehandelingWelUitvoerenCheck magDeVoorkeurMedewerkerDezeBehandelingWelUitvoerenCheck = new MagDeVoorkeurMedewerkerDezeBehandelingWelUitvoerenCheck();

    @Test
    void isCorrect() {
        var afspraak = new Afspraak();
        var medewerkerAfspraak = new Medewerker();
        medewerkerAfspraak.setId(1L);
        var behandelingAfspraak1 = new Behandeling();
        behandelingAfspraak1.setId(2L);
        var behandelingAfspraak2 = new Behandeling();
        behandelingAfspraak2.setId(3L);
        afspraak.setMedewerker(medewerkerAfspraak);
        afspraak.getBehandelingen().add(behandelingAfspraak1);
        afspraak.getBehandelingen().add(behandelingAfspraak2);

        var medewerker = new Medewerker();
        var behandelingMedewerker1 = new Behandeling();
        behandelingMedewerker1.setId(2L);
        var behandelingMedewerker2 = new Behandeling();
        behandelingMedewerker2.setId(3L);
        var behandelingMedewerker3 = new Behandeling();
        behandelingMedewerker3.setId(4L);
        medewerker.getBehandelingen().add(behandelingMedewerker1);
        medewerker.getBehandelingen().add(behandelingMedewerker2);
        medewerker.getBehandelingen().add(behandelingMedewerker3);

        when(medewerkerService.lees(1L)).thenReturn(medewerker);

        assertTrue(magDeVoorkeurMedewerkerDezeBehandelingWelUitvoerenCheck.isCorrect(afspraak));

        verifyNoMoreInteractions(medewerkerService);
    }

    @Test
    void isNietCorrect() {
        var afspraak = new Afspraak();
        var medewerkerAfspraak = new Medewerker();
        medewerkerAfspraak.setId(1L);
        var behandelingAfspraak1 = new Behandeling();
        behandelingAfspraak1.setId(2L);
        var behandelingAfspraak2 = new Behandeling();
        behandelingAfspraak2.setId(3L);
        afspraak.setMedewerker(medewerkerAfspraak);
        afspraak.getBehandelingen().add(behandelingAfspraak1);
        afspraak.getBehandelingen().add(behandelingAfspraak2);

        var medewerker = new Medewerker();
        var behandelingMedewerker1 = new Behandeling();
        behandelingMedewerker1.setId(2L);
        var behandelingMedewerker2 = new Behandeling();
        behandelingMedewerker2.setId(3L);
        var behandelingMedewerker3 = new Behandeling();
        behandelingMedewerker3.setId(4L);
        medewerker.getBehandelingen().add(behandelingMedewerker1);
        medewerker.getBehandelingen().add(behandelingMedewerker3);

        when(medewerkerService.lees(1L)).thenReturn(medewerker);

        assertFalse(magDeVoorkeurMedewerkerDezeBehandelingWelUitvoerenCheck.isCorrect(afspraak));

        verifyNoMoreInteractions(medewerkerService);
    }
}