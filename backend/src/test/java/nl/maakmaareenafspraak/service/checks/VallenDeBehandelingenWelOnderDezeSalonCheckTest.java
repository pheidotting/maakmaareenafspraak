package nl.maakmaareenafspraak.service.checks;

import nl.maakmaareenafspraak.domain.Afspraak;
import nl.maakmaareenafspraak.domain.Behandeling;
import nl.maakmaareenafspraak.domain.Categorie;
import nl.maakmaareenafspraak.domain.Salon;
import nl.maakmaareenafspraak.service.SalonService;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.Mockito.verifyNoMoreInteractions;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class VallenDeBehandelingenWelOnderDezeSalonCheckTest {

    @Mock
    private SalonService salonService;

    @InjectMocks
    private VallenDeBehandelingenWelOnderDezeSalonCheck vallenDeBehandelingenWelOnderDezeSalonCheck = new VallenDeBehandelingenWelOnderDezeSalonCheck();

    @Test
    void isCorrect() {
        var afspraak = new Afspraak();
        afspraak.setSalon(new Salon());
        afspraak.getSalon().setId(3L);
        var behandelingAfspraak1 = new Behandeling();
        behandelingAfspraak1.setId(1L);
        var behandelingAfspraak2 = new Behandeling();
        behandelingAfspraak2.setId(2L);
        afspraak.getBehandelingen().add(behandelingAfspraak1);
        afspraak.getBehandelingen().add(behandelingAfspraak2);

        var salon = new Salon();
        salon.setId(3L);
        var behandelingSalon1 = new Behandeling();
        behandelingSalon1.setId(1L);
        var behandelingSalon2 = new Behandeling();
        behandelingSalon2.setId(2L);
        var categorieSalon = new Categorie();
        categorieSalon.setId(6L);
        categorieSalon.getBehandelingen().add(behandelingSalon1);
        categorieSalon.getBehandelingen().add(behandelingSalon2);
        salon.getCategories().add(categorieSalon);

        when(salonService.lees(salon.getId())).thenReturn(salon);

        assertTrue(vallenDeBehandelingenWelOnderDezeSalonCheck.isCorrect(afspraak));

        verifyNoMoreInteractions(salonService);
    }

    @Test
    void isNietCorrect() {
        var afspraak = new Afspraak();
        afspraak.setSalon(new Salon());
        afspraak.getSalon().setId(3L);
        var behandelingAfspraak1 = new Behandeling();
        behandelingAfspraak1.setId(1L);
        var behandelingAfspraak2 = new Behandeling();
        behandelingAfspraak2.setId(2L);
        afspraak.getBehandelingen().add(behandelingAfspraak1);
        afspraak.getBehandelingen().add(behandelingAfspraak2);

        var salon = new Salon();
        salon.setId(3L);
        var behandelingSalon1 = new Behandeling();
        behandelingSalon1.setId(4L);
        var categorieSalon1 = new Categorie();
        categorieSalon1.getBehandelingen().add(behandelingSalon1);

        when(salonService.lees(salon.getId())).thenReturn(salon);

        assertFalse(vallenDeBehandelingenWelOnderDezeSalonCheck.isCorrect(afspraak));

        verifyNoMoreInteractions(salonService);
    }
}
