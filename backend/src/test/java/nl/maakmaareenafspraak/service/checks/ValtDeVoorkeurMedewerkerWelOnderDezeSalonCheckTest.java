package nl.maakmaareenafspraak.service.checks;

import nl.maakmaareenafspraak.domain.Afspraak;
import nl.maakmaareenafspraak.domain.Medewerker;
import nl.maakmaareenafspraak.domain.Salon;
import nl.maakmaareenafspraak.service.MedewerkerService;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.Mockito.verifyNoMoreInteractions;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class ValtDeVoorkeurMedewerkerWelOnderDezeSalonCheckTest {

    @Mock
    private MedewerkerService medewerkerService;

    @InjectMocks
    private ValtDeVoorkeurMedewerkerWelOnderDezeSalonCheck valtDeVoorkeurMedewerkerWelOnderDezeSalonCheck = new ValtDeVoorkeurMedewerkerWelOnderDezeSalonCheck();

    @Test
    void isCorrect() {
        var afspraak = new Afspraak();
        var salonBijAfspraak = new Salon();
        salonBijAfspraak.setId(1L);
        afspraak.setSalon(salonBijAfspraak);
        var salonBijMedewerker = new Salon();
        salonBijMedewerker.setId(1L);
        var medewerker = new Medewerker();
        medewerker.setId(2L);
        medewerker.setSalon(salonBijMedewerker);
        afspraak.setMedewerker(medewerker);

        when(medewerkerService.lees(2L)).thenReturn(medewerker);

        assertTrue(valtDeVoorkeurMedewerkerWelOnderDezeSalonCheck.isCorrect(afspraak));

        verifyNoMoreInteractions(medewerkerService);
    }

    @Test
    void isNietCorrect() {
        var afspraak = new Afspraak();
        var salonBijAfspraak = new Salon();
        salonBijAfspraak.setId(1L);
        afspraak.setSalon(salonBijAfspraak);
        var salonBijMedewerker = new Salon();
        salonBijMedewerker.setId(3L);
        var medewerker = new Medewerker();
        medewerker.setId(2L);
        medewerker.setSalon(salonBijMedewerker);
        afspraak.setMedewerker(medewerker);

        when(medewerkerService.lees(2L)).thenReturn(medewerker);

        assertFalse(valtDeVoorkeurMedewerkerWelOnderDezeSalonCheck.isCorrect(afspraak));

        verifyNoMoreInteractions(medewerkerService);
    }
}