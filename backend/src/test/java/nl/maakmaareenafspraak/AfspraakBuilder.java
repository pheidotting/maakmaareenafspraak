package nl.maakmaareenafspraak;

import nl.maakmaareenafspraak.domain.Afspraak;
import nl.maakmaareenafspraak.domain.Behandeling;
import nl.maakmaareenafspraak.domain.Medewerker;
import nl.maakmaareenafspraak.domain.Salon;

import java.time.LocalDateTime;

public class AfspraakBuilder {
    private Afspraak afspraak;

    private AfspraakBuilder() {
        this.afspraak = new Afspraak();
    }

    public static AfspraakBuilder getInstance() {
        return new AfspraakBuilder();
    }

    public AfspraakBuilder withTijdstip(LocalDateTime tijdstip) {
        afspraak.setTijdstip(tijdstip);
        return this;
    }

    public AfspraakBuilder withSalon(Salon salon) {
        afspraak.setSalon(salon);
        return this;
    }

    public AfspraakBuilder withMedewerker(Medewerker medewerker) {
        afspraak.setMedewerker(medewerker);
        return this;
    }

    public AfspraakBuilder withBehandeling(Behandeling behandeling) {
        afspraak.getBehandelingen().add(behandeling);
        return this;
    }

    public Afspraak build() {
        return afspraak;
    }
}
