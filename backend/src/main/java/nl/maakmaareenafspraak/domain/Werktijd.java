package nl.maakmaareenafspraak.domain;

import lombok.*;

import javax.persistence.*;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(callSuper = true)
@ToString(callSuper = true)
@Entity
@Table(name = "werkenopeningstijden")
@DiscriminatorValue(value = "W")
public class Werktijd extends AbstractWerktijd {
    @ManyToOne
    @JoinColumn(name = "medewerker")
    @EqualsAndHashCode.Exclude
    private Medewerker medewerker;
}
