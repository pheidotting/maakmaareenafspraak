package nl.maakmaareenafspraak.domain;

import lombok.*;
import org.apache.commons.lang3.builder.EqualsExclude;
import org.apache.commons.lang3.builder.HashCodeExclude;
import org.hibernate.envers.AuditJoinTable;
import org.hibernate.envers.Audited;

import javax.persistence.*;
import java.time.LocalDateTime;
import java.util.HashSet;
import java.util.Set;

@Audited
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode
@ToString
@Entity
@Table(name = "afspraak")
public class Afspraak {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Long id;
    @Column(name = "tijdstip")
    private LocalDateTime tijdstip;
    @Column(name = "opmerking")
    private String opmerking;
    @ManyToOne
    @JoinColumn(name = "klant")
    @EqualsAndHashCode.Exclude
    @ToString.Exclude
    private AbstractKlant klant;
    @ManyToOne
    @JoinColumn(name = "salon")
    private Salon salon;
    @ManyToOne
    @JoinColumn(name = "medewerker")
    private Medewerker medewerker;
    @HashCodeExclude
    @EqualsExclude
    @ToString.Exclude
    @ManyToMany
    @JoinTable(name = "afspraak_behandelingen", joinColumns = @JoinColumn(name = "afspraak"), inverseJoinColumns = @JoinColumn(name = "behandeling"))
    @AuditJoinTable
    private Set<Behandeling> behandelingen = new HashSet<>();
}
