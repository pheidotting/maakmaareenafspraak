package nl.maakmaareenafspraak.domain;

import lombok.Getter;
import lombok.Setter;
import org.hibernate.envers.DefaultRevisionEntity;
import org.hibernate.envers.RevisionEntity;
import org.springframework.data.annotation.CreatedBy;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

@Getter
@Setter
@Entity
@Table(name = "revinfo")
@RevisionEntity(AuditListener.class)
public class RevEntity extends DefaultRevisionEntity {
    @Column(name = "userid")
    @CreatedBy
    private String userid;
}
