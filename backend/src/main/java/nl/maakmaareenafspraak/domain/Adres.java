package nl.maakmaareenafspraak.domain;

import lombok.*;
import org.hibernate.envers.Audited;
import org.hibernate.envers.NotAudited;

import javax.persistence.*;

@Audited
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode
@ToString
@Entity
@Table(name = "adres")
public class Adres {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Long id;
    @Column(name = "straat")
    private String straat;
    @Column(name = "huisnummer")
    private String huisnummer;
    @Column(name = "postcode")
    private String postcode;
    @Column(name = "plaats")
    private String plaats;
    @OneToOne(cascade = CascadeType.ALL, targetEntity = Salon.class, fetch = FetchType.EAGER)
    @JoinColumn(name = "salon")
    @ToString.Exclude
    private Salon salon;
    @OneToOne
    @JoinColumn(name = "klant")
    @ToString.Exclude
    @NotAudited
    private Klant klant;
}
