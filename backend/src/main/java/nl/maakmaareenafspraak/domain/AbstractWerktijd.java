package nl.maakmaareenafspraak.domain;

import lombok.*;

import javax.persistence.*;
import java.time.DayOfWeek;
import java.time.LocalTime;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode
@ToString
@Entity
@Table(name = "werkenopeningstijden")
@DiscriminatorColumn(name = "soort", length = 1)
public abstract class AbstractWerktijd {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Long id;
    @Column(name = "dag")
    @Enumerated(EnumType.STRING)
    private DayOfWeek dag;
    @Column(name = "start")
    private LocalTime start;
    @Column(name = "eind")
    private LocalTime eind;
}
