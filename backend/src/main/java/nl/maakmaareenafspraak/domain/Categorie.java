package nl.maakmaareenafspraak.domain;

import lombok.*;
import org.hibernate.envers.Audited;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;

import static org.hibernate.envers.RelationTargetAuditMode.NOT_AUDITED;

@Audited(targetAuditMode = NOT_AUDITED)
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode
@ToString
@Entity
@Table(name = "categorie")
public class Categorie {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Long id;
    @ManyToOne
    @JoinColumn(name = "salon")
    @EqualsAndHashCode.Exclude
    @ToString.Exclude
    private Salon salon;
    @Column(name = "naam")
    private String naam;
    @Column(name = "omschrijving")
    private String omschrijving;
    @OneToMany(mappedBy = "categorie")
    @EqualsAndHashCode.Exclude
    @ToString.Exclude
    private Set<Behandeling> behandelingen = new HashSet<>();
}
