package nl.maakmaareenafspraak.domain;

import lombok.*;

import javax.persistence.*;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(callSuper = true)
@ToString(callSuper = true)
@Entity
@Table(name = "specifiekewerkenopeningstijden")
@DiscriminatorValue(value = "G")
public class ExtraGesloten extends AbstractSpecifiekeWerktijd {
    @ManyToOne
    @JoinColumn(name = "salon")
    private Salon salon;
}
