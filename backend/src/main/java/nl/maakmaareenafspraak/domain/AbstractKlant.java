package nl.maakmaareenafspraak.domain;

import lombok.*;
import org.hibernate.envers.Audited;

import javax.persistence.*;

@Audited
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode
@Entity
@Table(name = "klant")
@DiscriminatorColumn(name = "soort", length = 1)
public abstract class AbstractKlant {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Long id;
    @Column(name = "naam")
    private String naam;
    @Column(name = "emailadres")
    private String emailadres;
    @Column(name = "telefoonnummer")
    private String telefoonnummer;
}
