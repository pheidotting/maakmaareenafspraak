package nl.maakmaareenafspraak.domain;

import lombok.*;
import nl.maakmaareenafspraak.web.security.Inlogbaar;
import org.hibernate.envers.AuditJoinTable;
import org.hibernate.envers.Audited;
import org.hibernate.envers.NotAudited;

import javax.persistence.*;
import java.time.LocalDateTime;
import java.util.HashSet;
import java.util.Set;

@Audited
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode
@ToString
@Entity
@Table(name = "medewerker")
@DiscriminatorColumn(name = "soort", length = 1)
@DiscriminatorValue(value = "M")
public class Medewerker implements Inlogbaar {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Long id;
    @Column(name = "naam")
    private String naam;
    @Column(name = "gebruikersnaam")
    private String gebruikersnaam;
    @Column(name = "wijzigwachtwoordcode")
    private String wijzigWachtwoordCode;
    @Column(name = "wachtwoord")
    private String wachtwoord;
    @Column(name = "wijzigwachtwoordcodetijd")
    private LocalDateTime wijzigWachtwoordCodeTijd;
    @ManyToOne
    @JoinColumn(name = "salon")
    @EqualsAndHashCode.Exclude
    @ToString.Exclude
    @NotAudited
    private Salon salon;
    @OneToMany(mappedBy = "medewerker")
    @EqualsAndHashCode.Exclude
    @ToString.Exclude
    @NotAudited
    private Set<Werktijd> werktijden = new HashSet<>();
    @OneToMany(mappedBy = "medewerker")
    @EqualsAndHashCode.Exclude
    @ToString.Exclude
    @NotAudited
    private Set<ExtraWerktijd> extraWerktijden = new HashSet<>();
    @OneToMany(mappedBy = "medewerker")
    @EqualsAndHashCode.Exclude
    @ToString.Exclude
    @NotAudited
    private Set<Verlof> verloven = new HashSet<>();
    @ManyToMany
    @JoinTable(name = "medewerker_behandelingen", joinColumns = @JoinColumn(name = "medewerker"), inverseJoinColumns = @JoinColumn(name = "behandeling"))
    @AuditJoinTable
    private Set<Behandeling> behandelingen = new HashSet<>();

    public Werktijd addWerktijd() {
        var werktijd = new Werktijd();
        werktijd.setMedewerker(this);
        werktijden.add(werktijd);
        return werktijd;
    }

    public ExtraWerktijd addExtraWerktijd() {
        var extraWerktijd = new ExtraWerktijd();
        extraWerktijd.setMedewerker(this);
        extraWerktijden.add(extraWerktijd);
        return extraWerktijd;
    }

    public Verlof addVerlof() {
        var verlof = new Verlof();
        verlof.setMedewerker(this);
        verloven.add(verlof);
        return verlof;
    }

    @Override
    public String getUsername() {
        return gebruikersnaam;
    }

    @Override
    public void setUsername(String username) {
        this.gebruikersnaam = username;
    }

    @Override
    public String getPassword() {
        return wachtwoord;
    }

    @Override
    public void setPassword(String password) {
        this.wachtwoord = password;
    }
}
