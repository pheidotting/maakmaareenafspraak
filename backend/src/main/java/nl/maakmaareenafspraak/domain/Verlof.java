package nl.maakmaareenafspraak.domain;

import lombok.*;

import javax.persistence.*;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(callSuper = true)
@ToString(callSuper = true)
@Entity
@Table(name = "specifiekewerkenopeningstijden")
@DiscriminatorValue(value = "V")
public class Verlof extends AbstractSpecifiekeWerktijd {
    @ManyToOne
    @JoinColumn(name = "medewerker")
    private Medewerker medewerker;
}
