package nl.maakmaareenafspraak.domain;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import org.hibernate.envers.Audited;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.Table;

@Audited
@Getter
@Setter
@EqualsAndHashCode
@ToString
@Entity
@Table(name = "medewerker")
@DiscriminatorValue(value = "B")
public class Beheerder extends Medewerker {
}
