package nl.maakmaareenafspraak.domain;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.Table;

@Getter
@Setter
@EqualsAndHashCode
@Entity
@Table(name = "klant")
@DiscriminatorValue(value = "N")
public class NietIngelogdeKlant extends AbstractKlant {
}
