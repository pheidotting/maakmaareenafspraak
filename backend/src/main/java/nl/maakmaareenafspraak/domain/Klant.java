package nl.maakmaareenafspraak.domain;

import lombok.*;
import nl.maakmaareenafspraak.web.security.Inlogbaar;

import javax.persistence.*;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode
@Entity
@Table(name = "klant")
@DiscriminatorValue(value = "K")
public class Klant extends AbstractKlant implements Inlogbaar {
    @OneToOne(mappedBy = "klant")
    private Adres adres;
    @Column(name = "wachtwoord")
    private String wachtwoord;
    @Column(name = "photourl")
    private String photourl;

    @Override
    public String getUsername() {
        return getEmailadres();
    }

    @Override
    public void setUsername(String username) {
        setEmailadres(username);
    }

    @Override
    public String getPassword() {
        return wachtwoord;
    }

    @Override
    public void setPassword(String password) {
        this.wachtwoord = password;
    }
}
