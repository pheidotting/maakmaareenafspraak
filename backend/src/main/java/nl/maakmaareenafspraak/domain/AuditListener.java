package nl.maakmaareenafspraak.domain;

import org.hibernate.envers.RevisionListener;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.User;

import java.util.Optional;

public class AuditListener implements RevisionListener {
        @Override
        public void newRevision(Object o) {
                var revEntity = (RevEntity) o;
            String currentUser;
                try {
                        currentUser = Optional.ofNullable(SecurityContextHolder.getContext())//
                                .map(SecurityContext::getAuthentication)//
                                .filter(Authentication::isAuthenticated)//
                                .map(Authentication::getPrincipal)//
                                .map(User.class::cast)//
                                .map(User::getUsername)//
                                .orElse("unkown");
                } catch (Exception e) {
                        currentUser = "unkown";
                }

                revEntity.setUserid(currentUser);
        }
}
