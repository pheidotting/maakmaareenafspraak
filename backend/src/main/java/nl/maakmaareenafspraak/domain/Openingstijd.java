package nl.maakmaareenafspraak.domain;

import lombok.*;

import javax.persistence.*;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(callSuper = true)
@ToString(callSuper = true)
@Entity
@Table(name = "werkenopeningstijden")
@DiscriminatorValue(value = "O")
public class Openingstijd extends AbstractWerktijd {
    @ManyToOne
    @JoinColumn(name = "salon")
    @EqualsAndHashCode.Exclude
    @ToString.Exclude
    private Salon salon;
}
