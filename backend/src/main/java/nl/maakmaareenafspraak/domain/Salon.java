package nl.maakmaareenafspraak.domain;

import lombok.*;
import org.hibernate.envers.Audited;
import org.hibernate.envers.NotAudited;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;

@Audited
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode
@ToString
@Entity
@Table(name = "salon")
public class Salon {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Long id;
    @Column(name = "naam")
    private String naam;
    @OneToOne(mappedBy = "salon", cascade = CascadeType.ALL, fetch = FetchType.EAGER)
    @EqualsAndHashCode.Exclude
    private Adres adres;
    @Column(name = "telefoonnummer")
    private String telefoonnummer;
    @Column(name = "emailadres")
    private String emailadres;
    @OneToMany(mappedBy = "salon")
    @EqualsAndHashCode.Exclude
    @ToString.Exclude
    @NotAudited
    private Set<Medewerker> medewerkers = new HashSet<>();
    @OneToMany(mappedBy = "salon")
    @EqualsAndHashCode.Exclude
    @NotAudited
    private Set<Categorie> categories = new HashSet<>();
    @OneToMany(mappedBy = "salon")
    @EqualsAndHashCode.Exclude
    @NotAudited
    private Set<Openingstijd> openingstijden = new HashSet<>();
    @OneToMany(mappedBy = "salon")
    @EqualsAndHashCode.Exclude
    @NotAudited
    private Set<ExtraOpeningstijd> extraOpeningstijden = new HashSet<>();
    @OneToMany(mappedBy = "salon")
    @EqualsAndHashCode.Exclude
    @NotAudited
    private Set<ExtraGesloten> extraGeslotens = new HashSet<>();

    public Medewerker addMedewerker() {
        var medewerker = new Medewerker();
        medewerker.setSalon(this);
        medewerkers.add(medewerker);
        return medewerker;
    }

    public Behandeling addBehandeling() {
        var categorie = new Categorie();
        categorie.setSalon(this);
        var behandeling = new Behandeling();
        behandeling.setCategorie(categorie);
        categorie.getBehandelingen().add(behandeling);
        categories.add(categorie);
        return behandeling;
    }

    public Openingstijd addOpeningstijd() {
        var openingstijd = new Openingstijd();
        openingstijd.setSalon(this);
        openingstijden.add(openingstijd);
        return openingstijd;
    }

    public ExtraOpeningstijd addExtraOpeningstijd() {
        var extraOpeningstijd = new ExtraOpeningstijd();
        extraOpeningstijd.setSalon(this);
        extraOpeningstijden.add(extraOpeningstijd);
        return extraOpeningstijd;
    }

    public ExtraGesloten addExtraGesloten() {
        var extraGesloten = new ExtraGesloten();
        extraGesloten.setSalon(this);
        extraGeslotens.add(extraGesloten);
        return extraGesloten;
    }
}
