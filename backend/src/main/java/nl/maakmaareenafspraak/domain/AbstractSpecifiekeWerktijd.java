package nl.maakmaareenafspraak.domain;

import lombok.*;

import javax.persistence.*;
import java.time.LocalDateTime;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode
@ToString
@Entity
@Table(name = "specifiekewerkenopeningstijden")
@DiscriminatorColumn(name = "soort", length = 1)
public abstract class AbstractSpecifiekeWerktijd {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Long id;
    @Column(name = "start")
    private LocalDateTime start;
    @Column(name = "eind")
    private LocalDateTime eind;
}
