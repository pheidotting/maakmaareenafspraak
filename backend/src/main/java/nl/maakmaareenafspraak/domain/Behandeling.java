package nl.maakmaareenafspraak.domain;

import lombok.*;
import org.hibernate.envers.Audited;

import javax.persistence.*;

import static org.hibernate.envers.RelationTargetAuditMode.NOT_AUDITED;

@Audited(targetAuditMode = NOT_AUDITED)
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode
@ToString
@Entity
@Table(name = "behandeling")
public class Behandeling {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Long id;
    @ManyToOne
    @JoinColumn(name = "categorie")
    @EqualsAndHashCode.Exclude
    @ToString.Exclude
    private Categorie categorie;
    @Column(name = "naam")
    private String naam;
    @Column(name = "omschrijving")
    private String omschrijving;
    @Column(name = "tijdsduur")
    private Long tijdsduur;
    @Column(name = "prijs")
    private Double prijs;
}
