package nl.maakmaareenafspraak;

import nl.maakmaareenafspraak.domain.Afspraak;
import nl.maakmaareenafspraak.domain.Behandeling;

import java.time.LocalDateTime;

public class AfspraakUtils {
    private AfspraakUtils() {
    }

    public static LocalDateTime bepaalEindtijdAfspraak(Afspraak afspraak) {
        Long aantalTijden = afspraak.getBehandelingen().stream()//
                .mapToLong(Behandeling::getTijdsduur)//
                .sum();

        return afspraak.getTijdstip().plusMinutes(aantalTijden * 15);
    }
}
