package nl.maakmaareenafspraak.MaakMaarEenAfspraak;

import nl.maakmaareenafspraak.repository.envers.AuditorAwareImpl;
import nl.maakmaareenafspraak.service.KlantService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.data.domain.AuditorAware;
import org.springframework.data.jpa.repository.config.EnableJpaAuditing;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

@SpringBootApplication
@ComponentScan(basePackages = {"nl.maakmaareenafspraak"})
@EntityScan("nl.maakmaareenafspraak")
@EnableJpaRepositories(basePackages = {"nl.maakmaareenafspraak"})
@EnableJpaAuditing(auditorAwareRef = "auditorProvider")
public class MaakMaarEenAfspraakApplication {
    @Autowired
    private KlantService klantService;

    public static void main(String[] args) {
        SpringApplication.run(MaakMaarEenAfspraakApplication.class, args);
    }

    @Bean
    AuditorAware<Long> auditorProvider() {
        return new AuditorAwareImpl(klantService);
    }
}
