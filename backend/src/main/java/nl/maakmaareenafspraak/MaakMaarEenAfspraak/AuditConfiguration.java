package nl.maakmaareenafspraak.MaakMaarEenAfspraak;

import org.hibernate.envers.AuditReader;
import org.hibernate.envers.AuditReaderFactory;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;

@Configuration
public class AuditConfiguration {

    private final EntityManagerFactory entityManagerFactory;

    AuditConfiguration(EntityManagerFactory entityManagerFactory) {
        this.entityManagerFactory = entityManagerFactory;
    }

    @Bean
    EntityManager entityManager() {
        return entityManagerFactory.createEntityManager();
    }

    @Bean
    AuditReader auditReader() {
        return AuditReaderFactory.get(entityManager());
    }
}