package nl.maakmaareenafspraak;

import nl.maakmaareenafspraak.domain.Adres;
import org.apache.commons.lang3.StringUtils;

public class AdresUtils {
    private AdresUtils() {
    }

    public static boolean isAdresGevuld(Adres adres) {
        return !StringUtils.isEmpty(adres.getStraat()) && !StringUtils.isEmpty(adres.getPostcode()) && !StringUtils.isEmpty(adres.getPlaats());
    }
}
