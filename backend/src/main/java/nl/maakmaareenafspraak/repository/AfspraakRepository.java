package nl.maakmaareenafspraak.repository;

import nl.maakmaareenafspraak.domain.Afspraak;
import nl.maakmaareenafspraak.domain.Klant;
import nl.maakmaareenafspraak.domain.Salon;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.time.LocalDateTime;
import java.util.List;

@Repository
public interface AfspraakRepository extends JpaRepository<Afspraak, Long> {
    @Query("SELECT a FROM Afspraak a WHERE a.salon = :salon AND a.tijdstip >= :beginDatum AND a.tijdstip <= :eindDatum")
    List<Afspraak> haalAfsprakenOp(@Param("salon") Salon salon, @Param("beginDatum") LocalDateTime beginDatum, @Param("eindDatum") LocalDateTime eindDatum);

    @Query("SELECT a FROM Afspraak a WHERE a.klant = :klant")
    List<Afspraak> haalAfsprakenOpBijEenKlant(@Param("klant") Klant klant);

    @Query("SELECT a FROM Afspraak a WHERE a.salon = :salon")
    List<Afspraak> haalAfsprakenOpBijEenSalon(@Param("salon") Salon salon);

    @Query("SELECT a FROM Afspraak a WHERE a.salon = :salon AND a.tijdstip>:start AND a.tijdstip<:eind")
    List<Afspraak> haalAfsprakenOpBijSalonTussenTweeTijdstippen(@Param("salon") Salon salon, @Param("start") LocalDateTime start, @Param("eind") LocalDateTime eind);
}
