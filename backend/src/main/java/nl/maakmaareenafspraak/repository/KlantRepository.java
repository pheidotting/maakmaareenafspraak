package nl.maakmaareenafspraak.repository;

import nl.maakmaareenafspraak.domain.AbstractKlant;
import nl.maakmaareenafspraak.domain.Klant;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

@Repository
@Transactional
public interface KlantRepository extends JpaRepository<AbstractKlant, Long> {
    @Query("SELECT k FROM Klant k WHERE k.emailadres = :emailadres")
    Klant zoekOpEmailadres(@Param("emailadres") String emailadres);
}
