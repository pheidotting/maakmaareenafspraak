package nl.maakmaareenafspraak.repository;

import nl.maakmaareenafspraak.domain.Communicatie;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface CommunicatieRepository extends JpaRepository<Communicatie, Long> {
}
