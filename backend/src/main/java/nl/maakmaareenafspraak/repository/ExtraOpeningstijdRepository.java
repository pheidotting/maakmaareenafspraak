package nl.maakmaareenafspraak.repository;

import nl.maakmaareenafspraak.domain.ExtraOpeningstijd;
import nl.maakmaareenafspraak.domain.Salon;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.time.LocalDateTime;
import java.util.List;

public interface ExtraOpeningstijdRepository extends JpaRepository<ExtraOpeningstijd, Long> {
    @Query("SELECT o FROM ExtraOpeningstijd o WHERE o.salon = :salon AND o.start>=:start AND o.eind<=:eind")
    List<ExtraOpeningstijd> extraOpeningstijdenBijSalon(@Param("salon") Salon salon, @Param("start") LocalDateTime start, @Param("eind") LocalDateTime eind);
}
