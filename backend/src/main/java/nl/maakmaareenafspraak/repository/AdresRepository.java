package nl.maakmaareenafspraak.repository;

import nl.maakmaareenafspraak.domain.Adres;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

@Repository
@Transactional
public interface AdresRepository extends JpaRepository<Adres, Long> {
}
