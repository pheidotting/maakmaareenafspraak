package nl.maakmaareenafspraak.repository.envers;

import nl.maakmaareenafspraak.service.KlantService;
import org.springframework.data.domain.AuditorAware;
import org.springframework.security.core.context.SecurityContextHolder;

import java.util.Optional;

public class AuditorAwareImpl implements AuditorAware<Long> {
    private KlantService klantService;

    public AuditorAwareImpl(KlantService klantService) {
        this.klantService = klantService;
    }


    @Override
    public Optional<Long> getCurrentAuditor() {
        var authentication = SecurityContextHolder.getContext().getAuthentication();
        //        Klant klant=klantService.zoekOpEmailadres(authentication.getName()).orElse(new Klant());
        return Optional.of(99L);//klant.getId());
    }
}