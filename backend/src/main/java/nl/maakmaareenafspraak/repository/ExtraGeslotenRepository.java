package nl.maakmaareenafspraak.repository;

import nl.maakmaareenafspraak.domain.ExtraGesloten;
import nl.maakmaareenafspraak.domain.Salon;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.time.LocalDateTime;
import java.util.List;

public interface ExtraGeslotenRepository extends JpaRepository<ExtraGesloten, Long> {
    @Query("SELECT w FROM ExtraGesloten w WHERE w.salon = :salon AND w.start>=:start AND w.eind<=:eind")
    List<ExtraGesloten> extraGeslotenenBijMedewerker(@Param("salon") Salon salon, @Param("start") LocalDateTime start, @Param("eind") LocalDateTime eind);
}
