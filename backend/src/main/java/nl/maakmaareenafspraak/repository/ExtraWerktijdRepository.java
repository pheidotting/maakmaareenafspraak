package nl.maakmaareenafspraak.repository;

import nl.maakmaareenafspraak.domain.ExtraWerktijd;
import nl.maakmaareenafspraak.domain.Medewerker;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.time.LocalDateTime;
import java.util.List;

public interface ExtraWerktijdRepository extends JpaRepository<ExtraWerktijd, Long> {
    @Query("SELECT w FROM ExtraWerktijd w WHERE w.medewerker IN :medewerkers AND w.start>=:start AND w.eind<=:eind")
    List<ExtraWerktijd> extraWerktijdenBijMedewerker(@Param("medewerkers") List<Medewerker> medewerkers, @Param("start") LocalDateTime start, @Param("eind") LocalDateTime eind);
}
