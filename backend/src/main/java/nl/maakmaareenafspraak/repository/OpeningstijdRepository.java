package nl.maakmaareenafspraak.repository;

import nl.maakmaareenafspraak.domain.Openingstijd;
import nl.maakmaareenafspraak.domain.Salon;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface OpeningstijdRepository extends JpaRepository<Openingstijd, Long> {
    @Query("SELECT o FROM Openingstijd o WHERE o.salon = ?1")
    List<Openingstijd> openingstijdenBijSalon(Salon salon);
}
