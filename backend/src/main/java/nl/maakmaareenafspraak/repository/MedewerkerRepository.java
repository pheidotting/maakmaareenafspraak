package nl.maakmaareenafspraak.repository;

import nl.maakmaareenafspraak.domain.Medewerker;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

@Repository
@Transactional
public interface MedewerkerRepository extends JpaRepository<Medewerker, Long> {
    @Query("SELECT m FROM Medewerker m WHERE m.gebruikersnaam = ?1")
    Medewerker zoekOpGebruikersnaam(String gebruikersnaam);

    @Query("SELECT m FROM Medewerker m WHERE m.wijzigWachtwoordCode = ?1")
    Medewerker zoekOpWijzigWachtwoordCode(String wijzigWachtwoordCode);
}
