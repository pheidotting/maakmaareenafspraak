package nl.maakmaareenafspraak.repository;

import nl.maakmaareenafspraak.domain.Medewerker;
import nl.maakmaareenafspraak.domain.Verlof;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.time.LocalDateTime;
import java.util.List;

public interface VerlofRepository extends JpaRepository<Verlof, Long> {
    @Query("SELECT v FROM Verlof v WHERE v.medewerker IN :medewerkers AND v.start>=:start AND v.eind<=:eind")
    List<Verlof> verlovenBijMedewerker(@Param("medewerkers") List<Medewerker> medewerkers, @Param("start") LocalDateTime start, @Param("eind") LocalDateTime eind);
}
