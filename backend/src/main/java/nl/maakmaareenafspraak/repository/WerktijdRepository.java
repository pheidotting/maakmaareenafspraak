package nl.maakmaareenafspraak.repository;

import nl.maakmaareenafspraak.domain.Medewerker;
import nl.maakmaareenafspraak.domain.Werktijd;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface WerktijdRepository extends JpaRepository<Werktijd, Long> {
    @Query("SELECT w FROM Werktijd w WHERE w.medewerker IN ?1")
    List<Werktijd> werktijdenBijMedewerker(List<Medewerker> medewerkers);
}
