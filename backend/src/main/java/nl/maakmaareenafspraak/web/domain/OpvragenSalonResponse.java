package nl.maakmaareenafspraak.web.domain;

import lombok.Getter;
import lombok.Setter;

import java.util.ArrayList;
import java.util.List;

@Getter
@Setter
public class OpvragenSalonResponse {
    private Salon salon;
    private List<Dag> dagen = new ArrayList<>();
}
