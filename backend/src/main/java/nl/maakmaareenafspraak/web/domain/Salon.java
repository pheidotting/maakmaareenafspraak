package nl.maakmaareenafspraak.web.domain;

import lombok.*;

import java.util.ArrayList;
import java.util.List;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode
@ToString
public class Salon {
    private Long id;
    private String naam;
    private String straat;
    private String huisnummer;
    private String postcode;
    private String plaats;
    private String telefoonnummer;
    private String emailadres;

    private List<Medewerker> medewerkers = new ArrayList<>();
    private List<Behandeling> behandelingen = new ArrayList<>();
}
