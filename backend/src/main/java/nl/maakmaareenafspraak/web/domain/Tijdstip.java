package nl.maakmaareenafspraak.web.domain;

import lombok.*;

import java.time.LocalTime;
import java.util.ArrayList;
import java.util.List;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode
@ToString
public class Tijdstip {
    @EqualsAndHashCode.Exclude
    @ToString.Exclude
    private List<Long> medewekerIds = new ArrayList<>();
    private LocalTime tijd;
    @EqualsAndHashCode.Exclude
    @ToString.Exclude
    private int aantalAfspraken;
}
