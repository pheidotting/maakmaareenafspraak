package nl.maakmaareenafspraak.web.domain;

import lombok.*;

import java.util.List;

import static com.google.common.collect.Lists.newArrayList;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode
@ToString
public class Behandeling {
    private Long id;
    private String naam;
    private String omschrijving;
    private Long tijdsduur;
    private Double prijs;
    private List<Long> medewerkers = newArrayList();
}
