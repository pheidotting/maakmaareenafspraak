package nl.maakmaareenafspraak.web.controller;

import io.jsonwebtoken.*;
import io.jsonwebtoken.impl.TextCodec;
import nl.maakmaareenafspraak.domain.Medewerker;
import nl.maakmaareenafspraak.domain.Salon;
import nl.maakmaareenafspraak.service.MedewerkerService;
import nl.maakmaareenafspraak.web.security.JwtUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;

import javax.servlet.http.HttpServletRequest;
import java.util.Optional;

import static org.apache.http.HttpHeaders.AUTHORIZATION;

public class AbstractController {
    @Value("${bezkoder.app.jwtSecret}")
    private String jwtSecret;
    @Autowired
    private MedewerkerService medewerkerService;

    public JwtUtils.SoortUser getSoortUser(HttpServletRequest httpServletRequest) {
        var jwt = httpServletRequest.getHeader(AUTHORIZATION).replace("Bearer", "").trim();

        Jws<Claims> jws = Jwts.parser().setSigningKeyResolver(signingKeyResolver).parseClaimsJws(jwt);

        return JwtUtils.SoortUser.valueOf((String) jws.getBody().get("soortUser"));
    }

    public Optional<Medewerker> getIngelogdeMedewerker(HttpServletRequest httpServletRequest) {
        var jwt = httpServletRequest.getHeader(AUTHORIZATION).replace("Bearer", "").trim();

        Jws<Claims> jws = Jwts.parser().setSigningKeyResolver(signingKeyResolver).parseClaimsJws(jwt);

        var username = (String) jws.getBody().get("username");

        return medewerkerService.zoekOpGebruikersnaam(username);
    }

    public Optional<Salon> getSalon(HttpServletRequest httpServletRequest) {
        var jwt = httpServletRequest.getHeader(AUTHORIZATION).replace("Bearer", "").trim();

        Jws<Claims> jws = Jwts.parser().setSigningKeyResolver(signingKeyResolver).parseClaimsJws(jwt);

        var username = (String) jws.getBody().get("username");

        var medewerker = medewerkerService.zoekOpGebruikersnaam(username);

        if (medewerker.isPresent()) {
            return Optional.ofNullable(medewerker.get().getSalon());
        } else {
            return Optional.empty();
        }
    }

    private SigningKeyResolver signingKeyResolver = new SigningKeyResolverAdapter() {
        @Override
        public byte[] resolveSigningKeyBytes(JwsHeader header, Claims claims) {
            return TextCodec.BASE64.decode(jwtSecret);
        }
    };

}
