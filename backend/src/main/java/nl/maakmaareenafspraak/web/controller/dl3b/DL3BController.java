package nl.maakmaareenafspraak.web.controller.dl3b;

import lombok.extern.slf4j.Slf4j;
import nl.maakmaareenafspraak.domain.Openingstijd;
import nl.maakmaareenafspraak.domain.Werktijd;
import nl.maakmaareenafspraak.domain.*;
import nl.maakmaareenafspraak.service.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.bind.annotation.*;

import java.time.DayOfWeek;
import java.time.LocalTime;
import java.util.*;
import java.util.function.Function;
import java.util.stream.Collectors;

import static com.google.common.collect.Lists.newArrayList;
import static java.util.stream.Collectors.toSet;

@Slf4j
@RestController
@RequestMapping("/dl3b")
public class DL3BController {
    //DL3B
    //Dikke
    //Lul
    //3
    //Bier

    @Autowired
    private MedewerkerService medewerkerService;
    @Autowired
    private SalonService salonService;
    @Autowired
    private BehandelingService behandelingService;
    @Autowired
    private CategorieService categorieService;
    @Autowired
    private KlantService klantService;
    @Autowired
    private PasswordEncoder encoder;
    @Autowired
    private OpeningstijdService openingstijdService;
    @Autowired
    private WerktijdService werktijdService;

    @PutMapping("/medewerker/{salonId}/{behandelingen}/{beheerder}/{gebruikersnaam}")
    public ResponseEntity<Long> medewerker(@PathVariable("salonId") Long salonId, @PathVariable("behandelingen") String behandelingenString, @PathVariable("beheerder") Boolean beheerder, @PathVariable("gebruikersnaam") String gebruikersnaam) {
        var salon = salonService.lees(salonId);

        Set<Behandeling> behandelingen = new HashSet<>();
        if (behandelingenString != null && !"".equals(behandelingenString) && !"0".equals(behandelingenString)) {
            behandelingen = Arrays.stream(behandelingenString.split("-")).map(s -> behandelingService.lees(Long.valueOf(s))).collect(toSet());
        }
        Medewerker medewerker;
        if (!beheerder) {
            medewerker = new Medewerker(null, UUID.randomUUID().toString(), null, null, null, null, salon, null, null, null, behandelingen);
        } else {
            medewerker = new Beheerder();
            medewerker.setNaam(UUID.randomUUID().toString());
            medewerker.setSalon(salon);
            medewerker.setBehandelingen(behandelingen);
            medewerker.setGebruikersnaam(gebruikersnaam);
        }
        medewerkerService.opslaan(medewerker);
        return new ResponseEntity<>(medewerker.getId(), HttpStatus.CREATED);
    }

    @PutMapping("/wijzigwachtwoord/{medewerkerId}/{wachtwoord}")
    public ResponseEntity<Boolean> wijzigwachtwoord(@PathVariable("medewerkerId") Long medewerkerId, @PathVariable("wachtwoord") String wachtwoord) {
        var medewerker = medewerkerService.lees(medewerkerId);
        medewerker.setWachtwoord(encoder.encode(wachtwoord));
        medewerkerService.opslaan(medewerker);

        return new ResponseEntity<>(Boolean.TRUE, HttpStatus.CREATED);
    }

    @PutMapping("/behandeling/{salonId}/{tijdsduur}/{prijs}")
    public ResponseEntity<Long> behandeling(@PathVariable("salonId") Long salonId, @PathVariable("tijdsduur") Long tijdsduur, @PathVariable("prijs") Double prijs) {
        var salon = salonService.lees(salonId);

        var categorie = new Categorie();
        categorie.setNaam(UUID.randomUUID().toString());
        categorie.setSalon(salon);
        categorieService.opslaan(categorie);

        var behandeling = new Behandeling(null, categorie, UUID.randomUUID().toString(), UUID.randomUUID().toString() + UUID.randomUUID().toString(), tijdsduur, prijs);
        behandelingService.opslaan(behandeling);
        return new ResponseEntity<>(1L, HttpStatus.CREATED);//behandeling.getId(), HttpStatus.CREATED);
    }

    @PutMapping("/klant/{naam}/{email}/{wachtwoord}")
    public ResponseEntity<Long> klant(@PathVariable("naam") String naam, @PathVariable("email") String email, @PathVariable("wachtwoord") String wachtwoord) {
        var klant = new Klant();
        klant.setNaam(naam);
        klant.setEmailadres(email);
        klant.setWachtwoord(encoder.encode(wachtwoord));

        klantService.opslaan(klant);
        return new ResponseEntity<>(klant.getId(), HttpStatus.CREATED);
    }

    @PutMapping("/openingstijd/{salonid}/{dag}/{start}/{eind}")
    public ResponseEntity<Long> openingstijd(@PathVariable("salonid") Long salonid, @PathVariable("dag") String dag, @PathVariable("start") String start, @PathVariable("eind") String eind) {
        var openingstijd = new Openingstijd();
        openingstijd.setSalon(salonService.lees(salonid));
        openingstijd.setDag(DayOfWeek.valueOf(dag));
        String[] s = start.split("-");
        openingstijd.setStart(LocalTime.of(Integer.valueOf(s[0]), Integer.valueOf(s[1])));
        String[] e = eind.split("-");
        openingstijd.setEind(LocalTime.of(Integer.valueOf(e[0]), Integer.valueOf(e[1])));

        openingstijdService.opslaan(openingstijd);
        return new ResponseEntity<>(openingstijd.getId(), HttpStatus.CREATED);
    }

    @PutMapping("/salon/{naam}")
    public ResponseEntity<Long> salonMaken(@PathVariable("naam") String naam) {
        var salon = new Salon();
        salon.setNaam(naam);

        salonService.opslaan(salon);
        return new ResponseEntity<>(salon.getId(), HttpStatus.CREATED);
    }

    @PutMapping("/werktijd/{medwerker}/{dag}/{start}/{eind}")
    public ResponseEntity<Long> werktijd(@PathVariable("medwerker") Long medwerker, @PathVariable("dag") String dag, @PathVariable("start") String start, @PathVariable("eind") String eind) {
        var werktijd = new Werktijd();
        werktijd.setMedewerker(medewerkerService.lees(medwerker));
        werktijd.setDag(DayOfWeek.valueOf(dag));
        String[] s = start.split("-");
        werktijd.setStart(LocalTime.of(Integer.valueOf(s[0]), Integer.valueOf(s[1])));
        String[] e = eind.split("-");
        werktijd.setEind(LocalTime.of(Integer.valueOf(e[0]), Integer.valueOf(e[1])));

        werktijdService.opslaan(werktijd);
        return new ResponseEntity<>(werktijd.getId(), HttpStatus.CREATED);
    }

    @GetMapping("/openingstijden/{salonId}")
    public ResponseEntity<List<nl.maakmaareenafspraak.web.controller.dl3b.Openingstijd>> openingstijden(@PathVariable("salonId") Long salonId) {
        var salon = salonService.lees(salonId);
        return new ResponseEntity<>(openingstijdService.openingstijdenBijSalon(salon).stream()//
                .map(openingstijd -> {
                    var ot = new nl.maakmaareenafspraak.web.controller.dl3b.Openingstijd();
                    ot.setId(openingstijd.getId());
                    ot.setDag(openingstijd.getDag().toString());
                    ot.setStart(openingstijd.getStart().toString());
                    ot.setEind(openingstijd.getEind().toString());
                    return ot;
                }).collect(Collectors.toList()), HttpStatus.OK);
    }

    @GetMapping("/werktijden/{medewerkerId}")
    public ResponseEntity<List<nl.maakmaareenafspraak.web.controller.dl3b.Werktijd>> werktijden(@PathVariable("medewerkerId") Long medewerkerId) {
        var medewerker = medewerkerService.lees(medewerkerId);
        return new ResponseEntity<>(werktijdService.werktijdenBijMedewerker(newArrayList(medewerker)).stream()//
                .map(new Function<Werktijd, nl.maakmaareenafspraak.web.controller.dl3b.Werktijd>() {
                    @Override
                    public nl.maakmaareenafspraak.web.controller.dl3b.Werktijd apply(Werktijd werktijd) {
                        var wt = new nl.maakmaareenafspraak.web.controller.dl3b.Werktijd();
                        wt.setId(werktijd.getId());
                        wt.setDag(werktijd.getDag().toString());
                        wt.setStart(werktijd.getStart().toString());
                        wt.setEind(werktijd.getEind().toString());
                        return wt;
                    }
                }).collect(Collectors.toList()), HttpStatus.OK);
    }

}
