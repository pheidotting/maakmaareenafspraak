package nl.maakmaareenafspraak.web.controller.afspraak;

import lombok.Getter;
import lombok.Setter;

import java.time.LocalDateTime;
import java.util.List;

@Getter
@Setter
public class AfspraakMetHistorie {
    private Long id;
    private LocalDateTime tijdstip;
    private String opmerking;
    private String salon;
    private String medewerker;
    private List<String> behandelingen;
    private List<AfspraakHistorie> afspraakHistorieList;
}
