package nl.maakmaareenafspraak.web.controller.aanmelden;

import lombok.extern.slf4j.Slf4j;
import nl.maakmaareenafspraak.service.AanmeldenService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@Slf4j
@RestController
@RequestMapping("/aanmelden")
@CrossOrigin(origins = {"http://localhost:4200", "https://localhost:4200"})
public class AanmeldenController {
    @Autowired
    private AanmeldenService aanmeldenService;

    @PostMapping()
    public ResponseEntity<Long> opslaan(@RequestBody Aanmelden aanmelden) {
        log.info("Aanmelden nieuwe Salon");

        Long result = aanmeldenService.aanmelden(aanmelden);

        return new ResponseEntity<>(result, HttpStatus.OK);
    }

}
