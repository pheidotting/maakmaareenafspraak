package nl.maakmaareenafspraak.web.controller.aanmelden;

import lombok.Getter;
import lombok.Setter;

import java.util.List;

import static com.google.common.collect.Lists.newArrayList;

@Getter
@Setter
public class TreeNode {
    private Categorie data;
    private boolean expanded;
    private List<BehandelingHolder> children = newArrayList();
}
