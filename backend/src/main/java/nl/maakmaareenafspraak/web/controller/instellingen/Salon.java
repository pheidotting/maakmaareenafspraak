package nl.maakmaareenafspraak.web.controller.instellingen;

import lombok.*;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode
@ToString
public class Salon {
    private Long id;
    private String naam;
    private String straat;
    private String huisnummer;
    private String postcode;
    private String plaats;
    private String telefoonnummer;
    private String emailadres;
}
