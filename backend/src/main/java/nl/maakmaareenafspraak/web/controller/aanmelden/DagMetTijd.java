package nl.maakmaareenafspraak.web.controller.aanmelden;

import lombok.Getter;
import lombok.Setter;

import java.util.ArrayList;
import java.util.List;

@Getter
@Setter
public class DagMetTijd {
    private Dag dag;
    private List<Tijd> tijden = new ArrayList<>();
}
