package nl.maakmaareenafspraak.web.controller.dl3b;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class Werktijd {
    private Long id;
    private String dag;
    private String start;
    private String eind;

}
