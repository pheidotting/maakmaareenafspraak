package nl.maakmaareenafspraak.web.controller.instellingen;

import io.jsonwebtoken.ExpiredJwtException;
import lombok.extern.slf4j.Slf4j;
import nl.maakmaareenafspraak.service.SalonService;
import nl.maakmaareenafspraak.web.controller.AbstractController;
import nl.maakmaareenafspraak.web.controller.instellingen.mapper.InstellingenSalonMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.persistence.EntityNotFoundException;
import javax.servlet.http.HttpServletRequest;
import java.util.Optional;

@Slf4j
@RestController
@RequestMapping("/instellingen/beherensalon")
@CrossOrigin(origins = {"http://localhost:4200", "https://localhost:4200"})
public class SalonInstellingenBeherenController extends AbstractController {
    @Autowired
    private SalonService salonService;
    @Autowired
    private InstellingenSalonMapper salonMapper;

    @PostMapping("/opslaan")
    public ResponseEntity<Long> opslaan(@RequestBody Salon salon, HttpServletRequest httpServletRequest) {
        var salonIngelogd = getSalon(httpServletRequest);
        log.info("Opslaan Salon met naam {}", salon.getNaam());
        Long id = salonService.opslaan(salonMapper.mapVanWebNaarDomain(salonIngelogd.orElse(null)).apply(salon));
        return new ResponseEntity<>(id, HttpStatus.OK);
    }

    @GetMapping("/lees")
    public ResponseEntity<Salon> lees(HttpServletRequest httpServletRequest) {
        Optional<nl.maakmaareenafspraak.domain.Salon> salonDomain = null;
        try {
            salonDomain = getSalon(httpServletRequest);
        } catch (ExpiredJwtException e) {
            return new ResponseEntity<>(HttpStatus.UNAUTHORIZED);
        }
        if (salonDomain.isPresent()) {
            Salon salon;
            try {
                salon = salonMapper.mapVanDomainNaarWeb().apply(salonDomain.get());
            } catch (EntityNotFoundException ennfe) {
                return new ResponseEntity<>(HttpStatus.NOT_FOUND);
            }

            return new ResponseEntity<>(salon, HttpStatus.OK);
        } else {
            return new ResponseEntity<>(HttpStatus.UNAUTHORIZED);
        }
    }
}
