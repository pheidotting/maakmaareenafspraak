package nl.maakmaareenafspraak.web.controller.afspraak;

import lombok.Getter;
import lombok.Setter;

import java.time.LocalDateTime;
import java.util.List;

@Getter
@Setter
public class AfspraakMetIngelogdeKlant {
    private LocalDateTime tijdstip;
    private String opmerking;
    private Long salonId;
    private Long voorkeurMedewerkerId;
    private List<Long> behandelingIds;
}
