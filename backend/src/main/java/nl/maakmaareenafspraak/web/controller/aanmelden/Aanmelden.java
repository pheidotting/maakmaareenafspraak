package nl.maakmaareenafspraak.web.controller.aanmelden;

import lombok.Getter;
import lombok.Setter;

import java.util.ArrayList;
import java.util.List;

@Getter
@Setter
public class Aanmelden {
    private String naamSalon;
    private String adres;
    private String huisnummer;
    private String postcode;
    private String plaats;
    private String telefoonnummer;
    private String email;
    private String naamEigenaar;

    //Stap 2
    private List<Medewerker> medewerkers = new ArrayList<>();
    //Stap 3
    private List<DagMetTijd> dagenMetTijden = new ArrayList<>();
    //Stap 4
    private List<TreeNode> behandelingen = new ArrayList<>();
}
