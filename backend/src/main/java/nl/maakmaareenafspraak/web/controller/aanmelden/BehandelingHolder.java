package nl.maakmaareenafspraak.web.controller.aanmelden;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class BehandelingHolder {
    private Behandeling data;
}
