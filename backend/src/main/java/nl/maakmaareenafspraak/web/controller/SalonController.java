package nl.maakmaareenafspraak.web.controller;

import lombok.extern.slf4j.Slf4j;
import nl.maakmaareenafspraak.service.SalonService;
import nl.maakmaareenafspraak.service.TijdenService;
import nl.maakmaareenafspraak.web.domain.OpvragenSalonResponse;
import nl.maakmaareenafspraak.web.domain.Salon;
import nl.maakmaareenafspraak.web.mapper.SalonMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.persistence.EntityNotFoundException;
import java.time.LocalDate;
import java.util.List;

import static java.util.stream.Collectors.toList;

@Slf4j
@RestController
@RequestMapping("/salon")
@CrossOrigin(origins = {"http://localhost:4200", "https://localhost:4200"})
public class SalonController extends AbstractController {
    @Autowired
    private SalonService salonService;
    @Autowired
    private SalonMapper salonMapper;
    @Autowired
    private TijdenService tijdenService;

    @GetMapping("/alles")
    public ResponseEntity<List<Salon>> alles() {
        log.info("Ophalen alle Salons");
        return new ResponseEntity<>(salonService.alles().stream().map(salonMapper.mapVanDomainNaarWeb()).collect(toList()), HttpStatus.OK);
    }

    @DeleteMapping("/verwijderen/{id}")
    public ResponseEntity verwijderen(@PathVariable Long id) {
        salonService.verwijderen(id);
        return new ResponseEntity<>(HttpStatus.OK);
    }

    @GetMapping("/lees/{id}")
    public ResponseEntity<OpvragenSalonResponse> lees(@PathVariable Long id) {
        log.info("Ophalen Salon met id {}", id);

        var salonDomain = salonService.lees(id);
        Salon salon;
        try {
            salon = salonMapper.mapVanDomainNaarWeb().apply(salonDomain);
        } catch (EntityNotFoundException ennfe) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }

        var opvragenSalonResponse = new OpvragenSalonResponse();

        opvragenSalonResponse.setSalon(salon);
        var beginDatum = LocalDate.now();
        var eindDatum = beginDatum.plusMonths(1).withDayOfMonth(LocalDate.now().plusMonths(1).lengthOfMonth());

        opvragenSalonResponse.setDagen(tijdenService.ophalenWerktijden(salonDomain, beginDatum, eindDatum));

        return new ResponseEntity<>(opvragenSalonResponse, HttpStatus.OK);
    }

}
