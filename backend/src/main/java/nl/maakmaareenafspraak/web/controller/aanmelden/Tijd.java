package nl.maakmaareenafspraak.web.controller.aanmelden;

import lombok.Getter;
import lombok.Setter;

import java.time.LocalTime;
import java.util.ArrayList;
import java.util.List;

@Getter
@Setter
public class Tijd {
    private Dag dag;
    private LocalTime start;
    private LocalTime eind;
    private List<Medewerker> medewerkers = new ArrayList<>();
}
