package nl.maakmaareenafspraak.web.controller.dashboard;

import lombok.extern.slf4j.Slf4j;
import nl.maakmaareenafspraak.service.AfspraakService;
import nl.maakmaareenafspraak.service.MedewerkerService;
import nl.maakmaareenafspraak.web.controller.AbstractController;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import java.time.LocalDate;
import java.util.Map;

import static nl.maakmaareenafspraak.web.security.JwtUtils.SoortUser.BEHEERDER;
import static nl.maakmaareenafspraak.web.security.JwtUtils.SoortUser.MEDEWERKER;

@Slf4j
@CrossOrigin(origins = {"http://localhost:4200", "https://localhost:4200"})
@RestController
@RequestMapping("/dashboard")
public class DashboardController extends AbstractController {
    @Value("${bezkoder.app.jwtSecret}")
    private String jwtSecret;

    @Autowired
    private AfspraakService afspraakService;
    @Autowired
    private MedewerkerService medewerkerService;

    @GetMapping
    public ResponseEntity<Dashboard> dashboard(HttpServletRequest httpServletRequest) {
        var soortUser = getSoortUser(httpServletRequest);
        log.info("{}", soortUser);
        if (!soortUser.equals(MEDEWERKER) && !soortUser.equals(BEHEERDER)) {
            log.info("Geen MEDEWERKER of BEHEERDER, dus niet toegestaan");
            return new ResponseEntity(new Dashboard(), HttpStatus.UNAUTHORIZED);
        }
        var medewerkerOptional = getIngelogdeMedewerker(httpServletRequest);
        if (!medewerkerOptional.isPresent()) {
            log.info("Geen MEDEWERKER of BEHEERDER gevonden, dus niet toegestaan");
            return new ResponseEntity(new Dashboard(), HttpStatus.UNAUTHORIZED);
        }
        var medewerker = medewerkerOptional.get();
        log.info("Dus medewerker/beheerder met id {}", medewerker.getId());

        var dashboard = new Dashboard();

        var salon = medewerker.getSalon();
        Map<LocalDate, Long> aantalActueleAfpsraken = afspraakService.haalAantalAfsprakenPerDag(salon, LocalDate.now());
        Map<LocalDate, Long> gemiddeldAantalAfspraken = afspraakService.haalGemiddeldAantalAfsprakenPerDag(salon, aantalActueleAfpsraken.keySet());

        aantalActueleAfpsraken.keySet().stream().sorted().forEach(localDate -> {
            dashboard.getActueel().add(aantalActueleAfpsraken.get(localDate));
            dashboard.getGemiddeld().add(gemiddeldAantalAfspraken.get(localDate));
        });

        return new ResponseEntity(dashboard, HttpStatus.OK);
    }
}
