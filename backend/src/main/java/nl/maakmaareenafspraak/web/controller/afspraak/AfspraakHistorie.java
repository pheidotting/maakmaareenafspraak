package nl.maakmaareenafspraak.web.controller.afspraak;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.time.LocalDateTime;
import java.util.List;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class AfspraakHistorie {
    public enum SoortWijziging {
        TOEVOEGING, WIJZIGING, VERWIJDERING;
    }

    private LocalDateTime tijdstip;
    private LocalDateTime tijdstipWijziging;
    private String username;
    private String opmerking;
    private String medewerker;
    private SoortWijziging soortWijziging;
    private List<String> behandelingen;
}
