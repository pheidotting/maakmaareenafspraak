package nl.maakmaareenafspraak.web.controller.afspraak;

import lombok.extern.slf4j.Slf4j;
import nl.maakmaareenafspraak.domain.Klant;
import nl.maakmaareenafspraak.service.AfspraakService;
import nl.maakmaareenafspraak.service.KlantService;
import nl.maakmaareenafspraak.web.security.JwtUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

import static org.springframework.http.HttpHeaders.AUTHORIZATION;

@Slf4j
@RestController
@RequestMapping("/afspraak")
@CrossOrigin(origins = {"http://localhost:4200", "https://localhost:4200"})
public class AfspraakController {
    @Autowired
    private AfspraakService afspraakService;
    @Autowired
    private KlantService klantService;
    @Autowired
    private JwtUtils jwtUtils;

    @PostMapping(value = "/opslaanAfspraakMetNietIngelogdeKlant", consumes = "application/json")
    public ResponseEntity<Boolean> opslaan(@RequestBody AfspraakMetNietIngelogdeKlant afspraakMetNietIngelogdeKlant, HttpServletRequest httpServletRequest) {
        if (afspraakMetNietIngelogdeKlant.getVoorkeurMedewerkerId() != null) {
            log.info("Afspraak opslaan bij Salon {}, medewerker {} en behandelingen {}, om {}", afspraakMetNietIngelogdeKlant.getSalonId(), afspraakMetNietIngelogdeKlant.getVoorkeurMedewerkerId(), afspraakMetNietIngelogdeKlant.getBehandelingIds(), afspraakMetNietIngelogdeKlant.getTijdstip());
        } else {
            log.info("Afspraak opslaan bij Salon {}, behandelingen {}, om {}", afspraakMetNietIngelogdeKlant.getSalonId(), afspraakMetNietIngelogdeKlant.getBehandelingIds(), afspraakMetNietIngelogdeKlant.getTijdstip());
        }
        Boolean result = afspraakService.afspraakOpslaan(afspraakMetNietIngelogdeKlant.getTijdstip(),//
                afspraakMetNietIngelogdeKlant.getOpmerking(),//
                afspraakMetNietIngelogdeKlant.getNaam(),//
                afspraakMetNietIngelogdeKlant.getEmailadres(),//
                afspraakMetNietIngelogdeKlant.getTelefoonnummer(), //
                afspraakMetNietIngelogdeKlant.getSalonId(), //
                afspraakMetNietIngelogdeKlant.getVoorkeurMedewerkerId(),//
                afspraakMetNietIngelogdeKlant.getBehandelingIds());
        return new ResponseEntity<>(result, HttpStatus.CREATED);
    }

    @PostMapping("/opslaanAfspraakMetIngelogdeKlant")
    public ResponseEntity<Boolean> opslaan(@RequestBody AfspraakMetIngelogdeKlant afspraakMetIngelogdeKlant, HttpServletRequest httpServletRequest) {
        if (afspraakMetIngelogdeKlant.getVoorkeurMedewerkerId() != null) {
            log.info("Afspraak opslaan bij Salon {}, medewerker {} en behandelingen {}, om {}", afspraakMetIngelogdeKlant.getSalonId(), afspraakMetIngelogdeKlant.getVoorkeurMedewerkerId(), afspraakMetIngelogdeKlant.getBehandelingIds(), afspraakMetIngelogdeKlant.getTijdstip());
        } else {
            log.info("Afspraak opslaan bij Salon {}, behandelingen {}, om {}", afspraakMetIngelogdeKlant.getSalonId(), afspraakMetIngelogdeKlant.getBehandelingIds(), afspraakMetIngelogdeKlant.getTijdstip());
        }

        String jwt = httpServletRequest.getHeader(AUTHORIZATION).replace("Bearer", "").trim();

        var klant = klantService.zoekOpEmailadres(jwtUtils.getUserNameFromJwtToken(jwt)).orElse(new Klant());

        Boolean result = afspraakService.afspraakOpslaan(afspraakMetIngelogdeKlant.getTijdstip(),//
                afspraakMetIngelogdeKlant.getOpmerking(),//
                klant.getId(),//
                afspraakMetIngelogdeKlant.getSalonId(), //
                afspraakMetIngelogdeKlant.getVoorkeurMedewerkerId(),//
                afspraakMetIngelogdeKlant.getBehandelingIds());
        return new ResponseEntity<>(result, HttpStatus.CREATED);
    }

    @GetMapping("/allesVoorKlant")
    public ResponseEntity<List<AfspraakMetHistorie>> alleAfsprakenVoorKlant(HttpServletRequest httpServletRequest) {
        log.info("Ophalen alle Afspraken voor klant");

        String jwt = httpServletRequest.getHeader(AUTHORIZATION).replace("Bearer", "").trim();

        var klant = klantService.zoekOpEmailadres(jwtUtils.getUserNameFromJwtToken(jwt)).orElse(new Klant());

        log.info("Klant id : {}", klant.getId());

        List<AfspraakMetHistorie> result = afspraakService.allesAfsprakenMetHistorie(klant);

        return new ResponseEntity<>(result, HttpStatus.CREATED);
    }
}
