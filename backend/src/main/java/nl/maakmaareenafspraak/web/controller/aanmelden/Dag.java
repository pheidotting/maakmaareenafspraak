package nl.maakmaareenafspraak.web.controller.aanmelden;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class Dag {
    private String code;
    private String name;
    private String id;
}
