package nl.maakmaareenafspraak.web.controller.afspraak;

import lombok.Getter;
import lombok.Setter;

import java.time.LocalDateTime;
import java.util.List;

@Getter
@Setter
public class AfspraakMetNietIngelogdeKlant {
    private String naam;
    private String emailadres;
    private String telefoonnummer;
    private LocalDateTime tijdstip;
    private String opmerking;
    private Long salonId;
    private Long voorkeurMedewerkerId;
    private List<Long> behandelingIds;
}
