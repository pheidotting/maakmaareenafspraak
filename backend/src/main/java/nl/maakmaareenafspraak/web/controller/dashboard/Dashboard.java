package nl.maakmaareenafspraak.web.controller.dashboard;

import lombok.Getter;
import lombok.Setter;

import java.util.ArrayList;
import java.util.List;

@Getter
@Setter
public class Dashboard {
    private List<Long> actueel = new ArrayList<>();
    private List<Long> gemiddeld = new ArrayList<>();
}
