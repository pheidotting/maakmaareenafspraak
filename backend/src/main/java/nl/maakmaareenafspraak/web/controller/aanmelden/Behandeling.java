package nl.maakmaareenafspraak.web.controller.aanmelden;

import lombok.Getter;
import lombok.Setter;

import java.util.ArrayList;
import java.util.List;

@Getter
@Setter
public class Behandeling {
    private String id;
    private String naam;
    private Long tijdsduur;
    private Double prijs;
    private boolean editMode;
    private List<Medewerker> medewerkers = new ArrayList<>();
}
