package nl.maakmaareenafspraak.web.controller.instellingen;

import io.jsonwebtoken.ExpiredJwtException;
import lombok.extern.slf4j.Slf4j;
import nl.maakmaareenafspraak.service.MedewerkerService;
import nl.maakmaareenafspraak.web.controller.AbstractController;
import nl.maakmaareenafspraak.web.domain.Medewerker;
import nl.maakmaareenafspraak.web.mapper.MedewerkerMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import static com.google.common.collect.Lists.newArrayList;


@Slf4j
@RestController
@RequestMapping("/instellingen/medewerkers")
@CrossOrigin(origins = {"http://localhost:4200", "https://localhost:4200"})
public class MedewerkerBeherenController extends AbstractController {
    @Autowired
    private MedewerkerService medewerkerService;
    @Autowired
    private MedewerkerMapper medewerkerMapper;

    @GetMapping("/lijst")
    public ResponseEntity<List<Medewerker>> lees(HttpServletRequest httpServletRequest) {
        Optional<nl.maakmaareenafspraak.domain.Salon> salonDomain;
        try {
            salonDomain = getSalon(httpServletRequest);
        } catch (ExpiredJwtException e) {
            return new ResponseEntity<>(HttpStatus.UNAUTHORIZED);
        }
        if (salonDomain.isPresent()) {
            List<Medewerker> result = salonDomain.get().getMedewerkers().stream().map(medewerkerMapper.mapVanDomainNaarWeb()).collect(Collectors.toList());

            return new ResponseEntity<>(result, HttpStatus.OK);
        } else {
            return new ResponseEntity<>(HttpStatus.UNAUTHORIZED);
        }
    }

    @PostMapping("/opslaan")
    public ResponseEntity<Long> opslaan(@RequestBody Medewerker medewerker, HttpServletRequest httpServletRequest) {
        Optional<nl.maakmaareenafspraak.domain.Salon> salonDomain;
        try {
            salonDomain = getSalon(httpServletRequest);
        } catch (ExpiredJwtException e) {
            return new ResponseEntity<>(HttpStatus.UNAUTHORIZED);
        }
        if (salonDomain.isPresent()) {
            log.info("Opslaan medewerker met id {}", medewerker.getId());
            nl.maakmaareenafspraak.domain.Medewerker medewerkerDomain = null;
            if (medewerker.getId() != null) {
                medewerkerDomain = medewerkerService.lees(medewerker.getId());
                if (!medewerkerDomain.getSalon().getId().equals(salonDomain.get().getId())) {
                    return new ResponseEntity<>(HttpStatus.UNAUTHORIZED);
                }
            } else {
                medewerkerDomain = new nl.maakmaareenafspraak.domain.Medewerker();
            }

            Optional<nl.maakmaareenafspraak.domain.Salon> finalSalonDomain = salonDomain;
            var id = newArrayList(medewerker).stream()//
                    .map(medewerkerMapper.mapVanWebNaarDomain(medewerkerDomain))//
                    .map(mw -> {
                        mw.setSalon(finalSalonDomain.get());
                        return mw;
                    })//
                    .mapToLong(mw -> {
                        medewerkerService.opslaan(mw);
                        return mw.getId();
                    }).findFirst();

            return new ResponseEntity<>(id.getAsLong(), HttpStatus.OK);
        } else {
            return new ResponseEntity<>(HttpStatus.UNAUTHORIZED);
        }
    }

    @DeleteMapping("/verwijder/{id}")
    public ResponseEntity<Boolean> verwijder(@PathVariable("id") Long id, HttpServletRequest httpServletRequest) {
        Optional<nl.maakmaareenafspraak.domain.Salon> salonDomain;
        try {
            salonDomain = getSalon(httpServletRequest);
        } catch (ExpiredJwtException e) {
            return new ResponseEntity<>(HttpStatus.UNAUTHORIZED);
        }
        if (salonDomain.isPresent()) {
            var medewerkerDomain = medewerkerService.lees(id);
            if (!medewerkerDomain.getSalon().getId().equals(salonDomain.get().getId())) {
                return new ResponseEntity<>(HttpStatus.UNAUTHORIZED);
            }
            medewerkerService.verwijder(medewerkerDomain);

            return new ResponseEntity<>(true, HttpStatus.OK);
        } else {
            return new ResponseEntity<>(HttpStatus.UNAUTHORIZED);
        }
    }
}
