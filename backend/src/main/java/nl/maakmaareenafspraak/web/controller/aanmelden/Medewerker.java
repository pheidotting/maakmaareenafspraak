package nl.maakmaareenafspraak.web.controller.aanmelden;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@ToString
@Getter
@Setter
public class Medewerker {
    private String id;
    private String naam;
    private boolean aanwezig;
    private boolean beheerder;
}
