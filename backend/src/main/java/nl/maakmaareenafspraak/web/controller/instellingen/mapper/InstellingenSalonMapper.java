package nl.maakmaareenafspraak.web.controller.instellingen.mapper;

import ma.glasnost.orika.MapperFacade;
import ma.glasnost.orika.MapperFactory;
import ma.glasnost.orika.impl.DefaultMapperFactory;
import nl.maakmaareenafspraak.service.SalonService;
import nl.maakmaareenafspraak.web.controller.instellingen.Salon;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.function.Function;

@Component
public class InstellingenSalonMapper {
    private MapperFacade salonBoundMapper;
    private MapperFacade medewerkerBoundMapper;
    @Autowired
    private SalonService salonService;

    public InstellingenSalonMapper() {
        MapperFactory salonMapperFactory = new DefaultMapperFactory.Builder().build();
        salonMapperFactory.classMap(nl.maakmaareenafspraak.domain.Salon.class, Salon.class)//
                .field("adres.straat", "straat")//
                .field("adres.huisnummer", "huisnummer")//
                .field("adres.postcode", "postcode")//
                .field("adres.plaats", "plaats")//
                .byDefault()//
                .register();
        salonBoundMapper = salonMapperFactory.getMapperFacade();
        MapperFactory medewerkerMapperFactory = new DefaultMapperFactory.Builder().build();
        medewerkerBoundMapper = medewerkerMapperFactory.getMapperFacade();
    }

    public Function<nl.maakmaareenafspraak.domain.Salon, Salon> mapVanDomainNaarWeb() {
        return salon -> salonBoundMapper.map(salon, Salon.class);
    }

    public Function<Salon, nl.maakmaareenafspraak.domain.Salon> mapVanWebNaarDomain(nl.maakmaareenafspraak.domain.Salon s) {
        var salonDomain = s;
        return new Function<Salon, nl.maakmaareenafspraak.domain.Salon>() {
            @Override
            public nl.maakmaareenafspraak.domain.Salon apply(Salon salon) {
                nl.maakmaareenafspraak.domain.Salon salonUit;
                if (s == null || salon.getId() == null) {
                    salonUit = new nl.maakmaareenafspraak.domain.Salon();
                } else if (s != null) {
                    salonUit = s;
                } else {
                    salonUit = salonService.lees(salon.getId());
                }

                salonBoundMapper.map(salon, salonUit);
                return salonUit;
            }
        };
    }
}
