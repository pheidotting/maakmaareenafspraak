package nl.maakmaareenafspraak.web.controller.aanmelden;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class Categorie {
    private String id;
    private String naam;
    private String omschrijving;
    private boolean editMode;
}
