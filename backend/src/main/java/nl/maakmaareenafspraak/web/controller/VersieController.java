package nl.maakmaareenafspraak.web.controller;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.info.BuildProperties;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.PostConstruct;

@RestController
@RequestMapping("/versie")
public class VersieController {
    @Autowired
    BuildProperties buildProperties;

    private String buildVersion;
    private String applicationName;

    @PostConstruct
    private void logVersion() {
        buildVersion = buildProperties.getVersion();
        applicationName = buildProperties.getName();
    }

    @GetMapping
    public ResponseEntity<Info> versie() {
        return new ResponseEntity<>(new Info(buildVersion, applicationName), HttpStatus.ACCEPTED);
    }

    @Getter
    @Setter
    @AllArgsConstructor
    private class Info {
        private String version;
        private String name;
    }
}
