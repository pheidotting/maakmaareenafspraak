package nl.maakmaareenafspraak.web.controller.aanmelden;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class Data {
    private String id;
    private String naam;
    private Long tijdsduur;
    private Double prijs;
    private boolean editMode;
}
