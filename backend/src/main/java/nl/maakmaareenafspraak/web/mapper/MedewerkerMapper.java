package nl.maakmaareenafspraak.web.mapper;

import lombok.extern.slf4j.Slf4j;
import ma.glasnost.orika.MapperFacade;
import ma.glasnost.orika.MapperFactory;
import ma.glasnost.orika.impl.DefaultMapperFactory;
import nl.maakmaareenafspraak.service.MedewerkerService;
import nl.maakmaareenafspraak.web.domain.Medewerker;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.function.Function;

@Slf4j
@Component
public class MedewerkerMapper {
    private MapperFacade medewerkerBoundMapper;
    @Autowired
    private MedewerkerService medewerkerService;

    public MedewerkerMapper() {
        MapperFactory medewerkerMapperFactory = new DefaultMapperFactory.Builder().build();
        medewerkerBoundMapper = medewerkerMapperFactory.getMapperFacade();
    }

    public Function<nl.maakmaareenafspraak.domain.Medewerker, Medewerker> mapVanDomainNaarWeb() {
        return medewerker -> medewerkerBoundMapper.map(medewerker, Medewerker.class);
    }

    public Function<Medewerker, nl.maakmaareenafspraak.domain.Medewerker> mapVanWebNaarDomain(nl.maakmaareenafspraak.domain.Medewerker s) {
        var medewerkerWeb = s;
        return medewerker -> {
            nl.maakmaareenafspraak.domain.Medewerker medewerkerUit = medewerkerWeb;

            medewerkerUit.setNaam(medewerker.getNaam());
            return medewerkerUit;
        };
    }
}
