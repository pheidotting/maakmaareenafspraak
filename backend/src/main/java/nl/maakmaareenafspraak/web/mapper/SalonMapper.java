package nl.maakmaareenafspraak.web.mapper;

import ma.glasnost.orika.MapperFacade;
import ma.glasnost.orika.MapperFactory;
import ma.glasnost.orika.impl.DefaultMapperFactory;
import nl.maakmaareenafspraak.domain.Categorie;
import nl.maakmaareenafspraak.domain.Medewerker;
import nl.maakmaareenafspraak.service.SalonService;
import nl.maakmaareenafspraak.web.domain.Behandeling;
import nl.maakmaareenafspraak.web.domain.Salon;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Collection;
import java.util.Set;
import java.util.function.Function;
import java.util.stream.Stream;

import static java.util.stream.Collectors.toList;

@Component
public class SalonMapper {
    private MapperFacade salonBoundMapper;
    private MapperFacade medewerkerBoundMapper;
    @Autowired
    private SalonService salonService;

    public SalonMapper() {
        MapperFactory salonMapperFactory = new DefaultMapperFactory.Builder().build();
        salonMapperFactory.classMap(nl.maakmaareenafspraak.domain.Salon.class, Salon.class)//
                .field("adres.straat", "straat")//
                .field("adres.huisnummer", "huisnummer")//
                .field("adres.postcode", "postcode")//
                .field("adres.plaats", "plaats")//
                .byDefault()//
                .register();
        salonBoundMapper = salonMapperFactory.getMapperFacade();
        MapperFactory medewerkerMapperFactory = new DefaultMapperFactory.Builder().build();
        medewerkerBoundMapper = medewerkerMapperFactory.getMapperFacade();
    }

    public Function<nl.maakmaareenafspraak.domain.Salon, Salon> mapVanDomainNaarWeb() {
        return salon -> {
            var salonUit = salonBoundMapper.map(salon, Salon.class);
            salonUit.setMedewerkers(salon.getMedewerkers().stream()//
                    .map(medewerker -> medewerkerBoundMapper.map(medewerker, nl.maakmaareenafspraak.web.domain.Medewerker.class)).collect(toList()));
            salonUit.setBehandelingen(salon.getCategories().stream()//
                    .map(Categorie::getBehandelingen)//
                    .flatMap((Function<Set<nl.maakmaareenafspraak.domain.Behandeling>, Stream<nl.maakmaareenafspraak.domain.Behandeling>>) Collection::stream).map(behandeling -> {
                        var behandelingUit = medewerkerBoundMapper.map(behandeling, Behandeling.class);

                        behandelingUit.setMedewerkers(salon.getMedewerkers().stream()//
                                .filter(medewerker -> medewerker.getBehandelingen().stream()//
                                        .anyMatch(b -> b.getId().equals(behandeling.getId())))//
                                .map(Medewerker::getId).collect(toList()));

                        return behandelingUit;
                    }).collect(toList()));

            return salonUit;
        };
    }

    public Function<Salon, nl.maakmaareenafspraak.domain.Salon> mapVanWebNaarDomain(nl.maakmaareenafspraak.domain.Salon s) {
        var salonDomain = s;
        return new Function<Salon, nl.maakmaareenafspraak.domain.Salon>() {
            @Override
            public nl.maakmaareenafspraak.domain.Salon apply(Salon salon) {
                nl.maakmaareenafspraak.domain.Salon salonUit;
                if (s == null || salon.getId() == null) {
                    salonUit = new nl.maakmaareenafspraak.domain.Salon();
                } else if (s != null) {
                    salonUit = s;
                } else {
                    salonUit = salonService.lees(salon.getId());
                }

                salonBoundMapper.map(salon, salonUit);
                return salonUit;
            }
        };
    }
}
