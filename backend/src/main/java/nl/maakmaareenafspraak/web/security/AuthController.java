package nl.maakmaareenafspraak.web.security;

import com.google.auth.oauth2.TokenVerifier;
import lombok.extern.slf4j.Slf4j;
import nl.maakmaareenafspraak.domain.Beheerder;
import nl.maakmaareenafspraak.domain.Klant;
import nl.maakmaareenafspraak.service.InlogbaarService;
import nl.maakmaareenafspraak.service.KlantService;
import nl.maakmaareenafspraak.service.MedewerkerService;
import nl.maakmaareenafspraak.web.domain.Medewerker;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.client.RestTemplate;

import javax.validation.Valid;
import java.util.Base64;
import java.util.Optional;

@CrossOrigin(origins = {"http://localhost:4200", "https://localhost:4200"})
@RestController
@RequestMapping("/auth")
@Slf4j
public class AuthController {
    @Autowired
    AuthenticationManager authenticationManager;

    @Autowired
    PasswordEncoder encoder;

    @Autowired
    JwtUtils jwtUtils;
    @Autowired
    private KlantService klantService;
    @Autowired
    private MedewerkerService medewerkerService;
    @Autowired
    private InlogbaarService inlogbaarService;

    @PostMapping("/wijzigwachtwoord")
    public ResponseEntity<Boolean> wijzigwachtwoord(@Valid @RequestBody WijzigWachtwoordRequest wijzigWachtwoordRequest) {
        log.info("Wijzigen wachtwoord met code {}", wijzigWachtwoordRequest.getWijzigWachtwoordCode());

        var medewerker = medewerkerService.zoekOpWijzigWachtwoordCode(wijzigWachtwoordRequest.getWijzigWachtwoordCode());
        if (medewerker == null) {
            log.info("Medewerker niet gevonden");
            return ResponseEntity.ok(false);
        } else {
            medewerker.setWachtwoord(encoder.encode(wijzigWachtwoordRequest.getWachtwoord()));
            medewerker.setWijzigWachtwoordCode(null);
            medewerker.setWijzigWachtwoordCodeTijd(null);
            medewerkerService.opslaan(medewerker);
            return ResponseEntity.ok(true);
        }
    }

    @PostMapping("/signin")
    public ResponseEntity<JwtResponse> authenticateUser(@Valid @RequestBody LoginRequest loginRequest) {
        log.info("{} inloggen", loginRequest.getUsername());

            var authentication = authenticationManager.authenticate(new UsernamePasswordAuthenticationToken(loginRequest.getUsername(), loginRequest.getPassword()));

        SecurityContextHolder.getContext().setAuthentication(authentication);
        //        var klant = klantService.zoekOpEmailadres(loginRequest.getUsername()).orElse(new Klant());
        var inlogbaar = inlogbaarService.getInlogbaar(loginRequest.getUsername()).orElse(null);
        if (inlogbaar == null) {
            return new ResponseEntity(new JwtResponse(), HttpStatus.UNAUTHORIZED);
        }
        String naam = null;
        JwtUtils.SoortUser soortUser = null;
        if (inlogbaar instanceof Beheerder) {
            naam = ((Beheerder) inlogbaar).getNaam();
            soortUser = JwtUtils.SoortUser.BEHEERDER;
        } else if (inlogbaar instanceof Medewerker) {
            naam = ((Medewerker) inlogbaar).getNaam();
            soortUser = JwtUtils.SoortUser.MEDEWERKER;
        } else if (inlogbaar instanceof Klant) {
            naam = ((Klant) inlogbaar).getNaam();
            soortUser = JwtUtils.SoortUser.KLANT;
        }

        String photo = null;
        if ("patrick@heidotting.nll".equals(loginRequest.getUsername())) {
            photo = "https://tiogatours.nl/dynamic/img/amerika/arizona/grand-canyon-3.39.1506.jpg";
        }

        String jwt = jwtUtils.generateJwtToken(authentication, soortUser, naam, inlogbaar.getUsername(), photo);
        return ResponseEntity.ok(new JwtResponse(jwt));
    }

    //    @PostMapping("/signup")
    //    public ResponseEntity<?> registerUser(@Valid @RequestBody SignupRequest signUpRequest) {
    //        if (userRepository.existsByUsername(signUpRequest.getUsername())) {
    //            return ResponseEntity
    //                    .badRequest()
    //                    .body(new MessageResponse("Error: Username is already taken!"));
    //        }
    //
    //        if (userRepository.existsByEmail(signUpRequest.getEmail())) {
    //            return ResponseEntity
    //                    .badRequest()
    //                    .body(new MessageResponse("Error: Email is already in use!"));
    //        }
    //
    //        // Create new user's account
    //        User user = new User(signUpRequest.getUsername(),
    //                signUpRequest.getEmail(),
    //                encoder.encode(signUpRequest.getPassword()));
    //
    //        Set<String> strRoles = signUpRequest.getRole();
    //        Set<Role> roles = new HashSet<>();
    //
    //        if (strRoles == null) {
    //            Role userRole = roleRepository.findByName(ERole.ROLE_USER)
    //                    .orElseThrow(() -> new RuntimeException("Error: Role is not found."));
    //            roles.add(userRole);
    //        } else {
    //            strRoles.forEach(role -> {
    //                switch (role) {
    //                    case "admin":
    //                        Role adminRole = roleRepository.findByName(ERole.ROLE_ADMIN)
    //                                .orElseThrow(() -> new RuntimeException("Error: Role is not found."));
    //                        roles.add(adminRole);
    //
    //                        break;
    //                    case "mod":
    //                        Role modRole = roleRepository.findByName(ERole.ROLE_MODERATOR)
    //                                .orElseThrow(() -> new RuntimeException("Error: Role is not found."));
    //                        roles.add(modRole);
    //
    //                        break;
    //                    default:
    //                        Role userRole = roleRepository.findByName(ERole.ROLE_USER)
    //                                .orElseThrow(() -> new RuntimeException("Error: Role is not found."));
    //                        roles.add(userRole);
    //                }
    //            });
    //        }
    //
    //        user.setRoles(roles);
    //        userRepository.save(user);
    //
    //        return ResponseEntity.ok(new MessageResponse("User registered successfully!"));
    //    }

    @GetMapping("/viasocialmedia/{provider}/{email}/{picture}/{token}")
    public ResponseEntity<JwtResponse> viaSocialMedia(@PathVariable("provider") String provider, @PathVariable("token") String token, @PathVariable("email") String emailUrl, @PathVariable("picture") String pictureUrl) {
        log.info("JWT uitgeven voor iemand die ingelogd is via {}", provider);
        JwtResponse jwtResponse = null;
        var fbToken = token;
        try {
            String email = null;
            String sub = null;
            String name = null;
            String picture = null;
            if ("GOOGLE".equals(provider)) {
                var tokenVerifier = TokenVerifier.newBuilder().build();
                var jsonWebSignature = tokenVerifier.verify(token);
                email = (String) jsonWebSignature.getPayload().get("email");
                sub = (String) jsonWebSignature.getPayload().get("sub");
                name = (String) jsonWebSignature.getPayload().get("name");
                picture = (String) jsonWebSignature.getPayload().get("picture");
            } else if ("FACEBOOK".equals(provider)) {
                var restTemplate = new RestTemplate();
                ResponseEntity<FBRespone> response = restTemplate.getForEntity("https://graph.facebook.com/me?access_token=" + fbToken, FBRespone.class);
                var fbRespone = response.getBody();
                if (fbRespone != null) {
                    name = fbRespone.getName();
                    sub = fbRespone.getId();
                    email = emailUrl;
                    picture = new String(Base64.getDecoder().decode(pictureUrl));
                }
            }

            Optional<Klant> klantOptional = klantService.zoekOpEmailadres(email);
            Klant klant;
            if (!klantOptional.isPresent()) {
                klant = new Klant();
                klant.setWachtwoord(encoder.encode(sub));
            } else {
                klant = klantOptional.get();
            }
            klant.setEmailadres(email);
            klant.setNaam(name);
            klant.setPhotourl(picture);

            if (klant.getId() == null) {
                log.info("Klant was nog niet bekend, deze gaan we aanmaken");
            } else {
                log.info("Klant met id {} wordt bijgewerkt", klant.getId());
            }
            klantService.opslaan(klant);

            var authentication = authenticationManager.authenticate(new UsernamePasswordAuthenticationToken(email, sub));
            //Laten staan
            log.debug("{}", authentication);

            SecurityContextHolder.getContext().setAuthentication(authentication);
            String jwt = jwtUtils.generateJwtToken(authentication, JwtUtils.SoortUser.KLANT, name, email, picture);
            jwtResponse = new JwtResponse(jwt);

        } catch (TokenVerifier.VerificationException e) {
            log.info("Er ging iets mis {}", e);
            // invalid token
        }

        return ResponseEntity.ok(jwtResponse);
    }
}
