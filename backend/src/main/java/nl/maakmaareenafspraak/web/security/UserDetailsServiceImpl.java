package nl.maakmaareenafspraak.web.security;

import nl.maakmaareenafspraak.service.InlogbaarService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.function.Supplier;

import static com.google.common.collect.Lists.newArrayList;

@Service
public class UserDetailsServiceImpl implements UserDetailsService {
    @Autowired
    private InlogbaarService inlogbaarService;

    @Override
    @Transactional
    public User loadUserByUsername(String username) throws UsernameNotFoundException {
        var inlogbaar = inlogbaarService.getInlogbaar(username).orElseThrow(new Supplier<UsernameNotFoundException>() {
            @Override
            public UsernameNotFoundException get() {
                return new UsernameNotFoundException("User Not Found with username: " + username);
            }
        });

        return new User(username, inlogbaar.getPassword(), newArrayList());
    }

}