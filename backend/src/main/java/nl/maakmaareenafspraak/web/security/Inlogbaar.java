package nl.maakmaareenafspraak.web.security;

public interface Inlogbaar {
    String getUsername();

    void setUsername(String username);

    String getPassword();

    void setPassword(String password);
}
