package nl.maakmaareenafspraak.web.security;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class WijzigWachtwoordRequest {
    private String wijzigWachtwoordCode;
    private String wachtwoord;
}
