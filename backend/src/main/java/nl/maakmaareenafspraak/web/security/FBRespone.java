package nl.maakmaareenafspraak.web.security;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class FBRespone {
    private String name;
    private String id;
}
