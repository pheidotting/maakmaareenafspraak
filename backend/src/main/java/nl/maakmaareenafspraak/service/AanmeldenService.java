package nl.maakmaareenafspraak.service;

import lombok.extern.slf4j.Slf4j;
import nl.maakmaareenafspraak.domain.Beheerder;
import nl.maakmaareenafspraak.domain.Medewerker;
import nl.maakmaareenafspraak.service.aanmelden.*;
import nl.maakmaareenafspraak.web.controller.aanmelden.Aanmelden;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Map;

@Service
@Slf4j
public class AanmeldenService {
    @Autowired
    private SalonOpslaanService salonOpslaanService;
    @Autowired
    private MedewerkersOpslaanService medewerkersOpslaanService;
    @Autowired
    private OpeningstijdOpslaanService openingstijdOpslaanService;
    @Autowired
    private WerktijdenOpslaanService werktijdenOpslaanService;
    @Autowired
    private BehandelingenOpslaanService behandelingenOpslaanService;
    @Autowired
    private MailStuurService mailStuurService;

    @Transactional
    public Long aanmelden(Aanmelden aanmelden) {
        log.info("Aanmelden nieuwe Salon met naam {}", aanmelden.getNaamSalon());

        var salon = salonOpslaanService.salonOpslaan(aanmelden);
        Map<String, Medewerker> idEnMedeweker = medewerkersOpslaanService.opslaanMedewerkers(aanmelden, salon);
        openingstijdOpslaanService.opslaanOpeningstijden(aanmelden, salon);
        werktijdenOpslaanService.opslaanWerktijden(aanmelden, idEnMedeweker);
        behandelingenOpslaanService.opslaanBehandelingen(aanmelden, salon, idEnMedeweker);

        var beheerder = idEnMedeweker.keySet().stream()//
                .filter(s -> idEnMedeweker.get(s) instanceof Beheerder)//
                .map(idEnMedeweker::get)//
                .findFirst().orElse(null);
        if (beheerder == null) {
            log.error("Geen beheerder gevonden bij deze nieuwe Salon, activatiemail kon dus niet worden verstuurd");
        } else {
            mailStuurService.verstuurWachtwoordInstelMail(salon, beheerder);
        }

        return salon.getId();
    }
    //<p>Je activatielink is : <a th:href="http://localhost:4200/aanmelden.html(code=${medewerker.wijzigWachtwoordCode})">klik hier</a></p>
}
