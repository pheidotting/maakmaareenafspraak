package nl.maakmaareenafspraak.service;

import nl.maakmaareenafspraak.domain.Categorie;
import nl.maakmaareenafspraak.repository.CategorieRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class CategorieService {
    @Autowired
    private CategorieRepository categorieRepository;

    public void opslaan(Categorie categorie) {
        categorieRepository.save(categorie);
    }
}
