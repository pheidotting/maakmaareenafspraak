package nl.maakmaareenafspraak.service;

import lombok.extern.slf4j.Slf4j;
import nl.maakmaareenafspraak.domain.*;
import nl.maakmaareenafspraak.repository.AfspraakRepository;
import nl.maakmaareenafspraak.web.controller.afspraak.AfspraakHistorie;
import nl.maakmaareenafspraak.web.controller.afspraak.AfspraakMetHistorie;
import org.hibernate.envers.AuditReader;
import org.hibernate.envers.RevisionType;
import org.hibernate.envers.query.AuditEntity;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.persistence.EntityManager;
import java.math.BigInteger;
import java.time.*;
import java.util.*;

import static java.util.stream.Collectors.toList;
import static java.util.stream.Collectors.toSet;

@Slf4j
@Service
public class AfspraakService {
    @Autowired
    private AfspraakRepository afspraakRepository;
    @Autowired
    private KlantService klantService;
    @Autowired
    private MedewerkerService medewerkerService;
    @Autowired
    private SalonService salonService;
    @Autowired
    private BehandelingService behandelingService;
    @Autowired
    private AfspraakCheckService afspraakCheckService;
    @Autowired
    private AuditReader auditReader;
    @Autowired
    private EntityManager entityManager;
    @Autowired
    private OpeningstijdService openingstijdService;

    public Boolean afspraakOpslaan(LocalDateTime tijdstip, String opmerking, Long klantId, Long salonId, Long voorkeurMedewerkerId, List<Long> behandelingIds) {
        var klant = (Klant) klantService.lees(klantId);

        return afspraakOpslaan(tijdstip, opmerking, klant, salonId, voorkeurMedewerkerId, behandelingIds);
    }

    public Boolean afspraakOpslaan(LocalDateTime tijdstip, String opmerking, String naam, String emailadres, String telefoonnummer, Long salonId, Long voorkeurMedewerkerId, List<Long> behandelingIds) {
        var nietIngelogdeKlant = new NietIngelogdeKlant();
        nietIngelogdeKlant.setNaam(naam);
        nietIngelogdeKlant.setEmailadres(emailadres);
        nietIngelogdeKlant.setTelefoonnummer(telefoonnummer);
        klantService.opslaan(nietIngelogdeKlant);

        return afspraakOpslaan(tijdstip, opmerking, nietIngelogdeKlant, salonId, voorkeurMedewerkerId, behandelingIds);
    }

    private Boolean afspraakOpslaan(LocalDateTime tijdstip, String opmerking, AbstractKlant abstractKlant, Long salonId, Long voorkeurMedewerkerId, List<Long> behandelingIds) {
        Set<Behandeling> behandelingen = behandelingIds.stream().map(id -> behandelingService.lees(id)).collect(toSet());
        var salon = salonService.lees(salonId);
        Medewerker medewerker = null;
        if (voorkeurMedewerkerId != null) {
            medewerker = medewerkerService.lees(voorkeurMedewerkerId);
        }

        var afspraak = new Afspraak(null, tijdstip, opmerking, abstractKlant, salon, medewerker, behandelingen);
        if (afspraakCheckService.isDeTeMakenAfspraakCorrect(afspraak)) {
            log.info("Afspraak accoord");
            afspraakRepository.save(afspraak);
            return true;
        } else {
            log.info("Afspraak niet accoord");
            return false;
        }
    }

    public List<AfspraakMetHistorie> allesAfsprakenMetHistorie(Klant klant) {
        return afspraakRepository.haalAfsprakenOpBijEenKlant(klant).stream()//
                .peek(afspraak -> {
                    afspraak.getSalon().setExtraGeslotens(null);
                    afspraak.getSalon().setExtraOpeningstijden(null);
                })//
                .map(afspraak -> {
                    var afspraakMetHistorie = new AfspraakMetHistorie();

                    if (afspraak.getMedewerker() != null) {
                        afspraakMetHistorie.setMedewerker(Arrays.asList(afspraak.getMedewerker()).stream().map(medewerker -> medewerker == null ? null : medewerker.getNaam()).findFirst().orElse(null));
                    }
                    afspraakMetHistorie.setSalon(Arrays.asList(afspraak.getSalon()).stream().map(Salon::getNaam).findFirst().orElse(null));
                    afspraakMetHistorie.setBehandelingen(afspraak.getBehandelingen().stream().map(Behandeling::getNaam).collect(toList()));
                    afspraakMetHistorie.setId(afspraak.getId());
                    afspraakMetHistorie.setOpmerking(afspraak.getOpmerking());
                    afspraakMetHistorie.setTijdstip(afspraak.getTijdstip());

                    List<Object> results = auditReader.createQuery()//
                            .forRevisionsOfEntity(Afspraak.class, false, false)//
                            .add(AuditEntity.id().eq(afspraak.getId()))//
                            .getResultList();
                    afspraakMetHistorie.setAfspraakHistorieList(results.stream().map(revEntity -> {
                        Object[] c = (Object[]) revEntity;

                        Afspraak a = (Afspraak) c[0];
                        RevEntity r = (RevEntity) c[1];
                        RevisionType t = (RevisionType) c[2];

                        List<Object[]> behandelingObjecten = entityManager.createNativeQuery("select * from afspraak_behandelingen_aud where rev = " + r.getId()).getResultList();
                        List<String> behandelingen = behandelingObjecten.stream().map(objects -> (BigInteger) objects[3]).map(bigInteger -> behandelingService.lees(bigInteger.longValue()).getNaam()).collect(toList());

                        var soortWijziging = AfspraakHistorie.SoortWijziging.TOEVOEGING;
                        if (t == RevisionType.MOD) {
                            soortWijziging = AfspraakHistorie.SoortWijziging.WIJZIGING;
                        } else if (t == RevisionType.DEL) {
                            soortWijziging = AfspraakHistorie.SoortWijziging.VERWIJDERING;
                        }
                        var tijdstipWijziging = LocalDateTime.ofInstant(r.getRevisionDate().toInstant(), ZoneId.of("Europe/Amsterdam"));
                        String medewerker = null;
                        if (afspraak.getMedewerker() != null) {
                            medewerker = Arrays.asList(afspraak.getMedewerker()).stream()//
                                    .map(m -> m == null ? null : m.getNaam()).findFirst().orElse(null);
                        }

                        return new AfspraakHistorie(a.getTijdstip(), tijdstipWijziging, r.getUserid(), a.getOpmerking(), medewerker, soortWijziging, behandelingen);
                    }).collect(toList()));

                    return afspraakMetHistorie;
                }).collect(toList());
    }

    public List<Afspraak> haalAfsprakenOpBijEenKlant(Klant klant) {
        return afspraakRepository.haalAfsprakenOpBijEenKlant(klant);
    }

    public Map<LocalDate, Long> haalAantalAfsprakenPerDag(Salon salon, LocalDate vandaag) {
        List<DayOfWeek> dagenDatSalonOpenIs = openingstijdService.openingstijdenBijSalon(salon).stream()//
                .map(AbstractWerktijd::getDag)//
                .collect(toSet())//
                .stream()//
                .sorted()//
                .collect(toList());

        List<LocalDate> datums = new ArrayList<>();
        var datum = vandaag;
        while (datums.size() < 7) {
            if (dagenDatSalonOpenIs.contains(datum.getDayOfWeek())) {
                datums.add(datum);
            }
            datum = datum.plusDays(1);
        }

        Map<LocalDate, Long> result = new HashMap<>();
        datums.forEach(localDate -> {
            var start = LocalDateTime.of(localDate, LocalTime.of(0, 0));
            var eind = LocalDateTime.of(localDate, LocalTime.of(23, 59));
            result.put(localDate, Long.valueOf(afspraakRepository.haalAfsprakenOpBijSalonTussenTweeTijdstippen(salon, start, eind).size()));
        });

        return result;
    }

    public Map<LocalDate, Long> haalGemiddeldAantalAfsprakenPerDag(Salon salon, Set<LocalDate> datums) {
        List<Afspraak> afspraken = afspraakRepository.haalAfsprakenOpBijEenSalon(salon);

        Map<LocalDate, Long> result = new HashMap<>();

        datums.forEach(localDate -> {
            List<Afspraak> afsprakenDezeDag = afspraken.stream().filter(afspraak -> afspraak.getTijdstip().getDayOfWeek() == localDate.getDayOfWeek()).collect(toList());
            Set<LocalDate> datums1 = afsprakenDezeDag.stream().map(afspraak -> afspraak.getTijdstip().toLocalDate()).collect(toSet());

            result.put(localDate, (long) (afsprakenDezeDag.size() / (datums1.isEmpty() ? 1 : datums1.size())));
        });

        return result;
    }
}
