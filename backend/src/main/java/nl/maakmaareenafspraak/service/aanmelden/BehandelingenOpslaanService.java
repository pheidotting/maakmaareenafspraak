package nl.maakmaareenafspraak.service.aanmelden;

import lombok.extern.slf4j.Slf4j;
import nl.maakmaareenafspraak.domain.Behandeling;
import nl.maakmaareenafspraak.domain.Categorie;
import nl.maakmaareenafspraak.domain.Medewerker;
import nl.maakmaareenafspraak.domain.Salon;
import nl.maakmaareenafspraak.service.BehandelingService;
import nl.maakmaareenafspraak.service.CategorieService;
import nl.maakmaareenafspraak.service.MedewerkerService;
import nl.maakmaareenafspraak.web.controller.aanmelden.Aanmelden;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Map;

@Service
@Slf4j
public class BehandelingenOpslaanService {
    @Autowired
    private CategorieService categorieService;
    @Autowired
    private BehandelingService behandelingService;
    @Autowired
    private MedewerkerService medewerkerService;

    public void opslaanBehandelingen(Aanmelden aanmelden, Salon salon, Map<String, Medewerker> idEnMedeweker) {
        aanmelden.getBehandelingen().stream().forEach(treeNode -> {
            var categorie = new Categorie();
            categorie.setSalon(salon);
            categorie.setNaam(treeNode.getData().getNaam());
            categorie.setOmschrijving(treeNode.getData().getOmschrijving());
            categorieService.opslaan(categorie);
            treeNode.getChildren().stream().forEach(behandelingHolder -> {
                var behandeling = new Behandeling();
                behandeling.setNaam(behandelingHolder.getData().getNaam());
                behandeling.setPrijs(behandelingHolder.getData().getPrijs());
                behandeling.setTijdsduur(behandelingHolder.getData().getTijdsduur());

                behandeling.setCategorie(categorie);
                categorie.getBehandelingen().add(behandeling);
                behandelingService.opslaan(behandeling);

                behandelingHolder.getData().getMedewerkers().stream()//
                        .filter(nl.maakmaareenafspraak.web.controller.aanmelden.Medewerker::isAanwezig)//
                        .map(medewerker -> idEnMedeweker.get(medewerker.getId()))//
                        .forEach(medewerker -> medewerker.getBehandelingen().add(behandeling));
            });
        });
        idEnMedeweker.keySet().stream().forEach(s -> medewerkerService.opslaan(idEnMedeweker.get(s)));
    }
}
