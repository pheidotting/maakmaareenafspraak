package nl.maakmaareenafspraak.service.aanmelden;

import nl.maakmaareenafspraak.domain.Adres;
import nl.maakmaareenafspraak.domain.Salon;
import nl.maakmaareenafspraak.service.SalonService;
import nl.maakmaareenafspraak.web.controller.aanmelden.Aanmelden;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import static nl.maakmaareenafspraak.AdresUtils.isAdresGevuld;

@Service
public class SalonOpslaanService {
    @Autowired
    private SalonService salonService;

    public Salon salonOpslaan(Aanmelden aanmelden) {
        var salon = new Salon();
        salon.setNaam(aanmelden.getNaamSalon());
        salon.setEmailadres(aanmelden.getEmail());
        salon.setTelefoonnummer(aanmelden.getTelefoonnummer());
        var adres = new Adres();
        adres.setStraat(aanmelden.getAdres());
        adres.setHuisnummer(aanmelden.getHuisnummer());
        adres.setPostcode(aanmelden.getPostcode());
        adres.setPlaats(aanmelden.getPlaats());
        if (isAdresGevuld(adres)) {
            adres.setSalon(salon);
            salon.setAdres(adres);
        }

        salonService.opslaan(salon);
        return salon;
    }
}
