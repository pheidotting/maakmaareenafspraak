package nl.maakmaareenafspraak.service.aanmelden;

import nl.maakmaareenafspraak.domain.Werktijd;
import nl.maakmaareenafspraak.service.WerktijdService;
import nl.maakmaareenafspraak.web.controller.aanmelden.Aanmelden;
import nl.maakmaareenafspraak.web.controller.aanmelden.Medewerker;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.DayOfWeek;
import java.util.Map;

@Service
public class WerktijdenOpslaanService {
    @Autowired
    private WerktijdService werktijdService;

    public void opslaanWerktijden(Aanmelden aanmelden, Map<String, nl.maakmaareenafspraak.domain.Medewerker> idEnMedeweker) {
        aanmelden.getDagenMetTijden().stream()//
                .forEach(dagMetTijd -> dagMetTijd.getTijden().stream()//
                        .forEach(tijd -> tijd.getMedewerkers().stream()//
                                .filter(Medewerker::isAanwezig).map(medewerker -> {
                                    var werktijd = new Werktijd();
                                    werktijd.setDag(DayOfWeek.valueOf(dagMetTijd.getDag().getCode()));
                                    werktijd.setStart(tijd.getStart());
                                    werktijd.setEind(tijd.getEind());
                                    werktijd.setMedewerker(idEnMedeweker.get(medewerker.getId()));
                                    return werktijd;
                                }).forEach(werktijdService::opslaan)));
    }
}
