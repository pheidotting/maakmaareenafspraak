package nl.maakmaareenafspraak.service.aanmelden;

import lombok.extern.slf4j.Slf4j;
import nl.maakmaareenafspraak.domain.Beheerder;
import nl.maakmaareenafspraak.domain.Medewerker;
import nl.maakmaareenafspraak.domain.Salon;
import nl.maakmaareenafspraak.service.MedewerkerService;
import nl.maakmaareenafspraak.web.controller.aanmelden.Aanmelden;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.Map;

@Slf4j
@Service
public class MedewerkersOpslaanService {
    @Autowired
    private MedewerkerService medewerkerService;

    public Map<String, Medewerker> opslaanMedewerkers(Aanmelden aanmelden, Salon salon) {
        Map<String, Medewerker> idEnMedeweker = new HashMap<>();
        aanmelden.getMedewerkers().stream()//
                .map(medewerker -> {
                    log.info("Verwerken : {}", medewerker);
                    Medewerker mw;
                    if (medewerker.isBeheerder()) {
                        mw = new Beheerder();
                        medewerkerService.genereerWijzigWachtwoordCode(mw);
                    } else {
                        mw = new Medewerker();
                    }
                    mw.setGebruikersnaam(medewerkerService.genereerGebruikersnaam(salon.getNaam(), medewerker.getNaam()));
                    mw.setSalon(salon);
                    mw.setNaam(medewerker.getNaam());
                    idEnMedeweker.put(medewerker.getId(), mw);
                    return mw;
                })//
                .forEach(medewerkerService::opslaan);
        return idEnMedeweker;
    }
}
