package nl.maakmaareenafspraak.service.aanmelden;

import nl.maakmaareenafspraak.domain.Openingstijd;
import nl.maakmaareenafspraak.domain.Salon;
import nl.maakmaareenafspraak.service.OpeningstijdService;
import nl.maakmaareenafspraak.web.controller.aanmelden.Aanmelden;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.DayOfWeek;

@Service
public class OpeningstijdOpslaanService {
    @Autowired
    private OpeningstijdService openingstijdService;

    public void opslaanOpeningstijden(Aanmelden aanmelden, Salon salon) {
        aanmelden.getDagenMetTijden().stream()//
                .forEach(dagMetTijd -> dagMetTijd.getTijden().stream()//
                        .map(tijd -> {
                            var openingstijd = new Openingstijd();
                            openingstijd.setDag(DayOfWeek.valueOf(dagMetTijd.getDag().getCode()));
                            openingstijd.setStart(tijd.getStart());
                            openingstijd.setEind(tijd.getEind());
                            openingstijd.setSalon(salon);
                            return openingstijd;
                        }).forEach(openingstijdService::opslaan));
    }
}
