package nl.maakmaareenafspraak.service;

import nl.maakmaareenafspraak.domain.Behandeling;
import nl.maakmaareenafspraak.repository.BehandelingRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class BehandelingService {
@Autowired
    private BehandelingRepository behandelingRepository;

public Behandeling lees(Long id){
    return behandelingRepository.getById(id);
}
public void opslaan(Behandeling behandeling){
    behandelingRepository.save(behandeling);
}
}
