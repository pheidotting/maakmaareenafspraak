package nl.maakmaareenafspraak.service;

import nl.maakmaareenafspraak.domain.Openingstijd;
import nl.maakmaareenafspraak.domain.Salon;
import nl.maakmaareenafspraak.repository.OpeningstijdRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class OpeningstijdService {
    @Autowired
    private OpeningstijdRepository openingstijdRepository;

    public void opslaan(Openingstijd openingstijd) {
        openingstijdRepository.save(openingstijd);
    }

    public List<Openingstijd> openingstijdenBijSalon(Salon salon) {
        return openingstijdRepository.openingstijdenBijSalon(salon);
    }

}
