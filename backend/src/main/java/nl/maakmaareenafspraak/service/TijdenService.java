package nl.maakmaareenafspraak.service;

import lombok.extern.slf4j.Slf4j;
import nl.maakmaareenafspraak.domain.Salon;
import nl.maakmaareenafspraak.service.tijden.*;
import nl.maakmaareenafspraak.web.domain.Dag;
import nl.maakmaareenafspraak.web.domain.Tijdstip;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.Set;

import static java.util.stream.Collectors.toList;

@Slf4j
@Service
public class TijdenService {
    public final static int VIJFTIEN = 15;

    @Autowired
    private GenereerTijdenService genereerTijdenService;
    @Autowired
    private ToevoegenExtraOpeningstijdenService toevoegenExtraOpeningstijdenService;
    @Autowired
    private ToevoegenExtraGeslotensService toevoegenExtraGeslotensService;
    @Autowired
    private OmzettenNaarDagService omzettenNaarDagService;
    @Autowired
    private BepaalMedewerkerIdsBijWerktijdenService bepaalMedewerkerIdsBijWerktijdenService;
    @Autowired
    private BepaalMedewerkerIdsBijExtraWerktijdenService bepaalMedewerkerIdsBijExtraWerktijdenService;
    @Autowired
    private VerwijderMedewerkerIdsBijVerlofService verwijderMedewerkerIdsBijVerlofService;
    @Autowired
    private VerwerkAfsprakenService verwerkAfsprakenService;

    public List<Dag> ophalenWerktijden(Salon salon, LocalDate beginDatum, LocalDate eindDatum) {
        var beginTijd = LocalDateTime.of(beginDatum, LocalTime.of(0, 0));
        var eindTijd = LocalDateTime.of(eindDatum, LocalTime.of(23, 59));

        List<LocalDateTime> tijden = genereerTijdenService.genereerTijden(salon, beginDatum, eindDatum);
        tijden.addAll(toevoegenExtraOpeningstijdenService.toevoegenExtraOpeningstijden(salon, beginTijd, eindTijd));
        tijden.removeAll(toevoegenExtraGeslotensService.toevoegenExtraGeslotens(salon, beginTijd, eindTijd));

        Set<Dag> dagen = omzettenNaarDagService.omzettenNaarDagService(tijden);
        dagen = bepaalMedewerkerIdsBijWerktijdenService.bepaalMedewerkerIdsVoorWerktijden(salon, dagen);
        dagen = bepaalMedewerkerIdsBijExtraWerktijdenService.bepaalMedewerkerIdsVoorWerktijden(salon, dagen, beginTijd, eindTijd);
        dagen = verwijderMedewerkerIdsBijVerlofService.verwijderMedewerkerIdsBijVerloven(salon, dagen, beginTijd, eindTijd);
        dagen = verwerkAfsprakenService.verwerkAfspraken(salon, dagen, beginTijd, eindTijd);
        dagen = voegDagenToeZonderTijden(dagen, beginDatum, eindDatum);

        return dagen.stream()//
                .peek(dag -> {
                    if (dag.getDatum().equals(LocalDate.now())) {
                        dag.setTijdstippen(dag.getTijdstippen().stream()//
                                .filter(tijdstip -> tijdstip.getTijd().isAfter(LocalTime.now()))//
                                .collect(toList()));
                    }
                })//
                .peek(dag -> dag.setTijdstippen(dag.getTijdstippen().stream()//
                        .filter(tijdstip -> !tijdstip.getMedewekerIds().isEmpty())//
                        .collect(toList())))//
                .sorted(Comparator.comparing(Dag::getDatum))//
                .peek(dag -> dag.getTijdstippen().sort(Comparator.comparing(Tijdstip::getTijd)))//
                .collect(toList());
    }

    private Set<Dag> voegDagenToeZonderTijden(Set<Dag> dagen, LocalDate beginDatum, LocalDate eindDatum) {
        if (!dagen.isEmpty()) {
            List<LocalDate> alleDatums = new ArrayList<>();
            LocalDate dag = beginDatum;
            while (!dag.isAfter(eindDatum)) {
                alleDatums.add(dag);
                dag = dag.plusDays(1L);
            }
            alleDatums.removeAll(dagen.stream().map(Dag::getDatum).collect(toList()));
            dagen.addAll(alleDatums.stream().map(localDate -> {
                var dag1 = new Dag();
                dag1.setDatum(localDate);
                return dag1;
            }).collect(toList()));
        }
        return dagen;
    }
}
