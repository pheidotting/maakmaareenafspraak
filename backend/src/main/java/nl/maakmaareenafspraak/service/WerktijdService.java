package nl.maakmaareenafspraak.service;

import nl.maakmaareenafspraak.domain.Medewerker;
import nl.maakmaareenafspraak.domain.Werktijd;
import nl.maakmaareenafspraak.repository.WerktijdRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class WerktijdService {
    @Autowired
    private WerktijdRepository werktijdRepository;

    public void opslaan(Werktijd werktijd) {
        werktijdRepository.save(werktijd);
    }

    public List<Werktijd> werktijdenBijMedewerker(List<Medewerker> medewerkers) {
        return werktijdRepository.werktijdenBijMedewerker(medewerkers);
    }

}
