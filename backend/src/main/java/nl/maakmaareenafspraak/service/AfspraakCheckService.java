package nl.maakmaareenafspraak.service;

import lombok.extern.slf4j.Slf4j;
import nl.maakmaareenafspraak.domain.Afspraak;
import nl.maakmaareenafspraak.service.checks.Check;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@Slf4j
public class AfspraakCheckService {
    @Autowired
    private List<Check> checks;

    public boolean isDeTeMakenAfspraakCorrect(Afspraak afspraak) {
        return checks.stream().allMatch(check -> {
            if (!check.isCorrect(afspraak)) {
                log.info("{} niet correct", check.getClass().getSimpleName());
                return false;
            }
            return true;
        });
    }
}
