package nl.maakmaareenafspraak.service;

import nl.maakmaareenafspraak.domain.AbstractKlant;
import nl.maakmaareenafspraak.domain.Klant;
import nl.maakmaareenafspraak.repository.KlantRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class KlantService {
    @Autowired
    private KlantRepository klantRepository;

    public AbstractKlant lees(Long id){
       return klantRepository.getById(id);
    }
    public void opslaan(AbstractKlant klant){
        klantRepository.save(klant);
    }
public Optional<Klant> zoekOpEmailadres(String emailadres){
      return Optional.ofNullable( klantRepository.zoekOpEmailadres(emailadres));
    }
}
