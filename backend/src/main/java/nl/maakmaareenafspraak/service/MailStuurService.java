package nl.maakmaareenafspraak.service;

import lombok.extern.slf4j.Slf4j;
import nl.maakmaareenafspraak.domain.Communicatie;
import nl.maakmaareenafspraak.domain.Medewerker;
import nl.maakmaareenafspraak.domain.Salon;
import nl.maakmaareenafspraak.repository.CommunicatieRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.stereotype.Service;
import org.thymeleaf.TemplateEngine;
import org.thymeleaf.context.Context;

import javax.mail.MessagingException;
import java.time.LocalDateTime;

@Slf4j
@Service
public class MailStuurService {

    @Autowired
    private CommunicatieRepository communicatieRepository;

    @Autowired
    private TemplateEngine templateEngine;

    @Autowired
    private JavaMailSender emailSender;

    public void verstuurWachtwoordInstelMail(Salon salon, Medewerker medewerker) {
        var ctx = new Context();
        ctx.setVariable("medewerker", medewerker);
        ctx.setVariable("salon", salon);

        final String htmlContent = templateEngine.process("activatiemail.html", ctx);

        var message = emailSender.createMimeMessage();

        try {
            var helper = new MimeMessageHelper(message);
            helper.setFrom("Maak Maar Een Afspraak <welkom@maakmaareenafspraak.nl>");
            helper.setTo(salon.getEmailadres());
            helper.setSubject("Welkom bij Maak Maar Een Afspraak");
            helper.setText(htmlContent, true);
        } catch (MessagingException me) {
            log.error("Fout opgetreden bij het verzenden van een mail naar {}, {}", salon.getEmailadres(), me);
        }
        var communicatie = new Communicatie(null, salon.getEmailadres(), "Welkom bij Maak Maar Een Afspraak", LocalDateTime.now(), htmlContent);
        communicatieRepository.save(communicatie);
        try {
            emailSender.send(message);
        } catch (Exception e) {
            log.error("Fout opgetreden bij het verzenden van een mail naar {}, {}", salon.getEmailadres(), e);
        }
    }
}
