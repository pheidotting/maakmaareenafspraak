package nl.maakmaareenafspraak.service.checks;

import lombok.extern.slf4j.Slf4j;
import nl.maakmaareenafspraak.domain.Afspraak;
import nl.maakmaareenafspraak.domain.Behandeling;
import nl.maakmaareenafspraak.domain.Categorie;
import nl.maakmaareenafspraak.service.SalonService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Collection;

import static java.util.stream.Collectors.toList;

@Service
@Slf4j
public class VallenDeBehandelingenWelOnderDezeSalonCheck implements Check {
    @Autowired
    private SalonService salonService;

    @Override
    public boolean isCorrect(Afspraak afspraak) {
        log.info("Check uitvoeren");
        var salon = salonService.lees(afspraak.getSalon().getId());

        return afspraak.getBehandelingen().stream()//
                .map(Behandeling::getId)//
                .allMatch(//
                        behandeling -> salon.getCategories().stream()//
                                .map(Categorie::getBehandelingen)//
                                .flatMap(Collection::stream).map(Behandeling::getId)//
                                .collect(toList())//
                                .contains(behandeling)//
                );
    }
}
