package nl.maakmaareenafspraak.service.checks;

import lombok.extern.slf4j.Slf4j;
import nl.maakmaareenafspraak.domain.Afspraak;
import nl.maakmaareenafspraak.service.MedewerkerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
@Slf4j
public class ValtDeVoorkeurMedewerkerWelOnderDezeSalonCheck implements Check {
    @Autowired
    private MedewerkerService medewerkerService;

    @Override
    public boolean isCorrect(Afspraak afspraak) {
        log.info("Check uitvoeren");

        if (afspraak.getMedewerker() == null) {
            return true;
        }

        var medewerker = medewerkerService.lees(afspraak.getMedewerker().getId());

        return medewerker.getSalon().getId().equals(afspraak.getSalon().getId());
    }
}
