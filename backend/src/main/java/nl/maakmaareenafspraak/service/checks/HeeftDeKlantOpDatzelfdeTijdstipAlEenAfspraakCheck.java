package nl.maakmaareenafspraak.service.checks;

import lombok.extern.slf4j.Slf4j;
import nl.maakmaareenafspraak.domain.Afspraak;
import nl.maakmaareenafspraak.domain.Klant;
import nl.maakmaareenafspraak.domain.NietIngelogdeKlant;
import nl.maakmaareenafspraak.service.AfspraakService;
import nl.maakmaareenafspraak.service.KlantService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

import static nl.maakmaareenafspraak.AfspraakUtils.bepaalEindtijdAfspraak;

@Service
@Slf4j
public class HeeftDeKlantOpDatzelfdeTijdstipAlEenAfspraakCheck implements Check {
    @Autowired
    private KlantService klantService;
    @Autowired
    private AfspraakService afspraakService;

    @Override
    public boolean isCorrect(Afspraak afspraak) {
        log.info("Check uitvoeren");

        var start = afspraak.getTijdstip();
        var eind = bepaalEindtijdAfspraak(afspraak);

        if (afspraak.getKlant() instanceof NietIngelogdeKlant) {
            return true;
        } else {
            var klant = (Klant) klantService.lees(afspraak.getKlant().getId());
            List<Afspraak> afspraken = afspraakService.haalAfsprakenOpBijEenKlant(klant);
            return afspraken.stream().noneMatch(afspraak1 -> {
                var startBestaand = afspraak1.getTijdstip();
                var eindBestaand = bepaalEindtijdAfspraak(afspraak1);

                return !start.isBefore(startBestaand) && start.isBefore(eindBestaand) || eind.isBefore(eindBestaand) && eind.isAfter(startBestaand);
            });
        }
    }
}
