package nl.maakmaareenafspraak.service.checks;

import lombok.extern.slf4j.Slf4j;
import nl.maakmaareenafspraak.domain.Afspraak;
import nl.maakmaareenafspraak.service.TijdenService;
import nl.maakmaareenafspraak.web.domain.Tijdstip;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

import static java.util.stream.Collectors.toList;
import static nl.maakmaareenafspraak.AfspraakUtils.bepaalEindtijdAfspraak;

@Service
@Slf4j
public class CheckTijdBeschikbaarService implements Check {
    @Autowired
    private TijdenService tijdenService;

    @Override
    public boolean isCorrect(Afspraak afspraak) {
        log.info("Check of {} nog beschikbaar is", afspraak.getTijdstip());

        var dag = tijdenService.ophalenWerktijden(afspraak.getSalon(), afspraak.getTijdstip().toLocalDate(), afspraak.getTijdstip().toLocalDate()).stream()//
                .filter(dag1 -> dag1.getDatum().equals(afspraak.getTijdstip().toLocalDate())).findFirst().orElse(null);

        var start = afspraak.getTijdstip().toLocalTime();
        var eind = bepaalEindtijdAfspraak(afspraak).toLocalTime();

        if (dag == null) {
            return false;
        } else {
            List<Tijdstip> tijden = dag.getTijdstippen().stream()//
                    .filter(tijdstip -> tijdstip.getTijd().equals(start) || (tijdstip.getTijd().isBefore(eind) && tijdstip.getTijd().isAfter(start)))//
                    .collect(toList());

            if (tijden.isEmpty()) {
                return false;
            } else {
                if (afspraak.getMedewerker() != null) {
                    log.debug("Met voorkeur medewerker");
                    return tijden.stream().allMatch(tijdstip -> tijdstip.getMedewekerIds().contains(afspraak.getMedewerker().getId()));
                } else {
                    log.debug("Zonder voorkeur medewerker");
                    return tijden.stream().allMatch(tijdstip -> (tijdstip.getMedewekerIds().size() - tijdstip.getAantalAfspraken()) > 0);
                }
            }
        }
    }
}
