package nl.maakmaareenafspraak.service.checks;

import nl.maakmaareenafspraak.domain.Afspraak;
import org.springframework.stereotype.Service;

@Service
public interface Check {
    public abstract boolean isCorrect(Afspraak afspraak);
}
