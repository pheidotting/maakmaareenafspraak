package nl.maakmaareenafspraak.service;

import nl.maakmaareenafspraak.web.security.Inlogbaar;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class InlogbaarService {
    @Autowired
    private MedewerkerService medewerkerService;
    @Autowired
    private KlantService klantService;

    public Optional<? extends Inlogbaar> getInlogbaar(String username) {
        var klant = klantService.zoekOpEmailadres(username);
        if (klant.isPresent()) {
            return klant;
        }
        var medewwerker = medewerkerService.zoekOpGebruikersnaam(username);
        if (medewwerker.isPresent()) {
            return medewwerker;
        }
        return Optional.ofNullable(null);
    }
}
