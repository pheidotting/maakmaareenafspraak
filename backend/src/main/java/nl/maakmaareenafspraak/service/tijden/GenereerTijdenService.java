package nl.maakmaareenafspraak.service.tijden;

import lombok.extern.slf4j.Slf4j;
import nl.maakmaareenafspraak.domain.Openingstijd;
import nl.maakmaareenafspraak.domain.Salon;
import nl.maakmaareenafspraak.repository.OpeningstijdRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.util.*;

import static java.util.stream.Collectors.toList;
import static nl.maakmaareenafspraak.service.TijdenService.VIJFTIEN;

@Service
@Slf4j
public class GenereerTijdenService {
    @Autowired
    private OpeningstijdRepository openingstijdRepository;

    public List<LocalDateTime> genereerTijden(Salon salon, LocalDate beginDatum, LocalDate eindDatum) {
        log.info("genereerTijden voor salon met id {}, beginDatum {} en eindDatum {}", salon.getId(), beginDatum, eindDatum);
        List<Openingstijd> openingstijden = openingstijdRepository.openingstijdenBijSalon(salon);

        Set<LocalDateTime> tijden = new HashSet<>();
        var start = beginDatum;
        while (!start.isAfter(eindDatum)) {
            LocalDate finalStartDatum = start;
            tijden.addAll(openingstijden.stream()//
                    .filter(openingstijd -> openingstijd.getDag().equals(finalStartDatum.getDayOfWeek()))//
                    .map(openingstijd -> {
                        List<LocalDateTime> result = new ArrayList<>();
                        LocalTime tijd = openingstijd.getStart();
                        while (tijd.isBefore(openingstijd.getEind())) {
                            result.add(LocalDateTime.of(finalStartDatum, tijd));
                            tijd = tijd.plusMinutes(VIJFTIEN);
                        }
                        return result;
                    })//
                    .flatMap(Collection::stream).collect(toList()));
            start = start.plusDays(1L);
        }

        return tijden.stream().sorted().collect(toList());
    }
}
