package nl.maakmaareenafspraak.service.tijden;

import lombok.extern.slf4j.Slf4j;
import nl.maakmaareenafspraak.domain.ExtraGesloten;
import nl.maakmaareenafspraak.domain.Salon;
import nl.maakmaareenafspraak.repository.ExtraGeslotenRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import static com.google.common.collect.Lists.newArrayList;
import static java.util.stream.Collectors.toList;
import static nl.maakmaareenafspraak.service.TijdenService.VIJFTIEN;

@Slf4j
@Service
public class ToevoegenExtraGeslotensService {

    @Autowired
    private ExtraGeslotenRepository extraGeslotenRepository;

    public List<LocalDateTime> toevoegenExtraGeslotens(Salon salon, LocalDateTime beginTijd, LocalDateTime eindTijd) {
        log.info("Toevoegen Extra Openingstijden voor Salon met id {}, beginTijd {} en eindTijd {}", salon, beginTijd, eindTijd);

        List<ExtraGesloten> extraGeslotens = extraGeslotenRepository.extraGeslotenenBijMedewerker(salon, beginTijd, eindTijd);

        if (!extraGeslotens.isEmpty()) {
            return extraGeslotens.stream()//
                    .map(extraGesloten -> {
                        List<LocalDateTime> result = new ArrayList<>();
                        LocalDateTime tijd = extraGesloten.getStart();
                        while (tijd.isBefore(extraGesloten.getEind())) {
                            result.add(tijd);
                            tijd = tijd.plusMinutes(VIJFTIEN);
                        }
                        return result;
                    })//
                    .flatMap(Collection::stream).collect(toList());
        }
        return newArrayList();
    }
}
