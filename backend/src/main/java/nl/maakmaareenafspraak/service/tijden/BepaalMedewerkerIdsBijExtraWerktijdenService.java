package nl.maakmaareenafspraak.service.tijden;

import lombok.extern.slf4j.Slf4j;
import nl.maakmaareenafspraak.domain.ExtraWerktijd;
import nl.maakmaareenafspraak.domain.Salon;
import nl.maakmaareenafspraak.repository.ExtraWerktijdRepository;
import nl.maakmaareenafspraak.web.domain.Dag;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Set;

import static com.google.common.collect.Lists.newArrayList;
import static java.util.stream.Collectors.toSet;

@Slf4j
@Service
public class BepaalMedewerkerIdsBijExtraWerktijdenService {
    @Autowired
    private ExtraWerktijdRepository extraWerktijdRepository;

    public Set<Dag> bepaalMedewerkerIdsVoorWerktijden(Salon salon, Set<Dag> dagen, LocalDateTime beginTijd, LocalDateTime eindTijd) {
        log.info("Bepaal medewerkerIds voor Salon met id {} en {} dagen", salon.getId(), dagen.size());
        List<ExtraWerktijd> extraWerktijden = extraWerktijdRepository.extraWerktijdenBijMedewerker(newArrayList(salon.getMedewerkers()), beginTijd, eindTijd);

        if (!extraWerktijden.isEmpty()) {
            dagen.stream()//
                    .forEach(dag -> dag.getTijdstippen().stream()//
                            .forEach(tijdstip -> tijdstip.getMedewekerIds()//
                                    .addAll(bepaalMedewerkerIdsVoorExtraWerktijden(extraWerktijden, LocalDateTime.of(dag.getDatum(), tijdstip.getTijd())))));
        }

        return dagen;
    }

    private List<Long> bepaalMedewerkerIdsVoorExtraWerktijden(List<ExtraWerktijd> werktijden, LocalDateTime localDateTime) {
        return newArrayList(werktijden.stream()//
                .filter(werktijd -> !werktijd.getStart().isAfter(localDateTime) &&//
                        !werktijd.getEind().isBefore(localDateTime))//
                .map(werktijd -> werktijd.getMedewerker().getId()).collect(toSet()));
    }
}
