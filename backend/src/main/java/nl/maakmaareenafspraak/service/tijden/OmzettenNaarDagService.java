package nl.maakmaareenafspraak.service.tijden;

import lombok.extern.slf4j.Slf4j;
import nl.maakmaareenafspraak.web.domain.Dag;
import nl.maakmaareenafspraak.web.domain.Tijdstip;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import static java.util.stream.Collectors.toList;

@Slf4j
@Service
public class OmzettenNaarDagService {

    public Set<Dag> omzettenNaarDagService(List<LocalDateTime> tijden) {
        log.info("{} tijden omzetten naar dag", tijden);

        Set<Dag> dagen = new HashSet<>();

        tijden.stream().forEach(localDateTime -> {
            var dag = dagen.stream()//
                    .filter(dag1 -> dag1.getDatum().equals(localDateTime.toLocalDate()))//
                    .findFirst()//
                    .orElse(new Dag());

            dag.setDatum(localDateTime.toLocalDate());
            dag.setTijdstippen(tijden.stream()//
                    .filter(localDateTime12 -> localDateTime12.toLocalDate().equals(dag.getDatum()))//
                    .map(localDateTime1 -> {
                        var tijdstip = new Tijdstip();
                        tijdstip.setTijd(localDateTime1.toLocalTime());
                        return tijdstip;
                    })//
                    .collect(toList()));

            dagen.add(dag);
        });

        return dagen;
    }
}
