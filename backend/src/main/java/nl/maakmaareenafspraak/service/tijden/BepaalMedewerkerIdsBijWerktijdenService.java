package nl.maakmaareenafspraak.service.tijden;

import lombok.extern.slf4j.Slf4j;
import nl.maakmaareenafspraak.domain.Salon;
import nl.maakmaareenafspraak.domain.Werktijd;
import nl.maakmaareenafspraak.repository.WerktijdRepository;
import nl.maakmaareenafspraak.web.domain.Dag;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Set;

import static com.google.common.collect.Lists.newArrayList;
import static java.util.stream.Collectors.toSet;

@Slf4j
@Service
public class BepaalMedewerkerIdsBijWerktijdenService {
    @Autowired
    private WerktijdRepository werktijdRepository;

    public Set<Dag> bepaalMedewerkerIdsVoorWerktijden(Salon salon, Set<Dag> dagen) {
        log.info("Bepaal medewerkerIds voor Salon met id {} en {} dagen", salon.getId(), dagen.size());
        List<Werktijd> werktijden = werktijdRepository.werktijdenBijMedewerker(newArrayList(salon.getMedewerkers()));

        if (!werktijden.isEmpty()) {
            return dagen.stream()//
                    .map(dag -> {
                        dag.getTijdstippen().stream().forEach(tijdstip -> tijdstip.setMedewekerIds(bepaalMedewerkerIds(werktijden, LocalDateTime.of(dag.getDatum(), tijdstip.getTijd()))));
                        return dag;
                    }).collect(toSet());
        }
        return dagen;
    }

    private List<Long> bepaalMedewerkerIds(List<Werktijd> werktijden, LocalDateTime localDateTime) {
        return newArrayList(werktijden.stream()//
                .filter(werktijd -> localDateTime.getDayOfWeek().equals(werktijd.getDag()))//
                .filter(werktijd -> !werktijd.getStart().isAfter(localDateTime.toLocalTime()) &&//
                        !werktijd.getEind().isBefore(localDateTime.toLocalTime()))//
                .map(werktijd -> werktijd.getMedewerker().getId()).collect(toSet()));
    }

}
