package nl.maakmaareenafspraak.service.tijden;

import lombok.extern.slf4j.Slf4j;
import nl.maakmaareenafspraak.domain.Afspraak;
import nl.maakmaareenafspraak.domain.Behandeling;
import nl.maakmaareenafspraak.domain.Salon;
import nl.maakmaareenafspraak.repository.AfspraakRepository;
import nl.maakmaareenafspraak.web.domain.Dag;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Set;

import static nl.maakmaareenafspraak.AfspraakUtils.bepaalEindtijdAfspraak;

@Slf4j
@Service
public class VerwerkAfsprakenService {
    @Autowired
    private AfspraakRepository afspraakRepository;

    public Set<Dag> verwerkAfspraken(Salon salon, Set<Dag> dagen, LocalDateTime beginTijd, LocalDateTime eindTijd) {
        List<Afspraak> afspraken = afspraakRepository.haalAfsprakenOp(salon, beginTijd, eindTijd);
        log.info("Verwerken {} bestaande afspraken", afspraken.size());

        if (!afspraken.isEmpty()) {
            afspraken.stream()//
                    .filter(afspraak -> afspraak.getMedewerker() != null)//
                    .forEach(afspraak -> {
                        long totaleTijdsduur = afspraak.getBehandelingen().stream()//
                                .mapToLong(Behandeling::getTijdsduur).sum();
                        log.info("totaleTijdsduur {}", totaleTijdsduur);
                        var start = afspraak.getTijdstip().toLocalTime();
                        var eind = afspraak.getTijdstip().toLocalTime().plusMinutes(totaleTijdsduur * 15);

                        dagen.stream()//
                                .filter(dag12 -> dag12.getDatum().equals(afspraak.getTijdstip().toLocalDate()))//
                                .findFirst()//
                                .orElse(null).getTijdstippen()//
                                .stream()//
                                .filter(tijdstip -> tijdstip.getTijd().equals(start) || (tijdstip.getTijd().isAfter(start) && tijdstip.getTijd().isBefore(eind)))//
                                .forEach(tijdstip -> tijdstip.getMedewekerIds().remove(afspraak.getMedewerker().getId()));

                    });
            afspraken.stream()//
                    .filter(afspraak -> afspraak.getMedewerker() == null)//
                    .forEach(afspraak -> {
                        var start = afspraak.getTijdstip().toLocalTime();
                        var eind = bepaalEindtijdAfspraak(afspraak).toLocalTime();

                        dagen.stream()//
                                .filter(dag12 -> dag12.getDatum().equals(afspraak.getTijdstip().toLocalDate()))//
                                .findFirst()//
                                .orElse(null).getTijdstippen()//
                                .stream()//
                                .forEach(tijdstip -> tijdstip.setAantalAfspraken(tijdstip.getAantalAfspraken() + 1));
                    });
        }
        return dagen;
    }
}
