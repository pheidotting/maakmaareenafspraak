package nl.maakmaareenafspraak.service.tijden;

import lombok.extern.slf4j.Slf4j;
import nl.maakmaareenafspraak.domain.ExtraOpeningstijd;
import nl.maakmaareenafspraak.domain.Salon;
import nl.maakmaareenafspraak.repository.ExtraOpeningstijdRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import static com.google.common.collect.Lists.newArrayList;
import static java.util.stream.Collectors.toList;
import static nl.maakmaareenafspraak.service.TijdenService.VIJFTIEN;

@Slf4j
@Service
public class ToevoegenExtraOpeningstijdenService {
    @Autowired
    private ExtraOpeningstijdRepository extraOpeningstijdRepository;

    public List<LocalDateTime> toevoegenExtraOpeningstijden(Salon salon, LocalDateTime beginTijd, LocalDateTime eindTijd) {
        log.info("Toevoegen Extra Openingstijden voor Salon met id {}, beginTijd {} en eindTijd {}", salon, beginTijd, eindTijd);
        List<ExtraOpeningstijd> extraOpeningstijden = extraOpeningstijdRepository.extraOpeningstijdenBijSalon(salon, beginTijd, eindTijd);

        if (!extraOpeningstijden.isEmpty()) {
            return extraOpeningstijden.stream()//
                    .map(extraOpeningstijd -> {
                        List<LocalDateTime> result = new ArrayList<>();
                        LocalDateTime tijd = extraOpeningstijd.getStart();
                        while (tijd.isBefore(extraOpeningstijd.getEind())) {
                            result.add(tijd);
                            tijd = tijd.plusMinutes(VIJFTIEN);
                        }
                        return result;
                    })//
                    .flatMap(Collection::stream).collect(toList());
        }
        return newArrayList();
    }
}
