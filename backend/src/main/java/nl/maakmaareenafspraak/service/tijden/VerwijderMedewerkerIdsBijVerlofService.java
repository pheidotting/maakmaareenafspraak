package nl.maakmaareenafspraak.service.tijden;

import lombok.extern.slf4j.Slf4j;
import nl.maakmaareenafspraak.domain.Salon;
import nl.maakmaareenafspraak.domain.Verlof;
import nl.maakmaareenafspraak.repository.VerlofRepository;
import nl.maakmaareenafspraak.web.domain.Dag;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Set;

import static com.google.common.collect.Lists.newArrayList;
import static java.util.stream.Collectors.toSet;

@Slf4j
@Service
public class VerwijderMedewerkerIdsBijVerlofService {
    @Autowired
    private VerlofRepository verlofRepository;

    public Set<Dag> verwijderMedewerkerIdsBijVerloven(Salon salon, Set<Dag> dagen, LocalDateTime beginTijd, LocalDateTime eindTijd) {
        log.info("Verwijder medewerkerIds bij verloven voor Salon met id {} en {} dagen, beginTijd : {}, eindTijd : {}", salon.getId(), dagen.size(), beginTijd, eindTijd);
        List<Verlof> verloven = verlofRepository.verlovenBijMedewerker(newArrayList(salon.getMedewerkers()), beginTijd, eindTijd);

        if (!verloven.isEmpty()) {
            return dagen.stream()//
                    .map(dag -> {
                        dag.getTijdstippen().stream()//
                                .map(tijdstip -> {
                                    tijdstip.getMedewekerIds().removeAll(VerwijderMedewerkerIdsBijVerlofService.this.bepaalMedewerkerIdsVoorVerloven(verloven, LocalDateTime.of(dag.getDatum(), tijdstip.getTijd())));
                                    return tijdstip;
                                });

                        return dag;
                    }).collect(toSet());
        }
        return dagen;
    }

    private List<Long> bepaalMedewerkerIdsVoorVerloven(List<Verlof> verloven, LocalDateTime localDateTime) {
        return newArrayList(verloven.stream()//
                .filter(werktijd -> !werktijd.getStart().isAfter(localDateTime) &&//
                        !werktijd.getEind().isBefore(localDateTime))//
                .map(werktijd -> werktijd.getMedewerker().getId()).collect(toSet()));
    }
}
