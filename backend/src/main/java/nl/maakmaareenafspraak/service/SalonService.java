package nl.maakmaareenafspraak.service;

import lombok.extern.slf4j.Slf4j;
import nl.maakmaareenafspraak.domain.Salon;
import nl.maakmaareenafspraak.repository.AdresRepository;
import nl.maakmaareenafspraak.repository.SalonRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Slf4j
@Service
public class SalonService {
    @Autowired
    private AdresRepository adresRepository;
    @Autowired
    private SalonRepository salonRepository;

    public List<Salon> alles() {
        return salonRepository.findAll();
    }

    public Salon lees(Long id) {
        return salonRepository.getById(id);
    }

    public void verwijderen(Long id) {
        var salon = salonRepository.getById(id);
        salonRepository.delete(salon);
    }

    public Long opslaan(Salon salon) {
        if (salon.getAdres() != null) {
            log.info("Adres zetten {}", salon.getAdres());
            salon.getAdres().setSalon(salon);
        }
        log.info("Opslaan {}", salon);
        salonRepository.save(salon);
        return salon.getId();
    }
}
