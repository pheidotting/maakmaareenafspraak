package nl.maakmaareenafspraak.service;

import lombok.extern.slf4j.Slf4j;
import nl.maakmaareenafspraak.domain.Medewerker;
import nl.maakmaareenafspraak.repository.MedewerkerRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.Optional;
import java.util.UUID;

@Slf4j
@Service
public class MedewerkerService {
    @Autowired
    private MedewerkerRepository medewerkerRepository;

    public Medewerker lees(Long id) {
        return medewerkerRepository.getById(id);
    }

    public Optional<Medewerker> zoekOpGebruikersnaam(String gebruikersnaam) {
        return Optional.ofNullable(medewerkerRepository.zoekOpGebruikersnaam(gebruikersnaam));
    }

    public void opslaan(Medewerker medewerker) {
        medewerkerRepository.save(medewerker);
    }

    public void verwijder(Medewerker medewerker) {
        medewerkerRepository.delete(medewerker);
    }

    public void genereerWijzigWachtwoordCode(Medewerker medewerker) {
        var code = UUID.randomUUID().toString() + UUID.randomUUID().toString();
        medewerker.setWijzigWachtwoordCode(code.replace("-", "").substring(0, 30));
        medewerker.setWijzigWachtwoordCodeTijd(LocalDateTime.now());
    }

    public Medewerker zoekOpWijzigWachtwoordCode(String wijzigWachtwoordCode) {
        return medewerkerRepository.zoekOpWijzigWachtwoordCode(wijzigWachtwoordCode);
    }

    public String genereerGebruikersnaam(String salon, String medewerker) {
        var samengevoegd = salon.replace(".", "") + "." + medewerker.replace(".", "");
        var result = samengevoegd.toLowerCase().replace(" ", "").replace("&", "").replace(",", "");
        if (result.length() > 100) {
            result = result.substring(0, 100);
        }

        if (medewerkerRepository.zoekOpGebruikersnaam(result) != null) {
            var gegenereerd = UUID.randomUUID().toString().replace("-", "").substring(0, 10);
            log.error("Gebruikersnaam {}, komt al voor, ik genereer nu het volgende {}", result, gegenereerd);
            result = gegenereerd;
        }

        return result;
    }
}
