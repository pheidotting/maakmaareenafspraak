describe('Aanmeld Wizard', () => {
  it('Invullen naam en adres', () => {
    cy.visit('http://localhost:4200/aanmelden.html');

    // cy.get('#naarStap2').should('be.disabled');

    cy.get('#naamSalon').type('Salon Sabine');
    // cy.get('#naarStap2').should('be.disabled');

    // cy.get('#adres').type('Boogschutter 26');
  })
  it('Controleer automatisch invullen adres', () => {
    cy.get('#postcode').clear();
    cy.get('#postcode').type('7891tn');
    cy.get('#huisnummer').clear();
    cy.get('#huisnummer').type('26');
    cy.get('#plaats').focus();
    cy.get('#postcode').should('have.value', '7891TN');
    cy.get('#adres').should('have.value', 'Boogschutter');
    cy.get('#plaats').should('have.value', 'Klazienaveen');
  });
  it('invullen rest van stap 1', () => {
    cy.get('#telefoonnummer').type('0638751109');
    cy.get('#email').type('info@salon.nl');
    // cy.get('#naarStap2').should('be.disabled');
    cy.get('#naamEigenaar').type('Sabine Heidotting');
    cy.get('#plaats').focus();//anders wordt de eerste medewerker niet in de lijst met medewerkers geplaatst
    // cy.get('#naarStap2').should('not.be.disabled');
  })
  it('Naar stap 3', () => {
    cy.get('#naarStap2').click();
  })
  it('Stap 3, check of alle initiele velden er zijn', () => {
  })
  it('Naar stap 4', () => {
    cy.get('#naarStap4').click();
  })
  it('Testen stap 4', () => {
    cy.window().should('not.have.property', '6-naam-edit');
    cy.window().should('not.have.property', '6-naam-display');
    cy.window().should('not.have.property', '7-naam-edit');
    cy.window().should('not.have.property', '7-naam-display');
    cy.get('#categorieToevoegen').click();
    cy.window().should('have.property', '6-naam-edit');
    cy.window().should('have.property', '7-naam-edit');
    cy.window().should('have.property', '7-prijs-edit');
    cy.window().should('have.property', '7-tijdsduur-edit');
    cy.get('#6-naam-edit').should('have.attr', 'ng-reflect-model', 'Nieuwe Categorie');
    cy.get('#7-naam-edit').should('have.attr', 'ng-reflect-model', 'Nieuwe behandeling');
    cy.get('#6-naam-edit').type('Cat Nummer 6');
    cy.get('#7-naam-edit').type('Beh Nummer 7');
    cy.get('#7-tijdsduur-edit').should('have.attr', 'ng-reflect-model', '0');
    cy.get('#7-prijs-edit').should('have.attr', 'ng-reflect-model', '0');
    cy.get('#7-tijdsduur-edit').type('1');
    // cy.get('#7-prijs-edit').type('1');
    // cy.get('#7-tijdsduur-edit').should('have.attr', 'ng-reflect-model', '0');
    // cy.get('#7-tijdsduur-edit').type('16');
    // cy.get('#7-prijs-edit').type('12');
    // cy.get('#7-tijdsduur-edit').should('have.attr', 'ng-reflect-model', '15');
    // cy.get('#7-opslaan').click();
    // cy.window().should('not.have.property', '7-naam-edit');
    // cy.window().should('not.have.property', '7-prijs-edit');
    // cy.window().should('not.have.property', '7-tijdsduur-edit');
    // cy.window().should('have.property', '7-naam-display');


  })
  it('Bevestigen', () => {
    cy.get('#bevestigenButton').click();

    cy.intercept(
      {
        method: 'POST',
        url: '/aanmelden',
      },
      []
    ).as('aanmelden');

    cy.get('#bevestigenJa').click();

    cy.wait('@aanmelden').then((interception) => {
      assert.isNotNull(interception.request.body, '1st API call has data');

      var s = interception.request.body;
      assert.equal(s.naamSalon, "Salon Sabine");
      assert.equal(s.adres, "Boogschutter");
      assert.equal(s.huisnummer, "26");
      assert.equal(s.postcode, "7891TN");
      assert.equal(s.plaats, "Klazienaveen");
      assert.equal(s.telefoonnummer, "0638751109");
      assert.equal(s.email, "info@salon.nl");

      assert.equal(s.medewerkers.length, 1);
      assert.equal(s.medewerkers[0].naam, 'Sabine Heidotting');
    })
  })
})
