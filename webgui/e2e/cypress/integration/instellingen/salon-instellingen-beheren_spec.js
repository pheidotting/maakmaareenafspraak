describe('Beheren Salon Instellingen Wizard', () => {
    const leesSalonResponse = {
      "id": 1,
      "naam": "Patricks Haarmodesalon",
      "straat": "straat",
      "huisnummer": "12",
      "postcode": "1111AA",
      "plaats": "Klazienaveen",
      "telefoonnummer": "0638751109",
      "emailadres": "patrick@heidotting.nl"
    }
  it('Test', () => {

    cy.intercept('/instellingen/beherensalon/lees', leesSalonResponse);
    // cy.intercept(
    //     {
    //       method: 'GET',
    //       url: '/salon/lees',
    //     },
    //     []
    //   ).as('leesSalon');

    cy.visit('http://localhost:4200/instellingen/salon.html');


    // cy.wait('@leesSalon').then((interception) => {
    //   interception.response.body = '';
    // assert.isNotNull(interception.request.body, '1st API call has data');
    // })
  });
  it('Controleer voorgedefinieerde velden', () => {
    cy.get('#naamSalon').should('have.value', leesSalonResponse.naam);
    cy.get('#postcode').should('have.value', leesSalonResponse.postcode);
    cy.get('#huisnummer').should('have.value', leesSalonResponse.huisnummer);
    cy.get('#adres').should('have.value', leesSalonResponse.straat);
    cy.get('#plaats').should('have.value', leesSalonResponse.plaats);
    cy.get('#telefoonnummer').should('have.value', leesSalonResponse.telefoonnummer);
    cy.get('#email').should('have.value', leesSalonResponse.emailadres);
  });
  it('Postcode testen', () => {
    cy.get('#postcode').clear();
    cy.get('#postcode').type('a');
    cy.get('#plaats').focus();
    cy.get('#postcode').should('have.value', 'A');

    cy.get('#postcode').clear();
    cy.get('#postcode').type(' 1 1 1 1 A a ');
    cy.get('#plaats').focus();
    cy.get('#postcode').should('have.value', '1111AA');

    cy.get('#postcode').clear();
    cy.get('#postcode').type(' 1 1 1 1 A a A ');
    cy.get('#plaats').focus();
    cy.get('#postcode').should('have.value', '1111AA');
  });
  it('Controleer automatisch invullen adres', () => {
    cy.get('#postcode').clear();
    cy.get('#postcode').type('7891tn');
    cy.get('#huisnummer').clear();
    cy.get('#huisnummer').type('26');
    cy.get('#plaats').focus();
    cy.get('#adres').should('have.value', 'Boogschutter');
    cy.get('#plaats').should('have.value', 'Klazienaveen');
  });
  it('Controleer automatisch invullen adres', () => {
    cy.get('#naamSalon').clear();
    cy.get('#naamSalon').type('Nieuwe Salon Naam');
    cy.get('#telefoonnummer').clear();
    cy.get('#telefoonnummer').type('Nieuwe Telefoonnummer');
    cy.get('#email').clear();
    cy.get('#email').type('nieuw@email.nl');

    cy.intercept(
      {
        method: 'POST',
        url: '/instellingen/beherensalon/opslaan',
      },
      []
    ).as('opslaan');

    cy.window().should('not.have.property', 'messageOpgeslagen');
    cy.get('#opslaan').click();

    cy.wait('@opslaan').then((interception) => {
      cy.window().should('have.property', 'messageOpgeslagen');
      assert.isNotNull(interception.request.body, '1st API call has data');

      var s = interception.request.body;
      assert.equal("Nieuwe Salon Naam", s.naam);
      assert.equal("Boogschutter", s.straat);
      assert.equal("26", s.huisnummer);
      assert.equal("7891TN", s.postcode);
      assert.equal("Klazienaveen", s.plaats);
      assert.equal("Nieuwe Telefoonnummer", s.telefoonnummer);
      assert.equal("nieuw@email.nl", s.emailadres);
    });
  });
})
