describe('Beheren Medewerkers', () => {
  const medewerkersResponse = [{
    "id": 1,
    "naam": "Medewerker 1"
  }, {
    "id": 2,
    "naam": "Medewerker 2"
  }
  ]
  it('Test', () => {
    cy.intercept('/instellingen/medewerkers/lijst', medewerkersResponse);
    cy.visit('http://localhost:4200/instellingen/medewerkers.html');
  });
  it('Controleer voorgedefinieerde velden', () => {
    cy.window().should('have.property', 'displayMedewerker0');
    cy.window().should('have.property', 'displayMedewerker1');
    cy.window().should('not.have.property', 'editMedewerker0');
    cy.window().should('not.have.property', 'editMedewerker1');
    cy.get('#displayMedewerker0').should('have.text', 'Medewerker 1');
    cy.get('#displayMedewerker1').should('have.text', 'Medewerker 2');
  });
  it('Wijzig medewerker 1', () => {
    cy.get('#bewerkMedewerker0').click();
    cy.window().should('not.have.property', 'displayMedewerker0');
    cy.window().should('have.property', 'displayMedewerker1');
    cy.window().should('have.property', 'editMedewerker0');
    cy.window().should('not.have.property', 'editMedewerker1');
    //annuleren knop testen
    cy.get('#editMedewerker0').clear();
    cy.get('#editMedewerker0').type('ABC');
    cy.get('#cancelMedewerker0').click();
    cy.get('#displayMedewerker0').should('have.text', 'Medewerker 1');
    cy.window().should('have.property', 'displayMedewerker0');
    cy.window().should('have.property', 'displayMedewerker1');
    cy.window().should('not.have.property', 'editMedewerker0');
    cy.window().should('not.have.property', 'editMedewerker1');
    //en weer verder
    cy.get('#bewerkMedewerker0').click();
    cy.window().should('not.have.property', 'displayMedewerker0');
    cy.window().should('have.property', 'displayMedewerker1');
    cy.window().should('have.property', 'editMedewerker0');
    cy.window().should('not.have.property', 'editMedewerker1');

    cy.get('#editMedewerker0').clear();
    cy.get('#editMedewerker0').type('Naam Medewerker Een');

    cy.window().should('not.have.property', 'messageOpgeslagen');
    cy.window().should('not.have.property', 'messageVerwijderd');
    cy.intercept(
      {
        method: 'POST',
        url: '/instellingen/medewerkers/opslaan',
      },
      []
    ).as('opslaan');

    cy.get('#opslaanMedewerker0').click();
    cy.window().should('have.property', 'displayMedewerker0');
    cy.window().should('not.have.property', 'editMedewerker0');

    cy.wait('@opslaan').then((interception) => {
      assert.isNotNull(interception.request.body, '1st API call has data');

      var s = interception.request.body;
      assert.equal(1, s.id);
      assert.equal("Naam Medewerker Een", s.naam);
      cy.window().should('have.property', 'messageOpgeslagen');
    });
  });
  it('Wijzig medewerker 2', () => {
    cy.get('#bewerkMedewerker1').click();
    cy.window().should('have.property', 'displayMedewerker0');
    cy.window().should('not.have.property', 'displayMedewerker1');
    cy.window().should('not.have.property', 'editMedewerker0');
    cy.window().should('have.property', 'editMedewerker1');

    cy.get('#editMedewerker1').clear();
    cy.get('#editMedewerker1').type('Naam Medewerker Twee');

    cy.window().should('not.have.property', 'messageOpgeslagen');
    cy.window().should('not.have.property', 'messageVerwijderd');
    cy.intercept(
      {
        method: 'POST',
        url: '/instellingen/medewerkers/opslaan',
      },
      []
    ).as('opslaan');

    cy.get('#opslaanMedewerker1').click();
    cy.window().should('have.property', 'displayMedewerker1');
    cy.window().should('not.have.property', 'editMedewerker1');

    cy.wait('@opslaan').then((interception) => {
      assert.isNotNull(interception.request.body, '1st API call has data');

      var s = interception.request.body;
      assert.equal(2, s.id);
      assert.equal("Naam Medewerker Twee", s.naam);
      cy.window().should('have.property', 'messageOpgeslagen');
    });
  });
  it('Test dat wijzigingen worden opgeslagen als editmode wordt beindigd', () => {
    cy.get('#bewerkMedewerker1').click();
    cy.get('#editMedewerker1').clear();
    cy.get('#editMedewerker1').type('Dit Is Naam Medewerker Twee');

    cy.window().should('not.have.property', 'messageOpgeslagen');
    cy.window().should('not.have.property', 'messageVerwijderd');
    cy.intercept(
      {
        method: 'POST',
        url: '/instellingen/medewerkers/opslaan',
      },
      []
    ).as('opslaan');

    cy.get('#bewerkMedewerker0').click();

    cy.wait('@opslaan').then((interception) => {
      assert.isNotNull(interception.request.body, '1st API call has data');

      var s = interception.request.body;
      console.log(JSON.stringify(s));
      // assert.equal(2, s.id);
      assert.equal("Dit Is Naam Medewerker Twee", s.naam);
      cy.window().should('have.property', 'messageOpgeslagen');
    });
  });
  it('Test toevoegen medewerker', () => {
    //p-toast-detail ng-tns-c58-4
    cy.get('#medewerkerToevoegen').click();
    cy.window().should('not.have.property', 'verwijderMedewerker2');
    cy.get('#editMedewerker2').clear();
    cy.get('#editMedewerker2').type('Naam Medewerker Drie');

    cy.window().should('not.have.property', 'messageOpgeslagen');
    cy.window().should('not.have.property', 'messageVerwijderd');
    cy.intercept(
      {
        method: 'POST',
        url: '/instellingen/medewerkers/opslaan',
      },
      '46'
    ).as('opslaan');

    cy.get('#opslaanMedewerker2').click();
    cy.window().should('have.property', 'verwijderMedewerker2');

    cy.wait('@opslaan').then((interception) => {
      assert.isNotNull(interception.request.body, '1st API call has data');

      var s = interception.request.body;
      assert.equal("Naam Medewerker Drie", s.naam);
      cy.window().should('have.property', 'messageOpgeslagen');
    });
  });
  it('Test toevoegen medewerker beindig edit mode', () => {
    cy.get('#medewerkerToevoegen').click();
    cy.get('#editMedewerker3').clear();
    cy.get('#editMedewerker3').type('Naam Medewerker Vier');

    cy.window().should('not.have.property', 'messageOpgeslagen');
    cy.window().should('not.have.property', 'messageVerwijderd');
    cy.intercept(
      {
        method: 'POST',
        url: '/instellingen/medewerkers/opslaan',
      },
      []
    ).as('opslaan');

    cy.get('#bewerkMedewerker0').click();

    cy.wait('@opslaan').then((interception) => {
      assert.isNotNull(interception.request.body, '1st API call has data');

      var s = interception.request.body;
      assert.equal("Naam Medewerker Vier", s.naam);
      cy.window().should('have.property', 'messageOpgeslagen');
    });
    cy.get('#cancelMedewerker0').click();
  });
  it('Verwijder medewerker', () => {
    cy.intercept('/instellingen/medewerkers/lijst', medewerkersResponse);
    cy.visit('http://localhost:4200/instellingen/medewerkers.html');
    cy.get('#verwijderMedewerker0').click();

    cy.window().should('not.have.property', 'messageOpgeslagen');
    cy.window().should('not.have.property', 'messageVerwijderd');
    cy.intercept(
      {
        method: 'DELETE',
        url: '/instellingen/medewerkers/verwijder/1',
      },
      []
    ).as('verwijderen');

    cy.get('#bevestigenJa').click();

    cy.wait('@verwijderen').then((interception) => {
      cy.window().should('have.property', 'messageVerwijderd');
    });
  });
  it('Verwijder net toegevoegde  medewerker', () => {
    cy.get('#medewerkerToevoegen').click();
    cy.get('#editMedewerker1').clear();
    cy.get('#editMedewerker1').type('Naam Medewerker Vijf');

    cy.window().should('not.have.property', 'messageOpgeslagen');
    cy.window().should('not.have.property', 'messageVerwijderd');
    cy.intercept(
      {
        method: 'POST',
        url: '/instellingen/medewerkers/opslaan',
      },
      '99'
    ).as('opslaan');

    cy.get('#opslaanMedewerker1').click();

    cy.wait('@opslaan').then((interception) => {
      assert.isNotNull(interception.request.body, '1st API call has data');

      var s = interception.request.body;
      assert.equal("Naam Medewerker Vijf", s.naam);
      cy.window().should('have.property', 'messageOpgeslagen');
    });

    cy.window().should('not.have.property', 'editMedewerker1');
    cy.window().should('have.property', 'displayMedewerker1');
    cy.get('#bewerkMedewerker0').click();
    cy.get('#cancelMedewerker0').click();
    cy.get('#verwijderMedewerker1').click();

    cy.window().should('not.have.property', 'messageOpgeslagen');
    cy.window().should('not.have.property', 'messageVerwijderd');
    cy.intercept(
      {
        method: 'DELETE',
        url: '/instellingen/medewerkers/verwijder/99',
      },
      []
    ).as('verwijderen');

    cy.get('#bevestigenJa').click();

    cy.wait('@verwijderen').then((interception) => {
      cy.window().should('have.property', 'messageVerwijderd');
    });
  });
})
