import {Component, OnInit} from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {LocalDate} from "@js-joda/core";

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css']
})
export class DashboardComponent implements OnInit {
  basicData: any;
  basicOptions: any;

  constructor(private httpClient: HttpClient) {
    this.basicOptions = {
      legend: {
        labels: {
          fontColor: '#495057'
        }
      },
      scales: {
        xAxes: [{
          ticks: {
            fontColor: '#495057'
          }
        }],
        yAxes: [{
          ticks: {
            fontColor: '#495057'
          }
        }]
      }
    };
    this.httpClient.get("http://localhost:8080/dashboard").subscribe((response: any) => {
      this.basicData = {
        labels: [],
        datasets: [
          {
            label: 'Actueel',
            data: [],
            fill: false,
            borderColor: '#42A5F5'
          },
          {
            label: 'Gemiddeld',
            data: [],
            fill: false,
            borderColor: '#FFA726'
          }
        ]
      }
      response.actueel.forEach((l: number) => {
        this.basicData.datasets[0].data.push(l);
      });
      response.gemiddeld.forEach((l: number) => {
        this.basicData.datasets[1].data.push(l);
      });
      let dagen: string[] = [];
      dagen.push("Zondag")
      dagen.push("Maandag")
      dagen.push("Dinsdag")
      dagen.push("Woensdag")
      dagen.push("Donderdag")
      dagen.push("Vrijdag")
      dagen.push("Zaterdag")
      dagen.push("Zondag")
      this.basicData.labels.push(dagen[LocalDate.now().dayOfWeek().value()]);
      this.basicData.labels.push(dagen[LocalDate.now().plusDays(1).dayOfWeek().value()]);
      this.basicData.labels.push(dagen[LocalDate.now().plusDays(2).dayOfWeek().value()]);
      this.basicData.labels.push(dagen[LocalDate.now().plusDays(3).dayOfWeek().value()]);
      this.basicData.labels.push(dagen[LocalDate.now().plusDays(4).dayOfWeek().value()]);
      this.basicData.labels.push(dagen[LocalDate.now().plusDays(5).dayOfWeek().value()]);
      this.basicData.labels.push(dagen[LocalDate.now().plusDays(6).dayOfWeek().value()]);
    });
  }

  ngOnInit(): void {
  }

}
