import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {BrowserAnimationsModule} from "@angular/platform-browser/animations";
import {FullCalendarModule} from 'primeng/fullcalendar';

import {AppComponent} from './app.component';
import {ZoeksalonComponent} from "./afspraak/zoeksalon/zoeksalon.component";
import {AfspraakComponent} from "./afspraak/afspraak.component";
import {SelecteerMedewerkerEnBehandelingComponent} from "./afspraak/selecteer-medewerker-en-behandeling/selecteer-medewerker-en-behandeling.component";
import {TijdstipbepalenComponent} from "./afspraak/tijdstipbepalen/tijdstipbepalen.component";
import {DropdownModule} from "primeng/dropdown";
import {AvatarModule} from "primeng/avatar";
import {PickListModule} from "primeng/picklist";
import {ButtonModule} from "primeng/button";
import {HTTP_INTERCEPTORS, HttpClientModule} from "@angular/common/http";
import {DialogModule} from "primeng/dialog";
import {OverzichtComponent} from './afspraak/overzicht/overzicht.component';
import {StepsModule} from 'primeng/steps';
import {RouterModule, Routes} from "@angular/router";
import {CalendarModule} from "primeng/calendar";
import {MegaMenuModule} from "primeng/megamenu";
import {MenuModule} from "primeng/menu";
import {InputTextModule} from "primeng/inputtext";
import {PasswordModule} from "primeng/password";
import {PasswordStrengthMeterModule} from "angular-password-strength-meter";
import {ToastModule} from "primeng/toast";
import {NgxQRCodeModule} from "@techiediaries/ngx-qrcode";
import {
  FacebookLoginProvider,
  GoogleLoginProvider,
  SocialAuthServiceConfig,
  SocialLoginModule
} from "angularx-social-login";
import {MessagesModule} from "primeng/messages";
import {MessageService} from "primeng/api";
import {TokenInterceptor} from "./afspraak/interceptor/token-interceptor";
import {InputTextareaModule} from "primeng/inputtextarea";
import {TimelineModule} from "primeng/timeline";
import {AfspraakHistorieComponent} from './afspraak/afspraak-historie/afspraak-historie.component';
import {CardModule} from "primeng/card";
import {AfspraakMakenComponent} from './afspraak/afspraak-maken/afspraak-maken.component';
import {AanmeldenComponent} from './aanmelden/aanmelden/aanmelden.component';
import {Stap1Component} from './aanmelden/aanmelden/stap1/stap1.component';
import {InputSwitchModule} from "primeng/inputswitch";
import {FieldsetModule} from "primeng/fieldset";
import {SelectButtonModule} from "primeng/selectbutton";
import {Stap3Component} from './aanmelden/aanmelden/stap3/stap3.component';
import {Stap4Component} from './aanmelden/aanmelden/stap4/stap4.component';
import {InputMaskModule} from "primeng/inputmask";
import {CheckboxModule} from "primeng/checkbox";
import {InputNumberModule} from "primeng/inputnumber";
import {TreeTableModule} from "primeng/treetable";
import {ConfirmDialogModule} from "primeng/confirmdialog";
import {Stap5Component} from './aanmelden/aanmelden/stap5/stap5.component';
import {WachtwoordWijzigenComponent} from './commons/wachtwoord-wijzigen/wachtwoord-wijzigen.component';
import {DashboardComponent} from './dashboard/dashboard.component';
import {ChartModule} from "primeng/chart";
import {WerkRoosterComponent} from './commons/werk-rooster/werk-rooster.component';
import {MedewerkersComponent} from './commons/medewerkers/medewerkers.component';
import {BehandelingenComponent} from './commons/behandelingen/behandelingen.component';
import {SalonInstellingenComponent} from './commons/salon-instellingen/salon-instellingen.component';
import {SalonInstellingenBeherenComponent} from './instellingen/salon-instellingen-beheren/salon-instellingen-beheren.component';
import {WerkroosterBeherenComponent} from './instellingen/werkrooster-beheren/werkrooster-beheren.component';
import {MedewerkersBeherenComponent} from './instellingen/medewerkers-beheren/medewerkers-beheren.component';
import {BehandelingenBeherenComponent} from './instellingen/behandelingen-beheren/behandelingen-beheren.component';
import {TableModule} from "primeng/table";
import {Stap2Component} from './aanmelden/aanmelden/stap2/stap2.component';

const routes: Routes = [
  {path: 'afspraak-maken.html', component: AfspraakMakenComponent},
  {path: 'afspraken.html', component: AfspraakHistorieComponent},
  {path: 'aanmelden.html', component: AanmeldenComponent},
  {path: 'wijzigwachtwoord.html', component: WachtwoordWijzigenComponent},
  {path: 'dashboard.html', component: DashboardComponent},
  {path: 'instellingen/salon.html', component: SalonInstellingenBeherenComponent},
  {path: 'instellingen/rooster.html', component: WerkroosterBeherenComponent},
  {path: 'instellingen/medewerkers.html', component: MedewerkersBeherenComponent},
  {path: 'instellingen/behandelingen.html', component: BehandelingenBeherenComponent},
  {path: '**', redirectTo: 'afspraak-maken.html', pathMatch: 'full'},
];

@NgModule({
  declarations: [
    AppComponent,
    ZoeksalonComponent,
    AfspraakComponent,
    SelecteerMedewerkerEnBehandelingComponent,
    TijdstipbepalenComponent, OverzichtComponent, AfspraakHistorieComponent, AfspraakMakenComponent, AanmeldenComponent, Stap1Component, Stap3Component, Stap4Component, Stap5Component, WachtwoordWijzigenComponent, DashboardComponent, WerkRoosterComponent, MedewerkersComponent, BehandelingenComponent, SalonInstellingenComponent, SalonInstellingenBeherenComponent, WerkroosterBeherenComponent, MedewerkersBeherenComponent, BehandelingenBeherenComponent, Stap2Component
  ],
  imports: [
    ReactiveFormsModule,
    BrowserModule,
    DropdownModule,
    FormsModule,
    BrowserAnimationsModule,
    AvatarModule,
    ButtonModule,
    PickListModule,
    FullCalendarModule,
    HttpClientModule,
    DialogModule,
    StepsModule,
    RouterModule.forRoot([
      {path: '', component: AppComponent}
    ]),
    CalendarModule,
    MegaMenuModule,
    MenuModule,
    InputTextModule,
    PasswordModule,
    PasswordStrengthMeterModule,
    ToastModule,
    NgxQRCodeModule,
    SocialLoginModule,
    MessagesModule,
    InputTextareaModule,
    TimelineModule,
    CardModule,
    RouterModule.forRoot(routes),
    InputSwitchModule,
    FieldsetModule,
    SelectButtonModule,
    InputMaskModule,
    CheckboxModule,
    InputNumberModule,
    TreeTableModule,
    ConfirmDialogModule,
    ChartModule,
    TableModule
  ],
  exports: [RouterModule],
  providers: [{
    provide: 'SocialAuthServiceConfig',
    useValue: {
      autoLogin: false,
      providers: [
        {
          id: GoogleLoginProvider.PROVIDER_ID,
          provider: new GoogleLoginProvider(
            '497619291663-ckicvdg7e9i48un0trq37eoprkvft523.apps.googleusercontent.com'
          )
        },
        {
          id: FacebookLoginProvider.PROVIDER_ID,
          provider: new FacebookLoginProvider('457546195299282')
        }
      ]
    } as SocialAuthServiceConfig,
  }, MessageService, {
    provide: HTTP_INTERCEPTORS,
    useClass: TokenInterceptor,
    multi: true
  }],
  bootstrap: [AppComponent]
})
export class AppModule {
}
