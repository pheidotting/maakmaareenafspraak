import {Component, Injectable, OnInit} from '@angular/core';
import {SalonService} from "../service/salon/salon.service";
import {Afspraakinfo, Salon} from "../model/afspraakinfo/afspraakinfo";
import {AfspraakService} from "../service/afspraak/afspraak.service";

@Injectable()
@Component({
  selector: 'app-zoeksalon',
  templateUrl: './zoeksalon.component.html',
  styleUrls: ['./zoeksalon.component.css']
})
export class ZoeksalonComponent implements OnInit {
  salons: Salon[] = [];

  constructor(private salonService: SalonService, private _afspraakService: AfspraakService) {
  }

  ngOnInit(): void {
    this.salonService.haalSalons().subscribe(salons => {
        this.salons = salons;
      }
    );
  }

  getSalon(id: number): Salon {
    return this.salons.filter(s => s.id == id)[0];
  }

  initialen(id: number): string {
    return this.getSalon(id).naam.split(" ").map((n) => n[0]).join("");
  }

  naamEnPlaatsSalon(id): string {
    let salon = this.getSalon(id);
    let plaats = (salon.plaats == null || salon.plaats == '') ? '' : ', ' + salon.plaats;
    return salon.naam + plaats;
  }

  geselecteerdeSalon(): Salon {
    return this.salons.filter(s => s.id == this._afspraakService.afspraakInfo.stap1.salon.id)[0];
  }

  adresregel() {
    let salon: Salon = this.geselecteerdeSalon();
    if (salon != null) {
      let adresregel = salon.straat;
      if (adresregel != null && salon.plaats != null) {
        adresregel = adresregel + ', ' + salon.plaats;
      } else if (salon.plaats != null) {
        adresregel = salon.plaats;
      }
      if (salon.telefoonnummer != null) {
        if (adresregel == null) {
          adresregel = '';
        }
        adresregel = adresregel + ' (' + salon.telefoonnummer + ')';
      }

      return adresregel == null ? '' : adresregel.trim();
    } else {
      return '';
    }
  }

  verderKnopDisabled(): boolean {
    return this._afspraakService.afspraakInfo.stap1.salon.id == null;
  }

  afspraakInfo(): Afspraakinfo {
    return this._afspraakService.afspraakInfo;
  }

  ophalenSalonGegevens() {
    if (this.geselecteerdeSalon() != null) {
      this.salonService.haalSalon(this.geselecteerdeSalon().id).subscribe(value => {
        this.afspraakService.opvragenSalonResponse.next(value);
        this.afspraakService.list1 = value.salon.behandelingen;

        if (value.salon.medewerkers.length > 1) {
          this.afspraakService.stepItem[1].label = 'Kies een medewerker en behandeling';
        } else {
          this.afspraakService.stepItem[1].label = 'Kies een behandeling';
        }
      });
    } else {
      this.afspraakService.opvragenSalonResponse.next(null);
      this.afspraakService.list1 = null;
    }
  }

  naarVolgendeStap() {
    this.afspraakService.afspraakInfo.stap1.salon = this.geselecteerdeSalon();
    this.afspraakService.naarVolgendeStap();
  }

  get afspraakService(): AfspraakService {
    return this._afspraakService;
  }
}
