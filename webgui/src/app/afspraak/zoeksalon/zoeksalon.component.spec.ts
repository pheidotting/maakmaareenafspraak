// import {ComponentFixture, TestBed} from '@angular/core/testing';
//
// import {ZoeksalonComponent} from './zoeksalon.component';
// import {SalonService} from "../service/salon/salon.service";
// import {Salon} from "../afspraakinfo";
// import {Observable} from "rxjs";
// import { of } from 'rxjs';
// import {HttpClientTestingModule, HttpTestingController} from "@angular/common/http/testing";
// import {SalonServiceMock} from "../service/salon/salon.service.mock";
//
// describe('ZoeksalonComponent', () => {
//   // let component: ZoeksalonComponent;
//   // let fixture: ComponentFixture<ZoeksalonComponent>;
//   // let salonService: SalonService;
//
//   let component: ZoeksalonComponent;
//   let mock: SalonServiceMock;
//   // let component: HelloComponent;
//   // let userService: UserService;
//
//   beforeEach(() => {
//     TestBed.configureTestingModule({
//       declarations: [
//         ZoeksalonComponent,SalonServiceMock
//       ],
//       providers: [
//         // SpyHelper.provideMagicalMock(SalonService)
//       ],    });
//     component = TestBed.createComponent(ZoeksalonComponent).componentInstance;
//     mock = TestBed.get(SalonServiceMock);
//     spyOn(mock, 'haalSalons').and.returnValue(of([{
//       naam: 'salon',
//       telefoonnnummer: 'tel',
//       plaats: 'plaats',
//       adres: 'adres',
//       avatarAanwezig: false,
//       id: 1
//     }]));
//   });
//
//   // beforeEach(async () => {
//   //   salonService =      jasmine.createSpyObj('SalonService', ['haalSalons']);
//   //   await TestBed.configureTestingModule({
//   //     declarations: [ZoeksalonComponent],
//   //     providers: [salonService//,
//   //       // useValue: jasmine.createSpyObj('salonService', ['haalSalons'])
//   //     ]
//   //   }).compileComponents();
//   //
//   //   salonService.haalSalons.and.returnValue(new Observable<Salon[]>((observer) => {
//   //     observer.next([{
//   //       naam: 'salon',
//   //       telefoonnnummer: 'tel',
//   //       plaats: 'plaats',
//   //       adres: 'adres',
//   //       avatarAanwezig: false,
//   //       id: 1
//   //     }])
//   //     observer.complete()
//   //   }));
//   // });
//   //
//   // beforeEach(() => {
//   //   fixture = TestBed.createComponent(ZoeksalonComponent);
//   //   component = fixture.componentInstance;
//   //   fixture.detectChanges();
//   // });
//
//   it('should create', () => {
//     expect(component).toBeTruthy();
//   });
//
//   it('test adresregel zonder gegevens', () => {
//     component.salons = [{id: 1, adres: null, plaats: null, telefoonnnummer: null, naam: null, avatarAanwezig: false}];
//     component.afspraakInfo = {
//       stap1: {
//         salon: {
//           naam: 'salon',
//           telefoonnnummer: 'tel',
//           plaats: 'plaats',
//           adres: 'adres',
//           avatarAanwezig: false,
//           id: 1
//         }
//       }, stap2: null, stap3: null, klant: null
//     };
//
//     expect(component.adresregel()).toBe('');
//   });
//
//   it('test adresregel alleen straat', () => {
//     component.salons = [{
//       id: 1,
//       adres: 'straatnaam',
//       plaats: null,
//       telefoonnnummer: null,
//       naam: null,
//       avatarAanwezig: false
//     }];
//     component.afspraakInfo = {
//       stap1: {
//         salon: {
//           naam: 'salon',
//           telefoonnnummer: 'tel',
//           plaats: 'plaats',
//           adres: 'adres',
//           avatarAanwezig: false,
//           id: 1
//         }
//       }, stap2: null, stap3: null, klant: null
//     };
//
//     expect(component.adresregel()).toBe('straatnaam');
//   });
//
//   it('test adresregel alleen plaats', () => {
//     component.salons = [{
//       id: 1,
//       adres: null,
//       plaats: 'plaats',
//       telefoonnnummer: null,
//       naam: null,
//       avatarAanwezig: false
//     }];
//     component.afspraakInfo = {
//       stap1: {
//         salon: {
//           naam: 'salon',
//           telefoonnnummer: 'tel',
//           plaats: 'plaats',
//           adres: 'adres',
//           avatarAanwezig: false,
//           id: 1
//         }
//       }, stap2: null, stap3: null, klant: null
//     };
//
//     expect(component.adresregel()).toBe('plaats');
//   });
//
//   it('test adresregel alleen telefoonnnummer', () => {
//     component.salons = [{
//       id: 1,
//       adres: null,
//       plaats: null,
//       telefoonnnummer: 'telefoon',
//       naam: null,
//       avatarAanwezig: false
//     }];
//     component.afspraakInfo = {
//       stap1: {
//         salon: {
//           naam: 'salon',
//           telefoonnnummer: 'tel',
//           plaats: 'plaats',
//           adres: 'adres',
//           avatarAanwezig: false,
//           id: 1
//         }
//       }, stap2: null, stap3: null, klant: null
//     };
//
//     expect(component.adresregel()).toBe('(telefoon)');
//   });
//
//   it('test adresregel alles gevuld', () => {
//     component.salons = [{
//       id: 1,
//       adres: 'straat',
//       plaats: 'plaats',
//       telefoonnnummer: 'telefoon',
//       naam: null,
//       avatarAanwezig: false
//     }];
//     component.afspraakInfo = {
//       stap1: {
//         salon: {
//           naam: 'salon',
//           telefoonnnummer: 'tel',
//           plaats: 'plaats',
//           adres: 'adres',
//           avatarAanwezig: false,
//           id: 1
//         }
//       }, stap2: null, stap3: null, klant: null
//     };
//
//     expect(component.adresregel()).toBe('straat, plaats (telefoon)');
//   });
//
//   it('test naamEnPlaatsSalon', () => {
//     component.salons = [{
//       id: 1,
//       adres: null,
//       plaats: 'plaats',
//       telefoonnnummer: null,
//       naam: 'salonnaam',
//       avatarAanwezig: false
//     }];
//
//     expect(component.naamEnPlaatsSalon(1)).toBe('salonnaam, plaats');
//   });
//
//   it('test naamEnPlaatsSalon zonder plaats', () => {
//     component.salons = [{
//       id: 1,
//       adres: null,
//       plaats: null,
//       telefoonnnummer: null,
//       naam: 'salonnaam',
//       avatarAanwezig: false
//     }];
//
//     expect(component.naamEnPlaatsSalon(1)).toBe('salonnaam');
//   });
// });
