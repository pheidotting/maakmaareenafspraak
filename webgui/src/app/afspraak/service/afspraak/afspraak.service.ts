import {Injectable} from '@angular/core';
import {Afspraakinfo, Klant} from "../../model/afspraakinfo/afspraakinfo";
import {Behandeling} from "../../model/opvragen-salon-response/behandeling";
import {OpvragenSalonResponse} from "../../model/opvragen-salon-response/opvragen-salon-response.model";
import {TijdenBepalenService} from "../tijden/tijden-bepalen.service";
import {Dag} from "../../model/opvragen-salon-response/dag";
import {convert, LocalDateTime} from "@js-joda/core";
import {MenuItem} from "primeng/api";
import {Subject} from "rxjs";
import {HttpClient} from "@angular/common/http";
import {AfspraakMetNietIngelogdeKlant} from "../../model/afspraak-met-niet-ingelogde-klant";
import {AfspraakMetIngelogdeKlant} from "../../model/afspraak-met-ingelogde-klant";

@Injectable({
  providedIn: 'root'
})
export class AfspraakService {
  private _urlAfspraakOpslaanNietIngelogdeKlant: string = 'http://localhost:8080/afspraak/opslaanAfspraakMetNietIngelogdeKlant';
  private _urlAfspraakOpslaanIngelogdeKlant: string = 'http://localhost:8080/afspraak/opslaanAfspraakMetIngelogdeKlant';
  private tijdenBepalenService: TijdenBepalenService = new TijdenBepalenService();
  private _afspraakInfo: Afspraakinfo = new Afspraakinfo();
  private _huidigeStap: number = 1;
  private _list1: Behandeling[];
  private _list2: Behandeling[] = [];
  opvragenSalonResponse: Subject<OpvragenSalonResponse> = new Subject<OpvragenSalonResponse>();
  response: OpvragenSalonResponse;
  medewerkerId: number;
  dagen: Dag[] = [];
  dagenVol: Date[] = [];
  stepItem: MenuItem[];
  klant: Klant = new Klant();

  constructor(private httpClient: HttpClient) {
    this.stepItem = [
      {label: 'Kies jouw salon'},
      {label: 'Kies een medewerker en behandeling'},
      {label: 'Kies het tijdstip dat jou uitkomt'},
      {label: 'Overzicht'}
    ];
    this.opvragenSalonResponse.subscribe(response => {
      this.response = response;
    });
  }

  get list1(): Behandeling[] {
    return this._list1;
  }

  set list1(value: Behandeling[]) {
    this._list1 = value;
  }

  get list2(): Behandeling[] {
    return this._list2;
  }

  set list2(value: Behandeling[]) {
    this._list2 = value;
  }

  afspraakOpslaan() {
    let url;
    let klant: Klant = this.klant;
    let afspraakdetails: any;
    if (klant.jwt != null) {
      url = this._urlAfspraakOpslaanIngelogdeKlant
      let afspraakMetIngelogdeKlant: AfspraakMetIngelogdeKlant = new AfspraakMetIngelogdeKlant();
      afspraakMetIngelogdeKlant.opmerking = this.afspraakInfo.opmerking;
      afspraakMetIngelogdeKlant.tijdstip = LocalDateTime.of(this._afspraakInfo.stap3.datum, this._afspraakInfo.stap3.tijd);
      afspraakMetIngelogdeKlant.salonId = this._afspraakInfo.stap1.salon.id;
      if (this._afspraakInfo.stap2.medewerker != null) {
        afspraakMetIngelogdeKlant.voorkeurMedewerkerId = this._afspraakInfo.stap2.medewerker.id;
      }
      this._afspraakInfo.stap2.behandelingen.forEach(behandeling => afspraakMetIngelogdeKlant.behandelingIds.push(behandeling.id));
      afspraakdetails = afspraakMetIngelogdeKlant;
    } else {
      url = this._urlAfspraakOpslaanNietIngelogdeKlant;
      let afspraakMetNietIngelogdeKlant: AfspraakMetNietIngelogdeKlant = new AfspraakMetNietIngelogdeKlant();
      afspraakMetNietIngelogdeKlant.naam = klant.naam;
      afspraakMetNietIngelogdeKlant.emailadres = klant.emailadres;
      if (klant.telefoonnummer != null) {
        afspraakMetNietIngelogdeKlant.telefoonnummer = klant.telefoonnummer.replace('-', '').replace(' ', '');
      }
      afspraakMetNietIngelogdeKlant.fotoUrl = klant.photoUrl;
      afspraakMetNietIngelogdeKlant.opmerking = this.afspraakInfo.opmerking;
      afspraakMetNietIngelogdeKlant.tijdstip = LocalDateTime.of(this._afspraakInfo.stap3.datum, this._afspraakInfo.stap3.tijd);
      afspraakMetNietIngelogdeKlant.salonId = this._afspraakInfo.stap1.salon.id;
      if (this._afspraakInfo.stap2.medewerker != null) {
        afspraakMetNietIngelogdeKlant.voorkeurMedewerkerId = this._afspraakInfo.stap2.medewerker.id;
      }
      this._afspraakInfo.stap2.behandelingen.forEach(behandeling => afspraakMetNietIngelogdeKlant.behandelingIds.push(behandeling.id));
      afspraakdetails = afspraakMetNietIngelogdeKlant;
    }
    return this.httpClient.post(url, afspraakdetails);
  }

  naarVolgendeStap() {
    this._huidigeStap++;
    if (this._huidigeStap == 3) {
      let medewerker;
      // this.opvragenSalonResponse.subscribe(response => {
      if (this.medewerkerId != null && this.medewerkerId != 0) {
        medewerker = this.response.salon.medewerkers.filter(medewerker => medewerker.id == this.medewerkerId);
      } else {
        medewerker = null;
      }
      this.dagen = this.tijdenBepalenService.bepaalTijden(
        this.response.dagen,
        this._list2,
        medewerker
      );

      this.dagenVol = this.dagen.filter(dag => dag.tijdstippen.length == 0).map(dag => convert(dag.datum).toDate());
      // });
    }
  }

  naarVorigeStap() {
    this._huidigeStap = this._huidigeStap - 1;
  }

  prijsOpgemaakt(prijs: number) {
    let parts = prijs.toString().split('.');

    if (parts.length == 1) {
      return parts[0] + ',00';
    } else if (parts[1].length == 2) {
      return parts[0] + ',' + parts[1];
    } else if (parts[1].length == 1) {
      return parts[0] + ',' + parts[1] + '0';
    } else {
      return parts[0] + ',' + parts[1].substr(0, 2);
    }
  }

  get afspraakInfo(): Afspraakinfo {
    return this._afspraakInfo;
  }

  get huidigeStap(): number {
    return this._huidigeStap;
  }

  set huidigeStap(value: number) {
    this._huidigeStap = value;
  }
}
