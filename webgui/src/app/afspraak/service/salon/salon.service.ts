import {Injectable} from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {Salon} from "../../model/afspraakinfo/afspraakinfo";
import {Observable} from "rxjs";
import {OpvragenSalonResponse} from "../../model/opvragen-salon-response/opvragen-salon-response.model";
import {map} from "rxjs/operators";
import {LocalDate, LocalTime} from "@js-joda/core";

@Injectable({
  providedIn: 'root'
})
export class SalonService {
  private _urlSalons: string = 'http://localhost:8080/salon/alles';
  private _urlLeesSalon: string = 'http://localhost:8080/salon/lees/';

  constructor(private httpClient: HttpClient) {
  }

  haalSalons(): Observable<Salon[]> {
    return this.httpClient.get<Salon[]>(this._urlSalons);
  }

  haalSalon(id: number): Observable<OpvragenSalonResponse> {
    return this.httpClient.get<OpvragenSalonResponse>(this._urlLeesSalon + id)
      .pipe(map((data) => {
        data.dagen.forEach(dag => {
          dag.datum = LocalDate.parse('' + dag.datum);
          dag.tijdstippen.forEach(tijdstip => {
            tijdstip.tijd = LocalTime.parse('' + tijdstip.tijd);
          });
        });


        // console.log(data.dagen[0].datum);
        // data.dagen[0].datum = LocalDate.parse(""+ data.dagen[0].datum);
        //
        //   console.log(data.dagen[0].datum);
        //
        return data;
      }));
  }

  get urlSalons(): string {
    return this._urlSalons;
  }

  get urlLeesSalon(): string {
    return this._urlLeesSalon;
  }
}
