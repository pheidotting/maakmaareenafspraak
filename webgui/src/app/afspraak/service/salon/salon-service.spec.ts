import {async, inject, TestBed} from '@angular/core/testing';

import {SalonService} from './salon.service';
import {HttpClientTestingModule, HttpTestingController} from "@angular/common/http/testing";
import {Salon} from "../../model/afspraakinfo/afspraakinfo";

describe('SalonService', () => {
  let salonService: SalonService;
  let httpMock: HttpTestingController;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [
        HttpClientTestingModule,
      ],
      providers: [SalonService
      ],
    });

    salonService = TestBed.get(SalonService);
    httpMock = TestBed.get(HttpTestingController);
  });

  it('should be created', () => {
    expect(salonService).toBeTruthy();
  });

  it('test voor haalSalons', async(inject([HttpTestingController, SalonService],
    (httpClient: HttpTestingController, postService: SalonService) => {
      let salon: Salon = {
        id: 1,
        naam: 'Salon Sabine',
        plaats: 'Klazienaveen',
        avatarAanwezig: true,
        adres: 'Boogschutter 26 7891TN',
        telefoonnnummer: '06 - 38 75 11 09'
      };

      salonService.haalSalons().subscribe((salons) => {
        expect(salons).toEqual([salon]);
      });

      const req = httpMock.expectOne(salonService.urlSalons);
      expect(req.request.method).toBe("GET");
      req.flush([salon]);

      httpMock.verify();
    })));
});
