import {TestBed} from '@angular/core/testing';

import {TijdenBepalenService} from './tijden-bepalen.service';
import {HttpClientTestingModule} from "@angular/common/http/testing";
import {Medewerker} from "../../model/opvragen-salon-response/medewerker";
import {Behandeling} from "../../model/opvragen-salon-response/behandeling";
import {Dag} from "../../model/opvragen-salon-response/dag";
import {Tijdstip} from "../../model/opvragen-salon-response/tijdstip";
import {LocalDate, LocalTime} from "@js-joda/core";

describe('BepaalTijden', () => {
  let service: TijdenBepalenService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [
        HttpClientTestingModule,
      ],
      providers: [TijdenBepalenService
      ],
    });

    service = TestBed.get(TijdenBepalenService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  it('1 vrije medewerker, 1 behandeling van 2 eenheden, geen voorkeur, geen afspraken aanwezig', () => {
    let dagen: Dag[] = [];
    let behandelingen: Behandeling[] = [];
    let medewerker: Medewerker;

    let dag: Dag = new Dag();
    dag.datum = LocalDate.of(2021, 3, 22);
    let tijdstip1: Tijdstip = new Tijdstip();
    tijdstip1.tijd = LocalTime.of(9, 0);
    tijdstip1.medewekerIds = [1];
    tijdstip1.aantalAfspraken = 0;
    let tijdstip2: Tijdstip = new Tijdstip();
    tijdstip2.tijd = LocalTime.of(9, 15);
    tijdstip2.medewekerIds = [1];
    tijdstip2.aantalAfspraken = 0;
    let tijdstip3: Tijdstip = new Tijdstip();
    tijdstip3.tijd = LocalTime.of(9, 30);
    tijdstip3.medewekerIds = [1];
    tijdstip3.aantalAfspraken = 0;
    dag.tijdstippen = [tijdstip1, tijdstip2, tijdstip3];
    dagen.push(dag);

    let behandeling: Behandeling = new Behandeling();
    behandeling.tijdsduur = 30;
    behandelingen.push(behandeling);

    let result = service.bepaalTijden(dagen, behandelingen, medewerker);

    expect(result.length).toBe(1);
    expect(result[0].datum).toEqual(LocalDate.of(2021, 3, 22));
    expect(result[0].tijdstippen.length).toBe(2);
    expect(result[0].tijdstippen[0].tijd).toEqual(LocalTime.of(9, 0));
    expect(result[0].tijdstippen[0].medewekerIds).toEqual([1]);
    expect(result[0].tijdstippen[1].tijd).toEqual(LocalTime.of(9, 15));
    expect(result[0].tijdstippen[1].medewekerIds).toEqual([1]);
  });
  it('1 vrije medewerker, 1 behandeling van 2 eenheden, geen voorkeur, 1 afspraak aanwezig per eenheid', () => {
    let dagen: Dag[] = [];
    let behandelingen: Behandeling[] = [];
    let medewerker: Medewerker;

    let dag: Dag = new Dag();
    dag.datum = LocalDate.of(2021, 3, 22);
    let tijdstip1: Tijdstip = new Tijdstip();
    tijdstip1.tijd = LocalTime.of(9, 0);
    tijdstip1.medewekerIds = [1];
    tijdstip1.aantalAfspraken = 1;
    let tijdstip2: Tijdstip = new Tijdstip();
    tijdstip2.tijd = LocalTime.of(9, 15);
    tijdstip2.medewekerIds = [1];
    tijdstip2.aantalAfspraken = 1;
    let tijdstip3: Tijdstip = new Tijdstip();
    tijdstip3.tijd = LocalTime.of(9, 30);
    tijdstip3.medewekerIds = [1];
    tijdstip3.aantalAfspraken = 1;
    dag.tijdstippen = [tijdstip1, tijdstip2, tijdstip3];
    dagen.push(dag);

    let behandeling: Behandeling = new Behandeling();
    behandeling.tijdsduur = 30;
    behandelingen.push(behandeling);

    let result = service.bepaalTijden(dagen, behandelingen, medewerker);

    expect(result.length).toBe(1);
    expect(result[0].datum).toEqual(LocalDate.of(2021, 3, 22));
    expect(result[0].tijdstippen.length).toBe(0);
  });
  it('1 vrije medewerker, 1 behandeling van 2 eenheden, geen voorkeur, 1 afspraak aanwezig op eerste eenheid', () => {
    let dagen: Dag[] = [];
    let behandelingen: Behandeling[] = [];
    let medewerker: Medewerker;

    let dag: Dag = new Dag();
    dag.datum = LocalDate.of(2021, 3, 22);
    let tijdstip1: Tijdstip = new Tijdstip();
    tijdstip1.tijd = LocalTime.of(9, 0);
    tijdstip1.medewekerIds = [1];
    tijdstip1.aantalAfspraken = 1;
    let tijdstip2: Tijdstip = new Tijdstip();
    tijdstip2.tijd = LocalTime.of(9, 15);
    tijdstip2.medewekerIds = [1];
    tijdstip2.aantalAfspraken = 0;
    let tijdstip3: Tijdstip = new Tijdstip();
    tijdstip3.tijd = LocalTime.of(9, 30);
    tijdstip3.medewekerIds = [1];
    tijdstip3.aantalAfspraken = 0;
    dag.tijdstippen = [tijdstip1, tijdstip2, tijdstip3];
    dagen.push(dag);

    let behandeling: Behandeling = new Behandeling();
    behandeling.tijdsduur = 30;
    behandelingen.push(behandeling);

    let result = service.bepaalTijden(dagen, behandelingen, medewerker);

    expect(result.length).toBe(1);
    expect(result[0].datum).toEqual(LocalDate.of(2021, 3, 22));
    expect(result[0].tijdstippen.length).toBe(1);
    expect(result[0].tijdstippen[0].tijd).toEqual(LocalTime.of(9, 15));
    expect(result[0].tijdstippen[0].medewekerIds).toEqual([1]);
  });
  it('0 vrije medewerkers', () => {
    let dagen: Dag[] = [];
    let behandelingen: Behandeling[] = [];
    let medewerker: Medewerker;

    let dag: Dag = new Dag();
    dag.datum = LocalDate.of(2021, 3, 22);
    let tijdstip1: Tijdstip = new Tijdstip();
    tijdstip1.tijd = LocalTime.of(9, 0);
    tijdstip1.medewekerIds = [];
    tijdstip1.aantalAfspraken = 0;
    let tijdstip2: Tijdstip = new Tijdstip();
    tijdstip2.tijd = LocalTime.of(9, 15);
    tijdstip2.medewekerIds = [];
    tijdstip2.aantalAfspraken = 0;
    let tijdstip3: Tijdstip = new Tijdstip();
    tijdstip3.tijd = LocalTime.of(9, 30);
    tijdstip3.medewekerIds = [];
    tijdstip3.aantalAfspraken = 0;
    dag.tijdstippen = [tijdstip1, tijdstip2, tijdstip3];
    dagen.push(dag);

    let behandeling: Behandeling = new Behandeling();
    behandeling.tijdsduur = 30;
    behandelingen.push(behandeling);

    let result = service.bepaalTijden(dagen, behandelingen, medewerker);

    expect(result.length).toBe(1);
    expect(result[0].datum).toEqual(LocalDate.of(2021, 3, 22));
    expect(result[0].tijdstippen.length).toBe(0);
  });
  it('1 vrije medewerker, 2 behandeling van samen 3 eenheden, geen voorkeur, geen afspraken aanwezig', () => {
    let dagen: Dag[] = [];
    let behandelingen: Behandeling[] = [];
    let medewerker: Medewerker;

    let dag: Dag = new Dag();
    dag.datum = LocalDate.of(2021, 3, 22);
    let tijdstip1: Tijdstip = new Tijdstip();
    tijdstip1.tijd = LocalTime.of(9, 0);
    tijdstip1.medewekerIds = [1];
    tijdstip1.aantalAfspraken = 0;
    let tijdstip2: Tijdstip = new Tijdstip();
    tijdstip2.tijd = LocalTime.of(9, 15);
    tijdstip2.medewekerIds = [1];
    tijdstip2.aantalAfspraken = 0;
    let tijdstip3: Tijdstip = new Tijdstip();
    tijdstip3.tijd = LocalTime.of(9, 30);
    tijdstip3.medewekerIds = [1];
    tijdstip3.aantalAfspraken = 0;
    dag.tijdstippen = [tijdstip1, tijdstip2, tijdstip3];
    dagen.push(dag);

    let behandeling: Behandeling = new Behandeling();
    behandeling.tijdsduur = 30;
    behandelingen.push(behandeling);
    let behandeling2: Behandeling = new Behandeling();
    behandeling2.tijdsduur = 15;
    behandelingen.push(behandeling2);

    let result = service.bepaalTijden(dagen, behandelingen, medewerker);

    expect(result.length).toBe(1);
    expect(result[0].datum).toEqual(LocalDate.of(2021, 3, 22));
    expect(result[0].tijdstippen.length).toBe(1);
    expect(result[0].tijdstippen[0].tijd).toEqual(LocalTime.of(9, 0));
    expect(result[0].tijdstippen[0].medewekerIds).toEqual([1]);
  });
  it('2 vrije medewerker, 2 behandeling van samen 3 eenheden, voorkeur voor vrije medewerker, geen afspraken aanwezig', () => {
    let dagen: Dag[] = [];
    let behandelingen: Behandeling[] = [];
    let medewerker: Medewerker = new Medewerker();

    let dag: Dag = new Dag();
    dag.datum = LocalDate.of(2021, 3, 22);
    let tijdstip1: Tijdstip = new Tijdstip();
    tijdstip1.tijd = LocalTime.of(9, 0);
    tijdstip1.medewekerIds = [1, 2];
    tijdstip1.aantalAfspraken = 0;
    let tijdstip2: Tijdstip = new Tijdstip();
    tijdstip2.tijd = LocalTime.of(9, 15);
    tijdstip2.medewekerIds = [1, 2];
    tijdstip2.aantalAfspraken = 0;
    let tijdstip3: Tijdstip = new Tijdstip();
    tijdstip3.tijd = LocalTime.of(9, 30);
    tijdstip3.medewekerIds = [1, 2];
    tijdstip3.aantalAfspraken = 0;
    dag.tijdstippen = [tijdstip1, tijdstip2, tijdstip3];
    dagen.push(dag);

    let behandeling: Behandeling = new Behandeling();
    behandeling.tijdsduur = 30;
    behandelingen.push(behandeling);
    let behandeling2: Behandeling = new Behandeling();
    behandeling2.tijdsduur = 15;
    behandelingen.push(behandeling2);

    medewerker.id = 1;

    let result = service.bepaalTijden(dagen, behandelingen, medewerker);

    expect(result.length).toBe(1);
    expect(result[0].datum).toEqual(LocalDate.of(2021, 3, 22));
    expect(result[0].tijdstippen.length).toBe(1);
    expect(result[0].tijdstippen[0].tijd).toEqual(LocalTime.of(9, 0));
    expect(result[0].tijdstippen[0].medewekerIds).toEqual([1]);
  });
  it('2 vrije medewerker, 2 behandeling van samen 3 eenheden, voorkeur voor niet vrije medewerker, 0 afspraken', () => {
    let dagen: Dag[] = [];
    let behandelingen: Behandeling[] = [];
    let medewerker: Medewerker = new Medewerker();

    let dag: Dag = new Dag();
    dag.datum = LocalDate.of(2021, 3, 22);
    let tijdstip1: Tijdstip = new Tijdstip();
    tijdstip1.tijd = LocalTime.of(9, 0);
    tijdstip1.medewekerIds = [1, 2];
    tijdstip1.aantalAfspraken = 0;
    let tijdstip2: Tijdstip = new Tijdstip();
    tijdstip2.tijd = LocalTime.of(9, 15);
    tijdstip2.medewekerIds = [2];
    tijdstip2.aantalAfspraken = 0;
    let tijdstip3: Tijdstip = new Tijdstip();
    tijdstip3.tijd = LocalTime.of(9, 30);
    tijdstip3.medewekerIds = [2];
    tijdstip3.aantalAfspraken = 0;
    let tijdstip4: Tijdstip = new Tijdstip();
    tijdstip4.tijd = LocalTime.of(9, 45);
    tijdstip4.medewekerIds = [2];
    tijdstip4.aantalAfspraken = 0;
    dag.tijdstippen = [tijdstip1, tijdstip2, tijdstip3, tijdstip4];
    dagen.push(dag);

    let behandeling: Behandeling = new Behandeling();
    behandeling.tijdsduur = 30;
    behandelingen.push(behandeling);
    let behandeling2: Behandeling = new Behandeling();
    behandeling2.tijdsduur = 15;
    behandelingen.push(behandeling2);

    medewerker.id = 1;

    let result = service.bepaalTijden(dagen, behandelingen, medewerker);

    expect(result.length).toBe(1);
    expect(result[0].datum).toEqual(LocalDate.of(2021, 3, 22));
    expect(result[0].tijdstippen.length).toBe(0);
  });
  it('2 vrije medewerker, 2 behandeling van samen 3 eenheden, voorkeur voor vrije medewerker, 1 afspraken', () => {
    let dagen: Dag[] = [];
    let behandelingen: Behandeling[] = [];
    let medewerker: Medewerker = new Medewerker();

    let dag: Dag = new Dag();
    dag.datum = LocalDate.of(2021, 3, 22);
    let tijdstip1: Tijdstip = new Tijdstip();
    tijdstip1.tijd = LocalTime.of(9, 0);
    tijdstip1.medewekerIds = [1, 2];
    tijdstip1.aantalAfspraken = 0;
    let tijdstip2: Tijdstip = new Tijdstip();
    tijdstip2.tijd = LocalTime.of(9, 15);
    tijdstip2.medewekerIds = [1, 2];
    tijdstip2.aantalAfspraken = 1;
    let tijdstip3: Tijdstip = new Tijdstip();
    tijdstip3.tijd = LocalTime.of(9, 30);
    tijdstip3.medewekerIds = [1, 2];
    tijdstip3.aantalAfspraken = 0;
    let tijdstip4: Tijdstip = new Tijdstip();
    tijdstip4.tijd = LocalTime.of(9, 45);
    tijdstip4.medewekerIds = [2];
    tijdstip4.aantalAfspraken = 0;
    dag.tijdstippen = [tijdstip1, tijdstip2, tijdstip3, tijdstip4];
    dagen.push(dag);

    let behandeling: Behandeling = new Behandeling();
    behandeling.tijdsduur = 30;
    behandelingen.push(behandeling);
    let behandeling2: Behandeling = new Behandeling();
    behandeling2.tijdsduur = 15;
    behandelingen.push(behandeling2);

    medewerker.id = 1;

    let result = service.bepaalTijden(dagen, behandelingen, medewerker);

    expect(result.length).toBe(1);
    expect(result[0].datum).toEqual(LocalDate.of(2021, 3, 22));
    expect(result[0].tijdstippen.length).toBe(1);
    expect(result[0].tijdstippen[0].tijd).toEqual(LocalTime.of(9, 0));
    expect(result[0].tijdstippen[0].medewekerIds).toEqual([1]);
  });
  it('2 vrije medewerker, 2 behandeling van samen 3 eenheden, voorkeur voor vrije medewerker, 2 afspraken', () => {
    let dagen: Dag[] = [];
    let behandelingen: Behandeling[] = [];
    let medewerker: Medewerker = new Medewerker();

    let dag: Dag = new Dag();
    dag.datum = LocalDate.of(2021, 3, 22);
    let tijdstip1: Tijdstip = new Tijdstip();
    tijdstip1.tijd = LocalTime.of(9, 0);
    tijdstip1.medewekerIds = [1, 2];
    tijdstip1.aantalAfspraken = 0;
    let tijdstip2: Tijdstip = new Tijdstip();
    tijdstip2.tijd = LocalTime.of(9, 15);
    tijdstip2.medewekerIds = [1, 2];
    tijdstip2.aantalAfspraken = 2;
    let tijdstip3: Tijdstip = new Tijdstip();
    tijdstip3.tijd = LocalTime.of(9, 30);
    tijdstip3.medewekerIds = [1, 2];
    tijdstip3.aantalAfspraken = 0;
    let tijdstip4: Tijdstip = new Tijdstip();
    tijdstip4.tijd = LocalTime.of(9, 45);
    tijdstip4.medewekerIds = [2];
    tijdstip4.aantalAfspraken = 0;
    dag.tijdstippen = [tijdstip1, tijdstip2, tijdstip3, tijdstip4];
    dagen.push(dag);

    let behandeling: Behandeling = new Behandeling();
    behandeling.tijdsduur = 30;
    behandelingen.push(behandeling);
    let behandeling2: Behandeling = new Behandeling();
    behandeling2.tijdsduur = 15;
    behandelingen.push(behandeling2);

    medewerker.id = 1;

    let result = service.bepaalTijden(dagen, behandelingen, medewerker);

    expect(result.length).toBe(1);
    expect(result[0].datum).toEqual(LocalDate.of(2021, 3, 22));
    expect(result[0].tijdstippen.length).toBe(0);
  });
});
