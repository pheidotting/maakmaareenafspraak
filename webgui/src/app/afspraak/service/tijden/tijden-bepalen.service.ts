import {Injectable} from '@angular/core';
import {Dag} from "../../model/opvragen-salon-response/dag";
import {Behandeling} from "../../model/opvragen-salon-response/behandeling";
import {Medewerker} from "../../model/opvragen-salon-response/medewerker";
import {Tijdstip} from "../../model/opvragen-salon-response/tijdstip";

@Injectable({
  providedIn: 'root'
})
export class TijdenBepalenService {
  tijdseenheid: number = 15;

  constructor() {
  }

  bepaalTotaalTijdseenheden(behandelingen: Behandeling[]): number {
    let totaal: number = 0;
    behandelingen.forEach(behandeling => {
      if (behandeling.tijdsduur != null) {
        totaal = totaal + behandeling.tijdsduur;
      }
    });

    return totaal / this.tijdseenheid;
  }

  bepaalTijden(dagen: Dag[], behandelingen: Behandeling[], medewerker: Medewerker): Dag[] {
    let totaalTijdseenheden: number = this.bepaalTotaalTijdseenheden(behandelingen);
    // console.log('totaalTijdseenheden '+totaalTijdseenheden);

    dagen.forEach(dag => {
      // console.log('---------');
      // dag.tijdstippen.forEach(tijdstip => {
      //   console.log(tijdstip.tijd);
      // });
      // console.log('---------');
      if (medewerker != null) {
        // console.log('---------');
        // dag.tijdstippen.forEach(tijdstip => console.log(tijdstip.medewekerIds));
        // console.log('---------');
        dag.tijdstippen.forEach(tijdstip => {
          tijdstip.medewekerIdsOrigineel = tijdstip.medewekerIds.slice();
          tijdstip.medewekerIds = tijdstip.medewekerIds.filter(id => id == medewerker.id);
        });
        // console.log('||||||||||');
        // dag.tijdstippen.forEach(tijdstip => console.log(tijdstip.medewekerIds));
        // console.log('||||||||||');
      }
      dag.tijdstippen.forEach((tijdstip, index) => {
        // console.log('AA ' + tijdstip.medewekerIds + ' ' + tijdstip.tijd + ' '+tijdstip.aantalAfspraken);

        if (tijdstip.aantalAfspraken > 0) {
          // if (medewerker != null) {
          //   console.log('voorkeur medewerker aanwezig '+tijdstip.tijd);
          //   if (tijdstip.aantalAfspraken >= tijdstip.medewekerIds.length) {
          //     console.log('wegstrepen');
          //     console.log(tijdstip.medewekerIds);
          //     tijdstip.medewekerIds = tijdstip.medewekerIds.slice(tijdstip.medewekerIds.length - 1, tijdstip.aantalAfspraken * -1);
          //     // console.log(tijdstip.medewekerIds);
          //   }
          // }
          if (medewerker == null ||
            (tijdstip.medewekerIdsOrigineel != null && tijdstip.aantalAfspraken >= tijdstip.medewekerIdsOrigineel.length)) {
            tijdstip.medewekerIds = tijdstip.medewekerIds.slice(tijdstip.medewekerIds.length - 1, tijdstip.aantalAfspraken * -1);
          }
          // console.log('&*&*&*&*&*&*&*&');
          // console.log(tijdstip.tijd + ' - ' + tijdstip.medewekerIds.length);
          // console.log('&*&*&*&*&*&*&*&');
        }
        // console.log('BB ' + tijdstip.medewekerIds + ' ' + tijdstip.tijd + ' '+tijdstip.aantalAfspraken);

      });
      dag.tijdstippen.forEach((tijdstip, index) => {
        // console.log('ZZZZZ');
        // console.log(index + ' - ' + tijdstip.tijd + ' - ' + tijdstip.medewekerIds);
        if (!this.pastDit(dag.tijdstippen, index, totaalTijdseenheden)) {
          // console.log('XX ' + tijdstip.medewekerIds.length + ' - ' + tijdstip.medewekerIds + ' - ' + dag.tijdstippen[index].tijd);
          tijdstip.medewekerIds = tijdstip.medewekerIds.splice(0, -1);
          // console.log('YY ' + tijdstip.medewekerIds.length + ' - ' + tijdstip.medewekerIds + ' - ' + dag.tijdstippen[index].tijd);
        }
      });
    });

    dagen.forEach(dag => {
      dag.tijdstippen = dag.tijdstippen.filter(tijdstip => tijdstip.medewekerIds.length > 0);
    });

    // console.log('# # # # # # ');
    // dagen.forEach(dag => {
    //   console.log(dag.datum);
    //   dag.tijdstippen.forEach((tijdstip, index) => {
    //     console.log('  ' + tijdstip.tijd + ' - ' + tijdstip.aantalAfspraken + ' - ' + tijdstip.medewekerIds + ' - ' + tijdstip.medewekerIds.length);
    //   });
    // });
    return dagen;
  }

  pastDit(tijdstippen: Tijdstip[], index: number, totaalTijdseenheden: number): boolean {
    // console.log(index + '-' + JSON.stringify(tijdstippen[index]));
    let result: boolean = false;
    for (let i = index; i < index + totaalTijdseenheden; i++) {
      // if (tijdstippen[i] == null) {
      //   console.log('in loop ' + i + ' - ' + tijdstippen[i]);
      // } else {
      //   console.log('in loop ' + i + ' - ' + tijdstippen[i].medewekerIds + ' - ' + tijdstippen[i].tijd);
      // }
      result = true;
      if (tijdstippen[i] == null || (tijdstippen[i].medewekerIds != null && tijdstippen[i].medewekerIds.length == null)) {
        result = false;
        // console.log('1');
        break;
      } else if (tijdstippen[i].medewekerIds != null && tijdstippen[i].medewekerIds.length == 0) {
        result = false;
        // console.log('2');
        break;
        // } else {
        // console.log('else');
      }
    }

    // console.log('pastDit '+result);
    return result;
  }
}
