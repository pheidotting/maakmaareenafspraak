import {TestBed} from '@angular/core/testing';

import {TijdenBepalenService} from './tijden-bepalen.service';
import {HttpClientTestingModule} from "@angular/common/http/testing";
import {Behandeling} from "../../model/opvragen-salon-response/behandeling";
import {Tijdstip} from "../../model/opvragen-salon-response/tijdstip";
import {LocalTime} from "@js-joda/core";

describe('TijdenBepalenService', () => {
  let service: TijdenBepalenService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [
        HttpClientTestingModule,
      ],
      providers: [TijdenBepalenService
      ],
    });

    service = TestBed.get(TijdenBepalenService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  // let opvragenSalonResponse: OpvragenSalonResponse = {
  //   "salon": {
  //     "id": 34,
  //     "naam": "naam",
  //     "straat": "straat",
  //     "postcode": "postcode",
  //     "plaats": "plaats",
  //     "telefoonnummer": "telefoonnummer",
  //     "emailadres": "emailadres",
  //     "medewerkers": [
  //       {
  //         "id": 1,
  //         "naam": "medewerker1",
  //         "avatarAanwezig": false
  //       },
  //       {
  //         "id": 2,
  //         "naam": "medewerker2",
  //         "avatarAanwezig": false
  //       }
  //     ],
  //     "behandelingen": [
  //       {
  //         "id": 1,
  //         "naam": "naam1",
  //         "omschrijving": "omschrijving1",
  //         "tijdsduur": 15,
  //         "avatarAanwezig": false,
  //         "prijs": 10,
  //         "medewerkers": [
  //           1,
  //           2
  //         ]
  //       },
  //       {
  //         "id": 2,
  //         "naam": "naam1",
  //         "omschrijving": "omschrijving1",
  //         "tijdsduur": 30,
  //         "avatarAanwezig": false,
  //         "prijs": 20,
  //         "medewerkers": [
  //           1
  //         ]
  //       }
  //     ]
  //   },
  //   "dagen": [
  //     {
  //       "datum": "2021-03-22",
  //       "tijdstippen": [
  //         {
  //           "medewekerIds": [
  //             1,
  //             2
  //           ],
  //           "tijd": "09:00:00"
  //         },
  //         {
  //           "medewekerIds": [
  //             1,
  //             2
  //           ],
  //           "tijd": "09:15:00"
  //         },
  //         {
  //           "medewekerIds": [
  //             1,
  //             2
  //           ],
  //           "tijd": "09:30:00"
  //         },
  //         {
  //           "medewekerIds": [
  //             1,
  //             2
  //           ],
  //           "tijd": "09:45:00"
  //         },
  //         {
  //           "medewekerIds": [
  //             1,
  //             2
  //           ],
  //           "tijd": "10:00:00"
  //         },
  //         {
  //           "medewekerIds": [
  //             1,
  //             2
  //           ],
  //           "tijd": "10:15:00"
  //         },
  //         {
  //           "medewekerIds": [
  //             1,
  //             2
  //           ],
  //           "tijd": "10:30:00"
  //         },
  //         {
  //           "medewekerIds": [
  //             1,
  //             2
  //           ],
  //           "tijd": "10:45:00"
  //         },
  //         {
  //           "medewekerIds": [
  //             1
  //           ],
  //           "tijd": "11:00:00"
  //         },
  //         {
  //           "medewekerIds": [
  //             1
  //           ],
  //           "tijd": "11:15:00"
  //         },
  //         {
  //           "medewekerIds": [
  //             1,
  //             2
  //           ],
  //           "tijd": "11:30:00"
  //         },
  //         {
  //           "medewekerIds": [
  //             1,
  //             2
  //           ],
  //           "tijd": "11:45:00"
  //         }
  //       ]
  //     }
  //   ]
  // };

  it('test voor bepaalTotaalTijdseenheden', () => {
    let behandeling1: Behandeling = new Behandeling();
    behandeling1.tijdsduur = 15;
    let behandeling2: Behandeling = new Behandeling();
    behandeling2.tijdsduur = 30;
    let behandeling3: Behandeling = new Behandeling();

    expect(service.bepaalTotaalTijdseenheden([behandeling1, behandeling2, behandeling3])).toEqual(3);
  });
  it('test voor pastDit', () => {
    let tijdstip1: Tijdstip = new Tijdstip();
    tijdstip1.tijd = LocalTime.of(9, 0);
    let tijdstip2: Tijdstip = new Tijdstip();
    tijdstip2.tijd = LocalTime.of(9, 15);
    let tijdstip3: Tijdstip = new Tijdstip();
    tijdstip3.tijd = LocalTime.of(9, 30);

    expect(service.pastDit([tijdstip1, tijdstip2, tijdstip3], 0, 1)).toBe(true);
    expect(service.pastDit([tijdstip1, tijdstip2, tijdstip3], 0, 2)).toBe(true);
    expect(service.pastDit([tijdstip1, tijdstip2, tijdstip3], 0, 3)).toBe(true);
    expect(service.pastDit([tijdstip1, tijdstip2, tijdstip3], 0, 4)).toBe(false);
    expect(service.pastDit([tijdstip1, tijdstip2, tijdstip3], 1, 1)).toBe(true);
    expect(service.pastDit([tijdstip1, tijdstip2, tijdstip3], 1, 2)).toBe(true);
    expect(service.pastDit([tijdstip1, tijdstip2, tijdstip3], 1, 3)).toBe(false);
    expect(service.pastDit([tijdstip1, tijdstip2, tijdstip3], 1, 4)).toBe(false);
    expect(service.pastDit([tijdstip1, tijdstip2, tijdstip3], 2, 1)).toBe(true);
    expect(service.pastDit([tijdstip1, tijdstip2, tijdstip3], 2, 2)).toBe(false);
    expect(service.pastDit([tijdstip1, tijdstip2, tijdstip3], 2, 3)).toBe(false);
    expect(service.pastDit([tijdstip1, tijdstip2, tijdstip3], 2, 4)).toBe(false);
    expect(service.pastDit([tijdstip1, tijdstip2, tijdstip3], 3, 1)).toBe(false);
    expect(service.pastDit([tijdstip1, tijdstip2, tijdstip3], 3, 2)).toBe(false);
    expect(service.pastDit([tijdstip1, tijdstip2, tijdstip3], 3, 3)).toBe(false);
    expect(service.pastDit([tijdstip1, tijdstip2, tijdstip3], 3, 4)).toBe(false);
    expect(service.pastDit([tijdstip1, tijdstip2, tijdstip3], 4, 1)).toBe(false);
    expect(service.pastDit([tijdstip1, tijdstip2, tijdstip3], 4, 2)).toBe(false);
    expect(service.pastDit([tijdstip1, tijdstip2, tijdstip3], 4, 3)).toBe(false);
    expect(service.pastDit([tijdstip1, tijdstip2, tijdstip3], 4, 4)).toBe(false);
  });
});
