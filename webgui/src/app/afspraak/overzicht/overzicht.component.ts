import {Component, Input, OnInit} from '@angular/core';
import {Afspraakinfo} from "../model/afspraakinfo/afspraakinfo";
import {MessageService} from "primeng/api";
import {Observable, Subscription} from "rxjs";
import {AfspraakService} from "../service/afspraak/afspraak.service";
import {LocalDate} from "@js-joda/core";
import {InloggenServiceService} from "../../commons/inloggen/inloggen-service.service";
import {Ingelogd} from "../../commons/inloggen/ingelogd";

@Component({
  selector: 'app-overzicht',
  templateUrl: './overzicht.component.html',
  styleUrls: ['./overzicht.component.css'],
  providers: [MessageService]
})
export class OverzichtComponent implements OnInit {
  korteNetnummers: string[] = ['010', '013', '014', '015', '020', '023', '024', '026', '030', '033', '035', '036', '038', '040', '043', '045', '046', '050', '053', '055', '058', '070', '071', '072', '073', '074', '075', '076', '077', '078', '079'];

  // @Output() inloggenEventEmmitter: EventEmitter<Klant> = new EventEmitter<Klant>();
  private eventsSubscription: Subscription;

  @Input() events: Observable<void>;

  emailadresKlant: string;
  telefoonKlant: string;
  naamKlant: string;
  wilEenAccountAanmaken: boolean = false;

  accountAanmakenNaam: string;
  accountAanmakenMail: string;
  accountAanmakenWachtwoord: string;
  accountAanmakenWachtwoordNogmaals: string;
  accountAanmakenTelefoon: string;
  nieuwWachtwoordSterkGenoeg: boolean = false;

  afspraakGemaakt: boolean = false;
  ingelogd: boolean = false;

  constructor(private messageService: MessageService, private _afspraakService: AfspraakService, private inloggenService: InloggenServiceService) {
  }

  ngOnInit(): void {
    this.eventsSubscription = this.events.subscribe(() => this.uitloggen());
    if (this.inloggenService.ingelogd != null) {
      this.ingelogd = true;
    }
  }

  afspraakServicet(): AfspraakService {
    return this._afspraakService;
  }

  klantIsLeeg(): boolean {
    let klant: Ingelogd = this.inloggenService.ingelogd;
    return klant == null
      || klant.naam == null
      || klant.username == null;
  }

  afspraakInfo(): Afspraakinfo {
    return this._afspraakService.afspraakInfo;
  }

  totaalprijs() {
    let totaalprijs = 0;
    this.afspraakInfo().stap2.behandelingen.forEach(b => totaalprijs = totaalprijs + b.prijs);

    return totaalprijs;
  }

  tijdsduur() {
    let totaaltijdsduur = 0;
    this.afspraakInfo().stap2.behandelingen.forEach(b => totaaltijdsduur = totaaltijdsduur + b.tijdsduur);

    return totaaltijdsduur;
  }

  tijdsduurOpgemaakt(tijdsduur: number) {
    if (tijdsduur > 60) {
      let uren: number = Math.floor(tijdsduur / 60);
      let minuten: number = tijdsduur - (uren * 60);
      return uren + ' uur en ' + minuten + ' minuten';
    } else {
      return tijdsduur + ' minuten';
    }
  }

  accountAanmakenKnopDisabled(): boolean {
    return this.nieuwWachtwoordSterkGenoeg && this.wachtwoordenKomenOvereen();
  }

  onClickBevestigen() {
    if (this.inloggenService.ingelogd == null) {
      this.inloggenService.ingelogd = new Ingelogd();
      this.inloggenService.ingelogd.username = this.emailadresKlant;
      this.inloggenService.ingelogd.telefoonnummer = this.telefoonKlant;
      this.inloggenService.ingelogd.naam = this.naamKlant;
    }
    this._afspraakService.afspraakOpslaan().subscribe(response => {
      this.afspraakGemaakt = true;
      this.messageService.add({
        severity: 'success',
        summary: 'Afspraak gemaakt',
        detail: 'De afspraak is gemaakt. Er is een bevestiging gestuurd naar ' + this.inloggenService.ingelogd.username
      });
    });
  }

  onClickAccountMaken() {
    this.messageService.add({
      severity: 'success',
      summary: 'Aangemaakt',
      detail: 'Account is aangemaakt. Er is een bevestiging gestuurd naar ' + this.accountAanmakenMail
    });
    this.wilEenAccountAanmaken = false;
    let ingelogd = new Ingelogd();
    ingelogd.username = this.accountAanmakenMail;
    ingelogd.telefoonnummer = this.accountAanmakenTelefoon;
    ingelogd.naam = this.accountAanmakenNaam;
    this.ingelogd = true;
    this.inloggenService.ingelogd = ingelogd;
    // this.inloggenEventEmmitter.emit(ingelogd);
  }

  onClickAccountMakenPopupTonen() {
    this.wilEenAccountAanmaken = true;
  }

  bevestigenKnopDisabled() {
    if (this.afspraakService.klant == null) {
      if (this.emailadresKlant == null || this.emailadresKlant == '') {
        return true;
      } else {
        return false;
      }
    } else {
      return false;
    }
    return false;
  }

  datum() {
    let datum: LocalDate = this.afspraakInfo().stap3.datum;
    let maand: string = '' + (datum.month().value());
    if (maand.length == 1) {
      maand = '0' + maand;
    }
    let dag: string = '' + datum.dayOfMonth();
    if (dag.length == 1) {
      dag = '0' + dag;
    }

    return dag + '-' + maand + '-' + datum.year();
  }

  wachtwoordenKomenOvereen(): boolean {
    return this.accountAanmakenWachtwoord == this.accountAanmakenWachtwoordNogmaals;
  }

  onPasswordStrengthChanged(event: number) {
    this.nieuwWachtwoordSterkGenoeg = event > 3;
  }

  zetTelefoonKlantOm() {
    this.telefoonKlant = this.zetTelefoonnummerOm(this.telefoonKlant);
  }

  zetAccountAanmakenTelefoonOm() {
    this.accountAanmakenTelefoon = this.zetTelefoonnummerOm(this.accountAanmakenTelefoon);
  }

  zetTelefoonnummerOm(telefoonnummer: string) {
    let tel = telefoonnummer;
    if (tel !== null && tel !== undefined && tel.length === 10) {
      if (tel.substring(0, 2) === "06") {
        //06 nummers
        tel = tel.substring(0, 2) + " - " + tel.substring(2, 4) + " " + tel.substring(4, 6) + " " + tel.substring(6, 8) + " " + tel.substring(8, 10);
        // } else if (contains(this.korteNetnummers, tel.substring(0, 3))) {
      } else if (this.korteNetnummersContains(tel.substring(0, 3))) {
        //3 cijferig kengetal
        tel = tel.substring(0, 3) + " - " + tel.substring(3, 6) + " " + tel.substring(6, 8) + " " + tel.substring(8, 10);
      } else {
        //4 cijferig kengetal
        tel = tel.substring(0, 4) + " - " + tel.substring(4, 6) + " " + tel.substring(6, 8) + " " + tel.substring(8, 10);
      }
      return tel;
    }
  }

  korteNetnummersContains(mogelijkKortNetnummer: string): boolean {
    return this.korteNetnummers.filter(nummer => nummer === mogelijkKortNetnummer).length > 0;
  }

  uitloggen() {
    this.ingelogd = false;
  }

  get afspraakService(): AfspraakService {
    return this._afspraakService;
  }
}
