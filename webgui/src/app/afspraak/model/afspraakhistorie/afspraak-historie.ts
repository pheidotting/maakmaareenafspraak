import {LocalDateTime} from "@js-joda/core";

export class AfspraakHistorie {
  tijdstip: LocalDateTime;
  tijdstipWijziging: LocalDateTime;
  username: string;
  opmerking: string;
  medewerker: string;
  soortWijziging: string;
  behandelingen: string[] = [];
}
