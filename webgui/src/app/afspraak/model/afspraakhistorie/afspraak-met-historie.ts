import {LocalDateTime} from "@js-joda/core";
import {AfspraakHistorie} from "./afspraak-historie";

export class AfspraakMetHistorie {
  id: number;
  tijdstip: LocalDateTime;
  opmerking: string;
  salon: string;
  medewerker: string;
  behandelingen: string[] = [];
  afspraakHistorieList: AfspraakHistorie[] = [];
}
