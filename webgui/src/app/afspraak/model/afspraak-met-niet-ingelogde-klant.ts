import {LocalDateTime} from "@js-joda/core";

export class AfspraakMetNietIngelogdeKlant {
  naam: string;
  emailadres: string;
  telefoonnummer: string;
  fotoUrl: string;
  tijdstip: LocalDateTime;
  opmerking: string;
  salonId: number;
  voorkeurMedewerkerId: number;
  behandelingIds: number[] = [];
}
