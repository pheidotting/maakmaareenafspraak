import {LocalDateTime} from "@js-joda/core";

export class AfspraakMetIngelogdeKlant {
  tijdstip: LocalDateTime;
  opmerking: string;
  salonId: number;
  voorkeurMedewerkerId: number;
  behandelingIds: number[] = [];

}
