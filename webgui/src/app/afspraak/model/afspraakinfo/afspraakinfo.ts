import {Behandeling} from "../opvragen-salon-response/behandeling";
import {Medewerker} from "../opvragen-salon-response/medewerker";
import {LocalDate, LocalTime} from "@js-joda/core";

export class Afspraakinfo {
  stap1: Stap1 = new Stap1();
  stap2: Stap2 = new Stap2();
  stap3: Stap3 = new Stap3();
  opmerking: string;
}

class Stap1 {
  salon: Salon = new Salon();
}

class Stap2 {
  medewerker: Medewerker = new Medewerker();
  behandelingen: Behandeling[] = [];
}

class Stap3 {
  datum: LocalDate;
  tijd: LocalTime;
}

export class Salon {
  id: number;
  naam: string;
  straat: string;
  huisnummer: string;
  postcode: string;
  plaats: string;
  telefoonnummer: string;
  emailadres: string;
  avatarAanwezig: boolean;
}

export class Klant {
  id: number;
  naam: string;
  emailadres: string;
  telefoonnummer: string;
  photoUrl: string;
  jwt: string;
  social: string;
}
