import {Tijdstip} from "./tijdstip";
import {LocalDate} from "@js-joda/core";

export class Dag {
  datum: LocalDate;
  tijdstippen: Tijdstip[];
}
