export class Behandeling {
  id: number;
  naam: string;
  omschrijving: string;
  tijdsduur: number;
  prijs: number;
  avatarAanwezig: boolean;
  medewerkers: number[];
}
