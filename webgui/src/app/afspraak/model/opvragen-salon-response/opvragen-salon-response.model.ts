import {Salon} from "./salon";
import {Dag} from "./dag";


export class OpvragenSalonResponse {
  salon: Salon;
  dagen: Dag[];
}
