import {Medewerker} from "./medewerker";
import {Behandeling} from "./behandeling";

export class Salon {
  id: number;
  naam: string;
  straat: string;
  huisnummer: string;
  postcode: string;
  plaats: string;
  telefoonnummer: string;
  emailadres: string;

  medewerkers: Medewerker[];
  behandelingen: Behandeling[];
}
