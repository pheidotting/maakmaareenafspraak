import {Guid} from 'guid-typescript';

export class Medewerker {
  tijdelijkeId: string = Guid.create().toString();
  id: number;
  naam: string;
  avatarAanwezig: boolean;
}
