import {LocalTime} from "@js-joda/core";

export class Tijdstip {
  medewekerIds: number[];
  tijd: LocalTime;
  aantalAfspraken: number;
  medewekerIdsOrigineel: number[];
}
