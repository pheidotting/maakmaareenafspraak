import { ComponentFixture, TestBed } from '@angular/core/testing';

import { TijdstipbepalenComponent } from './tijdstipbepalen.component';

describe('TijdstipbepalenComponent', () => {
  let component: TijdstipbepalenComponent;
  let fixture: ComponentFixture<TijdstipbepalenComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ TijdstipbepalenComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(TijdstipbepalenComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
