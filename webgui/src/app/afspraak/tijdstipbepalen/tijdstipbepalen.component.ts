import {Component, Injectable, OnInit, ViewChild} from '@angular/core';
import {FullCalendar} from 'primeng/fullcalendar'
import {Afspraakinfo} from "../model/afspraakinfo/afspraakinfo";
import {SelectItemGroup} from "primeng/api";
import {AfspraakService} from "../service/afspraak/afspraak.service";
import {LocalDate, LocalTime} from "@js-joda/core";
import {Dag} from "../model/opvragen-salon-response/dag";
import {SelectItem} from "primeng/api/selectitem";

@Injectable()
@Component({
  selector: 'app-tijdstipbepalen',
  templateUrl: './tijdstipbepalen.component.html',
  styleUrls: ['./tijdstipbepalen.component.css']
})
export class TijdstipbepalenComponent implements OnInit {//}, AfterViewInit {
  @ViewChild('fc') fc: FullCalendar;

  vandaag: Date = new Date();
  tijden: SelectItemGroup[] = [];
  geklikt: string;

  constructor(private afspraakService: AfspraakService) {
  }

  afspraakServicet(): AfspraakService {
    return this.afspraakService;
  }

  afspraakInfo(): Afspraakinfo {
    return this.afspraakService.afspraakInfo;
  }

  onSelectPopup(event: Date) {
    let iMaand = event.getMonth() + 1;
    let iDag = event.getDate();

    let maand: string = '' + (iMaand);
    if (maand.length == 1) {
      maand = '0' + maand;
    }
    let dag: string = '' + iDag;
    if (dag.length == 1) {
      dag = '0' + dag;
    }

    this.afspraakInfo().stap3 = {datum: LocalDate.of(event.getFullYear(), iMaand, iDag), tijd: null};
    this.geklikt = dag + '-' + maand + '-' + event.getFullYear();
    this.display = true;

    let geselecteerdeDag: Dag = this.afspraakService.dagen.filter(dag => dag.datum.equals(LocalDate.of(event.getFullYear(), iMaand, iDag)))[0];
    let nulUur = LocalTime.of(0, 0);
    let zesUur = LocalTime.of(6, 0);
    let twaalfUur = LocalTime.of(12, 0);
    let achtienUur = LocalTime.of(18, 0);
    let drieentwintigUurNegenEnVijftig = LocalTime.of(23, 59);

    let nacht: SelectItem[] = [];
    let ochtend: SelectItem[] = [];
    let middag: SelectItem[] = [];
    let avond: SelectItem[] = [];
    geselecteerdeDag.tijdstippen.forEach(tijdstip => {
      let tijd: LocalTime = tijdstip.tijd;
      if (!tijd.isBefore(nulUur) && tijd.isBefore(zesUur)) {
        nacht.push({label: tijd + '', value: tijd + ''});
      }
      if (!tijd.isBefore(zesUur) && tijd.isBefore(twaalfUur)) {
        ochtend.push({label: tijd + '', value: tijd + ''});
      }
      if (!tijd.isBefore(twaalfUur) && tijd.isBefore(achtienUur)) {
        middag.push({label: tijd + '', value: tijd + ''});
      }
      if (!tijd.isBefore(achtienUur) && tijd.isBefore(drieentwintigUurNegenEnVijftig)) {
        avond.push({label: tijd + '', value: tijd + ''});
      }
    });

    this.tijden = [];
    if (nacht.length > 0) {
      this.tijden.push({label: 'Nacht', value: 'n', items: nacht})
    }
    if (ochtend.length > 0) {
      this.tijden.push({label: 'Ochtend', value: 'o', items: ochtend})
    }
    if (middag.length > 0) {
      this.tijden.push({label: 'Middag', value: 'm', items: middag})
    }
    if (avond.length > 0) {
      this.tijden.push({label: 'Avond', value: 'a', items: avond})
    }
    // this.tijden = [
    //   // {
    //   //   label: 'Nacht', value: 'n',
    //   //   items: [
    //   // {label: '4:00', value: '4:00'},
    //   //   ]
    //   // },
    //   {
    //     label: 'Ochtend', value: 'o',
    //     items: [
    //       {label: '9:00', value: '9:00'},
    //       {label: '9:15', value: '9:15'},
    //       {label: '10:30', value: '10:30'}
    //     ]
    //   },
    //   {
    //     label: 'Middag', value: 'm',
    //     items: [
    //       {label: '13:00', value: '13:00'},
    //       {label: '14:00', value: '14:00'},
    //       {label: '16:30', value: '16:30'},
    //       {label: '17:00', value: '17:00'}
    //     ]
    //   },
    //   {
    //     label: 'Avond', value: 'a',
    //     items: [
    //       {label: '20:00', value: '20:00'}
    //     ]
    //   }
    // ];
  }

  tijdstip: string;
  display: boolean = false;

  ngOnInit() {
  }

  onClickDoorNaarStap4() {
    this.afspraakInfo().stap3.tijd = LocalTime.parse(this.tijdstip);
    this.afspraakService.naarVolgendeStap()
  }

  onClickNaarVorigeStap() {
    // this.terugNaarStap2EventEmitter.emit(this.afspraakInfo());
  }
}
