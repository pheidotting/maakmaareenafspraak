import {Component, Injectable, OnInit} from '@angular/core';
import {Afspraakinfo} from "./model/afspraakinfo/afspraakinfo";
import {MegaMenuItem, MessageService} from 'primeng/api';
import {Subject} from "rxjs";
import {AfspraakService} from "./service/afspraak/afspraak.service";
import {Router} from "@angular/router";
import {InloggenServiceService} from "../commons/inloggen/inloggen-service.service";

@Injectable()
@Component({
  selector: 'app-afspraak',
  templateUrl: './afspraak.component.html',
  styleUrls: ['./afspraak.component.css'],
  providers: [MessageService]
})
export class AfspraakComponent implements OnInit {
  uitloggenEventsSubject: Subject<void> = new Subject<void>();

  huidigeStap(): number {
    return this._afspraakService.huidigeStap;
  }

  inloggenMail: string;
  inloggenPass: string;

  megaMenuItems: MegaMenuItem[];

  tUitloggen: string = 'Uitloggen';
  tInloggen: string = 'Inloggen';

  constructor(private router: Router, private messageService: MessageService, private _afspraakService: AfspraakService, private _inloggenService: InloggenServiceService) {
    this._inloggenService.ingelogdSubject.subscribe(klant => {
      this.setMegaMenu();
    });
  }

  onclickInloggen() {
    this._inloggenService.inloggen(this.inloggenMail, this.inloggenPass);
    this._inloggenService.ingelogdSubject.subscribe(observer => {
      if (this._inloggenService.ingelogd != null) {
        this.messageService.add({
          severity: 'success',
          summary: 'Ingelogd',
          detail: 'Je bent ingelogd'
        });
      }
    });
    this.inloggenMail = null;
    this.inloggenPass = null;
  }

  afspraakInfo(): Afspraakinfo {
    return this._afspraakService.afspraakInfo;
  }

  getNaamInlogdeKlant(): string {
    if (this._inloggenService.ingelogd != null) {
      return this._inloggenService.ingelogd.naam;
    } else {
      return null;
    }
  }

  setMegaMenu() {
    if (this.getNaamInlogdeKlant() == null) {
      this.megaMenuItems = [
        {
          label: this.tInloggen, command: (event) => {
            if (this.tInloggen == event.item.label) {
              this._inloggenService.wilInloggen = true;
            }
          }
        }
        , {
          label: 'Afspraak maken', command: (event) => {
            if ('Afspraak maken' == event.item.label) {
              this.router.navigate([`../afspraak-maken.html`]);
            }
          }
        }
        , {
          label: 'Salon aanmelden', command: (event) => {
            if ('Salon aanmelden' == event.item.label) {
              this.router.navigate([`../aanmelden.html`]);
            }
          }
        }

      ]

    } else if (this._inloggenService.ingelogd != null && this._inloggenService.ingelogd.soortUser == 'KLANT') {
      this.megaMenuItems = [
        {
          label: this.getNaamInlogdeKlant(),
          items: [
            [
              {
                label: 'Acties',
                items:
                  [
                    {
                      label: this.tUitloggen, command: (event) => {
                        if (this.tUitloggen == event.item.label) {
                          this._inloggenService.uitloggen();
                          this.setMegaMenu();
                          this.messageService.add({
                            severity: 'success',
                            summary: 'Uitgelogd',
                            detail: 'Je bent uitgelogd'
                          });
                          this.uitloggenEventsSubject.next();
                        }
                      }
                    }
                  ]
              }
            ]
          ]
        }
        , {
          label: 'Afspraak maken', command: (event) => {
            if ('Afspraak maken' == event.item.label) {
              this.router.navigate([`../afspraak-maken.html`]);
            }
          }
        }
        , {
          label: 'Alle gemaakte afspraken', command: (event) => {
            if ('Alle gemaakte afspraken' == event.item.label) {
              this.router.navigate([`../afspraken.html`]);
            }
          }
        }
      ]
    } else if (this._inloggenService.ingelogd != null && this._inloggenService.ingelogd.soortUser == 'BEHEERDER') {
      this.megaMenuItems = [
        {
          label: this.getNaamInlogdeKlant(),
          items: [
            [
              {
                label: 'Acties',
                items:
                  [
                    {
                      label: this.tUitloggen, command: (event) => {
                        if (this.tUitloggen == event.item.label) {
                          this._inloggenService.uitloggen();
                          this.setMegaMenu();
                          this.messageService.add({
                            severity: 'success',
                            summary: 'Uitgelogd',
                            detail: 'Je bent uitgelogd'
                          });
                          this.uitloggenEventsSubject.next();
                        }
                      }
                    }
                  ]
              }
            ]
          ]
        }, {
          label: 'Dashboard', command: (event) => {
            if ('Dashboard' == event.item.label) {
              this.router.navigate([`../dashboard.html`]);
            }
          }
        }, {
          label: 'Instellingen',
          items: [
            [
              {
                items:
                  [
                    {
                      label: 'Salon', command: (event) => {
                        if ('Salon' == event.item.label) {
                          this.router.navigate([`../instellingen/salon.html`]);
                        }
                      }
                    },
                    {
                      label: 'Rooster', command: (event) => {
                        if ('Rooster' == event.item.label) {
                          this.router.navigate([`../instellingen/rooster.html`]);
                        }
                      }
                    },
                    {
                      label: 'Medewerkers', command: (event) => {
                        if ('Medewerkers' == event.item.label) {
                          this.router.navigate([`../instellingen/medewerkers.html`]);
                        }
                      }
                    },
                    {
                      label: 'Behandelingen', command: (event) => {
                        if ('Behandelingen' == event.item.label) {
                          this.router.navigate([`../instellingen/behandelingen.html`]);
                        }
                      }
                    }
                  ]
              }
            ]
          ]
        }
      ]
    }
  }

  ngOnInit(): void {
    this.setMegaMenu();
  }


  initialen(naam: string): string {
    return naam.split(" ").map((n) => n[0]).join("");
  }

  get afspraakService(): AfspraakService {
    return this._afspraakService;
  }

  get inloggenService(): InloggenServiceService {
    return this._inloggenService;
  }
}
