import {Injectable} from '@angular/core';
import {HttpEvent, HttpHandler, HttpInterceptor, HttpRequest} from '@angular/common/http';
import {Observable} from "rxjs";
import {Klant} from "../model/afspraakinfo/afspraakinfo";

@Injectable()
export class TokenInterceptor implements HttpInterceptor {
  constructor() {
  }

  intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    if (!request.url.endsWith('signin')) {
      if (localStorage.getItem("mmea-jwt") != null) {
        let klant: Klant = JSON.parse(localStorage.getItem("mmea-jwt"));
        if (klant != null) {
          request = request.clone({
            setHeaders: {
              Authorization: `Bearer ${klant.jwt}`
            }
          });
        }
      }
    }
    const newRequest = request.clone({url: request.url.replace(':8080', ':8585')});
    return next.handle(newRequest);
    // return next.handle(request);
  }
}
