import {ComponentFixture, TestBed} from '@angular/core/testing';

import {AfspraakHistorieComponent} from './afspraak-historie.component';

describe('AfspraakHistorieComponent', () => {
  let component: AfspraakHistorieComponent;
  let fixture: ComponentFixture<AfspraakHistorieComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [AfspraakHistorieComponent]
    })
      .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AfspraakHistorieComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
