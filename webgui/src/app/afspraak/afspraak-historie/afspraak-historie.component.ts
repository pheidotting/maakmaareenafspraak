import {Component, OnInit} from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {AfspraakMetHistorie} from "../model/afspraakhistorie/afspraak-met-historie";
import {DateTimeFormatter, LocalDateTime} from "@js-joda/core";
import {AfspraakHistorie} from "../model/afspraakhistorie/afspraak-historie";

@Component({
  selector: 'app-afspraak-historie',
  templateUrl: './afspraak-historie.component.html',
  styleUrls: ['./afspraak-historie.component.css']
})
export class AfspraakHistorieComponent implements OnInit {
  private url: string = 'http://localhost:8080/afspraak/allesVoorKlant';

  constructor(private httpClient: HttpClient) {
  }

  events: any[];

  ngOnInit() {
    this.httpClient.get(this.url).subscribe((response: AfspraakMetHistorie[]) => {
      response.forEach((afspraakMetHistorie: AfspraakMetHistorie) => {
        afspraakMetHistorie.tijdstip = LocalDateTime.parse('' + afspraakMetHistorie.tijdstip);
        afspraakMetHistorie.afspraakHistorieList.forEach((afspraakHistorie: AfspraakHistorie) => {
          afspraakHistorie.tijdstip = LocalDateTime.parse('' + afspraakHistorie.tijdstip);
          afspraakHistorie.tijdstipWijziging = LocalDateTime.parse('' + afspraakHistorie.tijdstipWijziging);
        });
      });
      this.events = response;
    });
  }

  maakDatumOp(tijdstip: LocalDateTime): string {
    return tijdstip.format(DateTimeFormatter.ofPattern('dd-MM-y HH:mm'));
  }
}
