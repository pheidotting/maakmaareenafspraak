import {Component, Injectable, OnInit} from '@angular/core';
import {Afspraakinfo} from "../model/afspraakinfo/afspraakinfo";
import {MessageService} from 'primeng/api';
import {Subject} from "rxjs";
import {AfspraakService} from "../service/afspraak/afspraak.service";
import {InloggenServiceService} from "../../commons/inloggen/inloggen-service.service";
import {Ingelogd} from "../../commons/inloggen/ingelogd";

@Injectable()
@Component({
  selector: 'app-afspraak-maken',
  templateUrl: './afspraak-maken.component.html',
  styleUrls: ['./afspraak-maken.component.css'],
  providers: [MessageService]
})
export class AfspraakMakenComponent implements OnInit {
  uitloggenEventsSubject: Subject<void> = new Subject<void>();

  huidigeStap(): number {
    return this._afspraakService.huidigeStap;
  }

  constructor(private _afspraakService: AfspraakService, private inloggenService: InloggenServiceService) {
  }

  afspraakInfo(): Afspraakinfo {
    return this._afspraakService.afspraakInfo;
  }

  ngOnInit(): void {
  }

  get afspraakService(): AfspraakService {
    return this._afspraakService;
  }

  inloggenStap4(ingelogd: Ingelogd) {
    this.inloggenService.ingelogd = ingelogd;
  }

}
