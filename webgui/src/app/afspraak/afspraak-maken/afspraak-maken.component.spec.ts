import {ComponentFixture, TestBed} from '@angular/core/testing';

import {AfspraakMakenComponent} from './afspraak-maken.component';

describe('AfspraakMakenComponent', () => {
  let component: AfspraakMakenComponent;
  let fixture: ComponentFixture<AfspraakMakenComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [AfspraakMakenComponent]
    })
      .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AfspraakMakenComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
