import {Component, Injectable, OnInit} from '@angular/core';
import {Afspraakinfo} from "../model/afspraakinfo/afspraakinfo";
import {AfspraakService} from "../service/afspraak/afspraak.service";
import {Behandeling} from "../model/opvragen-salon-response/behandeling";
import {Medewerker} from "../model/opvragen-salon-response/medewerker";

@Injectable()
@Component({
  selector: 'app-selecteer-medewerker-en-behandeling',
  templateUrl: './selecteer-medewerker-en-behandeling.component.html',
  styleUrls: ['./selecteer-medewerker-en-behandeling.component.css']
})
export class SelecteerMedewerkerEnBehandelingComponent implements OnInit {
  behandelingenBackup: Behandeling[] = [];
  medewerkersLijst: Medewerker[] = [];

  // elementType = NgxQrcodeElementTypes.URL;
  // correctionLevel = NgxQrcodeErrorCorrectionLevels.HIGH;
  // value = 'http://heidotting.nl/';

  constructor(private _afspraakService: AfspraakService) {
  }

  medewerkers() {
    return this.medewerkersLijst;
  }

  afspraakInfo(): Afspraakinfo {
    return this.afspraakService.afspraakInfo;
  }

  ngOnInit(): void {
    if (this.afspraakService.response != null) {
      this.medewerkersLijst = this.afspraakService.response.salon.medewerkers;
      this.afspraakService.response.salon.behandelingen.forEach(b => this.behandelingenBackup.push(b));
    } else {
      this.afspraakService.opvragenSalonResponse.subscribe(response => {
        this.medewerkersLijst = response.salon.medewerkers;
        response.salon.behandelingen.forEach(b => this.behandelingenBackup.push(b));
      });
    }
  }

  geselecteerdeMedewerker(): Medewerker {
    return this.medewerkers().filter(m => m.id == this.afspraakService.medewerkerId)[0];
  }

  getMedewerker(id): Medewerker {
    return this.medewerkers().filter(s => s.id == id)[0];
  }

  initialen(id: number): string {
    return this.getMedewerker(id).naam.split(" ").map((n) => n[0]).join("");
  }

  initialenBehandeling(behandeling: Behandeling): string {
    return behandeling.naam.split(" ").map((n) => n[0]).join("");
  }

  totaalprijs() :number{
    let totaalprijs:number = 0;
    this.afspraakService.list2.forEach(b => totaalprijs = totaalprijs + b.prijs);

    return totaalprijs;
  }

  totaleTijdsduur() {
    let totaaltijdsduur = 0;
    this.afspraakService.list2.forEach(b => totaaltijdsduur = totaaltijdsduur + b.tijdsduur);

    return totaaltijdsduur;
  }

  tijdsduurOpgemaakt(tijdsduur: number) {
    if (tijdsduur > 60) {
      let uren: number = Math.floor(tijdsduur / 60);
      let minuten: number = tijdsduur - (uren * 60);
      return uren + ' uur en ' + minuten + ' minuten';
    } else {
      return tijdsduur + ' minuten';
    }
  }

  sourceHeader() {
    let tekst: string = 'Behandelingen bij ' + this.afspraakInfo().stap1.salon.naam;
    if (this.medewerkers.length > 1 && this.geselecteerdeMedewerker() != null) {
      tekst = tekst + ' en ' + this.geselecteerdeMedewerker().naam;
    }
    return tekst;
  }

  targetHeader() {
    if (this.totaalprijs() > 0) {
      return 'Totaalprijs : € ' + this.afspraakService.prijsOpgemaakt(this.totaalprijs()) + ', Tijdsduur : ' + this.tijdsduurOpgemaakt(this.totaleTijdsduur())
    } else {
      return '';
    }
  }

  naarVolgendeStap() {
    this.afspraakService.afspraakInfo.stap2 = {
      medewerker: this.geselecteerdeMedewerker(),
      behandelingen: this.afspraakService.list2
    };
    this.afspraakService.naarVolgendeStap();
  }

  onModelChangeMedewerker(event: any) {
    this.afspraakService.list2 = [];
    if (event.value != null) {
      this.afspraakService.list1 = this.behandelingenBackup.filter(b => b.medewerkers.filter(m => m == event.value).length > 0);
    } else {
      this.afspraakService.list1 = this.behandelingenBackup;
    }
  }

  get afspraakService(): AfspraakService {
    return this._afspraakService;
  }
}
