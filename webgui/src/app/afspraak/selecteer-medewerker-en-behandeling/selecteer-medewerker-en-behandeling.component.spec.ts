import {ComponentFixture, TestBed} from '@angular/core/testing';

import {SelecteerMedewerkerEnBehandelingComponent} from './selecteer-medewerker-en-behandeling.component';

describe('SelecteerMedewerkerEnBehandelingComponent', () => {
  let component: SelecteerMedewerkerEnBehandelingComponent;
  let fixture: ComponentFixture<SelecteerMedewerkerEnBehandelingComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ SelecteerMedewerkerEnBehandelingComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(SelecteerMedewerkerEnBehandelingComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  xit('should create', () => {
    expect(component).toBeTruthy();
  });
});
