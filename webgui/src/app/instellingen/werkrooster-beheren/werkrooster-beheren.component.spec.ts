import {ComponentFixture, TestBed} from '@angular/core/testing';

import {WerkroosterBeherenComponent} from './werkrooster-beheren.component';

describe('WerkroosterBeherenComponent', () => {
  let component: WerkroosterBeherenComponent;
  let fixture: ComponentFixture<WerkroosterBeherenComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [WerkroosterBeherenComponent]
    })
      .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(WerkroosterBeherenComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
