import {ComponentFixture, TestBed} from '@angular/core/testing';

import {BehandelingenBeherenComponent} from './behandelingen-beheren.component';

describe('BehandelingenBeherenComponent', () => {
  let component: BehandelingenBeherenComponent;
  let fixture: ComponentFixture<BehandelingenBeherenComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [BehandelingenBeherenComponent]
    })
      .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(BehandelingenBeherenComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
