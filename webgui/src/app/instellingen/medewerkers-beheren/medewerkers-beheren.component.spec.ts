import {ComponentFixture, TestBed} from '@angular/core/testing';

import {MedewerkersBeherenComponent} from './medewerkers-beheren.component';

describe('MedewerkersBeherenComponent', () => {
  let component: MedewerkersBeherenComponent;
  let fixture: ComponentFixture<MedewerkersBeherenComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [MedewerkersBeherenComponent]
    })
      .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(MedewerkersBeherenComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
