import {Component, Input, OnInit, ViewChild} from '@angular/core';
import {Medewerker} from "../../afspraak/model/opvragen-salon-response/medewerker";
import {HttpClient} from "@angular/common/http";
import {MessageService} from "primeng/api";
import {MedewerkersComponent} from "../../commons/medewerkers/medewerkers.component";

@Component({
  selector: 'app-medewerkers-beheren',
  templateUrl: './medewerkers-beheren.component.html',
  styleUrls: ['./medewerkers-beheren.component.css'],
  providers: [MessageService]
})
export class MedewerkersBeherenComponent implements OnInit {
  urlLees: string = 'http://localhost:8080/instellingen/medewerkers/lijst';
  urlOpslaan: string = 'http://localhost:8080/instellingen/medewerkers/opslaan';
  urlVerwijderen: string = 'http://localhost:8080/instellingen/medewerkers/verwijder/';
  @Input() medewerkers: Medewerker[];
  @ViewChild(MedewerkersComponent) child;

  constructor(private httpClient: HttpClient, private messageService: MessageService) {
  }

  ngOnInit() {
    this.httpClient.get(this.urlLees).subscribe((response: Medewerker[]) => {
      this.medewerkers = response;
    });
  }

  medewerkerOpslaan(index: number): void {
    this.httpClient.post(this.urlOpslaan, this.child.medewerkers[index]).subscribe((response: number) => {
      if (!!response) {
        this.child.medewerkers[index].id = response;
        this.messageService.add({
          id: 'messageOpgeslagen',
          severity: 'success',
          summary: 'Opgeslagen',
          detail: 'De gegevens zijn opgeslagen.'
        });
      } else {
        this.messageService.add({
          severity: 'error',
          summary: 'Fout',
          detail: 'Er ging iets fout, de gegevens zijn niet opgeslagen.'
        });
      }
    });
  }

  medewerkerVerwijderen(id: number): void {
    this.httpClient.delete(this.urlVerwijderen + id).subscribe((response: boolean) => {
      if (response) {
        this.messageService.add({
          id: 'messageVerwijderd',
          severity: 'success',
          summary: 'Verwijderd',
          detail: 'De gegevens zijn verwijderd.'
        });
      } else {
        this.messageService.add({
          severity: 'error',
          summary: 'Fout',
          detail: 'Er ging iets fout, de gegevens zijn niet verwijderd.'
        });
      }
    });
  }
}
