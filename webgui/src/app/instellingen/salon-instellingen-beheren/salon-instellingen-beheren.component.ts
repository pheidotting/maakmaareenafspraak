import {Component, Input, OnInit} from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {Salon} from "../../afspraak/model/opvragen-salon-response/salon";
import {MessageService} from "primeng/api";

@Component({
  selector: 'app-salon-instellingen-beheren',
  templateUrl: './salon-instellingen-beheren.component.html',
  styleUrls: ['./salon-instellingen-beheren.component.css']
})
export class SalonInstellingenBeherenComponent implements OnInit {
  urlLees: string = 'http://localhost:8080/instellingen/beherensalon/lees';
  urlOpslaan: string = 'http://localhost:8080/instellingen/beherensalon/opslaan';
  @Input() salon: Salon;

  constructor(private httpClient: HttpClient, private messageService: MessageService) {
  }

  ngOnInit(): void {
    this.httpClient.get(this.urlLees).subscribe((response: Salon) => {
      this.salon = response;
    });
  }

  onClickOpslaan(): void {
    this.httpClient.post(this.urlOpslaan, this.salon).subscribe(observer => {
      this.messageService.add({
        id: 'messageOpgeslagen',
        severity: 'success',
        summary: 'Gelukt',
        detail: 'De gegevens zijn opgeslagen'
      });
    });
  }
}
