import {ComponentFixture, TestBed} from '@angular/core/testing';

import {SalonInstellingenBeherenComponent} from './salon-instellingen-beheren.component';

describe('SalonInstellingenBeherenComponent', () => {
  let component: SalonInstellingenBeherenComponent;
  let fixture: ComponentFixture<SalonInstellingenBeherenComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [SalonInstellingenBeherenComponent]
    })
      .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(SalonInstellingenBeherenComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
