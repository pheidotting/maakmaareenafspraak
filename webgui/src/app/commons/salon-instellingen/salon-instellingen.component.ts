import {Component, Input, OnChanges, SimpleChanges} from '@angular/core';
import {FormControl, FormGroup, Validators} from "@angular/forms";
import {Salon} from "../../afspraak/model/opvragen-salon-response/salon";
import {HttpClient} from "@angular/common/http";

@Component({
  selector: 'app-salon-instellingen',
  templateUrl: './salon-instellingen.component.html',
  styleUrls: ['./salon-instellingen.component.css']
})
export class SalonInstellingenComponent implements OnChanges {
  form: FormGroup;
  @Input() salon: Salon;
  salonGezet: boolean = false;

  constructor(private httpClient: HttpClient) {
    this.form = new FormGroup({
      naamSalon: new FormControl([Validators.required]),
      adres: new FormControl(),
      huisnummer: new FormControl(),
      postcode: new FormControl(),
      plaats: new FormControl(),
      telefoonnummer: new FormControl(),
      email: new FormControl([Validators.required]),
    });
    this.form.valueChanges.subscribe(values => {
      if (this.salonGezet) {
        this.salon.naam = values.naamSalon;
        this.salon.straat = values.adres;
        this.salon.huisnummer = values.huisnummer;
        this.salon.postcode = values.postcode;
        this.salon.plaats = values.plaats;
        this.salon.telefoonnummer = values.telefoonnummer;
        this.salon.emailadres = values.email;
      }
    });
  }

  ngOnChanges(changes: SimpleChanges): void {
    this.updateSalon(changes.salon.currentValue);
  }

  updateSalon(salon: Salon): void {
    if (!!salon) {
      this.form.get('naamSalon').setValue(salon.naam);
      this.form.get('adres').setValue(salon.straat);
      this.form.get('huisnummer').setValue(salon.huisnummer);
      this.form.get('postcode').setValue(salon.postcode);
      this.form.get('plaats').setValue(salon.plaats);
      this.form.get('telefoonnummer').setValue(salon.telefoonnummer);
      this.form.get('email').setValue(salon.emailadres);

      this.salonGezet = true;
    }
  }

  onChangeCheckPostcode(): void {
    let postcode = this.form.get('postcode').value;
    if (postcode != null && postcode != '') {
      postcode = postcode.toUpperCase().replaceAll(' ', '');
      if (postcode.length > 6) {
        postcode = postcode.substring(0, 6);
      }
      this.form.controls['postcode'].setValue(postcode);
    }
    this.haalAdresOp();
  }

  onChangeCheckHuisNummer(): void {
    this.haalAdresOp();
  }

  haalAdresOp(): void {
    let postcode = this.form.get('postcode').value;
    let huisnummer = this.form.get('huisnummer').value;
    if (huisnummer != null && huisnummer != '' && postcode != null && postcode != '') {
      this.httpClient.get('http://geodata.nationaalgeoregister.nl/locatieserver/free?fq=postcode:' + postcode + '&fq=huisnummer~' + huisnummer + '*').subscribe((response: any) => {
        this.form.controls['adres'].setValue(response.response.docs[0].straatnaam);
        this.form.controls['plaats'].setValue(response.response.docs[0].woonplaatsnaam);
      });
    }
  }

}
