import {ComponentFixture, TestBed} from '@angular/core/testing';

import {SalonInstellingenComponent} from './salon-instellingen.component';

describe('SalonInstellingenComponent', () => {
  let component: SalonInstellingenComponent;
  let fixture: ComponentFixture<SalonInstellingenComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [SalonInstellingenComponent]
    })
      .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(SalonInstellingenComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
