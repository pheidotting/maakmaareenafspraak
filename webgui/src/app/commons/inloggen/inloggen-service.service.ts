import {Injectable} from '@angular/core';
import {Ingelogd} from "./ingelogd";
import {HttpClient} from "@angular/common/http";
import {JwtResponse} from "../../afspraak/model/jwt-response";
import jwt_decode from "jwt-decode";
import {Subject} from "rxjs";
import {FacebookLoginProvider, GoogleLoginProvider, SocialAuthService, SocialUser} from "angularx-social-login";
import {LocalDateTime, ZoneOffset} from "@js-joda/core";

@Injectable({
  providedIn: 'root'
})
export class InloggenServiceService {
  private urlInloggen: string = 'http://localhost:8080/auth/signin';

  ingelogdSubject: Subject<Ingelogd> = new Subject<Ingelogd>();
  ingelogd: Ingelogd;
  inloggenFoutmelding: string;
  socialUser: SocialUser;
  socialUserSubject: Subject<SocialUser> = new Subject<SocialUser>();
  wilInloggen: boolean = false;

  constructor(private httpClient: HttpClient, private authService: SocialAuthService) {
    this.authService.authState.subscribe((socialUser) => {
      this.setSocialUser(socialUser)
    });
    this.socialUserSubject.subscribe(socialUser => {
      this.setIngelogdMetSocialUser(socialUser);
    });
    this.haalIngelogdUitBrowser();
  }

  inloggen(inloggenMail: string, inloggenPass: string): void {
    this.inloggenFoutmelding = null;
    this.httpClient.post(this.urlInloggen, {
      username: inloggenMail, password: inloggenPass
    }).subscribe((response: JwtResponse) => {
      let decoded_jwt: any = jwt_decode(response.token);

      this.ingelogd = new Ingelogd();
      this.ingelogd.soortUser = decoded_jwt.soortUser;
      this.ingelogd.username = decoded_jwt.username;
      this.ingelogd.naam = decoded_jwt.naam;
      this.ingelogd.photoUrl = decoded_jwt.photo;

      this.ingelogd.jwt = response.token;
      localStorage.setItem("mmea-jwt", JSON.stringify(this.ingelogd));

      this.ingelogdSubject.next(this.ingelogd);
      this.wilInloggen = false;
    }, error => {
      if (error.status == 401) {
        this.inloggenFoutmelding = 'De combinatie gebruikersnaam/wachtwoord is onjuist.'
      } else {
        this.inloggenFoutmelding = 'Er is een onbekende fout opgetreden';
      }
    });
  }

  uitloggen(): void {
    this.ingelogd = null;
    localStorage.setItem("mmea-jwt", null);
    this.ingelogdSubject.next(this.ingelogd);
  }

  setSocialUser(socialUser: SocialUser): void {
    this.socialUser = socialUser;
    this.socialUserSubject.next(this.socialUser);
  }

  setIngelogdMetSocialUser(socialUser: SocialUser): void {
    if (socialUser != null) {
      this.ingelogd.soortUser = 'KLANT';
      this.ingelogd.username = socialUser.email;
      this.ingelogd.naam = socialUser.name;
      this.ingelogd.photoUrl = socialUser.photoUrl;
      this.ingelogd.social = socialUser.provider;

      let token;
      let mail;
      let picture;
      if (socialUser.provider == 'GOOGLE') {
        token = socialUser.idToken;
        mail = 'dummy';
        picture = 'dummy';
      } else {
        token = socialUser.authToken;
        mail = socialUser.email;
        picture = btoa(socialUser.photoUrl);
      }

      this.httpClient.get("http://localhost:8080/auth/viasocialmedia/" + socialUser.provider + '/' + mail + '/' + picture + '/' + token).subscribe((response: JwtResponse) => {
        this.ingelogd.jwt = response.token;
        localStorage.setItem("mmea-jwt", JSON.stringify(this.ingelogd));
        localStorage.setItem("mmea-socialuser", JSON.stringify(socialUser));
      });
    } else {
      this.ingelogd = null;
      localStorage.setItem("mmea-jwt", null);
    }

    this.ingelogdSubject.next(this.ingelogd);
  }

  haalIngelogdUitBrowser(): void {
    let klant: Ingelogd = JSON.parse(localStorage.getItem("mmea-jwt"));
    if (klant != null && klant.jwt != null) {
      let decoded_jwt: any = jwt_decode(klant.jwt);
      let expTime: LocalDateTime = LocalDateTime.ofEpochSecond(decoded_jwt.exp, ZoneOffset.ofHours(2));
      if (LocalDateTime.now().isBefore(expTime)) {
        this.ingelogd = klant;

        this.ingelogdSubject.next(this.ingelogd);
      }
    } else if (klant != null && klant.jwt == null && klant.social != null) {
      let u: SocialUser = JSON.parse(localStorage.getItem("mmea-socialuser"));
      this.authService.initState.subscribe(observer => {
        let provider = GoogleLoginProvider.PROVIDER_ID;
        if (u.provider == 'FACEBOOK') {
          provider = FacebookLoginProvider.PROVIDER_ID;
        }
        this.authService.refreshAuthToken(provider).then(response => {
          this.socialUserSubject.next(u);
        });
      });
    }
  }

  signInWithGoogle(): void {
    this.authService.signIn(GoogleLoginProvider.PROVIDER_ID);
    this.wilInloggen = false;
  }

  signInWithFB(): void {
    this.authService.signIn(FacebookLoginProvider.PROVIDER_ID);
    this.wilInloggen = false;
  }

  signOut(): void {
    this.authService.signOut().then(() => {
      this.socialUser = null;
      this.socialUserSubject.next(null);
    });
  }

}
