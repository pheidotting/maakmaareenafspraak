export class Ingelogd {
  username: string;
  naam: string;
  photoUrl: string;
  soortUser: string;
  jwt: string;
  social: string;
  telefoonnummer: string;

}
