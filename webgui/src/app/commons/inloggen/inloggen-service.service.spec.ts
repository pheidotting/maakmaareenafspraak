import {TestBed} from '@angular/core/testing';

import {InloggenServiceService} from './inloggen-service.service';

describe('InloggenServiceService', () => {
  let service: InloggenServiceService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(InloggenServiceService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
