import {ComponentFixture, TestBed} from '@angular/core/testing';

import {WerkRoosterComponent} from './werk-rooster.component';

describe('WerkRoosterComponent', () => {
  let component: WerkRoosterComponent;
  let fixture: ComponentFixture<WerkRoosterComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [WerkRoosterComponent]
    })
      .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(WerkRoosterComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
