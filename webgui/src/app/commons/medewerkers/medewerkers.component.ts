import {Component, EventEmitter, Input, Output} from '@angular/core';
import {Medewerker} from "../../afspraak/model/opvragen-salon-response/medewerker";
import {ConfirmationService} from "primeng/api";

@Component({
  selector: 'app-medewerkers',
  templateUrl: './medewerkers.component.html',
  styleUrls: ['./medewerkers.component.css'],
  providers: [ConfirmationService]
})
export class MedewerkersComponent {
  @Input() medewerkers: Medewerker[];
  editMode: number = -1;
  naamBackup: string;
  @Input() eersteMedewerkerWijzigbaar = true;
  @Output() medewerkerOplsaanEventEmitter: EventEmitter<number> = new EventEmitter<number>();
  @Output() medewerkerVerwijderenEventEmitter: EventEmitter<number> = new EventEmitter<number>();

  constructor(private confirmationService: ConfirmationService) {
  }

  onClickBewerkNaam(i: number): void {
    this.onClickOpslaan();
    this.editMode = i;
    this.naamBackup = this.medewerkers[i].naam;
  }

  onClickOpslaan(): void {
    if (this.editMode > -1) {
      this.medewerkerOplsaanEventEmitter.emit(this.editMode);
      this.editMode = -1;
      this.naamBackup = null;
    }
  }

  onClickVerwijder(i: number): void {
    this.confirmationService.confirm({
      message: 'Weet je zeker dat je de medewerker met naam ' + this.medewerkers[i].naam + ' wilt verwijderen?',
      accept: () => {
        this.medewerkerVerwijderenEventEmitter.emit(this.medewerkers[i].id);
        this.medewerkers.splice(i, 1);
      }
    });
  }

  onClickMedewerkerToevoegen(): void {
    this.onClickOpslaan();
    this.medewerkers.push(new Medewerker());
    this.editMode = this.medewerkers.length - 1;
  }

  onClickAnnuleren(): void {
    this.medewerkers[this.editMode].naam = this.naamBackup;
    this.editMode = -1;
  }
}
