import {Component, OnInit} from '@angular/core';
import {ActivatedRoute} from "@angular/router";
import {HttpClient} from "@angular/common/http";

@Component({
  selector: 'app-wachtwoord-wijzigen',
  templateUrl: './wachtwoord-wijzigen.component.html',
  styleUrls: ['./wachtwoord-wijzigen.component.css']
})
export class WachtwoordWijzigenComponent implements OnInit {
  nieuwWachtwoord: string;
  nieuwWachtwoordNogmaals: string;
  nieuwWachtwoordSterkGenoeg: boolean = false;
  activatieCode: string;
  wachtwoordGewijzigd: boolean = false;

  constructor(private activatedRoute: ActivatedRoute, private httpClient: HttpClient) {
    this.activatedRoute.queryParams.subscribe(params => {
      this.activatieCode = params['code'];
    });
  }

  ngOnInit(): void {
  }

  komenWachtwoordenOvereen(): boolean {
    if (!this.isVeldGevuld(this.nieuwWachtwoord) && !this.isVeldGevuld(this.nieuwWachtwoordNogmaals)) {
      return true;
    } else if (this.isVeldGevuld(this.nieuwWachtwoord) && this.isVeldGevuld(this.nieuwWachtwoordNogmaals)) {
      return this.nieuwWachtwoord == this.nieuwWachtwoordNogmaals;
    }
    return false;
  }

  isVeldGevuld(veld: string): boolean {
    return veld != null && veld != ''
  }

  onPasswordStrengthChanged(event: number) {
    this.nieuwWachtwoordSterkGenoeg = event > 3;
  }

  wijzigKnopEnabled(): boolean {
    return this.nieuwWachtwoordSterkGenoeg && this.komenWachtwoordenOvereen();
  }

  wijzigWachtwoord() {
    console.log('Call naar de backend!, nieuw wachtwoord : ' + this.nieuwWachtwoord + ', activatiecode : ' + this.activatieCode);
    this.httpClient.post('http://localhost:8080/auth/wijzigwachtwoord', {
      'wijzigWachtwoordCode': this.activatieCode,
      'wachtwoord': this.nieuwWachtwoord
    }).subscribe(observer => {
      console.log(observer);
      this.wachtwoordGewijzigd = true;
    });
  }

}
