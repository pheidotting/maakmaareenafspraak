import {ComponentFixture, TestBed} from '@angular/core/testing';

import {WachtwoordWijzigenComponent} from './wachtwoord-wijzigen.component';

describe('WachtwoordWijzigenComponent', () => {
  let component: WachtwoordWijzigenComponent;
  let fixture: ComponentFixture<WachtwoordWijzigenComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [WachtwoordWijzigenComponent]
    })
      .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(WachtwoordWijzigenComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
