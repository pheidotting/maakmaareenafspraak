import {Component, OnInit} from '@angular/core';
import {AanmeldenService} from "../aanmelden.service";

@Component({
  selector: 'app-aanmelden',
  templateUrl: './aanmelden.component.html',
  styleUrls: ['./aanmelden.component.css']
})
export class AanmeldenComponent implements OnInit {
  constructor(private _aanmeldenService: AanmeldenService) {
  }

  ngOnInit(): void {
  }

  get aanmeldenService(): AanmeldenService {
    return this._aanmeldenService;
  }
}
