import {TreeNode} from "primeng/api";
import {DagMetTijd, Medewerker} from "../../../aanmelden.service";

export class Aanmelden {
  //Stap 1
  naamSalon: string;
  adres: string;
  huisnummer: string;
  postcode: string;
  plaats: string;
  telefoonnummer: string;
  email: string;

  naamEigenaar: string;// = 'Sabine Heidotting';

//Stap 2
  medewerkers: Medewerker[] = [];
//Stap 3
  dagenMetTijden: DagMetTijd[] = [];
//Stap 4
  behandelingen: TreeNode[] = [];

}
