import {Component, OnInit} from '@angular/core';
import {AanmeldenService, Medewerker} from "../../aanmelden.service";
import {Guid} from "guid-typescript";
import {ConfirmationService, TreeNode} from "primeng/api";

@Component({
  selector: 'app-stap4',
  templateUrl: './stap4.component.html',
  styleUrls: ['./stap4.component.css'],
  providers: [ConfirmationService]
})
export class Stap4Component implements OnInit {
  medewerkers: Medewerker[];
  indexCategorieOfBehandeling: number = 0;

  constructor(private _aanmeldenService: AanmeldenService, private confirmationService: ConfirmationService) {
    this.medewerkers = this.aanmeldenService.aanmelden.medewerkers.map(m => {
      let medewerker: Medewerker = new Medewerker();
      medewerker.aanwezig = true;
      medewerker.voornaam = this.voornaam(m.naam);
      medewerker.id = m.id;
      return medewerker;
    });
  }

  ngOnInit(): void {
    this.vulBehandelingen();
    this.aanmeldenService.aanmelden.behandelingen.forEach(categorie => {
      categorie.data.medewerkers = this.copyMedewerkers();
      categorie.data.medewerkers.forEach(medewerker => {
        medewerker.tonen = false;
      });
      categorie.children.forEach(behandeling => {
        behandeling.data.medewerkers = this.copyMedewerkers();
        behandeling.data.medewerkers.forEach(medewerker => {
          medewerker.tonen = true;
        });
      });
    });
  }

  onClickBehandelingToevoegen(id: string): void {
    let kopie: TreeNode[] = this.kopieer();
    kopie.forEach(b => {
      if (b.data.id == id) {
        b.children.push({
          data: {
            id: Guid.create().toString(),
            naam: 'Nieuwe behandeling',
            tijdsduur: 0,
            editMode: true,
            index: this.hoogIndexCategorieOfBehandeling(),
            medewerkers: this.copyMedewerkers()
          },
        });
      }
    });
    this.aanmeldenService.aanmelden.behandelingen = kopie;
  }

  onClickCategorieToevoegen(): void {
    let kopie: TreeNode[] = [];
    this.aanmeldenService.aanmelden.behandelingen.forEach(b => {
      kopie.push(b);
    })
    kopie.push({
      data: {
        id: Guid.create().toString(),
        naam: 'Nieuwe Categorie',
        editMode: true,
        medewerkers: this.copyMedewerkers(),
        index: this.hoogIndexCategorieOfBehandeling()
      },
      expanded: true,
      children: [{
        data: {
          id: Guid.create().toString(),
          naam: 'Nieuwe behandeling',
          tijdsduur: 0,
          prijs: 0,
          editMode: true,
          index: this.hoogIndexCategorieOfBehandeling(),
          medewerkers: this.copyMedewerkers()
        },
      }]
    });

    this.aanmeldenService.aanmelden.behandelingen = kopie;
  }

  opslaan(id: string): void {
    let kopie: TreeNode[] = this.kopieer();
    kopie.forEach(b => {
      if (b.data.id == id) {
        b.data.editMode = false
      }
      b.children.forEach(c => {
        if (c.data.id == id) {
          c.data.editMode = false;
        }
      })
    })
    this.aanmeldenService.aanmelden.behandelingen = kopie;
  }

  kopieer(): TreeNode[] {
    let kopie: TreeNode[] = [];
    this.aanmeldenService.aanmelden.behandelingen.forEach(b => {
      b.children.forEach(c => {
      })
      kopie.push(b);
    })
    return kopie;
  }

  verwijder(id: string): void {
    let kopie: TreeNode[] = this.kopieer();
    let indexCategorie;
    let indexBehandeling;
    kopie.forEach((b, index1) => {
      if (b.data.id == id) {
        indexCategorie = index1;
      }
      b.children.forEach((c, index2) => {
        if (c.data.id == id) {
          indexBehandeling = index2;
        }
      })
      if (indexBehandeling != null) {
        this.confirmationService.confirm({
          message: 'Weet je zeker dat je de behandeling met naam ' + b.children[indexBehandeling].data.naam + ' wilt verwijderen?',
          accept: () => {
            b.children.splice(indexBehandeling, 1);
          }
        });
        indexBehandeling = null;
      }
    })

    if (indexCategorie != null) {
      this.confirmationService.confirm({
        message: 'Weet je zeker dat je de categorie met naam ' + kopie[indexCategorie].data.naam + ' wilt verwijderen?',
        accept: () => {
          kopie.splice(indexCategorie, 1);
        }
      });
    }

    this.aanmeldenService.aanmelden.behandelingen = kopie;
  }

  editModeAan(id: string): void {
    let kopie: TreeNode[] = this.kopieer();
    kopie.forEach(b => {
      if (b.data.id == id) {
        b.data.editMode = true;
      }
      b.children.forEach(c => {
        if (c.data.id == id) {
          c.data.editMode = true;
        }
      })
    })

    this.aanmeldenService.aanmelden.behandelingen = kopie;
  }

  voornaam(naam: string): string {
    return naam.split(' ')[0];
  }

  copyMedewerkers(): Medewerker[] {
    return this.medewerkers.map(m => {
      let mw: Medewerker = new Medewerker();
      mw.aanwezig = m.aanwezig;
      mw.voornaam = m.voornaam;
      mw.id = m.id;
      return mw;
    });
  }

  onChangeCheckTijdsduur(id: string): void {
    let kopie: TreeNode[] = this.kopieer();
    kopie.forEach(b => {
      b.children.forEach(c => {
        if (c.data.id == id) {
          if (c.data.tijdsduur % 15 != 0) {
            c.data.tijdsduur = Math.floor(c.data.tijdsduur / 15) * 15;
          }
        }
      })
    })

    this.aanmeldenService.aanmelden.behandelingen = kopie;
  }

  get aanmeldenService(): AanmeldenService {
    return this._aanmeldenService;
  }

  vulBehandelingen() {
    this.aanmeldenService.aanmelden.behandelingen = [
      {
        data: {
          id: Guid.create().toString(),
          naam: 'Haar',
          editMode: false,
          index: this.hoogIndexCategorieOfBehandeling()
        },
        expanded: false,
        children: [{
          data: {
            id: Guid.create().toString(),
            naam: 'Knippen',
            tijdsduur: '15',
            prijs: '15.00',
            editMode: false,
            index: this.hoogIndexCategorieOfBehandeling()
          },
        }, {
          data: {
            id: Guid.create().toString(),
            naam: 'Kleuren',
            tijdsduur: '30',
            prijs: '17.00',
            editMode: false,
            index: this.hoogIndexCategorieOfBehandeling()
          },
        }
        ]
      },
      {
        data: {
          id: Guid.create().toString(),
          naam: 'Wenkbrauwen',
          editMode: false,
          index: this.hoogIndexCategorieOfBehandeling()
        },
        expanded: false,
        children: [{
          data: {
            id: Guid.create().toString(),
            naam: 'Bijwerken',
            tijdsduur: '15',
            prijs: '10.00',
            editMode: false,
            index: this.hoogIndexCategorieOfBehandeling()
          },
        }
        ]
      }
    ]
  }

  onClickBevestigEnNaarDeBackend(): void {
    this.confirmationService.confirm({
      header: 'Bevestigen',
      message: 'Hiermee wordt de aanmelding van je salon \'' + this.aanmeldenService.aanmelden.naamSalon + '\' definitief, doorgaan?',
      accept: () => {
        this.aanmeldenService.aanmeldenSalon();
        this.aanmeldenService.onClickNaarVolgendeStap();
      }
    });
  }

  hoogIndexCategorieOfBehandeling(): number {
    this.indexCategorieOfBehandeling = this.indexCategorieOfBehandeling + 1;
    return this.indexCategorieOfBehandeling;
  }
}
