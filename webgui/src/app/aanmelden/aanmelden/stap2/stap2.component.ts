import {Component, OnInit} from '@angular/core';
import {AanmeldenService} from "../../aanmelden.service";

@Component({
  selector: 'app-stap2',
  templateUrl: './stap2.component.html',
  styleUrls: ['./stap2.component.css']
})
export class Stap2Component implements OnInit {

  constructor(private _aanmeldenService: AanmeldenService) {
  }

  ngOnInit(): void {
  }

  get aanmeldenService(): AanmeldenService {
    return this._aanmeldenService;
  }
}
