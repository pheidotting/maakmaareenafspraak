import {Component, Input, OnInit} from '@angular/core';
import {AanmeldenService, Medewerker} from "../../aanmelden.service";
import {FormControl, FormGroup} from "@angular/forms";
import {ConfirmationService} from "primeng/api";
import {Salon} from "../../../afspraak/model/opvragen-salon-response/salon";

interface Optie {
  name: string,
  code: string
}

@Component({
  selector: 'app-stap1',
  templateUrl: './stap1.component.html',
  styleUrls: ['./stap1.component.css'],
  providers: [ConfirmationService]
})
export class Stap1Component implements OnInit {
  alleen: boolean = true;
  opties: Optie [];

  optieIkWerkAlleen = {name: 'Ik werk alleen', code: 'alleen'};
  optieIkWerkNietAlleen = {name: 'Ik heb collega\'s', code: 'nietalleen'};

  selectedOptie: Optie = this.optieIkWerkAlleen;

  form: FormGroup;
  @Input() salon: Salon = new Salon();
  naamEigenaar: string;

  constructor(private _aanmeldenService: AanmeldenService) {
    this.form = new FormGroup({
      alleen: new FormControl(this.selectedOptie),
    });
    this.opties = [
      this.optieIkWerkAlleen,
      this.optieIkWerkNietAlleen
    ];
  }

  ngOnInit(): void {
  }

  onClickNaarVolgendeStap(): void {
    // if (this.form.valid) {
    this.aanmeldenService.aanmelden.naamSalon = this.salon.naam;
    this.aanmeldenService.aanmelden.adres = this.salon.straat;
    this.aanmeldenService.aanmelden.huisnummer = this.salon.huisnummer;
    this.aanmeldenService.aanmelden.postcode = this.salon.postcode;
    this.aanmeldenService.aanmelden.plaats = this.salon.plaats;
    this.aanmeldenService.aanmelden.telefoonnummer = this.salon.telefoonnummer;
    this.aanmeldenService.aanmelden.email = this.salon.emailadres;
    this.aanmeldenService.aanmelden.naamEigenaar = this.naamEigenaar;

    this.aanmeldenService.onClickNaarVolgendeStap();

    if (this.alleen) {
      this.aanmeldenService.onClickNaarVolgendeStap();
    }
    // }
  }

  onChangeSetOptie(): void {
    this.alleen = this.selectedOptie == this.form.controls['alleen'].value;
  }

  onChangeWerkMedewerkerBij(): void {
    if (this.aanmeldenService.aanmelden.medewerkers.length == 0) {
      this.aanmeldenService.aanmelden.medewerkers.push(new Medewerker());
    }
    this.aanmeldenService.aanmelden.medewerkers[0].naam = this.naamEigenaar;
  }

  get aanmeldenService(): AanmeldenService {
    return this._aanmeldenService;
  }

}
