import {Component, OnInit} from '@angular/core';
import {LocalTime} from "@js-joda/core";
import {AanmeldenService, Dag, DagMetTijd, Medewerker, Tijd} from "../../aanmelden.service";
import {ConfirmationService} from "primeng/api";

@Component({
  selector: 'app-stap3',
  templateUrl: './stap3.component.html',
  styleUrls: ['./stap3.component.css'],
  providers: [ConfirmationService]
})
export class Stap3Component implements OnInit {
  maandag: Dag = {code: 'MONDAY', name: 'Maandag', id: 0};
  dinsdag: Dag = {code: 'TUESDAY', name: 'Dinsdag', id: 1};
  woensdag: Dag = {code: 'WEDNESDAY', name: 'Woensdag', id: 2};
  donderdag: Dag = {code: 'THURSDAY', name: 'Donderdag', id: 3};
  vrijdag: Dag = {code: 'FRIDAY', name: 'Vrijdag', id: 4};
  zaterdag: Dag = {code: 'SATURDAY', name: 'Zaterdag', id: 5};
  zondag: Dag = {code: 'SUNDAY', name: 'Zondag', id: 6};
  negenUur = LocalTime.of(9, 0);
  twaalfUur = LocalTime.of(12, 0);
  dertienUur = LocalTime.of(13, 0);
  zeventienUur = LocalTime.of(17, 0);
  achttienUur = LocalTime.of(18, 0);
  eenentwintigUur = LocalTime.of(21, 0);

  medewerkers: Medewerker[];

  constructor(private _aanmeldenService: AanmeldenService, private confirmationService: ConfirmationService) {
    this.medewerkers = this.aanmeldenService.aanmelden.medewerkers.map(m => {
      let medewerker: Medewerker = new Medewerker();
      medewerker.aanwezig = true;
      medewerker.voornaam = this.voornaam(m.naam);
      medewerker.id = m.id;
      return medewerker;
    });

    let maa: DagMetTijd = new DagMetTijd();
    maa.dag = this.maandag;

    let maandagOchtend = new Tijd();
    maandagOchtend.dag = this.maandag;
    maandagOchtend.start = this.negenUur;
    maandagOchtend.eind = this.twaalfUur;
    maandagOchtend.medewerkers = this.copyMedewerkers();
    maa.tijden.push(maandagOchtend);

    let maandagMiddag = new Tijd();
    maandagMiddag.dag = this.maandag;
    maandagMiddag.start = this.dertienUur;
    maandagMiddag.eind = this.zeventienUur;
    maandagMiddag.medewerkers = this.copyMedewerkers();
    maa.tijden.push(maandagMiddag);
    this.aanmeldenService.aanmelden.dagenMetTijden.push(maa);

    let din: DagMetTijd = new DagMetTijd();
    din.dag = this.dinsdag;

    let dinsdagOchtend = new Tijd();
    dinsdagOchtend.dag = this.dinsdag;
    dinsdagOchtend.start = this.negenUur;
    dinsdagOchtend.eind = this.twaalfUur;
    dinsdagOchtend.medewerkers = this.copyMedewerkers();
    din.tijden.push(dinsdagOchtend);

    let dinsdagMiddag = new Tijd();
    dinsdagMiddag.dag = this.dinsdag;
    dinsdagMiddag.start = this.dertienUur;
    dinsdagMiddag.eind = this.zeventienUur;
    dinsdagMiddag.medewerkers = this.copyMedewerkers();
    din.tijden.push(dinsdagMiddag);
    this.aanmeldenService.aanmelden.dagenMetTijden.push(din);

    let woe: DagMetTijd = new DagMetTijd();
    woe.dag = this.woensdag;

    let woensdagOchtend = new Tijd();
    woensdagOchtend.dag = this.woensdag;
    woensdagOchtend.start = this.negenUur;
    woensdagOchtend.eind = this.twaalfUur;
    woensdagOchtend.medewerkers = this.copyMedewerkers();
    woe.tijden.push(woensdagOchtend);

    let woensdagMiddag = new Tijd();
    woensdagMiddag.dag = this.woensdag;
    woensdagMiddag.start = this.dertienUur;
    woensdagMiddag.eind = this.zeventienUur;
    woensdagMiddag.medewerkers = this.copyMedewerkers();
    woe.tijden.push(woensdagMiddag);
    this.aanmeldenService.aanmelden.dagenMetTijden.push(woe);

    let don: DagMetTijd = new DagMetTijd();
    don.dag = this.donderdag;

    let donderdagOchtend = new Tijd();
    donderdagOchtend.dag = this.donderdag;
    donderdagOchtend.start = this.negenUur;
    donderdagOchtend.eind = this.twaalfUur;
    donderdagOchtend.medewerkers = this.copyMedewerkers();
    don.tijden.push(donderdagOchtend);

    let donderdagMiddag = new Tijd();
    donderdagMiddag.dag = this.donderdag;
    donderdagMiddag.start = this.dertienUur;
    donderdagMiddag.eind = this.zeventienUur;
    donderdagMiddag.medewerkers = this.copyMedewerkers();
    don.tijden.push(donderdagMiddag);

    let donderdagAvond = new Tijd();
    donderdagAvond.dag = this.donderdag;
    donderdagAvond.start = this.achttienUur;
    donderdagAvond.eind = this.eenentwintigUur;
    donderdagAvond.medewerkers = this.copyMedewerkers();
    don.tijden.push(donderdagAvond);
    this.aanmeldenService.aanmelden.dagenMetTijden.push(don);

    let vri: DagMetTijd = new DagMetTijd();
    vri.dag = this.vrijdag;

    let vrijdagOchtend = new Tijd();
    vrijdagOchtend.dag = this.vrijdag;
    vrijdagOchtend.start = this.negenUur;
    vrijdagOchtend.eind = this.twaalfUur;
    vrijdagOchtend.medewerkers = this.copyMedewerkers();
    vri.tijden.push(vrijdagOchtend);

    let vrijdagMiddag = new Tijd();
    vrijdagMiddag.dag = this.vrijdag;
    vrijdagMiddag.start = this.dertienUur;
    vrijdagMiddag.eind = this.zeventienUur;
    vrijdagMiddag.medewerkers = this.copyMedewerkers();
    vri.tijden.push(vrijdagMiddag);
    this.aanmeldenService.aanmelden.dagenMetTijden.push(vri);

    let zat: DagMetTijd = new DagMetTijd();
    zat.dag = this.zaterdag;

    let zaterdagOchtend = new Tijd();
    zaterdagOchtend.dag = this.zaterdag;
    zaterdagOchtend.start = this.negenUur;
    zaterdagOchtend.eind = this.dertienUur;
    zaterdagOchtend.medewerkers = this.copyMedewerkers();
    zat.tijden.push(zaterdagOchtend);
    this.aanmeldenService.aanmelden.dagenMetTijden.push(zat);

    let zon: DagMetTijd = new DagMetTijd();
    zon.dag = this.zondag;
    this.aanmeldenService.aanmelden.dagenMetTijden.push(zon);
  }

  ngOnInit(): void {
  }

  onClickTijdVerwijderen(dag: DagMetTijd, index: number): void {
    let tijd: Tijd = dag.tijden[index];
    this.confirmationService.confirm({
      message: 'Weet je zeker dat je het tijdblok op ' + dag.dag.name + ', met begin ' + tijd.start + ' en eind ' + tijd.eind + ' wilt verwijderen?',
      accept: () => {
        dag.tijden.splice(index, 1);
      }
    });
  }

  onClickTijdToevoegen(dag: DagMetTijd): void {
    let tijd: Tijd = new Tijd();
    tijd.medewerkers = this.copyMedewerkers();
    dag.tijden.push(tijd);
  }

  copyMedewerkers(): Medewerker[] {
    return this.medewerkers.map(m => {
      let mw: Medewerker = new Medewerker();
      mw.aanwezig = m.aanwezig;
      mw.voornaam = m.voornaam;
      mw.id = m.id;
      return mw;
    });
  }

  voornaam(naam: string): string {
    return naam.split(' ')[0];
  }

  onChangeValidateTijd(dag: DagMetTijd, i: number, start: boolean) {
    dag.tijden[i].startVoorEind = true;
    dag.tijden[i].startTijdGeldig = true;
    dag.tijden[i].eindTijdGeldig = true;
    let tijd: LocalTime;
    if (start) {
      tijd = dag.tijden[i].start;
    } else {
      tijd = dag.tijden[i].eind;
    }
    try {
      LocalTime.parse('' + tijd);
    } catch (e) {
      if (start) {
        dag.tijden[i].start = null;
        dag.tijden[i].startTijdGeldig = false;
      } else {
        dag.tijden[i].eind = null;
        dag.tijden[i].eindTijdGeldig = false;
      }
    }
    if (!start && dag.tijden[i].startTijdGeldig && dag.tijden[i].eindTijdGeldig) {
      try {
        let startTijd: LocalTime = LocalTime.parse('' + dag.tijden[i].start);
        let eindTijd: LocalTime = LocalTime.parse('' + dag.tijden[i].eind);
        if (!eindTijd.isAfter(startTijd)) {
          dag.tijden[i].startVoorEind = false;
        }
      } catch (e) {
      }
    }
  }

  get aanmeldenService(): AanmeldenService {
    return this._aanmeldenService;
  }
}
