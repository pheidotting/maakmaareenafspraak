import {TestBed} from '@angular/core/testing';

import {AanmeldenService} from './aanmelden.service';

describe('AanmeldenServiceService', () => {
  let service: AanmeldenService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(AanmeldenService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
