import {Injectable} from '@angular/core';
import {Aanmelden} from "./aanmelden/model/aanmelden/aanmelden";
import {LocalTime} from "@js-joda/core";
import {HttpClient} from "@angular/common/http";
import {Guid} from "guid-typescript";

export class Dag {
  code: string;
  name: string;
  id: number;
}

export class Medewerker {
  id: string = Guid.create().toString();
  naam: string;
  voornaam: string;
  aanwezig: boolean = true;
  beheerder: boolean = false;
}

export class Tijd {
  dag: Dag;
  start: LocalTime;
  eind: LocalTime;
  startVoorEind: boolean = true;
  startTijdGeldig: boolean = true;
  eindTijdGeldig: boolean = true;
  medewerkers: Medewerker[] = [];
}

export class DagMetTijd {
  dag: Dag;
  tijden: Tijd[] = [];
}

// export class BehandelingHolder {
//   behandeling:Behandeling;
// }
// export class Behandeling {
//   id: number;
//   id: string;
//   naam: string;
//   omschrijving: string;
//   tijdsduur: number;
//   prijs: number;
//   avatarAanwezig: boolean;
// medewerkers: Medewerker[];
// editMode:boolean=false;
// }

// export interface TreeNode {
//   data?: any;
//   children?: TreeNode[];
//   leaf?: boolean;
//   expanded?: boolean;
// }
// export class Categorie{
//   id:  string;
//   naam:string;
//   omschrijving:string;
//   editMode:boolean=false;
// }
// export class CategorieHolder{
//   id: string;
//   categorie:Categorie;
//   children: Behandeling[]=[];
//   expanded: boolean;
// }

@Injectable({
  providedIn: 'root'
})
export class AanmeldenService {
  private aanmeldUrl: string = 'http://localhost:8080/aanmelden';

  huidigeStap: number = 1;

  aanmelden: Aanmelden;

  constructor(private httpClient: HttpClient) {
    this.aanmelden = new Aanmelden();
  }

  onClickNaarVolgendeStap(): void {
    this.huidigeStap = this.huidigeStap + 1;
  }

  onClickNaarVorigeStap(): void {
    this.huidigeStap = this.huidigeStap - 1;
    if (this.huidigeStap == 2 && this.aanmelden.medewerkers.length == 1) {
      this.huidigeStap = this.huidigeStap - 1;
    }
  }

  aanmeldenSalon(): void {
    this.aanmelden.behandelingen.forEach(behandeling => {
      behandeling.children.forEach(c => {
        c.parent = null;
      });
    });
    console.log(JSON.stringify(this.aanmelden));
    this.httpClient.post(this.aanmeldUrl, this.aanmelden).subscribe(observer => {
      console.log(observer);
    });
  }
}
